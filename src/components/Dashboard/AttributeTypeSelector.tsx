import * as React from 'react';

import {
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Dropdown
} from 'reactstrap';
import { FundAttributes } from 'store/Fund/types';
import { FUND_ATTRIBUTES_TITLES } from 'store/Fund/constants';
import * as ReactDOM from 'react-dom';

const selectorRoot = document.getElementById('attribute-type-selector-dropdown') || document.body;

const styles = require('./AttributeTypeSelector.scss');

interface Props {
    currentValue: FundAttributes;
    changeAttributeType: (attrType: string) => void;
}

interface State {
    isOpen: boolean;
}

class AttributeTypeSelector extends React.PureComponent<Props, State> {
    state = {
        isOpen: false
    };

    private el: HTMLDivElement;

    constructor(props: Props) {
        super(props);

        this.el = document.createElement('div');
    }

    componentDidMount() {
        selectorRoot.appendChild(this.el);
    }

    componentWillUnmount() {
        selectorRoot.removeChild(this.el);
    }

    render() {
        const {currentValue} = this.props;

        let dropDownMenu = (
            <div className={styles.dropdown}>
                <DropdownMenu
                    modifiers={{
                        preventOverflow: {enabled: false},
                        hide: {enabled: true}
                    }}
                >
                    <DropdownItem
                        onClick={this.handleChooseCoverage}
                        disabled={currentValue === FundAttributes.CreditOfficer}
                    >
                        Coverage
                    </DropdownItem>
                    <DropdownItem
                        onClick={this.handleChooseStrategy}
                        disabled={currentValue === FundAttributes.Strategy}
                    >
                        Strategy
                    </DropdownItem>
                    <DropdownItem
                        onClick={this.handleChooseProduct}
                        disabled={currentValue === FundAttributes.Product}
                    >
                        Product
                    </DropdownItem>
                </DropdownMenu>
            </div>
        );

        dropDownMenu = ReactDOM.createPortal(dropDownMenu, this.el);
        return (
            <Dropdown className={styles.dropdown} isOpen={this.state.isOpen} toggle={this.emptyToggle}>
                <DropdownToggle>
                    <span className={styles['attribute-title']}>
                        {FUND_ATTRIBUTES_TITLES[currentValue]}
                    </span>
                    <span className={'icon-gear'}/>
                </DropdownToggle>
                <div>
                    {dropDownMenu}
                </div>
            </Dropdown>
        );
    }

    private emptyToggle = (e?: any) => {
        if (e && !e.which && e.type === 'click') {
            this.toggle();
        }
    }

    private toggle = () => {
        this.setState((prevState: State) => ({
            isOpen: !prevState.isOpen
        }));
    }

    private handleChooseCoverage = (e: any) => {
        this.handleItemClick(e, FundAttributes.CreditOfficer);
    }

    private handleChooseStrategy = (e: any) => {
        this.handleItemClick(e, FundAttributes.Strategy);
    }

    private handleChooseProduct = (e: any) => {
        this.handleItemClick(e, FundAttributes.Product);
    }

    private handleItemClick = (e: any, type: FundAttributes) => {
        this.props.changeAttributeType(type);
    }
}

export default AttributeTypeSelector;
