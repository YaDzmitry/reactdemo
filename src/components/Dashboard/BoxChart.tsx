import * as React from 'react';
import 'anychart';

import { BoxChartItem } from 'store/Dashboard/types';
import { ANY_CHART_LICENSE_KEY } from 'store/constants';

const styles = require('./BoxChart.scss');
const MIN_WIDTH_FOR_ITEM = 65;
const MAX_ITEMS_PER_PAGE = 15;

interface Props {
    items: BoxChartItem[];
}

class BoxChart extends React.PureComponent<Props> {
    private chart: anychart.charts.Cartesian;
    private rand = Math.round(Math.random() * 1000000000);
    private renderedItems: BoxChartItem[] | null = null;
    private itemsPerPage = 15;

    private nameLines: number = 1;
    private namesPerLine: number = 10;
    private approximateLineHeightPx = 22;
    private minContainerHeightPx = 320;
    private recalcHeightForLinesCount = 3;
    private namePerLineCoeff = 1500 / 10;

    private readonly containerRef: React.RefObject<HTMLDivElement>;

    constructor(props: Props) {
        super(props);

        this.containerRef = React.createRef();
    }

    componentDidMount() {
        this.initChart();
        this.renderChart(this.props.items);
    }

    componentDidUpdate() {
        const {items} = this.props;
        if (items !== this.renderedItems) {
            this.renderChart(items);
        }
    }

    render() {
        return (
            <div ref={this.containerRef} className={styles.container} id={this.containerId} />
        );
    }

    private initChart() {
        const containerWidth = this.containerRef.current ? this.containerRef.current.clientWidth : 0;

        this.itemsPerPage = Math.floor(containerWidth / MIN_WIDTH_FOR_ITEM);
        if (this.itemsPerPage > MAX_ITEMS_PER_PAGE) {
            this.itemsPerPage = MAX_ITEMS_PER_PAGE;
        }

        this.namesPerLine = containerWidth / this.namePerLineCoeff;

        this.chart = anychart.box();
        anychart.licenseKey(ANY_CHART_LICENSE_KEY);
        this.chart.credits().enabled(false);
        this.chart
            .title(false)
            .container(this.containerId)
            .xScroller(true);

        const interactivity = this.chart.interactivity();
        interactivity.selectionMode('none');

        this.chart.xAxis().ticks().enabled(false);
        this.chart.xAxis().labels().fontColor(styles['limed-spruce-transparent']);
        this.chart.xAxis().overlapMode('allowOverlap');
        this.chart.xAxis().staggerMode(true);
        this.chart.xAxis().staggerLines(this.nameLines);
        this.chart.xZoom().setToPointsCount(this.itemsPerPage);

        this.chart.xScroller().listen('scrollerchange', (e: {startRatio: number, endRatio: number}) => {
            if (this.renderedItems) {
                this.scrollerChange(this.renderedItems.length, e.startRatio, e.endRatio);
            }
        });

        const scale = this.chart.xScale() as anychart.scales.Linear;
        const ticks = scale.ticks();
        ticks.interval(1);

        this.chart.yAxis().ticks().enabled(false);
        this.chart.yAxis().labels()
            .fontColor(styles['limed-spruce-transparent'])
            .format(function (this: {value: number}) {
                return `${this.value}%`;
            });
        this.chart.yGrid().enabled(true).stroke();

        const yAxis1 = this.chart.yAxis(1);
        yAxis1.orientation('right').ticks().enabled(false);
        yAxis1.labels().enabled(false);

        const series = this.chart.box([]);
        series.pointWidth(38);
        series.whiskerWidth('79%');

        series.normal().whiskerStroke(styles['havelock-blue'], 3);
        series.normal().fill(styles['hawkes-blue'], 1);
        series.normal().stroke(styles['havelock-blue'], 1);
        series.hovered().stroke(styles['havelock-blue'], 1);
        series.normal().medianStroke(styles['havelock-blue'], 1);
        series.hovered().medianStroke(styles['havelock-blue'], 1);

        series.tooltip().format(function (this: {
            lowest: number, q1: number, median: number, q3: number, highest: number
        }) {
            return `Highest: ${this.highest}%
                Q3: ${this.q3}%
                Median: ${this.median}%
                Q1: ${this.q1}%
                Lowest: ${this.lowest}%`;
        });
    }

    private renderChart(items: BoxChartItem[]) {
        let series = this.chart.getSeries(0) as anychart.core.cartesian.series.Column;

        if (items.length === 0) {
            series.data([]);
            return;
        }

        // create area series with passed data
        const columns = this.getChartItems(items);
        series.data(columns);

        const tooltip = series.tooltip();
        tooltip.titleFormat(function (this: {x: string, index: number}) {
            const fund_count = items[this.index].fund_count;
            return `${this.x} - ${fund_count} ${fund_count === 1 ? 'fund' : 'funds'}`;
        });

        this.chart.xZoom().setToPointsCount(this.itemsPerPage);

        this.scrollerChange(
            items.length,
            this.chart.xZoom().getStartRatio(),
            this.chart.xZoom().getEndRatio()
        );

        this.chart.draw();

        this.renderedItems = items;
    }

    private get containerId() {
        return `anychart-container-box-chart-${this.rand}`;
    }

    private getChartItems(items: BoxChartItem[]) {
        return items.map(item => ({
            x: item.value,
            low: item.low,
            q1: item.q1,
            median: item.median,
            q3: item.q3,
            high: item.high
        }));
    }

    private scrollerChange = (itemsCount: number, startRatio: number, endRatio: number) => {
        if (this.renderedItems) {
            const nameLines = this.getCountOfNameLines(itemsCount, startRatio, endRatio);
            if (this.nameLines !== nameLines) {
                this.nameLines = nameLines;

                if (this.containerRef.current) {
                    const newHeight = this.minContainerHeightPx +
                        Math.max(0, nameLines - this.recalcHeightForLinesCount + 1) * this.approximateLineHeightPx;
                    this.containerRef.current.style.height = `${newHeight}px`;
                }

                this.chart.xAxis().staggerLines(nameLines);
            }
        }
    }

    private getCountOfNameLines(itemsCount: number, startRatio: number, endRatio: number) {
        const ratio = endRatio - startRatio;
        return Math.ceil(itemsCount * ratio / this.namesPerLine);
    }
}

export default BoxChart;
