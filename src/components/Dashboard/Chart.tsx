import * as React from 'react';
import 'anychart';

import { AumChartItem,  DashboardChartType, PerformanceChartItem } from 'store/Dashboard/types';
import { defaultColor } from 'helpers/html';
import { ANY_CHART_LICENSE_KEY, DEFAULT_CURRENCY_VALUE, FormatTypeChoices } from 'store/constants';
import NumericHelper from 'helpers/NumericInputHelper';

const styles = require('./Chart.scss');

type ChartItem = AumChartItem | PerformanceChartItem;

interface Props {
    items: ChartItem[];
    currencyName: string;
    chartType: DashboardChartType;
}

class Chart extends React.PureComponent<Props> {
    chart: anychart.charts.Cartesian;
    rand = Math.round(Math.random() * 1000000000);

    private renderedItems: ChartItem[] | null = null;
    private renderedType: DashboardChartType | null = null;

    componentDidMount() {
        this.initChart();
        this.renderChart(this.props.chartType, this.props.items);
    }

    componentDidUpdate(prevProps: Props) {
        const {items, chartType} = this.props;
        if (items !== this.renderedItems || chartType !== this.renderedType) {
            this.renderChart(chartType, items);
        }
    }

    render() {
        return (
            <div className={styles.container} id={this.containerId} />
        );
    }

    private initChart() {
        const that = this;

        this.chart = anychart.column();
        anychart.licenseKey(ANY_CHART_LICENSE_KEY);
        this.chart.credits().enabled(false);
        this.chart
            .title(false)
            .container(this.containerId);

        this.chart.xAxis().ticks().enabled(false);
        this.chart.xAxis().labels().fontColor(styles['limed-spruce-transparent']);

        this.chart.xAxis().labels()
            .format(function (this: {value: number}) {
                return that.formatChartValue(that.props.chartType, this.value);
            });

        this.chart.yAxis().ticks().enabled(false);
        this.chart.yAxis().labels().fontColor(styles['limed-spruce-transparent']);
        this.chart.yGrid().enabled(true).stroke();

        const yAxis1 = this.chart.yAxis(1);
        yAxis1.orientation('right').ticks().enabled(false);
        yAxis1.labels().enabled(false);

        // set scale minimum
        this.chart.yScale().minimum(0);

        const series = this.chart.column([]);

        series.tooltip().format(function (this: {value: number}) {
            return `Number of funds: ${this.value}`;
        });
    }

    private renderChart(chartType: DashboardChartType, items: ChartItem[]) {
        const that = this;
        let series = this.chart.getSeries(0) as anychart.core.cartesian.series.Column;

        if (items.length === 0) {
            series.data([]);
            return;
        }

        const max = items.reduce(
            (accum, current) => (current.count > accum) ? current.count : accum,
            0
        );

        const yScale = this.chart.yScale() as anychart.scales.Linear;
        const interval = max > 0 ?  Math.ceil(max / 4) : 1;
        yScale.ticks().interval(interval);

        // create area series with passed data
        const columns = this.getChartItems(items);

        series.data(columns);
        series.fill(defaultColor);

        const tooltip = series.tooltip();
        tooltip.titleFormat(function (this: {x: number, index: number}) {
            return that.formatColumnTooltip(chartType, columns[this.index][2], columns[this.index][3]);
        });

        // initiate chart drawing
        this.chart.draw();

        this.renderedItems = items;
        this.renderedType = chartType;
    }

    private get containerId() {
        return `anychart-container-${this.props.chartType}-${this.rand}`;
    }

    private getChartItems(items: ChartItem[]) {
        return items.map(item => [item.value, item.count, item.extreme_items.from, item.extreme_items.to]);
    }

    private formatChartValue(chartType: DashboardChartType, value: number) {
        if (chartType !== DashboardChartType.AumDistribution) {
            return value + '%';
        } else {
            return this.formatMoneyValue(value);
        }
    }

    private formatColumnTooltip(chartType: DashboardChartType, from: number, to: number) {
        const isFromEqualTo = (Math.round(from * 1000) === Math.round(to * 1000));

        if (chartType !== DashboardChartType.AumDistribution) {
            if (isFromEqualTo) {
                return `${from}%`;
            }
            return `${from}% to ${to}%`;
        }

        if (isFromEqualTo) {
            return this.formatMoneyValue(from);

        }
        return `${this.formatMoneyValue(from)} to ${this.formatMoneyValue(to)}`;
    }

    private formatMoneyValue(value: number) {
        const formatType = +value > 1000000000
            ? FormatTypeChoices.NumeralThreeDecimalLower
            : FormatTypeChoices.NumeralOneDecimalLower;
        return NumericHelper.getFormattedNumeral(
            '' + value,
            DEFAULT_CURRENCY_VALUE,
            true,
            formatType,
            '0'
        );
    }
}

export default Chart;
