import * as React from 'react';

import { FUND_ATTRIBUTES_TITLES } from 'store/Fund/constants';
import NumericHelper from 'helpers/NumericInputHelper';
import { FormatTypeChoices } from 'store/constants';
import { FiltersState } from 'store/Filters/types';
import { BoxChartPeriodType } from 'store/Dashboard/types';
import { DispersionChartTypeTitles } from 'store/Dashboard/constants';

const styles = require('./BoxChartBlock.scss');

interface Props {
    periodType: BoxChartPeriodType;
    fundsCount: number;
    boxChartCount: number;
    BoxChart: React.ReactNode;
    currentFilter: FiltersState;
}

const BoxChartBlock: React.FC<Props> = ({currentFilter, fundsCount, boxChartCount, periodType, BoxChart}) => (
    <div className={`${styles.wrapper} ${styles['full-width']}`}>
        <div className={styles.header}>
            <div className={styles.title}>
                Performance Dispersion by
                &nbsp;
                {FUND_ATTRIBUTES_TITLES[currentFilter.attribute_type]}
                &nbsp;
                ({DispersionChartTypeTitles[periodType]})
            </div>
            {fundsCount !== boxChartCount &&
            <div className={styles['fund-count-info']}>
                {NumericHelper.getFormattedNumeral(
                    `${boxChartCount}`,
                    '',
                    false,
                    FormatTypeChoices.NumeralInteger
                )} Funds
            </div>}
        </div>
        <div className={styles.chart}>
            {BoxChart}
        </div>
    </div>
);

export default BoxChartBlock;
