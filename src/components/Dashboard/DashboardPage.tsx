import * as React from 'react';

import { FilterDecoratorChange } from 'decorators/FilterDecorator';

import MainLayout from '../Shared/Layouts/MainLayout';
import { NavigationButton } from 'store/types';
import { FiltersState } from 'store/Filters/types';

const styles = require('./DashboardPage.scss');

interface Props {
    menuItems: NavigationButton[];
    menuSubItems?: React.ReactNode[];
    applyFilter?: FilterDecoratorChange;
    currentFilter?: FiltersState;
    title?: string;
}

const DashboardPage: React.FC<Props> = ({
    menuItems, menuSubItems, children, applyFilter, currentFilter, title = 'Dashboard'
}) => (
    <div className="gray-wrapper">
        <MainLayout
            title={title}
            menuItems={menuItems}
            menuSubItems={menuSubItems}
            handleChange={applyFilter}
            currentFilter={currentFilter}
            contentClassName={styles['layout-content']}
        >
            <div className={styles.content}>
                {children}
            </div>
        </MainLayout>
    </div>
);

export default DashboardPage;
