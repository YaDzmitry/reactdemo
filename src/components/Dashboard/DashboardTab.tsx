import * as React from 'react';
import { PureComponent } from 'react';
import { Link } from 'react-router-dom';

import { FilterDecoratorChange } from 'decorators/FilterDecorator';
import {
    AdvancedFilterDecoratorSearch,
    AdvancedFilterDecoratorToggle,
    AdvancedFilterDecoratorUpdate
} from 'decorators/AdvancedFilterDecorator';

import UiDataTable from 'components/Shared/Ui/Tables/UiDataTable';
import DataTable from 'components/Shared/DataTable/DataTable';
import FilterPanel from 'components/Shared/Filters/FilterPanel';
import DateSlider, { DateSliderStep } from 'components/Shared/Ui/DateSlider/DateSlider';
import {
    Alignment,
    CellTypeChoices,
    DEFAULT_ASYNC_FILTER,
    FilterType,
    TableRowActionIcon,
    TableRowActionType
} from 'store/constants';
import { AsyncFilterName, Chip, FilterInfo, FiltersState } from 'store/Filters/types';
import { DashboardGridItem, DatePeriod } from 'store/Dashboard/types';
import { FilterItem, TableHeader, TableRowAction } from 'store/types';
import { FundAttributes } from 'store/Fund/types';
import StickyContainer from 'components/Shared/StickyBlock/Container';
import StickyBlock from 'components/Shared/StickyBlock/Sticky';
import TableFilterPanel from 'components/Shared/Panels/TableFilterPanel';
import { FilterEntity } from 'store/Diligence/DiligenceRequest/types';

import AttributeTypeSelector from './AttributeTypeSelector';
import AumOrPerfChartBlock from './AumOrPerfChartBlock';
const styles = require('./DashboardTab.scss');

const getCustomRowStyles = () => [styles['attribute-grid']];

interface Props {
    applyFilter: (filter: FiltersState) => void;
    rows: DashboardGridItem[];
    rowsCount: number;
    fundsCount: number;
    firmsCount: number;
    attrType: FundAttributes;
    datePeriod: DatePeriod;

    firmFilter: FilterInfo;
    portfolioFilter: FilterInfo;
    creditOfficerFilter: FilterInfo;
    productFilter: FilterInfo;
    strategyFilter: FilterInfo;

    getChoices: (page: string, filterName: string, filter: FiltersState) => void;
    setAsyncFilters: (filterName: string, filter: FiltersState, pageName: string) => void;
    chips: Chip[];
    filterPageName: string;
    onEyeClick?: (attrType: string, row: DashboardGridItem) => void;
    getGridConfiguration: () => void;
    setFilters: (filterName: string, filter: FiltersState) => void;
    currentFilter: FiltersState;
    isLoading: boolean;
    handleChange: FilterDecoratorChange;

    /* ADVANCED FILTERS */
    handleToggleModal?: AdvancedFilterDecoratorToggle;
    handleUpdateModal?: AdvancedFilterDecoratorUpdate;
    handleAttributeSearch?: AdvancedFilterDecoratorSearch;
    isFilterModalOpen: boolean;
    currentModalFilter?: FiltersState;
    searchFilterEntities: FilterEntity[];

    children: {
        DashboardCharts?: {[key: string]: React.ReactNode};
        DispersionCharts?: {[key: string]: React.ReactNode}
    };
}

export class DashboardTab extends PureComponent<Props> {
    static defaultProps = {
    };
    _currentFilter: FiltersState;
    _headers: TableHeader[];

    rowActions: TableRowAction[] = [
        {
            type: TableRowActionType.Image,
            icon: TableRowActionIcon.View,
            handler: (id, row) => {
                if (this.props.onEyeClick) {
                    this.props.onEyeClick(this.attrType, row);
                }
            },
        } as TableRowAction,
    ];

    render() {

        const filters: FilterItem[] = this.getFilters();
        const {
            datePeriod, isLoading, currentFilter
        } = this.props;
        const {DashboardCharts = {}, DispersionCharts} = this.props.children;

        const dashboardChartsKeys = Object.keys(DashboardCharts || {}),
            chartsRows = Array.from(Array(this.dashboardChartsRowsCount).keys());

        return (
            <StickyContainer>
                <StickyBlock topOffset={1} disableHardwareAcceleration={true}>
                    {({style}) => (
                        <div style={{...style}} className={`${styles['sticky-block']} ${styles['filter-wrapper']}`}>
                            <TableFilterPanel possibleFixedElementClassName={''}>
                                <div className="row">
                                    <div className="col-lg-12 col-md-12">
                                        <FilterPanel
                                            chips={this.props.chips}
                                            filters={filters}
                                            currentFilter={this.props.currentFilter}
                                            handleChange={this.props.handleChange}
                                            filterPageName={this.props.filterPageName}

                                            handleToggleModal={this.props.handleToggleModal}
                                            handleUpdateModal={this.props.handleUpdateModal}
                                            handleAttributeSearch={this.props.handleAttributeSearch}
                                            isFilterModalOpen={this.props.isFilterModalOpen}
                                            advancedFilters={this.getAdvancedFilters()}
                                            searchFilterEntities={this.props.searchFilterEntities}
                                            currentModalFilter={this.props.currentModalFilter}
                                        />
                                    </div>
                                </div>

                                <div>
                                    <DateSlider
                                        defaultDate={currentFilter.period}
                                        fromDate={datePeriod.first_period}
                                        toDate={datePeriod.last_period}
                                        changeDate={this.handleDateChange}
                                        step={DateSliderStep.Month}
                                    />
                                </div>
                            </TableFilterPanel>
                        </div>
                    )}
                </StickyBlock>

                {chartsRows.map(key => (
                    <div key={key} className={styles['charts-row']}>
                        <div className={`${styles['chart-wrapper']} ${styles.left}`}>
                            {dashboardChartsKeys.hasOwnProperty(key * 2) && (
                                DashboardCharts[dashboardChartsKeys[key * 2]]
                            )}
                        </div>

                        <div className={`${styles['chart-wrapper']} ${styles.right}`}>
                            {dashboardChartsKeys.hasOwnProperty(key * 2 + 1) && (
                                DashboardCharts[dashboardChartsKeys[key * 2 + 1]]
                            )}
                        </div>
                    </div>
                ))}

                {DispersionCharts && Object.keys(DispersionCharts).map(key => (
                    <div key={key} className={styles['charts-row']}>
                        {DispersionCharts[key]}
                    </div>
                ))}

                <div className={`row ${styles['donuts-row']}`}>
                    <div className={`${styles['table-block']}`}>
                        <UiDataTable>
                            <DataTable
                                rows={this.props.rows}
                                count={this.props.rowsCount}
                                headers={this.headers}
                                hasSorting={false}
                                hasPagination={true}
                                withCheckboxes={false}
                                getCustomRowStyles={getCustomRowStyles}
                                currentFilter={this.props.currentFilter}
                                handleChange={this.props.handleChange}
                                rowActions={this.rowActions}
                                actionsTitle={' '}
                                useColoredScrollBar={true}
                                scrollClassName="dashboard"
                                isLoading={isLoading}
                                isHeaderFixed={false}
                            />
                        </UiDataTable>
                    </div>
                </div>
            </StickyContainer>
        );
    }

    private handleDateChange = (period: string) => {
        this.props.handleChange({period});
    }

    private handleAttributeTypeChange = (attrType: FundAttributes) => {
        this.props.handleChange({attribute_type: attrType});
    }

    private getAdvancedFilters() {
        const currentPageName = this.props.filterPageName;
        return [
            {
                name: 'firm_id',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Firms',
                choices: this.props.firmFilter.choices,
                choicesCount: this.props.firmFilter.count,
                visible: false,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(currentPageName, AsyncFilterName.firm, filter);
                },
                currentFilter: this.props.firmFilter.filter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.firm,
                defaultFilter: DEFAULT_ASYNC_FILTER,
                isFilterValueObject: true,
            }, {
                name: 'credit_officer',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Coverage',
                choices: this.props.creditOfficerFilter.choices,
                choicesCount: this.props.creditOfficerFilter.count,
                visible: false,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(currentPageName, AsyncFilterName.creditOfficer, filter);
                },
                currentFilter: this.props.creditOfficerFilter.filter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.creditOfficer,
                defaultFilter: DEFAULT_ASYNC_FILTER,
            }, {
                name: 'product',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Product',
                choices: this.props.productFilter.choices,
                choicesCount: this.props.productFilter.count,
                visible: false,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(currentPageName, AsyncFilterName.product, filter);
                },
                currentFilter: this.props.productFilter.filter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.product,
                defaultFilter: DEFAULT_ASYNC_FILTER,
            }, {
                name: 'strategy',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Strategy',
                choices: this.props.strategyFilter.choices,
                choicesCount: this.props.strategyFilter.count,
                visible: false,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(currentPageName, AsyncFilterName.strategy, filter);
                },
                currentFilter: this.props.strategyFilter.filter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.strategy,
                defaultFilter: DEFAULT_ASYNC_FILTER,
            }, {
                name: 'portfolio_id',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Portfolio',
                choices: this.props.portfolioFilter.choices,
                choicesCount: this.props.portfolioFilter.count,
                visible: false,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(currentPageName, AsyncFilterName.portfolio, filter);
                },
                currentFilter: this.props.portfolioFilter.filter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.portfolio,
                defaultFilter: DEFAULT_ASYNC_FILTER,
                isFilterValueObject: true,
            }
        ];
    }

    private getFilters() {
        return [
            {
                name: 'search',
                type: FilterType.DiligenceSearch,
                title: `Search`,
                className: 'col-lg-9 col-sm-8'
            },
            {
                name: 'stats',
                type: FilterType.Statistics,
                title: 'Stats', className: 'col-lg-3 col-sm-4',
                stats: [
                    {name: 'Firms', value: this.props.firmsCount || 0},
                    {name: 'Unique Funds', value: this.props.fundsCount || 0}
                ],
            }
        ];
    }

    private get headers() {
        if (this.props.currentFilter !== this._currentFilter) {
            this._currentFilter = this.props.currentFilter;
            this._headers = [
                {
                    name: 'value',
                    title: (
                        <AttributeTypeSelector
                            currentValue={this.attrType}
                            changeAttributeType={this.handleAttributeTypeChange}
                        />
                    ),
                    'class': styles['attribute-name'],
                    headerAlignment: Alignment.Left,
                    active: true,
                    cellType: CellTypeChoices.Custom,
                    transformer: row => (
                        <Link
                            to="#"
                            onClick={() => this.setAttributeType(this.attrType, row)}
                        >
                            {row.value}
                        </Link>
                    )
                }, {
                    name: 'firms_monitor',
                    headerAlignment: Alignment.Center,
                    valueAlignment: Alignment.Right,
                    title: 'Unique Firms',
                    active: true,
                },  {
                    name: 'funds_monitor',
                    headerAlignment: Alignment.Center,
                    valueAlignment: Alignment.Right,
                    title: 'Unique Funds',
                    active: true,
                }, {
                    name: 'attr_fund_data.alert',
                    headerAlignment: Alignment.Center,
                    valueAlignment: Alignment.Right,
                    title: 'Alerts',
                    active: true,
                }, {
                    name: 'attr_fund_data.partial_data_alert',
                    headerAlignment: Alignment.Center,
                    valueAlignment: Alignment.Right,
                    title: 'Partial Data Alerts',
                    active: true,
                }, {
                    name: 'attr_fund_data.partial_data',
                    headerAlignment: Alignment.Center,
                    valueAlignment: Alignment.Right,
                    title: 'Partial Data',
                    active: true,
                }, {
                    name: 'attr_fund_data.overdue',
                    headerAlignment: Alignment.Center,
                    valueAlignment: Alignment.Right,
                    title: 'Overdue',
                    active: true,
                }, {
                    name: 'attr_fund_data.partial',
                    headerAlignment: Alignment.Center,
                    valueAlignment: Alignment.Right,
                    title: 'Partial Data Rec\'d',
                    active: true,
                }, {
                    name: 'attr_fund_data.late',
                    headerAlignment: Alignment.Center,
                    valueAlignment: Alignment.Right,
                    title: 'Received Late',
                    active: true,
                }
            ];
        }

        return this._headers;
    }

    private setAttributeType(attrType: string, row: DashboardGridItem) {
        this.props.handleChange({[attrType]: [{value: row.value, label: row.value}]});
    }

    private get attrType() {
        return this.props.currentFilter.attribute_type;
    }

    private get dashboardChartsRowsCount() {
        const {DashboardCharts} = this.props.children;

        if (DashboardCharts) {
            return Math.ceil( Object.keys(DashboardCharts).length / 2);
        }

        return 0;
    }
}

export default DashboardTab;
