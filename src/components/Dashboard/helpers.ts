const styles = require('../Shared/Charts/Donut.scss');

export function getColorByName(pieceName: string, availavleKeys: string[]) {
    let color;
    switch (pieceName) {
        case 'no_alerts':
        case 'no_alert':
        case 'on_time':
            color = styles.green;
            break;
        case 'late':
            color = styles.yellow;
            break;
        case 'partial':
        case 'partial_data':
            if (availavleKeys && availavleKeys.indexOf('late') >= 0) {
                color = styles.redtransparent;
            } else {
                color = styles.yellow;
            }
            break;
        case 'partial_data_alerts':
        case 'partial_data_alert':
            color =  styles.redtransparent;
            break;
        case 'overdue':
        case 'alerts':
        case 'alert':
            color = styles.red;
            break;
        case 'awaiting_data':
            color = styles.lightgray;
            break;
        default:
            color = styles.darkgray;
            break;
    }

    return color.replace(/"/g, '');
}
