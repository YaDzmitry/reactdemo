import * as React from 'react';

import MainLayout from '../Shared/Layouts/MainLayout';

interface Props {}

const HomePage: React.FC<Props> = (props) => (
    <MainLayout title="Home" menuItems={[]}>
        {props.children}
    </MainLayout>
);

export default HomePage;
