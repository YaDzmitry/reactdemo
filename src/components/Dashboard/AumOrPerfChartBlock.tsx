import * as React from 'react';

import NumericHelper from 'helpers/NumericInputHelper';
import { FormatTypeChoices } from 'store/constants';

const styles = require('./AumOrPerfChartBlock.scss');

interface Props {
    title: string;
    fundsCount: number;
    chartFundsCount: number;
    Chart?: React.ReactNode;
}

const AumOrPerfChartBlock: React.FC<Props> = ({title, fundsCount, chartFundsCount, Chart}) => (
    <div className={`${styles.wrapper}`}>
        <div className={styles.header}>
            <div className={styles.title}>
                {title}
            </div>
            {fundsCount !== chartFundsCount &&
            <div className={styles['fund-count-info']}>
                {NumericHelper.getFormattedNumeral(
                    `${chartFundsCount}`,
                    '',
                    false,
                    FormatTypeChoices.NumeralInteger
                )} Funds
            </div>
            }
        </div>
        <div className={styles.chart}>
            {Chart}
        </div>
    </div>
);

export default AumOrPerfChartBlock;
