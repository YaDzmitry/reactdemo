import * as React from 'react';

import { DonutPiece } from 'store/Shared/types';
import { StatisticDonutDataType } from 'store/Dashboard/types';

import Donut from 'components/Shared/Charts/Donut';
import { getColorByName } from './helpers';

const styles = require('./StatisticDonut.scss');

export interface Props {
    title: string;
    total: number;
    pieces: DonutPiece[];
    donutType: StatisticDonutDataType;
}

interface InternalProps {
    onClick?: (piece: DonutPiece) => void;
}

class StatisticDonut extends React.PureComponent<Props & InternalProps> {
    private memoizedPieces: DonutPiece[];
    private _preparedPieces: DonutPiece[];

    private _total: number;

    render() {
        const {title, donutType, total: centerTotal} = this.props;

        return (
            <div className={`${styles.wrapper}`}>
                <div className={styles.header}>
                    <div className={styles.title}>
                        {title}
                    </div>
                </div>
                <div className={styles.chart}>
                    <Donut
                        className={styles.container}
                        centerTotal={centerTotal}
                        total={this.total}
                        totalTitle={this.title}
                        totalSubtitle={this.subTitle}
                        pieces={this.preparedPieces}
                        legendConfig={{show: true}}
                        donutType={donutType}
                        onClick={this.props.onClick}
                    />
                </div>
            </div>
        );
    }

    private get title() {
        const {donutType} = this.props;

        switch (donutType) {
            case StatisticDonutDataType.PerformanceReport:
                return 'Overdue';
            case StatisticDonutDataType.Alerts:
                return 'Alerts';
            default:
                return '';
        }
    }

    private get subTitle() {
        const {donutType} = this.props;

        switch (donutType) {
            case StatisticDonutDataType.Alerts:
                return 'Includes Partial Data';
            case StatisticDonutDataType.PerformanceReport:
                return 'Includes Partial data';
            default:
                return '';
        }
    }

    private get preparedPieces() {
        if (this.props.pieces !== this.memoizedPieces) {
            this.recalculateInternalValues();
        }

        return this._preparedPieces;
    }

    private get total(): number {
        if (this.props.pieces !== this.memoizedPieces) {
            this.recalculateInternalValues();
        }

        return this._total || 0;
    }

    private recalculateInternalValues() {
        const {donutType, pieces} = this.props;

        this.memoizedPieces = pieces;

        if (pieces.length === 0) {
            this._preparedPieces = [];
        } else {
            const namesOrder = (donutType === StatisticDonutDataType.Alerts)
                ? ['no_alert', 'alert', 'partial_data_alert', 'partial_data', 'awaiting_data', 'no_ruleset']
                : ['on_time', 'overdue', 'partial', 'late', 'awaiting_data', 'no_request'];

            const mapPieces = {};
            pieces.forEach(piece => mapPieces[piece.name] = {
                ...piece,
                color: getColorByName(piece.name, namesOrder)
            });

            this._preparedPieces = namesOrder.map(name => mapPieces[name]);
        }

        this._total = pieces.reduce((accumulator, piece: DonutPiece) => accumulator + piece.value, 0);
    }
}

export default StatisticDonut;
