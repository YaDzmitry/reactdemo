import * as React from 'react';

import HeaderHelper from 'helpers/headerHelper';
import GlobalHelper from 'helpers/globalHelper';

import { TableHeader } from 'store/types';
import { FundModel } from 'store/Fund/models/fund';
import { SetDatasetDetailTemporaryHeadersAction } from 'store/DataManager/actions';
import { headerNames } from 'store/VisibleHeaders/types';
import { DEFAULT_VISIBLE_HEADERS } from 'store/VisibleHeaders/constants';

import Checkbox from 'components/Shared/Ui/Checkbox';
import ButtonDefault from 'components/Shared/Ui/Buttons/ButtonDefault';
import ButtonPrimary from 'components/Shared/Ui/Buttons/ButtonPrimary';
import Modal from 'components/Shared/Modals/Modal';

interface ColumnType {
    active: boolean;
    name: string;
}

interface Props {
    temporaryHeaders: TableHeader[];
    setDatasetDetailTemporaryHeaders: (temporaryHeaders: TableHeader[]) => SetDatasetDetailTemporaryHeadersAction;
    isOpen: boolean;
    headers: TableHeader[];
    handleColumns: (column: any) => void;
    closeModal: () => any;
    modalParams: any;
    fund: FundModel;
    saveSettingsDatasetFund: (id: string, payload: any, requestor: boolean) => any;
}

export default class DatasetsColumnsModal extends React.PureComponent<Props> {
    navHeaders = [
        'beginning_balance',
        'ending_balance',
        'subscriptions',
        'redemptions',
        'income',
        'nav_per_share',
        'known_future_redemptions',
    ];
    performanceHeaders = [
        'perf_daily',
        'perf_mtd',
        'perf_weekly',
        'perf_qtd',
        'perf_month',
        'perf_ytd',
    ];

    handleUpdate = () => {
        const visibleFields: string[] = [];
        this.props.temporaryHeaders.forEach((item: TableHeader) => {
            if (item.active && item.columnName
                && DEFAULT_VISIBLE_HEADERS[headerNames.fundDatasets].indexOf(item.columnName) === -1
            ) {
                visibleFields.push(item.columnName);
            }
        });
        const payload = {
            visible_fields: visibleFields,
        };
        this.props.saveSettingsDatasetFund(this.props.fund.id, payload, true);

        this.props.handleColumns(this.props.temporaryHeaders);

        this.props.closeModal();
    }

    handleCloseModal = () => {
        this.props.handleColumns(this.props.headers);
        this.props.setDatasetDetailTemporaryHeaders(this.props.headers);
        this.props.closeModal();
    }

    handleColumns = (column: ColumnType) => {
        const newColumns = this.props.temporaryHeaders.map((item: any) => {
            let answer = item;
            if (item.name === column.name) {
                answer = {
                    ...item,
                    ...column
                };
            }

            return answer;
        });
        this.props.setDatasetDetailTemporaryHeaders(newColumns);
    }

    render () {
        const {isOpen} = this.props;

        const buttons = [
            (<ButtonPrimary buttonLarge={true} handleClick={this.handleUpdate}>Update</ButtonPrimary>),
            (
                <ButtonDefault
                    buttonLarge={true}
                    inverse={true}
                    handleClick={this.handleCloseModal}
                >
                    Cancel
                </ButtonDefault>
            )
        ];
        const headers = HeaderHelper.clearColumn(this.props.temporaryHeaders, this.navHeaders);
        const navHeaders = headers.map((item: any, idx: number) => {
            return (
                <div key={idx} className="col-md-6">
                    <Checkbox
                        idx={`nav-${idx}`}
                        className="aligned-checkboxes"
                        checked={item.active}
                        handleChange={(event) => this.handleColumns({ active: !item.active, name: item.name })}
                    >
                        {GlobalHelper.replaceElemInString(item.extraTitle)}
                    </Checkbox>
                </div>
            );
        });
        const perfHeaders = HeaderHelper.clearColumn(this.props.temporaryHeaders, this.performanceHeaders);
        const performanceHeaders = perfHeaders.map((item: any, idx: number) => {
            return (
                <div key={idx} className="col-md-6">
                    <Checkbox
                        idx={`perf-${idx}`}
                        className="aligned-checkboxes"
                        checked={item.active}
                        handleChange={(event) => this.handleColumns({ active: !item.active, name: item.name })}
                    >
                        {GlobalHelper.replaceElemInString(item.extraTitle)}
                    </Checkbox>
                </div>
            );
        });

        const body = (
            <div className="publ-datasets-show-hide-modal-wrapper">
                <div className="row">
                    <div className="col-md-12">
                        <div className="block-title">NAV</div>
                    </div>
                    {navHeaders}
                    <div className="col-md-12">
                        <div className="block-title">Performance</div>
                    </div>
                    {performanceHeaders}
                </div>
            </div>
        );

        return (
            <Modal
                isOpen={isOpen}
                toggle={this.handleCloseModal}
                title="Show/hide Columns"
                body={body}
                buttons={buttons}
            />
        );
    }
}