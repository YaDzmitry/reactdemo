import * as React from 'react';
import { Form, FormGroup, Col } from 'reactstrap';
import { isEqual } from 'lodash';

import FundHelper from 'helpers/fundHelper';

import { NewFundModel } from 'store/Fund/models/fund';
import { DEFAULT_ITEMS_PER_PAGE } from 'store/constants';
import Input from '../Shared/Ui/Input';
import Loader from '../Shared/Ui/Loader';
import NotDataFound from '../Shared/NotDataFound/NotDataFound';
import ColoredScrollbar from '../Shared/Scroll/ColoredScrollbar';

interface Props {
    newFund: NewFundModel;
    isGridLoading: boolean;
    clearFirmFromErm: () => void;
    getFirmFromErm: (data: any) => void;
    changeNewFund: (field: string, value: string) => void;
    funds: any[];
}

const styles = require('./AddNewFundForm.scss');

class AddNewFundForm extends React.PureComponent<Props> {

    state = {
        inputValue: '',
        isTrying: true,
        active: false,
    };

    componentWillReceiveProps(nextProps: Props) {
        if (nextProps.funds !== this.props.funds) {
           this.setState({
               isTrying: !this.state.isTrying
           });
        }
    }

    componentWillUnmount() {
        this.props.changeNewFund('name', '');
        this.props.clearFirmFromErm();
    }

    handleFundChange = (value: any) => {
        this.setState({active: value});
        this.props.changeNewFund('name', value);
    }

    handleInputChange = (value: any) => {
        this.setState({active: false, inputValue: value, isTrying: true}, () => {
            this.props.getFirmFromErm({search: value, limit: DEFAULT_ITEMS_PER_PAGE});
            this.props.changeNewFund('name', '');
        });
    }

    handleSelectFirm = (item: any) => {
        this.props.changeNewFund('name', item);
        this.setState({active: item, inputValue: item.legal_name});
    }

    render() {
        let otherInfo: any = '';
        let suggestions: any = '';

        if (this.props.funds.length > 0) {
            suggestions = `${this.props.funds.length} Suggestions: `; // </div>;
            otherInfo = this.props.funds.map((item: any, idx: number) => {
                return (
                <li
                    className={isEqual(this.state.active, item) ? 'active-item item--entity' : 'item--entity'}
                    key={idx}
                    onClick={() => this.handleSelectFirm(item)}
                >
                    {FundHelper.getEntitiesContentForNewFund(item)}
                </li>
                );
            });
        }

        let searching: JSX.Element | null  = null;

        if (this.props.isGridLoading) {
            searching = (<NotDataFound text={`searching...`} data={1}/>);
        }

        let dataNotFound: JSX.Element | null  = null;

        if (!this.state.isTrying && !this.props.isGridLoading) {
            dataNotFound =  <NotDataFound text={`No funds found`} data={this.props.funds}/>;
        }

        return (
            <Form className={styles.add_new_fund}>
                <FormGroup row={true}>
                    <Col sm={12}>
                        <Input
                            value={this.state.inputValue}
                            material={true}
                            placeholder="Starting typing for suggestions"
                            handleChange={(value: any) => this.handleInputChange(value)}
                            focused={true}
                        />
                    </Col>
                </FormGroup>
                {
                    <div className="funds-wrapper">
                        <div className="suggestions">{suggestions}</div>
                        <ColoredScrollbar className={`add-new-fund-scroll`} alwaysShowTracks={true}>
                            {!this.props.isGridLoading && <ul className="items">{otherInfo}</ul>}
                        </ColoredScrollbar>
                    </div>

                }
                {searching}
                {dataNotFound}
                <Loader className={styles.scroll} isLoading={this.props.isGridLoading}/>
            </Form>
        );
    }
}

export default AddNewFundForm;
