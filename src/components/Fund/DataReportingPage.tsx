import * as React from 'react';

import { FundModel } from '../../store/Fund/models/fund';
import { Id, NavigationButton } from '../../store/types';
import { Request } from '../../store/Requests/models/request';

import MainLayout from '../Shared/Layouts/MainLayout';
import ButtonPrimary from '../Shared/Ui/Buttons/ButtonPrimary';
import ButtonDanger from '../Shared/Ui/Buttons/ButtonDanger';
import SettingsHeader from './SettingsHeader';
import ReportingHistory from './ReportingHistory';
import { FundReportingHistory } from '../../store/Fund/types';
import { NavLink } from 'react-router-dom';
import ConfirmSendModal from '../../containers/Requests/ConfirmSendModal';

interface Props {
    menuItems: NavigationButton[];
    fund: FundModel;
    history: FundReportingHistory[];
    editedRequest: Request | null;
    saveEditedRequest: () => any;
    isEditMode: boolean;
    toggleMode: () => void;
    sendRequest: (fundId: Id) => any;
    openConfirmSendModal: () => void;
    fundIsLoading: boolean;
    children: {
        RequestEditor: JSX.Element
    };
}

const styles = require('./DataReportingPage.scss');

class DataReportingPage extends React.PureComponent<Props> {
    handleSave = () => {
        this.props.saveEditedRequest();
    }

    render() {
        const { menuItems, fund, isEditMode, toggleMode, fundIsLoading, openConfirmSendModal } = this.props;

        const RequestEditor = this.props.children.RequestEditor;
        const breadCrumbs = (
            <div className="ap-breadcrumbs">
                <NavLink to="/firms">Firms</NavLink>
                <span className="breadcrumb-slash">/</span>
                <NavLink to={`/firms/${this.props.fund.firm.id}`}>{this.props.fund.firm.name}</NavLink>
                <span className="breadcrumb-slash">/</span>
                <NavLink to="/funds">Funds</NavLink>
                <span className="breadcrumb-slash">/</span>
                <span>{this.props.fund.name}</span>
            </div>
        );
        return (
            <MainLayout
                title={fund.name}
                menuItems={menuItems}
                breadCrumbs={breadCrumbs}
            >
                <div className={`${styles.content}`}>
                    {/*<div className={styles.report}>*/}
                        {/*<div className="dataReporting__header">*/}
                            {/*<span className="dateReporting__header_title">*/}
                                {/*Reporting History*/}
                            {/*</span>*/}
                        {/*</div>*/}

                        {/*<ReportingHistory />*/}
                    {/*</div>*/}

                    <div className={styles.settings}>
                        <SettingsHeader
                            request={this.props.editedRequest}
                            isEditMode={isEditMode}
                            toggleMode={toggleMode}
                            openConfirmSendModal={openConfirmSendModal}
                        />

                        {!fundIsLoading && RequestEditor}

                        {!fundIsLoading && isEditMode &&
                        <div className={`${styles.buttonPanel} row d-flex justify-content-end`}>
                            <ButtonPrimary handleClick={this.handleSave}>
                                Save Request
                            </ButtonPrimary>

                            <ButtonDanger inverse={true} handleClick={toggleMode}>
                                Cancel
                            </ButtonDanger>
                        </div>
                        }
                        <ConfirmSendModal
                            sendRequest={this.props.sendRequest}
                        />
                    </div>
                </div>
            </MainLayout>
        );
    }
}

export default DataReportingPage;