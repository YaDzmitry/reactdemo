import * as React from 'react';
import { Link } from 'react-router-dom';

import { Request } from 'store/Requests/models/request';
import { editIcon, sendIcon } from 'store/constants';
import { RequestStatus } from 'store/Requests/types';

interface Props {
    request: Request | null;
    isEditMode: boolean;
    toggleMode: () => void;
    openConfirmSendModal: (event: any) => any;
}

const styles = require('./SettingsHeader.scss');

const SettingsHeader: React.FC<Props> = ({request, isEditMode, toggleMode, openConfirmSendModal}) => (
    <div className={`${styles.settingsHeader} dataReporting__header`}>
        <span className="dateReporting__header_title">
            Data Reporting Settings
        </span>
        {
            !isEditMode && request && request.id &&
            <span className={styles.editButton}>
                <Link to="#" onClick={toggleMode}>
                    <img
                        src={editIcon}
                        alt="pencil"
                        className={styles.tableIcon}
                    /> Edit
                </Link>
            </span>
        }
        {
            !isEditMode && request && request.id && request.status === RequestStatus.Pending &&
            <span className={styles.sendButton}>
                <Link to="#" onClick={openConfirmSendModal}>
                    <img
                        src={sendIcon}
                        alt="send"
                        className={styles.tableIcon}
                    /> Send
                </Link>
            </span>
        }
    </div>
);

export default SettingsHeader;
