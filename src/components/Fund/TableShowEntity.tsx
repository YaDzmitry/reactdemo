import * as React from 'react';
import { DEFAULT_VALUE_NONE, FormatTypeChoices } from 'store/constants';
import { formatText } from 'helpers/tableHelper';
import TableDifferentRow from 'components/Shared/Ui/Tables/TableDifferentRow';

interface Props {
    entity: any;
}

const TableShowEntity: React.FC<Props> = ({entity}) => {

    return (
        <TableDifferentRow inverse={true}>
            <h6><b>Other Info</b></h6>
            <table>
                <tbody>
                <tr>
                    <th>status</th>
                    <td>{entity.status}</td>
                </tr>
                <tr>
                    <th>updated at</th>
                    <td>
                        {formatText(entity.updated_at, FormatTypeChoices.DateFormat, DEFAULT_VALUE_NONE)}
                    </td>
                </tr>
                </tbody>
            </table>

            <h6 className="mt-3 mb-2">
                data sources
            </h6>

            <table>
                <thead>
                <tr>
                    <th>data source</th>
                    <th>valid from</th>
                    <th>valid to</th>
                </tr>
                </thead>
                <tbody>
                {
                    entity.datasources.map((datasource: any, i: number) => {
                        return (
                            <tr key={i}>
                                <td>{datasource.data_source}</td>
                                <td>
                                    {formatText(
                                        datasource.valid_from,
                                        FormatTypeChoices.DateFormat,
                                        DEFAULT_VALUE_NONE
                                    )}
                                </td>
                                <td>
                                    {formatText(datasource.valid_to, FormatTypeChoices.DateFormat, DEFAULT_VALUE_NONE)}
                                </td>
                            </tr>
                        );
                    })
                }
                </tbody>
            </table>
        </TableDifferentRow>
    );
};

export default TableShowEntity;
