import * as React from 'react';
import { RefObject } from 'react';
import { Prompt } from 'react-router';
import { cloneDeep, isEmpty } from 'lodash';
import * as moment from 'moment';
import memoize from 'memoize-one';
import { Location, History } from 'history';

import FundHelper from 'helpers/fundHelper';
import DataManagerHelper from 'helpers/dataManagerHelper';
import GlobalHelper from 'helpers/globalHelper';
import { FilterDecoratorChange } from 'decorators/FilterDecorator';

import UiDataTable from 'components/Shared/Ui/Tables/UiDataTable';
import DataTable from 'components/Shared/DataTable/DataTable';
import ButtonPrimary from 'components/Shared/Ui/Buttons/ButtonPrimary';
import UiWrapper from 'components/Shared/Ui/Wrappers/BaseWrapper';
import { headersInModal } from 'components/DataManager/DatasetDetailModals/NavConfigurationModal';
import ColumnsModal from 'containers/Fund/FundDatasetsColumns';
import BulkUploadModal from 'containers/DataManager/DatasetsBulkUploadModal';

import { CustomTableRow, Id, TableHeader, TableRowAction } from 'store/types';
import { DEFAULT_DATE_FORMAT, TableRowActionIcon, TableRowActionType } from 'store/constants';
import { FundModel } from 'store/Fund/models/fund';
import { PUBLISHED_DATASETS_COLUMNS_MODAL } from 'store/DataManager/constants';
import { FiltersState } from 'store/Filters/types';
import { Dataset, DatasetRow, DatasetWithRowType } from 'store/DataManager/models/dataset';
import { hasHistory } from 'store/DataManager/datasetHelper';
import { SetHeadersAction } from 'store/VisibleHeaders/actions';
import { headerNames } from 'store/VisibleHeaders/types';
import { fundDatasetsHeaders } from 'containers/Fund/constants';
import { DataEntry } from 'store/Fund/types';
import {
    ClearSelectedPeriodAction, SaveRequestorDatasetAction,
    GetFundDatasetHistoryAction, ClearFundDatasetHistoryAction, SetDatasetListIsUpdatedAction, GetFundAction,
    MakeDatasetTheLatestAction
} from 'store/Fund/actions';
import { DATASET_VERSION } from 'containers/DataManager/DatasetDetailModals/DatasetConfigurationModal';
import DatasetsLeavePageModal from 'containers/Fund/DatasetsLeavePageModal';
import { name as confirmModalName } from 'containers/Shared/ConfirmModal';
import { BULK_UPLOAD_MODAL } from 'store/Fund/constants';
import { UploadStatus } from 'store/BulkUpload/types';

export interface Props {
    count: number;
    headers: TableHeader[];
    results: Dataset[];
    currentFund: FundModel;
    getFundDatasetHistory: (id: Id, dataset: Dataset) => GetFundDatasetHistoryAction;
    clearFundDatasetHistory: (id: Id, period?: string) => ClearFundDatasetHistoryAction;
    exportFundDatasets: (id: Id) => void;
    openModal: (name: string, params: any) => any;
    handleColumns: (columns: any) => void;
    handleChange: FilterDecoratorChange;
    currentFilter: FiltersState;
    isGridLoading: boolean;
    selectedPeriod: string | null;
    setHeaders: (block: string, headers: TableHeader[]) => SetHeadersAction;
    saveRequestorDataset: (dataset: Dataset) => SaveRequestorDatasetAction;
    makeDatasetTheLatest: (dataset: DatasetWithRowType) => MakeDatasetTheLatestAction;
    history: History;
    location: Location;
    clearSelectedPeriod: () => ClearSelectedPeriodAction;
    uploadStatus: UploadStatus;
    applyFilter: FilterDecoratorChange;
    datasetListIsUpdated: boolean;
    setDatasetListIsUpdated: (flag: boolean) => SetDatasetListIsUpdatedAction;
    getFund: (fundId: Id, withRequest?: boolean) => GetFundAction;
}

interface EditingRows {
    [key: string]: Partial<DatasetRow>;
}

interface State {
    editingRows: EditingRows;
    headerFields: TableHeader[];
    dataResults: DatasetWithRowType[];
    confirmedNavigation: boolean;
    focusedOnSelected: boolean;
}

const styles = require('../DataManager/DataManager.scss');

export const editableHeaders: string[] = [
    'beginning_balance', 'subscriptions', 'redemptions', 'income', 'ending_balance',
    'known_future_redemptions', 'nav_per_share', 'perf_daily', 'perf_weekly',
    'perf_month', 'perf_mtd', 'perf_qtd', 'perf_ytd', 'data_version', 'currency'
];

export const readonlyHeaders: string[] = ['data_source', 'updated_at'];

const selectedPeriodClass = 'selected-row';

const getDataWithUserUpdates = (rows: DatasetWithRowType[], editingRows: EditingRows) => {
    if (rows) {
        return rows.map(row => {
            if (!!editingRows[row.period] && row.rowType === CustomTableRow.AddNewDataset) {
                delete(row.rowType);
                return {...row, ...editingRows[row.period]};
            } else if (!!editingRows[row.period] && !row.rowType) {
                return {...row, ...editingRows[row.period]};
            }
            return row;
        });
    }
    return [];
};

const getMinPeriod = (rows: Dataset[], activePeriodString: string) => {
    const periods = rows.map(row => {
        return row;
    });

    let minPeriod: string | undefined;

    minPeriod = periods
        .map(periodRow => periodRow.period)
        .reduce(
            (prev: string, current: string) => {
                if (moment(prev).diff(moment(current), 'month', true) < 0) {
                    return prev;
                }
                return current;
            },
            activePeriodString
        );

    return minPeriod;
};

const adjustEmptyRows = (
    rows: DatasetWithRowType[],
    count: number,
    handleRowClick: (period: string) => void,
    fundApId: Id,
): DatasetWithRowType[] => {
    const newRows: DatasetWithRowType[] = rows.slice();
    const activePeriodString = DataManagerHelper.getActivePeriod();

    let minPeriod: string | undefined;

    minPeriod = getMinPeriod(newRows, activePeriodString);

    const emptyRow = {
        fund_ap_id: fundApId,
        period: activePeriodString,
        rowType: CustomTableRow.AddNewDataset,
        handleClick: handleRowClick,
        internal_dataset_id: null,
    };
    // add the first row (with active period)
    const activePeriodIndex = newRows.findIndex(row => activePeriodString === row.period);
    if (count === 0 || activePeriodIndex === -1) {
        newRows.splice( 0, 0, emptyRow);
    }

    // add the last row
    const pureRowsLength = rows.filter((item: DatasetWithRowType)  => !item.rowType).length;
    if (rows && pureRowsLength === count && count !== 0) {
        const newMinDate = moment(minPeriod)
            .subtract(1, 'month')
            .endOf('month')
            .format(DEFAULT_DATE_FORMAT);
        newRows.push({
            ...emptyRow,
            period: newMinDate,
        });
    }

    // fill the missed periods with empty rows
    let prevPeriod = activePeriodString;
    let nextPeriod = activePeriodString;
    while (moment(nextPeriod).diff(moment(minPeriod), 'month', true) > 0) {
        nextPeriod = moment(prevPeriod)
            .subtract(1, 'month')
            .endOf('month')
            .format(DEFAULT_DATE_FORMAT);
        const foundLastIndex = GlobalHelper.findLastIndex(newRows, 'period', prevPeriod);
        const foundNextIndex = newRows.findIndex(newRow => nextPeriod === newRow.period);
        if (foundLastIndex !== -1 && foundNextIndex === -1) {
            newRows.splice( foundLastIndex + 1, 0, {
                ...emptyRow,
                period: nextPeriod,
            });
        }
        prevPeriod = nextPeriod;
    }

    return newRows;
};

class DatasetsPage extends React.Component<Props, State> {
    wrapperRef: RefObject<HTMLTableRowElement>;
    rowActions: TableRowAction[] = [
        {
            type: TableRowActionType.Image,
            icon: TableRowActionIcon.Discard,
            tooltip: 'Show/hide publish history',
            handler: (id: Id, row: Dataset) => this.loadDatasetHistory(id, row),
            hideClass: 'invisible-action',
            isVisible: (row: DatasetWithRowType) => hasHistory(row) && !DatasetsPage.isHistoryRow(row),
        },
        {
            type: TableRowActionType.Image,
            icon: TableRowActionIcon.Locked,
            tooltip: 'Unlock dataset - Make amendments to a locked dataset',
            handler: (id: Id, row: Dataset) => this.handleStartEditAction(row.period, row),
            isVisible: (row: DatasetWithRowType) => !this.isEditing(row) && !DatasetsPage.isHistoryRow(row)
        },
        {
            type: TableRowActionType.Image,
            icon: TableRowActionIcon.Unlocked,
            tooltip:  'Save dataset',
            handler: (id: Id, row: Dataset) => this.saveRowAction(row.period, row, id),
            isVisible: (row: DatasetWithRowType) => this.isEditing(row) && !DatasetsPage.isHistoryRow(row)
        },
        {
            type:  TableRowActionType.Image,
            icon: TableRowActionIcon.UpdateInDB,
            tooltip:  'Make latest',
            handler: (id: Id, row: Dataset) => this.makeLatest(row.period, row),
            isVisible: (row: DatasetWithRowType) => DatasetsPage.canBeSetAsLatest(row)
                && DatasetsPage.isHistoryRow(row),
        },
    ];

    getButtons = memoize((openModal, exportFundDatasets, currentFund) => [(
        <ButtonPrimary
            className="float-right ml-3"
            inverse={true}
            handleClick={() => openModal(PUBLISHED_DATASETS_COLUMNS_MODAL, {})}
        >
            Columns...
        </ButtonPrimary>
    ),
    (
        <ButtonPrimary
            className="float-right ml-3"
            inverse={true}
            handleClick={() => exportFundDatasets(currentFund.id)}
        >
            Download
        </ButtonPrimary>
    ),
    (
        <ButtonPrimary
            className="float-right ml-3"
            handleClick={() => openModal(BULK_UPLOAD_MODAL, {})}
            inverse={true}
        >
            Bulk Upload
        </ButtonPrimary>
    )]);

    state = {
        headerFields: [],
        editingRows: {},
        dataResults: [],
        confirmedNavigation: false,
        focusedOnSelected: false,
    };
    headerFields: TableHeader[] = [];

    getHeaders = memoize((isEditing, getCustomCellStyle): TableHeader[] =>
        fundDatasetsHeaders.map((header: TableHeader) => {
            let additionalParams = {};

            if (header.name === 'data_version') {
                additionalParams = {
                    params: {
                        optionsFunc: (options: any[], headerName: string, fieldPeriod: string) => {
                            if (headerName === 'data_version' && fieldPeriod) {
                                if (moment().diff(moment(fieldPeriod), 'days') < -1) {
                                    return options.filter(item => item.value !== DATASET_VERSION.final);
                                }
                            }

                            return options;
                        },
                        field: 'period',
                    },
                };
            }

            return {
                ...header,
                ...editableHeaders.indexOf(header.name) !== -1
                    ? {...additionalParams, hasEditing: isEditing, getCustomCellStyles: getCustomCellStyle}
                    : {},
                ...readonlyHeaders.indexOf(header.name) !== -1
                    ? {...additionalParams, getCustomCellStyles: getCustomCellStyle}
                    : {},
            };
        })
    );

    static canBeSetAsLatest = (row: DatasetWithRowType) => {
        return !row.is_latest;
    }

    static isHistoryRow = (row: DatasetWithRowType) => {
        return row.rowType === CustomTableRow.RequestorHistoryDataset;
    }

    constructor(props: Props) {
        super(props);
        this.wrapperRef = React.createRef();
    }

    componentDidMount() {
        this.props.setHeaders(headerNames.fundDatasets, fundDatasetsHeaders);
    }

    componentDidUpdate(prevProps: Props, prevState: State) {
        if (this.props.results !== prevProps.results || this.state.editingRows !== prevState.editingRows) {
            this.setState({
                dataResults: getDataWithUserUpdates(
                    adjustEmptyRows(
                        this.props.results,
                        this.props.count,
                        this.handleRowClick,
                        this.props.currentFund.ap_id,
                    ),
                    this.state.editingRows,
                )
            });
        }
        if (this.props.selectedPeriod !== null && !this.state.focusedOnSelected) {
            const selectedRow = window.document.querySelector(`.${selectedPeriodClass}`);
            if (selectedRow) {
                this.setState({focusedOnSelected: true}, () => {
                    selectedRow.scrollIntoView( false);
                });
            } else if (this.state.dataResults && this.state.dataResults.length > 0 && !this.doesSelectedPeriodExist) {
                this.setState({focusedOnSelected: true}, () => {
                    const tableElement = window.document.querySelector(`.ui-table--responsive`);
                    if (tableElement) {
                        window.scrollTo(0, tableElement.scrollHeight);
                    }
                });
            }
        }
        if (this.props.uploadStatus === UploadStatus.Success && prevProps.uploadStatus === UploadStatus.New) {
            this.props.handleChange({scroller: false});
            this.props.setDatasetListIsUpdated(true);
        }
    }

    componentWillUnmount() {
        this.props.clearSelectedPeriod();

        if (!!this.props.datasetListIsUpdated) {
            this.props.getFund(this.props.currentFund.id);
            this.props.setDatasetListIsUpdated(false);
        }
    }

    makeLatest = (period: string, dataset: DatasetWithRowType) => {
        this.props.makeDatasetTheLatest(dataset);
    }

    handleRowClick = (period: string) => {
        const newDataResults: DatasetWithRowType[] = this.state.dataResults.map((dataResult: DatasetWithRowType) => {
            if (dataResult.period === period && dataResult.rowType === CustomTableRow.AddNewDataset) {
                delete(dataResult.rowType);
                delete(dataResult.handleClick);
            }
            return dataResult;
        });
        this.setState({ dataResults: newDataResults});

        this.handleStartEditAction(period, {period: period, fund_ap_id: this.props.currentFund.ap_id || ''});
    }

    loadDatasetHistory = (id: string, dataset: Dataset) => {
        if (dataset.activeRow) {
            this.props.clearFundDatasetHistory(id, dataset.period);
        } else {
            this.props.getFundDatasetHistory(id, dataset);
        }
    }

    handleStartEditAction = (period: string, dataset: Dataset) => {
        const editingRows = {
            ...this.state.editingRows,
            [period]: dataset
        };
        this.setState({editingRows: editingRows}, () => {
            if (this.props.selectedPeriod === period) {
                this.props.clearSelectedPeriod();
            }
        });
    }

    saveRowAction = (period: string, dataset: Dataset, id: string) => {
        const datasetCopy = cloneDeep(dataset);
        let editingRows = this.state.editingRows;
        if (editingRows && editingRows[period]) {
            delete(editingRows[period]);
        }
        this.setState({editingRows: editingRows}, () => {
            if (datasetCopy.data_source !== DataEntry.Requestor && datasetCopy.internal_dataset_id === null) {
                // creating
                delete(datasetCopy.id);
            } else {
                // updating
                Object.keys(datasetCopy).forEach((key: string) => {
                    // remove non-editable fields except internal_dataset_id
                    if ([...editableHeaders, 'internal_dataset_id'].indexOf(key) === -1) {
                        delete(datasetCopy[key]);
                    // set empty values to null to clear the values in DB
                    } else if (datasetCopy.hasOwnProperty(key) && datasetCopy[key] === undefined) {
                        datasetCopy[key] = null;
                    }
                });
            }

            this.props.saveRequestorDataset(datasetCopy);
            this.props.setDatasetListIsUpdated(true);
            this.props.clearFundDatasetHistory(id, period);
        });
    }

    isEditing = (row: DatasetWithRowType) => {
        if (this.state.editingRows) {
            return !!this.state.editingRows[row.period];
        }
        return false;
    }

    getCustomRowStyle = (row: Dataset) => {
        if (this.props.selectedPeriod && row.period === this.props.selectedPeriod) {
            return [selectedPeriodClass];
        }
        return [];
    }

    getCustomCellStyle = (row: Dataset) => {
        if (this.props.selectedPeriod && row.period === this.props.selectedPeriod) {
            return [selectedPeriodClass];
        }
        return [];
    }

    handleSetWidthHeaders = (headerObject: TableHeader) => {
        const foundObject = this.headerFields.find(item => item.name === headerObject.name);

        if (!foundObject) {
            this.headerFields.push(headerObject);
        } else {
            this.headerFields = this.headerFields.map(item => {
                let answer = item;
                if (item.name === headerObject.name) {
                    answer = headerObject;
                }

                return answer;
            });
        }

        this.setState({headerFields: this.headerFields});
    }

    handleColumnChange = (value: string, name: string, rowNumber: number) => {
        let dataResults: Dataset[] = this.state.dataResults;
        if (dataResults && dataResults[rowNumber]) {
            const datasetPeriod = dataResults[rowNumber] && dataResults[rowNumber].period;
            let editingRow: Dataset = cloneDeep(dataResults[rowNumber]);
            editingRow[name] = value;

            dataResults[rowNumber] = {
                ...dataResults[rowNumber],
                ...editingRow
            };

            this.setState({
                editingRows: {
                    ...this.state.editingRows,
                    [datasetPeriod]: {
                        ...this.state.editingRows[datasetPeriod],
                        ...editingRow
                    }
                },
                dataResults,
            });
        }
    }

    showModal = (location) => {
        this.props.openModal(confirmModalName, {location, handleYesClick: this.handleConfirmNavigationClick});
    }

    handleBlockedNavigation = (nextLocation: Location<any>): boolean => {
        const {confirmedNavigation} = this.state;
        if (!confirmedNavigation && this.pageIsBlocked) {
            this.showModal(nextLocation);
            return false;
        }

        return true;
    }

    handleConfirmNavigationClick = (pathname: string) => {
        if (pathname) {
            this.setState({confirmedNavigation: true}, () => {
                this.props.history.push(pathname);
            });
        }
    }

    render() {
        const { count, currentFund, headers, isGridLoading,
            exportFundDatasets, handleChange, currentFilter, openModal } = this.props;

        let widthForLine: number = 0;
        headersInModal.map((headeritem: string) => {
            this.state.headerFields.map((item: any) => {
                const valueInHeaderProps = headers.find((head: TableHeader) => head.name === item.name);
                const headerActivity = valueInHeaderProps ? valueInHeaderProps.active : false;
                if (item.name === headeritem && headerActivity) {
                    widthForLine += item.width;
                }
            });
        });

        let styleForLeft: any =  null;
        let styleForRight: any =  null;

        if (this.wrapperWidth) {
            const widthForLeft = widthForLine / this.wrapperWidth * 100;
            styleForLeft = {
                marginLeft: '70px',
                width: `${widthForLeft}%`,
            };

            const widthForRight = (this.wrapperWidth - widthForLine) / this.wrapperWidth * 100;
            styleForRight = {
                width: `calc(${widthForRight}% - 70px)`,
            };
        }

        const { currency } = currentFund;
        const currentCurrency = FundHelper.getCurrencyLabel(FundHelper.getCurrencyItem(currency));

        const navBlock = (
            <UiWrapper forwardedRef={this.wrapperRef} withoutMargin={true}>
                <div className={[styles.rowModified, 'row'].join(' ')}>
                    <div style={styleForLeft} className="navRow">
                        <div className="row">
                            <button className="clear-button">
                                <span className="titleNav">NAV</span>
                                <span className="CurrencyLabel">{currentCurrency}</span>
                            </button>
                        </div>
                    </div>
                    <div style={styleForRight}>
                        <button className="clear-button">
                            <span className="titleNav">Performance (%)</span>
                        </button>
                    </div>
                </div>
            </UiWrapper>
        );

        return (
            <>
                <div className={[styles.wrapper, styles.dataset].join(' ')}>
                    <UiDataTable className="table-text--center table-text--vertical-middle">
                        <DataTable
                            buttons={this.getButtons(openModal, exportFundDatasets, currentFund)}
                            count={count}
                            currentFilter={currentFilter}
                            customContent={navBlock}
                            handleSetWidthHeaders={this.handleSetWidthHeaders}
                            handleColumnChange={this.handleColumnChange}
                            handleChange={handleChange}
                            headers={this.getHeaders(this.isEditing, this.getCustomCellStyle)}
                            isLoading={isGridLoading}
                            rows={this.state.dataResults}
                            rowActions={this.rowActions}
                            visibleHeaders={headers}
                            hasPagination={true}
                            hasSorting={false}
                            withCheckboxes={false}
                            actionsTitle={' '}
                            getCustomRowStyles={this.getCustomRowStyle}
                        />
                    </UiDataTable>
                </div>
                <ColumnsModal
                    handleColumns={this.props.handleColumns}
                    fund={currentFund}
                />

                <Prompt
                    when={this.pageIsBlocked}
                    message={this.handleBlockedNavigation}
                />

                <DatasetsLeavePageModal />
                <BulkUploadModal />
            </>
        );
    }

    private get wrapperWidth() {
        return this.wrapperRef.current ? this.wrapperRef.current.clientWidth : 0;
    }

    private get pageIsBlocked() {
        return !isEmpty(this.state.editingRows);
    }

    private get doesSelectedPeriodExist() {
        const {selectedPeriod} = this.props;
        if (selectedPeriod !== null) {
            return !!this.state.dataResults.find(
                (dataResult: DatasetWithRowType) => dataResult.period === selectedPeriod
            );
        }
        return false;
    }
}

export default DatasetsPage;
