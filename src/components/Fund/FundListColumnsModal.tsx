import * as React from 'react';

import { SaveFundListColumnsAction, SetFundListTemporaryHeadersAction } from 'store/Fund/actions';
import { CloseModalAction } from 'store/Modals/General/actions';
import { TableHeader } from 'store/types';
import ConfigureColumnsModal from 'components/Shared/Modals/ConfigureColumns/ConfigureColumnsModal';
import { DEFAULT_VISIBLE_HEADERS } from 'store/VisibleHeaders/constants';
import { headerNames } from 'store/VisibleHeaders/types';
import { GridConfigurationType } from 'store/Fund/types';

interface Props {
    accountId: string;
    temporaryHeaders: TableHeader[];
    setFundListTemporaryHeaders: (headers: TableHeader[]) => SetFundListTemporaryHeadersAction;
    isOpen: boolean;
    headers: TableHeader[];
    handleColumns: (columns: TableHeader[]) => void;
    closeModal: () => CloseModalAction;
    modalParams: any;
    saveFundListColumns: (
        visibleHeaders: string[],
        instanceId: string,
        gridType: string,
    ) => SaveFundListColumnsAction;
}

interface HeaderSetting {
    label: string;
    values: string[];
}

const headerList: HeaderSetting[] = [{
    label: 'NAV',
    values: [
        'beginning_balance', 'subscriptions', 'redemptions', 'income', 'ending_balance', 'known_future_redemptions',
        'nav_per_share'
    ],
}, {
    label: 'Performance',
    values: ['perf_daily', 'perf_weekly', 'perf_month', 'perf_mtd', 'perf_qtd', 'perf_ytd'],
}, {
    label: 'Fund',
    values: ['fund_sec', 'fund_lei', 'fund_internal_id', 'fund_currency'],
}, {
    label: 'Firm',
    values: ['firm_crd', 'firm_sec', 'firm_lei', 'firm_internal_id'],
}, {
    label: 'Data',
    values: ['date_received', 'data_status', 'reporting_history', 'entry_type'],
}, {
    label: 'Attributes',
    values: ['product', 'strategy', 'credit_officer'],
}];

export default class FundListColumnsModal extends React.Component<Props> {

    componentDidMount () {
        this.props.setFundListTemporaryHeaders(this.props.headers);
    }

    render() {
        const {
            isOpen, headers, temporaryHeaders, modalParams, accountId,
            setFundListTemporaryHeaders, handleColumns, closeModal, saveFundListColumns
        } = this.props;
        return (
            <ConfigureColumnsModal
                instanceId={accountId}
                headerList={headerList}
                headers={headers}
                temporaryHeaders={temporaryHeaders}
                defaultVidibleHeaders={DEFAULT_VISIBLE_HEADERS[headerNames.fundList]}
                setTemporaryHeaders={setFundListTemporaryHeaders}
                isOpen={isOpen}
                handleColumns={handleColumns}
                closeModal={closeModal}
                modalParams={modalParams}
                saveColumns={saveFundListColumns}
                gridType={GridConfigurationType.FundList}
            />
        );
    }
}