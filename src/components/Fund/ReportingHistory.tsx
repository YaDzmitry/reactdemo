import * as React from 'react';
import * as classNames from 'classnames';

import { FundReportingHistory } from '../../store/Fund/types';

import mock from '../../mock/json/mockFundReporting';

interface Props {
}

const styles = require('./ReportingHistory.scss');

const mockHistory = mock.history;
const months = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

interface YearInfo {
    year: number;
    history: (FundReportingHistory | null)[];
}

class ReportingHistory extends React.PureComponent<Props> {
    render () {
        const mapByYear = this.getMapByYearHistory(mockHistory as FundReportingHistory[]);

        return (
            <div className={styles.history}>
                <div className={`${styles.header} d-flex justify-content-between`}>
                    <div>
                        <label>Current Status:</label>
                        <span>On time</span>
                    </div>

                    <div>
                        <label>Average delivery Day:</label>
                        <span>+1</span>
                    </div>
                </div>

                <div className={`${styles.content} d-flex flex-column-reverse align-items-stretch`}>
                    {mapByYear.map((year: YearInfo, yearIdx: number) => {
                        return (
                            <div key={yearIdx} className={`${styles.yearRow} d-flex justify-content-between`}>
                                <div className={`${styles.item} ${styles.yearItem}`}>
                                    {year.year}
                                </div>
                                {year.history.map((item, idx) => {
                                    if (item === null) {
                                        return (
                                            <div key={idx} className={`${styles.item} ${styles.inactive}`}>
                                                <span>0</span>
                                            </div>
                                        );
                                    }
                                    const {shift} = item;

                                    const className = classNames({
                                        [styles.success]: shift >= 0,
                                        [styles.fail]: shift < 0
                                    });

                                    return (
                                        <div key={idx} className={`${styles.item} ${className}`}>
                                            <span>{item.shift > 0 && '+'}{item.shift}</span>
                                        </div>
                                    );
                                })}
                            </div>
                        );
                    })}

                    <div className={`${styles.year} d-flex justify-content-between`}>
                        {months.map((month, idx) => (
                            <div key={idx} className={`${styles.item} ${styles.yearItem}`}>
                                <span>{month}</span>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        );
    }

    getMapByYearHistory (history: FundReportingHistory[]) {
        let mapByYear: YearInfo[] = [];
        let items: FundReportingHistory[] = [];
        let currentYear = history.length && history[0].year;

        let historyItem: FundReportingHistory;
        for (historyItem of history) {
            if (currentYear !== historyItem.year) {
                mapByYear.push({
                    year: currentYear,
                    history: items
                });
                currentYear = historyItem.year;
                items = [];
            }

            items.push(historyItem);
        }

        if (items.length) {
            items.push(...(new Array(12 - items.length)).fill(null));
            mapByYear.push({
                year: currentYear,
                history: items
            });
        }

        return mapByYear;
    }
}

export default ReportingHistory;
