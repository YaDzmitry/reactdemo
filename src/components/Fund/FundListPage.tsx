import * as React from 'react';

import MainLayout from 'components/Shared/Layouts/MainLayout';
import { FilterDecoratorChange } from '../../decorators/FilterDecorator';
import { FilterType } from '../../store/constants';
import { FilterItem } from '../../store/types';

const styles = require('./FundListPage.scss');

interface Props {
    count: number;
    applyFilter?: FilterDecoratorChange;
    currentFilter?: any;
}

const filters: FilterItem[] = [
    {
        name: 'period',
        type: FilterType.SingleDate,
        title: 'Start Date',
        material: true,
        customInputIcon: true,
        className: `ui-material`,
        withLabel: false,
    },
];

const FundListPage: React.FC<Props> = ({children, count, applyFilter, currentFilter}) => (
    <MainLayout
        title={`Funds (${count})`}
        menuItems={[]}
        filters={filters}
        handleChange={applyFilter}
        currentFilter={currentFilter}
    >
        <div className={styles.content}>
            {children}
        </div>
    </MainLayout>
);

export default FundListPage;
