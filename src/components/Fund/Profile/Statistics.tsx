import * as React from 'react';
import * as classNames from 'classnames';

import { formatText } from 'helpers/tableHelper';

import { Account } from 'store/Account/models/account';
import { ChartConfig, FundStatistics, StatisticsDataType, StatsValue } from 'store/Fund/types';
import { addBlueIcon, DEFAULT_VALUE_DASH, FormatTypeChoices } from 'store/constants';
import { Id } from 'store/types';

import FundNameRow from './StatisticsRows/FundNameRow';
import StatisticsDifferenceRow from './StatisticsRows/DifferenceRow';
import StatisticsResultRow from './StatisticsRows/ResultRow';

const styles = require('./Statistics.scss');

interface Props {
    data: FundStatistics[];
    intervalTitle: string;
    removeFund: (fundId: Id) => void;
    openModal: () => void;
    getColor: (index: number) => string;
    chartConfig: ChartConfig;
    account: Account;
}

export const tableRowClass = classNames({[styles['table-row']]: true});
export const columnValueClass = {[styles['column-value']]: true};

export function formatCellValue(value: StatsValue, dataType?: StatisticsDataType) {
    if (dataType === StatisticsDataType.Partial) {
        return formatText(dataType, FormatTypeChoices.FirstCharUpper);
    } else {
        return formatText(value, FormatTypeChoices.Percent, DEFAULT_VALUE_DASH);
    }
}

export function getColumnClass(
    generalClass: {[key: string]: boolean},
    difference: StatsValue,
    inverse: boolean = false
) {
    return {
        ...generalClass,
        [styles.positive]: difference !== null && (+difference < 0 && !inverse || +difference > 0 && inverse),
        [styles.negative]: difference !== null && (+difference > 0 && !inverse || +difference < 0 && inverse),
        [styles.zero]: difference === 0,
    };
}

export function getDrawdownColumnClass(
    generalClass: {[key: string]: boolean},
    baseFundValue: StatsValue,
    comparedFundValue: StatsValue,
) {
    return {
        ...generalClass,
        [styles.positive]: baseFundValue !== null && comparedFundValue !== null
            && (+baseFundValue >= +comparedFundValue),
        [styles.negative]: baseFundValue !== null && comparedFundValue !== null
            && (+baseFundValue < +comparedFundValue),
        [styles.zero]: comparedFundValue === 0,
    };
}

class Statistics extends React.Component<Props> {

    private columns = [``, `Return (Ann'd)`, `Volatility (Ann'd)`, `Drawdown (PtT)`];

    handleAddFund = () => {
        this.props.openModal();
    }

    handleRemoveFund = (fundId: Id) => {
        this.props.removeFund(fundId);
    }

    getDifference = (value: StatsValue, key: string): StatsValue => {
        const currentFundData = this.thisFundStatistics;
        if (value === null && currentFundData && currentFundData.hasOwnProperty(key) && currentFundData[key] === null) {
            return null;
        } else if (currentFundData && currentFundData.hasOwnProperty(key)) {
            return (currentFundData[key] && +currentFundData[key] || 0) - (value && +value || 0);
        }
        return null;
    }

    isDifferenceBlockVisible = (fundStatistic: FundStatistics): boolean => {
        const thisFund = this.thisFundStatistics;

        return thisFund && thisFund.data_type === StatisticsDataType.AllData
            && fundStatistic.data_type === StatisticsDataType.AllData;
    }

    render() {
        const {data, intervalTitle} = this.props;
        const tableClass = classNames({[styles.table]: true});

        return (
            <div className={styles.wrapper}>
                <div className={styles.header}>
                    <span>Statistics</span>
                    <span className={styles['stats-interval']}>{intervalTitle}</span>
                </div>

                <div className={tableClass}>
                    <div className={tableRowClass}>
                        {this.columns.map((label, index) => {
                            const headerClassName = classNames({
                                [styles['header-column']]: true,
                                [styles['first-column']]: index === 0,
                            });

                            return (<div key={index} className={headerClassName}><span>{label}</span></div>);
                        })}
                    </div>
                    {data && data.map((fundStat: FundStatistics, index: number) => {
                        return (
                            <React.Fragment key={index}>
                                {
                                    index !== 0 &&
                                    <FundNameRow
                                        fundStat={fundStat}
                                        index={index}
                                        removeFund={this.handleRemoveFund}
                                        color={this.props.getColor(index)}
                                        account={this.props.account}
                                    />
                                }
                                <StatisticsResultRow fundStat={fundStat} index={index}/>
                                {
                                    index !== 0 && this.isDifferenceBlockVisible(fundStat) &&
                                    <StatisticsDifferenceRow
                                        fundStat={fundStat}
                                        index={index}
                                        getDifference={this.getDifference}
                                        thisFundStat={this.thisFundStatistics}
                                    />
                                }
                            </React.Fragment>
                        );
                    })}
                </div>

                <div onClick={this.handleAddFund} className={styles['add-another-wrapper']}>
                    <img src={addBlueIcon} alt={`add another`}/>
                    <span className={styles['add-another']}>Add another...</span>
                </div>

            </div>
        );
    }

    private get thisFundStatistics() {
        return this.props.data && this.props.data[0];
    }
}

export default Statistics;
