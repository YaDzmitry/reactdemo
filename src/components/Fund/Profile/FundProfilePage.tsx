import * as React from 'react';
import 'anychart';
import { Moment } from 'moment';
import * as moment from 'moment';

import GlobalHelper from 'helpers/globalHelper';
import FundHelper from 'helpers/fundHelper';
import { formatText } from 'helpers/tableHelper';
import { colors, generateRandomColor } from 'helpers/html';
import DataManagerHelper from 'helpers/dataManagerHelper';

import ViewLink from 'components/Shared/Ui/ViewLink';
import TableDifferentRow from 'components/Shared/Ui/Tables/TableDifferentRow';
import InfoTable, { InfoTableData } from 'components/Shared/DataTable/InfoTable';

import {
    DEFAULT_VALUE_DASH, DataPointChoices, FormatTypeChoices, HistoryPeriodChoices,
    DEFAULT_ITEMS_PER_PAGE
} from 'store/constants';
import { ChartConfig, FundStatistics, FundStatisticsDataset, FundProfileTableData } from 'store/Fund/types';
import { FundModel, EntityDatasets } from 'store/Fund/models/fund';
import { ChangeChartIntervalAction, SelectPeriodAction } from 'store/Fund/actions';
import { Id } from 'store/types';
import { AlertsOverview } from 'store/AlertsManager/Alerts/types';
import { Account } from 'store/Account/models/account';
import { isRequestorAccount } from 'store/User/helpers';

import { ChartType } from 'store/Profiles/types';

import Chart from './Chart/Chart';
import ChartHeader from './Chart/Header';
import AlertsOverviewComponent from './AlertsOverview';
import DataPoint from './DataPoint';
import Statistics from './Statistics';
import YearMonthTable from './YearMonthTable';
import { FilterNames, FiltersState } from 'store/Filters/types';
import { initialFundDatasets } from 'store/Fund/constants';

interface Props {
    fund: FundModel;
    mainFundDatasets: EntityDatasets;
    navTableData: FundProfileTableData;
    performanceTableData: FundProfileTableData;
    setHistoryPeriod: (historyPeriod: HistoryPeriodChoices) => any;
    currentHistoryPeriod: HistoryPeriodChoices;
    fundDetails: InfoTableData[];
    serviceProviders: InfoTableData[];
    statistics: FundStatistics[];
    chartStatistics: FundStatistics[];
    alertsOverview: AlertsOverview[];
    removeFund: (fundId: Id) => void;
    openModal: () => void;
    viewFundAlerts: (period: string) => void;
    showDataEntry: boolean;
    account: Account;
    changeChartInterval: (start: number, end: number) => ChangeChartIntervalAction;
    chartConfig: ChartConfig;
    selectPeriod: (period: string | null) => SelectPeriodAction;
    datasetsFilter: FiltersState;
    setFilters: (filterName: string, filter: FiltersState) => void;
}

interface State {
    chartType: ChartType;
}

const styles = require('./FundProfilePage.scss');

class FundProfilePage extends React.Component<Props, State> {
    colors = colors;

    state = {
        chartType: ChartType.Aum
    };

    selectPeriodHandler = (period: string) => {
        // load several pages to show 6 years of datasets when click on the aum/performance cell
        const newFilter = {
            ...this.props.datasetsFilter,
            extraLimit: Math.ceil((6 * 12) / DEFAULT_ITEMS_PER_PAGE) * DEFAULT_ITEMS_PER_PAGE,
            offset: 0
        };
        this.props.setFilters(FilterNames.datasetsTab, newFilter);
        this.props.selectPeriod(period);
    }

    render() {
        const {
            showDataEntry, fund,
            navTableData, performanceTableData, statistics, chartStatistics,
            removeFund, openModal, alertsOverview,
            fundDetails, viewFundAlerts, serviceProviders, account, changeChartInterval, chartConfig,
        } = this.props;

        const {chartType} = this.state;

        const currency = GlobalHelper.getValueFromObject(fund, 'currency');
        const currencyItem = FundHelper.getCurrencyItem(currency);
        const currencyLabel = FundHelper.getCurrencyLabel(currencyItem);
        const latestDataset = this.latestDataset;
        const date = formatText(DataManagerHelper.getActivePeriod(), FormatTypeChoices.DateFormat);

        return (
            <div className="row">
                <div className="col-xl-8 col-lg-8 col-md-12 col-sm-12">
                    <div className={styles.container}>
                        <div className={styles['block-wrapper']}>
                            <ChartHeader
                                chartType={chartType}
                                statistics={statistics}
                                intervalTitle={this.intervalTitle}
                                getColor={this.getColor}
                                switchToAum={this.switchToAum}
                                switchToGrowth={this.switchToGrowth}
                            />
                            <Chart
                                chartType={chartType}
                                fundId={fund.id}
                                statistics={chartStatistics}
                                currencyName={currency}
                                getColor={this.getColor}
                                changeChartInterval={changeChartInterval}
                            />
                        </div>
                    </div>

                    <div className={styles['data-points-wrapper']}>
                        <DataPoint
                            type={DataPointChoices.Balance}
                            value={latestDataset.ending_balance}
                            currencyName={currency}
                        >
                            NAV as of {date}
                        </DataPoint>
                        <DataPoint
                            type={DataPointChoices.PerformanceMonth}
                            value={latestDataset.perf_month}
                        >
                            Month Performance as of {date}
                        </DataPoint>
                        <DataPoint
                            type={DataPointChoices.PerformanceYTD}
                            value={this.perfYTD}
                        >
                            YTD Performance as of {date}
                        </DataPoint>
                    </div>
                    <ViewLink
                        link={
                            isRequestorAccount(account)
                                ? `/funds/${fund.id}/datasets`
                                : `/funds/${fund.id}/data`
                        }
                    >
                        View all NAV History
                    </ViewLink>

                    <div className={styles.container}>
                        <YearMonthTable
                            showCurrency={navTableData.multiCurrency}
                            data={navTableData.data}
                            header={`AUM (${currencyLabel})`}
                            formatType={FormatTypeChoices.NumeralThreeDecimalLower}
                            showDataEntry={showDataEntry}
                            fundId={fund.id}
                            account={account}
                            onCellClick={this.selectPeriodHandler}
                        />
                    </div>

                    <div className={styles.container}>
                        <YearMonthTable
                            data={performanceTableData.data}
                            header="Performance (%)"
                            formatType={FormatTypeChoices.NumeralDefault}
                            showDataEntry={showDataEntry}
                            fundId={fund.id}
                            account={account}
                            onCellClick={this.selectPeriodHandler}
                        />
                    </div>
                    {
                        serviceProviders && serviceProviders.length > 0 &&
                        <div className={styles.container}>
                            <TableDifferentRow>
                                <InfoTable
                                    data={serviceProviders}
                                    header="Service Providers"
                                    headerClass={styles['fund-profile-providers']}
                                    showDataEntry={showDataEntry}
                                />
                            </TableDifferentRow>
                        </div>
                    }
                </div>
                <div className="col-xl-4 col-lg-4 col-md-12 col-sm-12">
                    <div className={styles.container}>
                        <Statistics
                            data={statistics}
                            intervalTitle={this.intervalTitle}
                            removeFund={removeFund}
                            openModal={openModal}
                            getColor={this.getColor}
                            chartConfig={chartConfig}
                            account={account}
                        />
                    </div>
                    <div className={styles.container}>
                        <AlertsOverviewComponent
                            data={alertsOverview}
                            viewFundAlerts={viewFundAlerts}
                            fundId={fund.id}
                        />
                    </div>
                    <div className={styles.container}>
                        <TableDifferentRow>
                            <InfoTable
                                data={fundDetails}
                                header="Fund Details"
                                headerClass={styles['fund-profile-details']}
                                showDataEntry={showDataEntry}
                            />
                        </TableDifferentRow>
                    </div>
                </div>
            </div>
        );
    }

    private getColor = (index: number) => {
         if (this.colors[index] !== undefined) {
             return this.colors[index];
         }

         const randomColor = generateRandomColor();
         this.colors[index] = randomColor;

         return randomColor;
    }

    private switchToAum = () => {
        this.setState({
            chartType: ChartType.Aum
        });
    }

    private switchToGrowth = () => {
        this.setState({
            chartType: ChartType.Growth
        });
    }

    private calculateYTD = (datasets: FundStatisticsDataset[]) => {
        if (datasets.length > 0) {
            let isEmptyData = false;
            const currentYear = DataManagerHelper.getActivePeriod().slice(0, 4),
                filteredDataset = datasets.filter(item => {
                    if (item.period.startsWith(currentYear)) {
                        if (item.perf_month) {
                            return item;
                        } else {
                            isEmptyData = true;
                        }
                    }

                    return false;
                });

            if (!isEmptyData) {
                return (
                    filteredDataset.reduce(
                        (res, dataset) => res * (1 + (+dataset.perf_month || 0) / 100),
                        1
                    ) - 1) * 100;
            }
        }

        return null;
    }

    private get intervalTitle() {
        return `${this.startInterval} to ${this.endInterval}`;
    }

    private get startInterval() {
        return formatText(this.props.chartConfig.start * 1000, FormatTypeChoices.FullMonthAndYear, DEFAULT_VALUE_DASH);
    }

    private get endInterval() {
        let date: number | Moment = 0;

        if (this.props.chartConfig.end) {
            date = moment.unix(this.props.chartConfig.end);

            if (date.date() !== date.daysInMonth()) {
                date = date.subtract(1, 'months');
            }
        }

        return formatText(date, FormatTypeChoices.FullMonthAndYear, DEFAULT_VALUE_DASH);
    }

    private get perfYTD() {
        const {mainFundDatasets} = this.props,
            latestDataset = this.latestDataset;

        if (!latestDataset.perf_ytd) {
            return this.calculateYTD(mainFundDatasets.history_datasets);
        }

        return latestDataset.perf_ytd;
    }

    private get latestDataset() {
        const {mainFundDatasets} = this.props;

        let latestDataset = mainFundDatasets.latest_dataset;
        if (latestDataset && latestDataset.period === DataManagerHelper.getActivePeriod()) {
            return latestDataset;
        }
        return {...initialFundDatasets.latest_dataset};
    }
}

export default FundProfilePage;
