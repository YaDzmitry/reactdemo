import * as React from 'react';
import * as classNames from 'classnames';

import { formatText } from 'helpers/tableHelper';

import AlertPeriodCell from 'components/AlertsManager/Alerts/AlertPeriodCell';
import AlertStatusCell from 'components/Fund/Profile/AlertsOverviewCells/AlertStatusCell';
import ViewLink from 'components/Shared/Ui/ViewLink';

import { AlertsOverview } from 'store/AlertsManager/Alerts/types';
import { RulePeriod } from 'store/AlertsManager/Rulesets/types';
import { FormatTypeChoices } from 'store/constants';
import { Id } from 'store/types';

const styles = require('./AlertsOverview.scss');

interface Props {
    data?: AlertsOverview[];
    fundId: Id;
    viewFundAlerts: (period: string) => void;
}
const tableRowClass = classNames({[styles['table-row']]: true});
class AlertsOverviewComponent extends React.Component<Props> {

    private alertsOverviewHeaders: string[] = ['', '1mo', '3mo', '12mo', 'Other', 'Status'];

    renderTable = () => {
        const tableClass = classNames({[styles.table]: true});

        const {data} = this.props;
        return (
            <div className={tableClass}>
                <div className={tableRowClass}>
                    {this.alertsOverviewHeaders.map((header, idx) => {
                        const headerClassName = classNames({
                            [styles['table-cell']]: true,
                            [styles['top-header']]: true,
                            [styles['left-header']]: idx === 0,
                            [styles['status-column']]: idx === this.alertsOverviewHeaders.length - 1,
                        });

                        return (<div key={idx} className={headerClassName}><span>{header}</span></div>);
                    })}
                </div>

                {data && data.map((alertItem: AlertsOverview, itemId: number) => {
                    return this.renderRow(alertItem, itemId);
                })}
            </div>
        );
    }

    renderRow = (alertItem: AlertsOverview, id: number) => {
        const columnValueClass = classNames({
            [styles['table-cell']]: true,
            [styles.value]: true,
        });
        const periodColumnClass = classNames({
            [styles['table-cell']]: true,
            [styles['left-header']]: true,
        });
        const onRowClick = () => {
            this.props.viewFundAlerts(alertItem.period_date);
        };
        return (
            <div className={tableRowClass} key={id} onClick={onRowClick}>
                <div className={periodColumnClass}>
                    {formatText(alertItem.period_date, FormatTypeChoices.DateFormatShort)}
                </div>

                <div className={columnValueClass}>
                    <AlertPeriodCell period={alertItem.periods[RulePeriod.OneMonth]} size="sm"/>
                </div>
                <div className={columnValueClass}>
                    <AlertPeriodCell period={alertItem.periods[RulePeriod.ThreeMonths]} size="sm"/>
                </div>
                <div className={columnValueClass}>
                    <AlertPeriodCell period={alertItem.periods[RulePeriod.TwelveMonths]} size="sm"/>
                </div>
                <div className={columnValueClass}>
                    <AlertPeriodCell period={alertItem.periods[RulePeriod.Other]} size="sm"/>
                </div>
                <AlertStatusCell value={alertItem.status}/>
            </div>
        );
    }

    render() {
        const { data, fundId } = this.props;
        return (
            <>
                <div className={styles.wrapper}>
                    <div className={styles.header}>
                        Alerts Overview
                    </div>

                    <div>
                        {data && data.length > 0 && this.renderTable()}
                    </div>
                </div>

                <ViewLink link={`/alerts/${fundId}`}>View all alerts</ViewLink>
            </>
        );
    }
}

export default AlertsOverviewComponent;