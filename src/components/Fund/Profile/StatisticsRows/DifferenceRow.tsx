import * as React from 'react';
import * as classNames from 'classnames';

import {
    tableRowClass, columnValueClass, formatCellValue, getColumnClass, getDrawdownColumnClass
} from 'components/Fund/Profile/Statistics';
import { FundStatistics, StatsValue } from 'store/Fund/types';

const styles = require('../Statistics.scss');

interface DifferenceRowProps {
    index: number;
    fundStat: FundStatistics;
    thisFundStat: FundStatistics;
    getDifference: (value: StatsValue, key: string) => StatsValue;
}

const DifferenceRow: React.FC<DifferenceRowProps> = ({index, fundStat, thisFundStat, getDifference}) => {
    const rowHeaderClass = classNames({
        [styles['this-fund']]: index === 0,
        [styles['result-label']]: true,
        [styles['first-column']]: true,
    });

    const volatilityDiff = getDifference(fundStat.volatility_ann, 'volatility_ann');
    const returnDiff = getDifference(fundStat.return_ann, 'return_ann');
    const drawdownDiff = getDifference(fundStat.drawdown_ann, 'drawdown_ann');
    return (
        <div className={tableRowClass}>
            <div className={rowHeaderClass}>{'Fund Difference'}</div>
            <div className={classNames(getColumnClass(columnValueClass, returnDiff, true))}>
                {formatCellValue(returnDiff)}
            </div>
            <div className={classNames(getColumnClass(columnValueClass, volatilityDiff))}>
                {formatCellValue(volatilityDiff)}
            </div>
            <div
                className={classNames(getDrawdownColumnClass(
                    columnValueClass,
                    thisFundStat.drawdown_ann,
                    fundStat.drawdown_ann
                ))}
            >
                {formatCellValue(drawdownDiff)}
            </div>
        </div>
    );
};

export default DifferenceRow;
