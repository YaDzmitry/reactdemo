import * as React from 'react';
import * as classNames from 'classnames';

import { tableRowClass, columnValueClass, formatCellValue } from 'components/Fund/Profile/Statistics';
import { FundStatistics, StatisticsDataType } from 'store/Fund/types';

const styles = require('../Statistics.scss');

interface ResultRowProps {
    index: number;
    fundStat: FundStatistics;
}

const ResultRow: React.FC<ResultRowProps> = ({index, fundStat}) => {
    const rowHeaderClass = classNames({
        [styles['this-fund']]: index === 0,
        [styles['result-label']]: true,
        [styles['first-column']]: true,
    });

    const cellClassName = classNames({
        ...columnValueClass,
        [styles.partial]: fundStat.data_type === StatisticsDataType.Partial,
        [styles['no-data']]: fundStat.data_type === StatisticsDataType.NoData,
    });

    return (
        <div className={tableRowClass}>
            <div className={rowHeaderClass}>
                {index === 0 ? 'This fund' : 'Value'}
            </div>
            <div className={cellClassName}>
                {formatCellValue(fundStat.return_ann, fundStat.data_type)}
            </div>
            <div className={cellClassName}>
                {formatCellValue(fundStat.volatility_ann, fundStat.data_type)}
            </div>
            <div className={cellClassName}>
                {formatCellValue(fundStat.drawdown_ann, fundStat.data_type)}
            </div>
        </div>
    );
};

export default ResultRow;
