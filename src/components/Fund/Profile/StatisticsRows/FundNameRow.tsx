import * as React from 'react';
import * as classNames from 'classnames';
import { NavLink } from 'react-router-dom';

import { tableRowClass } from 'components/Fund/Profile/Statistics';

import { Account } from 'store/Account/models/account';
import { FundStatistics } from 'store/Fund/types';
import { Id } from 'store/types';
import { isRequestorAccount } from 'store/User/helpers';
import { isPeerGroup } from 'store/Fund/helpers/fundStatistics';

import { crossIcon } from 'store/constants';

const styles = require('../Statistics.scss');

interface FundNameProps {
    index: number;
    fundStat: FundStatistics;
    removeFund: (fundId: Id) => void;
    color: string;
    account: Account;
}

const FundNameRow: React.FC<FundNameProps> = ({index, fundStat, removeFund, color, account}) => {
    const fundNameClass = classNames({
        [styles['fund-name']]: true,
        [styles[`fund-name-${index}`]]: !!index,
    });
    const linkToFundProfile = isRequestorAccount(account) ? `/funds/${fundStat.id}` : `/funds/${fundStat.id}/profile`;
    const linkToGroup = `/profiles/groups/${fundStat.id}`;
    return (
        <div className={tableRowClass}>
            <div className={fundNameClass}>
                <img
                    className={styles.remove}
                    onClick={() => removeFund(fundStat.id)}
                    src={crossIcon}
                    alt={`remove`}
                />
                <NavLink
                    to={isPeerGroup(fundStat) ? linkToGroup : linkToFundProfile}
                    style={{color}}
                >
                    {fundStat.name}
                </NavLink>
            </div>
        </div>
    );
};

export default FundNameRow;
