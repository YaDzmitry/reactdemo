import * as React from 'react';
import * as classNames from 'classnames';

import { ChartType } from 'store/Profiles/types';
import { FundStatistics, StatisticsDataType } from 'store/Fund/types';
import Popover from 'components/Shared/Ui/Popover';

const styles = require('./Header.scss');

interface Props {
    chartType: ChartType;
    statistics: FundStatistics[];
    intervalTitle: string;

    getColor: (index: number) => string;
    switchToAum: () => void;
    switchToGrowth: () => void;
}

export const Header: React.FC<Props> = ({
    chartType, statistics, intervalTitle,
    getColor, switchToAum, switchToGrowth
}) => {
    const aumButtonClassName = classNames({
        [styles['report-type-button']]: true,
        [styles['report-type-button-checked']]: chartType === ChartType.Aum
    });

    const cumPerfButtonClassName = classNames({
        [styles['report-type-button']]: true,
        [styles['report-type-button-checked']]: chartType === ChartType.Growth
    });

    return (
        <div>
            <div className={styles.header}>
                <div>
                    {chartType === ChartType.Aum
                        ? 'AUM & Performance'
                        : 'Growth & Performance'}
                </div>
                <div>
                    <button className={aumButtonClassName} onClick={switchToAum}>
                        AUM
                    </button>
                    <button className={cumPerfButtonClassName} onClick={switchToGrowth}>
                        GROWTH
                    </button>
                </div>
            </div>
            <div className={styles['missing-data']}>
                {statistics
                    .map((stat, idx) => ({
                        ...stat,
                        color: getColor(idx)
                    }))
                    .filter(stat =>
                        stat.data_type === StatisticsDataType.NoData || stat.data_type === StatisticsDataType.Partial
                    )
                    .map(stat => (
                        <Popover
                            id={`missing-data-${stat.id}`}
                            key={stat.id}
                            body={`${stat.name} is missing one or more data points for ${intervalTitle}`}
                            className={styles['item-popover']}
                            enabled={true}
                        >
                            <div
                                style={{backgroundColor: stat.color, borderColor: stat.color}}
                                className={styles.item}
                            >
                                Partial Data
                            </div>
                        </Popover>
                    ))
                }
            </div>
        </div>
    );
};

export default Header;
