import * as React from 'react';
import * as moment from 'moment';
import { Moment } from 'moment';

import { formatText } from 'helpers/tableHelper';
import NumericHelper from 'helpers/NumericInputHelper';
import { FundStatistics, FundStatisticsDataset } from 'store/Fund/types';
import { Id } from 'store/types';
import { ANY_CHART_LICENSE_KEY, DEFAULT_VALUE_DASH, FormatTypeChoices } from 'store/constants';
import FundStatisticCalculator from 'store/Fund/helpers/statistics';

import { ChangeChartIntervalAction } from 'store/Fund/actions';
import { ChartType } from 'store/Profiles/types';

const styles = require('./Chart.scss');

const MAX_COLUMN_WIDTH = 12;

interface Props {
    fundId: Id;
    statistics: FundStatistics[];
    currencyName: string;
    chartType: ChartType;

    getColor: (index: number) => string;
    changeChartInterval: (start: number, end: number) => ChangeChartIntervalAction;
}

class Chart extends React.PureComponent<Props> {
    chart: anychart.charts.Stock;
    rangeSelector: anychart.ui.RangeSelector;
    plotLines: anychart.core.stock.Plot;
    plotColumns: anychart.core.stock.Plot;
    rangeSelected: boolean = false;

    private renderedStatistics: FundStatistics[] | null = null;
    private renderedType: ChartType | null = null;

    componentDidMount() {
        this.initChart();
        this.renderChart(this.props.chartType, this.props.statistics);
    }

    componentDidUpdate(prevProps: Props) {
        const {statistics, chartType} = this.props;
        if (statistics !== this.renderedStatistics || chartType !== this.renderedType) {
            this.renderChart(chartType, statistics);
        }
    }

    componentWillUnmount() {
        this.chart.removeAllListeners();
    }

    render() {
        return (
            <div className={styles.container} id="anychart-container" />
        );
    }

    private initChart() {
        this.chart = anychart.stock(true);
        anychart.licenseKey(ANY_CHART_LICENSE_KEY);
        this.chart
            .title(false)
            .container('anychart-container');
        this.chart.credits().enabled(false);

        this.plotLines = this.chart.plot(0).legend(false).height('70%').crosshair(false);
        this.plotLines.yGrid().enabled(true).stroke();

        this.plotColumns = this.chart.plot(1).legend(false).height('30%').crosshair(false);

        const scale = this.plotColumns.yScale() as anychart.scales.Linear;
        scale.ticks().count(3);
        this.plotColumns.yGrid().enabled(true);

        this.plotColumns.yAxis().labels().format(function(this: any) {
            return this.value + '%';
        });

        this.rangeSelector = anychart.ui.rangeSelector();

        const ranges = [
            {type: 'ytd', text: 'YTD'},
            {type: 'unit', unit: 'Year', count: 1, text: '1YR', anchor: 'last-date'},
            {type: 'unit', unit: 'Year', count: 3, text: '3YR', anchor: 'last-date'},
            {type: 'unit', unit: 'Year', count: 5, text: '5YR', anchor: 'last-date'}
        ] as anychart.ui.RangeSelector.Range[];

        // apply the changes
        this.rangeSelector
            .zoomLabelText('')
            .ranges(ranges);

        this.rangeSelector.render(this.chart);

        this.chart.listen('selectedrangechangefinish', (e: any) => {
            this.props.changeChartInterval(this.getRangeStart(e), this.getRangeEnd(e));
            if (this.props.chartType === ChartType.Growth) {
                this.renderChart(this.props.chartType, this.props.statistics);
            }
        });

        this.chart.listen('selectedrangechange', (e: any) => {
            if (e.source === 'select-range') {
                this.props.changeChartInterval(this.getRangeStart(e), this.getRangeEnd(e));
                if (this.props.chartType === ChartType.Growth) {
                    this.renderChart(this.props.chartType, this.props.statistics);
                }
            }
        });
    }

    private initSelectedRange() {
        this.chart.selectRange('year', 1);
        this.rangeSelected = true;

        const selectedRange = this.chart.getSelectedRange();

        this.props.changeChartInterval(this.getRangeStart(selectedRange), this.getRangeEnd(selectedRange));
    }

    private renderChart(chartType: ChartType, statistics: FundStatistics[]) {
        if (statistics.length === 0) {
            return;
        }

        const that = this;

        const fundData = this.getFundData(chartType, statistics),
            scrollerData: anychart.data.TableMapping = this.getScrollerData(chartType, statistics);

        this.plotLines.removeAllSeries();
        this.plotColumns.removeAllSeries();

        this.plotLines.legend().listen('legendItemClick', function(event: {itemIndex: number}) {
            // get item's index
            const index = event.itemIndex;

            that.plotColumns.getSeries(index).enabled(! that.plotLines.getSeries(index).enabled());
        });

        this.plotLines.yAxis().labels()
            .format(function(this: any) {
                if (chartType === ChartType.Growth) {
                    return this.value + '%';
                } else {
                    const formatType = +this.value > 1000000000
                        ? FormatTypeChoices.NumeralThreeDecimalLower
                        : FormatTypeChoices.NumeralOneDecimalLower;
                    return NumericHelper.getFormattedNumeral(
                        '' + this.value,
                        that.props.currencyName,
                        true,
                        formatType,
                        '0'
                    );
                }
            });

        Object.keys(fundData).forEach((id: Id, idx: number) => {
            const data = fundData[id];

            let lineSeries = this.plotLines.line(data.lineData);
            lineSeries.name(data.title).stroke(`2 ${this.props.getColor(idx)}`);

            let columnSeries = this.plotColumns.column(data.columnData);
            columnSeries.name(data.title).fill(this.props.getColor(idx));

            columnSeries.pointWidth('90%');
            columnSeries.tooltip().enabled(false);

            lineSeries.tooltip().format(function(this: any) {
                if (isNaN(this.index)) {
                    return `${this.seriesName}`;
                }
                const lineVal = that.getLineTooltipLabel(
                    chartType, data, this.value,
                    data.datasets[this.index].currency
                );

                const perfMonth = data.datasets[this.index].perf_month;
                const columnValue = formatText(
                    perfMonth, FormatTypeChoices.Percent, DEFAULT_VALUE_DASH
                );

                const seriesName = id === that.props.fundId ? 'This Fund' : this.seriesName;
                return `${seriesName} ${lineVal}, ${columnValue}`;
            });

            columnSeries.rendering().point(this.drawPoint(idx, statistics.length));
        });

        if (scrollerData) {
            this.chart.scroller().removeAllSeries();
            this.chart.scroller().line(scrollerData);
        }

        // initiate drawing the chart
        this.chart.draw();

        if (!this.rangeSelected) {
            this.initSelectedRange();
        }

        this.renderedStatistics = statistics;
        this.renderedType = chartType;
    }

    private getFundData(chartType: ChartType, statistics: FundStatistics[]) {
        const fundData = {};

        statistics.forEach(stat => {
            let data: Object[][] = [];
            switch (chartType) {
                case ChartType.Growth:
                    const selectedRange = this.chart.getSelectedRange();
                    const startDate = moment.unix(this.getRangeStart(selectedRange));

                    const firstIndex = stat.datasets.history_datasets
                        .findIndex(dataset => this.isDatasetOnCumPerfChart(dataset, startDate));

                    const cumulativeData = FundStatisticCalculator.getCumulativePeriodData(
                        stat.datasets.history_datasets
                            .filter(dataset => this.isDatasetOnCumPerfChart(dataset, startDate))
                            .map(dataset => +dataset.perf_month / 100 + 1)
                    );

                    const datasets = stat.datasets.history_datasets
                        .map((dataset, idx) => {
                            return {
                                ...dataset,
                                cum_perf: this.isDatasetOnCumPerfChart(dataset, startDate)
                                    ? cumulativeData[idx - firstIndex] * 100
                                    : 100
                            };
                        });

                    data = datasets.map((dataset: FundStatisticsDataset & {cum_perf: number}) =>
                        [dataset.period, dataset.cum_perf, dataset.perf_month]
                    );
                    break;
                default:
                    data = stat.datasets.history_datasets.map(dataset =>
                        [dataset.period, dataset.ending_balance, dataset.perf_month]
                    );
                    break;
            }
            const dataTable = anychart.data.table();
            dataTable.addData(data);

            fundData[stat.id] =  {
                id: stat.id,
                title: stat.name,
                currency: stat.currency,
                datasets: stat.datasets.history_datasets,
                dataTable: dataTable,
                lineData: dataTable.mapAs({x: 0, value: 1}),
                columnData: dataTable.mapAs({x: 0, value: 2}),
            };
        });

        return fundData;
    }

    private getScrollerData(chartType: ChartType, statistics: FundStatistics[]) {
        const datasets = statistics[0].datasets.history_datasets,
            dataTable = anychart.data.table();

        dataTable.addData(datasets.map(dataset => {
            return [
                dataset.period,
                chartType === ChartType.Aum ? dataset.ending_balance : dataset.perf_month,
                dataset.perf_month
            ];
        }));

        return dataTable.mapAs({x: 0, value: 1});
    }

    private drawPoint(pointIndex: number, pointsCount: number) {
        return function drawer(this: any) {
            if (this.missing) {
                return;
            }
            const shapes = this.getShapesGroup(this.pointState);

            const pointWidth = Math.min(this.pointWidth / pointsCount, MAX_COLUMN_WIDTH);

            // calculate the left value of the x-axis
            const leftX = this.x - this.pointWidth / 2 + pointWidth * pointIndex;

            // calculate the right value of the x-axis
            const rightX = leftX + pointWidth;

            shapes.path
                // draw column with rounded edges
                .moveTo(leftX, this.zero)
                .lineTo(leftX, this.value)
                .lineTo(rightX, this.value)
                .lineTo(rightX, this.zero)
                // close by connecting the last point with the first straight line
                .close();
        };
    }

    private isDatasetOnCumPerfChart(dataset: FundStatisticsDataset, startDate: Moment) {
        return startDate.diff(dataset.period) < 0;
    }

    private getLineTooltipLabel(chartType: ChartType, fundData: {currency: string}, value: string, currency: string) {
        return chartType === ChartType.Aum
            ? NumericHelper.getFormattedNumeral(
                value !== null ? '' + value : value,
                currency,
                true,
                FormatTypeChoices.NumeralDefault,
                DEFAULT_VALUE_DASH
            )
            : formatText(value, FormatTypeChoices.Percent, DEFAULT_VALUE_DASH);
    }

    private getRangeStart(range: {firstSelected: number}) {
        return range.firstSelected / 1000;
    }

    private getRangeEnd(range: {lastSelected: number}) {
        return range.lastSelected / 1000;
    }
}

export default Chart;
