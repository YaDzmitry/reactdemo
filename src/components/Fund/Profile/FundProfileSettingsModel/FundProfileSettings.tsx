import * as React from 'react';

import Checkbox from 'components/Shared/Ui/Checkbox';
import ButtonDefault from 'components/Shared/Ui/Buttons/ButtonDefault';
import ButtonPrimary from 'components/Shared/Ui/Buttons/ButtonPrimary';
import Modal from 'components/Shared/Modals/Modal';

import { CloseModalAction } from 'store/Modals/General/actions';
import { AccountSettings } from 'store/Account/models/account';
import { UpdateAccountSettingsAction, UpdateUserAccountSettingsAction } from 'store/Auth/actions';
import { isAdmin } from 'store/Auth/userHelper';
import ActiveUser from 'store/Auth/models/activeUser';
import { Id } from 'store/types';
import { UserAccountSettings } from 'store/Auth/models/userAccount';

import { tooltipIcon } from 'store/constants';

const styles = require('./FundProfileSettings.scss');

const tooltips = {
    enableShowDataSource: `This will set the default setting 
        for all fund profiles to Show Data Source at the account level. 
        Each user can edit this initial setting to hide or show data sources as desired.`,

    rememberSelectedItems: `This will save all edits to account-level default settings
        for fund comparisons (benchmarks, peer groups, other funds). 
        This option may be deselected to return all fund profiles to the default benchmark comparisons.`,
};

const rememberItemsWarningMessage = `Warning: Saving this setting will restore account-level benchmarks for
    all fund profiles. All existing comparisons will be deleted for all profiles!`;

export interface FundProfileProps {
    activeUser: ActiveUser;
    isOpen: boolean;
    closeModal: () => CloseModalAction;
    rememberSelectedFunds?: boolean;
    enableShowDataSource: boolean;
    updateUserAccountSettings:
        (settings: UserAccountSettings) => UpdateUserAccountSettingsAction;
    updateAccountSettings: (settings: AccountSettings) => UpdateAccountSettingsAction;
    fundId: Id;
}

interface State {
    rememberSelectedFunds?: boolean;
    showRememberItemsWarning?: boolean;
    enableShowDataSource?: boolean;

    prevRememberSelectedFunds?: boolean;
    prevEnableShowDataSource?: boolean;
}

export default class FundProfileSettings extends React.PureComponent<FundProfileProps, State> {
    static defaultState = {
        rememberSelectedFunds: undefined,
        showRememberItemsWarning: false,
        enableShowDataSource: undefined,
        prevRememberSelectedFunds: undefined,
        prevEnableShowDataSource: undefined,
    };

    state: State = FundProfileSettings.defaultState;

    static getDerivedStateFromProps(props: FundProfileProps, state: State) {
        let {
            rememberSelectedFunds, showRememberItemsWarning, enableShowDataSource,
            prevRememberSelectedFunds, prevEnableShowDataSource
        } = state;

        // do not set state properties until the response was returned from the backend
        if (props.rememberSelectedFunds !== state.prevRememberSelectedFunds) {
            rememberSelectedFunds = props.rememberSelectedFunds;
            prevRememberSelectedFunds = props.rememberSelectedFunds;
            showRememberItemsWarning = props.rememberSelectedFunds;
        }
        if (props.enableShowDataSource !== state.prevEnableShowDataSource) {
            enableShowDataSource = props.enableShowDataSource;
            prevEnableShowDataSource = props.enableShowDataSource;
        }

        return {
            rememberSelectedFunds,
            showRememberItemsWarning,
            enableShowDataSource,
            prevRememberSelectedFunds,
            prevEnableShowDataSource,
        };
    }

    handleUpdate = () => {
        this.props.updateUserAccountSettings({remember_selected_funds: this.state.rememberSelectedFunds});

        if (isAdmin(this.props.activeUser)) {
            this.props.updateAccountSettings({display_fund_data_source: this.state.enableShowDataSource});
        }

        this.props.closeModal();
    }

    handleCloseModal = () => {
        this.props.closeModal();
    }

    handleChartStatistics = () => {
        this.setState({rememberSelectedFunds: !this.state.rememberSelectedFunds});
    }

    handleAccountSettings = () => {
        this.setState({enableShowDataSource: !this.state.enableShowDataSource});
    }

    render () {
        const {isOpen} = this.props;

        return (
            <Modal
                isOpen={isOpen}
                toggle={this.handleCloseModal}
                title="Fund Profile Settings"
                body={this.body}
                buttons={this.buttons}
                className={styles['settings-modal-content']}
                modalClassName={styles['settings-modal']}
            />
        );
    }
    private get body() {
        return (
            <div className={styles['body-wrapper']}>
                <div className={styles.subtitle}>{`Chart & Statistics`}</div>

                <div>{this.rememberSelectedItems}</div>
                <div className={styles.description}>
                    <span><img src={tooltipIcon} alt="question" className={styles.question}/></span>
                    {tooltips.rememberSelectedItems}
                    {
                        this.state.showRememberItemsWarning && !this.state.rememberSelectedFunds &&
                        <div className={styles.warning}>
                            {rememberItemsWarningMessage}
                        </div>
                    }
                </div>

                {
                    isAdmin(this.props.activeUser) &&
                    (
                        <>
                            <div className={styles.subtitle}>{`Account Settings`}</div>

                            <div>{this.enableShowDataSource}</div>
                            <div className={styles.description}>
                                <span>{<img src={tooltipIcon} alt="question" className={styles.question}/>}</span>
                                {tooltips.enableShowDataSource}
                            </div>
                        </>
                    )
                }
            </div>
        );
    }

    private get rememberSelectedItems() {
        return (
            <Checkbox
                idx="rememberSelectedItems"
                className={styles.checkbox}
                checked={this.state.rememberSelectedFunds}
                handleChange={this.handleChartStatistics}
            >
                {`Remember my selected funds/benchmarks/peer groups`}
            </Checkbox>
        );
    }

    private get enableShowDataSource() {
        return (
            <Checkbox
                idx="enableShowDataSource"
                className={styles.checkbox}
                checked={this.state.enableShowDataSource}
                handleChange={this.handleAccountSettings}
            >
                {`Enable "Show Data Source" as default on all fund profiles`}
            </Checkbox>
        );
    }

    private get buttons() {
        return [
            (
                <ButtonPrimary className={styles['button-save']} handleClick={this.handleUpdate}>
                    Save
                </ButtonPrimary>
            ),
            (
                <ButtonDefault inverse={true} handleClick={this.handleCloseModal}>
                    Cancel
                </ButtonDefault>
            )
        ];
    }
}
