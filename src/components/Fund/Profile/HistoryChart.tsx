import * as React from 'react';
import * as classNames from 'classnames';
import * as numeral from 'numeral';
import { Line } from 'react-chartjs-2';
import { ChartDataSets } from 'chart.js';

import FundHelper from 'helpers/fundHelper';
import NumericHelper from 'helpers/NumericInputHelper';

import { FormatTypeChoices, HistoryPeriodChoices } from 'store/constants';
import { historyPeriods } from 'store/Fund/constants';

const styles = require('./HistoryChart.scss');
const MAX_Y_AXIS_VALUE = '900000000';
const MIN_Y_AXIS_VALUE = '0';

interface Props {
    datasets: any[];
    header: string;
    currencyName: string;
    setHistoryPeriod: (historyPeriod: HistoryPeriodChoices) => any;
    currentHistoryPeriod: HistoryPeriodChoices;
}

class HistoryChart extends React.Component<Props> {
    xlabels: string[] = [];
    dataset: ChartDataSets = {
        label: 'test',
        fill: false,
        borderColor: '#4DA1FF',
        pointBackgroundColor: '#FFFFFF',
        lineTension: 0
    };
    options: any = {
        tooltips: {
            borderColor: '#4DA1FF',
            borderWidth: 2,
            backgroundColor: '#FFFFFF',
            bodyFontColor: 'black',
            displayColors: false,
            xPadding: 20,
            callbacks: {
                beforeTitle: () => '',
                title: () => '',
                afterTitle: () => '',
                beforeLabel: () => '',
                label: (tooltipItem: any, data: any) => {
                    const index: number = tooltipItem.index as number;
                    let difference: string = '';
                    let value: number = numeral(data.datasets[0].data[index]).value();

                    if (tooltipItem.index) {
                        const prevValue = numeral(data.datasets[0].data[index - 1]).value();
                        if (prevValue !== null && numeral(prevValue).value() !== 0) {
                            const differenceValue = Math.abs(((value / prevValue) - 1) * 100);
                            difference = differenceValue.toFixed(2);
                            difference = prevValue < value ? `+${difference}` : `-${difference}`;
                        }
                    }

                    const formattedCurrency = NumericHelper.getFormattedNumeral(
                        '' + value,
                        this.props.currencyName,
                        true,
                        FormatTypeChoices.NumeralShortenDecimalLower,
                        '0'
                    );

                    return `${formattedCurrency} ${difference ? difference + '%' : '' }`;
                },
                labelColor: () => ({borderColor: '#FFFFFF', backgroundColor: '#FFFFFF'}),
                footer: () => ''
            }
        },
        spanGaps: true,
        legend: {
            display: false
        },
        responsive: true,
        maintainAspectRatio: false,
        scales: {
            xAxes: [{
                gridLines: {
                    display: false,
                    drawBorder: false,
                },
                ticks: {
                    fontColor: 'rgba(50, 60, 71, 0.50)',
                    fontSize: 10,
                    padding: 12,
                }
            }],
            yAxes: [{
                gridLines: {
                    drawBorder: false,
                },
                ticks: {
                    callback: (label: string): string => {
                        return NumericHelper.getFormattedNumeral(
                            label,
                            this.props.currencyName,
                            true,
                            FormatTypeChoices.NumeralDefault,
                            '0'
                        );
                    },
                    fontColor: 'rgba(50, 60, 71, 0.50)',
                    fontSize: 10,
                    padding: 12,
                },
            }]
        }
    };
    isEmpty: boolean = true;

    componentWillReceiveProps(props: Props) {
        const data: number[] = [];
        this.isEmpty = true;
        this.xlabels = [];

        let max = Number.MIN_SAFE_INTEGER;
        let min = Number.MAX_SAFE_INTEGER;

        if (Array.isArray(this.props.datasets)) {
            props.datasets.forEach(item => {
                if (item.data) {
                    this.isEmpty = false;
                }

                this.xlabels.push(item.label as string);

                if (item.data && (+item.data > max)) {
                    max = item.data;
                }

                if (item.data && (+item.data < min)) {
                    min = item.data;
                }

                data.push(item.data as number);
            });
        }

        if (max && min && !this.isEmpty) {
            // get boundary points to display on yAxes
            let maxValue = FundHelper.getBoundaryLabel(max, true);
            let minValue = FundHelper.getBoundaryLabel(min, false);
            // check the order of magnitude for boundary points
            const maxLength = ('' + Math.abs(+maxValue)).length;
            const minLength = ('' + Math.abs(+minValue)).length;
            // adjust the order of magnitude to be the same for boundary points
            if (maxLength > minLength && +minValue > 0) {
                minValue = '0';
            } else if (maxLength > minLength && +minValue < 0) {
                minValue = '' + (-1 * Math.pow(10, maxLength - 1));
            } else if (minLength > maxLength && +maxValue < 0) {
                maxValue = '0';
            } else if (minLength > maxLength && +maxValue > 0) {
                maxValue = '' + Math.pow(10, minLength - 1);
            }
            this.options.scales.yAxes[0].ticks.max = maxValue;
            this.options.scales.yAxes[0].ticks.min = minValue;
        }

        if (this.isEmpty) {
            this.options.scales.yAxes[0].ticks.min = MIN_Y_AXIS_VALUE;
            this.options.scales.yAxes[0].ticks.max = MAX_Y_AXIS_VALUE;
        }

        this.dataset.data = data;
    }

    render() {
        return (
            <div className={styles['chart-container']}>
                <div className="d-flex justify-content-between">
                    <div className={styles.header}>
                        <label>{this.props.header}</label>
                    </div>
                    <div>
                    {
                        historyPeriods.map((period, index) => {
                            const selectedClassName = classNames({
                                [styles['history-period']]: true,
                                [styles.selected]: this.props.currentHistoryPeriod === period.value,
                            });
                            return (
                                <span
                                    className={selectedClassName}
                                    key={index}
                                    onClick={(event) => this.props.setHistoryPeriod(period.value)}
                                >
                                    {period.label}
                                </span>
                            );
                        })
                    }
                    </div>
                </div>
                <div className={`${styles.chart} d-flex justify-content-between`}>
                    <Line
                        data={
                            {
                                labels: this.xlabels,
                                datasets: [this.dataset]
                            }
                        }
                        options={this.options}
                        height={270}
                    />
                    {this.isEmpty && <span className={styles['no-chart-data']}>No Data</span>}
                </div>
            </div>
        );
    }
}

export default HistoryChart;