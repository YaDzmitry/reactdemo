import * as React from 'react';
import * as classNames from 'classnames';

import { EvaluationResultStatus } from 'store/AlertsManager/Alerts/types';
import { EvaluationResultShortStatusTitles } from 'store/AlertsManager/Alerts/constants';
import {
    isAlert, isAwaitingData, isNoAlert, isNoRuleset, isPartialData, isPartialDataAlert
} from 'store/AlertsManager/Alerts/helpers';

interface Props {
    value: EvaluationResultStatus;
}

const styles = require('../AlertsOverview.scss');

const AlertStatusCell: React.FC<Props> = ({value}) => {
    const statusLabel = EvaluationResultShortStatusTitles.hasOwnProperty(value)
        ? EvaluationResultShortStatusTitles[value]
        : '';
    const statusClassName = classNames({
        [styles['table-cell']]: true,
        [styles.value]: true,
        [styles['status-column']]: true,
        [styles.alert]: isAlert(value),
        [styles.partialData]: isPartialData(value),
        [styles.partialDataAlert]: isPartialDataAlert(value),
        [styles.noAlert]: isNoAlert(value),
        [styles.noRuleset]: isNoRuleset(value),
        [styles.awaitingData]: isAwaitingData(value),
    });

    return (
        <div className={statusClassName}>{statusLabel}</div>
    );
};

export default AlertStatusCell;
