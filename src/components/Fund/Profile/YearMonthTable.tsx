import * as React from 'react';
import * as classNames from 'classnames';
import * as moment from 'moment';
import { NavLink } from 'react-router-dom';

import DataManagerHelper from 'helpers/dataManagerHelper';
import { formatText } from 'helpers/tableHelper';
import FundHelper from 'helpers/fundHelper';

import Popover from 'components/Shared/Ui/Popover';

import { FormatTypeChoices, DEFAULT_DATE_FORMAT, DEFAULT_VALUE_DASH } from 'store/constants';
import { MonthData, YearMonthData } from 'store/Fund/types' ;
import { isPipe, isFundManager, isRequestor } from 'store/Fund/helpers/dataEntry';
import { Account } from 'store/Account/models/account';
import { Id } from 'store/types';
import { isRequestorAccount } from 'store/User/helpers';
import DeviceHelper from 'helpers/deviceHelper';

const styles = require('./YearMonthTable.scss');

const months = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

const addDataPopover = 'Click here to provide data';
const editDataPopover = 'Click here to edit data';

interface Props {
    data: YearMonthData[];
    showCurrency: boolean;
    header: string;
    formatType: FormatTypeChoices;
    showDataEntry: boolean;
    fundId: Id;
    account: Account;
    onCellClick?: (period: string) => any;
}

class YearMonthTable extends React.PureComponent<Props> {
    static defaultProps = {
        showCurrency: false
    };
    private keySalt: number;

    constructor(props: Props) {
        super(props);
        this.keySalt = Math.floor(Math.random() * (1000000 - 1)) + 1;
    }

    render() {
        const {data, showCurrency, header, showDataEntry, formatType, fundId, account, onCellClick} = this.props;
        return (
            <div className={styles.wrapper}>
                <div className={styles['header-row']}>
                    <div className={styles.header}><label>{header}</label></div>
                </div>
                <div className={styles.table}>
                    <div className={styles['table-row']}>
                        {
                            months.map((month, idx) => {
                                const headerClassName = classNames({
                                    [styles['table-cell']]: true,
                                    [styles['top-header']]: true,
                                    [styles['left-header']]: idx === 0,
                                });

                                return (
                                    <div key={idx} className={headerClassName}>
                                        <span>{month}</span>
                                    </div>
                                );
                            })
                        }
                    </div>
                    {data.map((year: YearMonthData, yearIdx: number) => {
                        const yearColumnClass = classNames({
                            [styles['table-cell']]: true,
                            [styles['left-header']]: true,
                        });
                        return (
                            <div key={yearIdx} className={styles['table-row']}>
                                <div className={yearColumnClass}>{year.year}</div>
                                {year.months.map((item: MonthData, idx: number) => {
                                    const itemValue = formatText(item.value, formatType, DEFAULT_VALUE_DASH);

                                    const className = classNames({
                                        [styles['table-cell']]: true,
                                        [styles.value]: true,
                                        [styles['with-data-entry']]: showDataEntry && item.dataEntry !== null,
                                        [styles.requestor]: showDataEntry && isRequestor(item.dataEntry),
                                        [styles['pipe']]: showDataEntry && isPipe(item.dataEntry),
                                        [styles['fund-manager']]: showDataEntry && isFundManager(item.dataEntry),
                                    });

                                    const period = moment()
                                        .year(parseInt(year.year, 10))
                                        .month(idx)
                                        .endOf('month')
                                        .format(DEFAULT_DATE_FORMAT);

                                    const popoverBody = itemValue === DEFAULT_VALUE_DASH
                                        ? addDataPopover
                                        : editDataPopover;

                                    const onClickFunction = () => {
                                        if (onCellClick) {
                                            onCellClick(period);
                                        }
                                    };
                                    const currencyItem = FundHelper.getCurrencyItem(item.currency),
                                        currencyIcon = FundHelper.getCurrencyIcon(currencyItem);

                                    return (
                                        <div key={idx} className={className}>
                                            {
                                                !DataManagerHelper.isFuturePeriod(period)
                                                && isRequestorAccount(account) &&
                                                <Popover
                                                    id={`cell-${year.year}-${idx}-${this.keySalt}`}
                                                    body={popoverBody}
                                                    className={styles['cell-popover']}
                                                    enabled={!DeviceHelper.isTouchSupported()}
                                                >
                                                    <NavLink to={`/funds/${fundId}/datasets`}>
                                                        <div onClick={onClickFunction}>
                                                            {showCurrency && currencyIcon}
                                                            {itemValue}
                                                        </div>
                                                    </NavLink>
                                                </Popover>
                                            }
                                            {
                                                !(!DataManagerHelper.isFuturePeriod(period)
                                                    && isRequestorAccount(account)) &&
                                                <div>
                                                    {showCurrency && currencyIcon}
                                                    {itemValue}
                                                </div>
                                            }
                                        </div>
                                    );
                                })}
                            </div>
                        );
                    })}
                </div>
            </div>
        );
    }
}

export default YearMonthTable;
