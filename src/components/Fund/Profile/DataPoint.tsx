import * as React from 'react';
import * as classNames from 'classnames';

import FundHelper from 'helpers/fundHelper';
import { formatText } from 'helpers/tableHelper';
import NumericHelper from 'helpers/NumericInputHelper';

import {
    DEFAULT_CURRENCY_VALUE, downArrowRedIcon, upArrowGreenIcon,
    DataPointChoices, FormatTypeChoices
} from 'store/constants';

const styles = require('./DataPoint.scss');

interface Props {
    type: DataPointChoices;
    value: number | string | null;
    currencyName?: string;
}

class DataPoint extends React.Component<Props> {

    render() {
        const { value, currencyName } = this.props;
        let icon = '';
        let stringValue: string = '';
        const styleClasses = classNames({
            [styles['data-point']]: true,
            [styles['below-zero']]: this.props.type !== DataPointChoices.Balance
                && !!value && value < 0,
            [styles['above-zero']]: this.props.type !== DataPointChoices.Balance
                && (!!value && value > 0 || value === 0),
            [styles['empty-value']]: !value && value !== 0,
        });
        if (value || value === 0) {
            switch (this.props.type) {
                case DataPointChoices.PerformanceMonth:
                    icon = value < 0 ? downArrowRedIcon : upArrowGreenIcon;
                    stringValue = formatText(value, FormatTypeChoices.Percent);
                    break;
                case DataPointChoices.PerformanceYTD:
                    icon = value < 0 ? downArrowRedIcon : upArrowGreenIcon;
                    stringValue = formatText(value, FormatTypeChoices.Percent) + ' YTD';
                    break;
                case DataPointChoices.Balance:
                default:
                    stringValue = NumericHelper.getFormattedNumeral('' + value, currencyName, true);
                    break;
            }
        } else {
            const currencyIcon = FundHelper.getCurrencyIcon(
                FundHelper.getCurrencyItem(currencyName || DEFAULT_CURRENCY_VALUE)
            );
            stringValue = this.props.type === DataPointChoices.Balance ? `${currencyIcon}-` : '-%';
        }
        return (
            <div className={styles.wrapper}>
                <div>
                    <label className={styles.label}>{this.props.children}</label>
                </div>
                <div>
                    {icon && <img className={styles.icon} src={icon}/>}
                    <span className={styleClasses}>{stringValue}</span>
                </div>
            </div>
        );
    }
}

export default DataPoint;
