import * as React from 'react';
const styles = require('./ContactsPage.scss');

const ContactsPage: React.FC<{}> = ({children}) => (
    <div className={styles.content}>
        {children}
    </div>
);

export default ContactsPage;
