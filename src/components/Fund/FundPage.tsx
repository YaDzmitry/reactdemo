import * as React from 'react';
import { NavigationButton } from 'store/types';

interface Props {
    menuItems: NavigationButton[];
}

const FundPage: React.FC<Props> = ({children}) => (
    <div className="gray-wrapper">
        {children}
    </div>
);

export default FundPage;
