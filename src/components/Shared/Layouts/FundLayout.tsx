import * as React from 'react';
import { NavLink } from 'react-router-dom';

import { FundModel } from '../../../store/Fund/models/fund';
import { NavigationButton } from '../../../store/types';
import MainLayout from './MainLayout';

interface Props {
    menuItems: NavigationButton[];
    fund: FundModel;
    baseUrl?: string;
}

class FundLayout extends React.PureComponent<Props> {
    renderBreadCrumps = () => {
        const {fund} = this.props;

        if (this.props.baseUrl === '/funds') {
            return (
                <div className="ap-breadcrumbs">
                    <NavLink to="/funds">Funds</NavLink>
                    <span className="breadcrumb-slash">/</span>
                    <span>{fund.name}</span>
                </div>
            );
        }

        return (
            <div className="ap-breadcrumbs">
                <NavLink to="/data-manager">Data Manager</NavLink>
                <span className="breadcrumb-slash">/</span>
                <span>{fund.name}</span>
            </div>
        );
    }

    render() {
        const {fund} = this.props;
        const breadCrumbs = this.renderBreadCrumps();
        return (
            <MainLayout
                title={fund.name}
                menuItems={this.props.menuItems}
                breadCrumbs={breadCrumbs}
            >
                {this.props.children}
            </MainLayout>
        );
    }
}

export default FundLayout;