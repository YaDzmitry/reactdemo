import * as React from 'react';
import { Action } from 'redux';

import GlobalHelper from 'helpers/globalHelper';

import { CloseModalAction } from 'store/Modals/General/actions';
import { TableHeader } from 'store/types';
import { GridConfigurationType } from 'store/Fund/types';

import Checkbox from 'components/Shared/Ui/Checkbox';
import ButtonDefault from 'components/Shared/Ui/Buttons/ButtonDefault';
import ButtonPrimary from 'components/Shared/Ui/Buttons/ButtonPrimary';
import Modal from 'components/Shared/Modals/Modal';

interface Props {
    instanceId: string;
    headerList: HeaderSetting[];
    temporaryHeaders: TableHeader[];
    defaultVidibleHeaders: string[];
    setTemporaryHeaders:  (headers: TableHeader[]) => Action;
    isOpen: boolean;
    headers: TableHeader[];
    handleColumns: (columns: TableHeader[]) => void;
    closeModal: () => CloseModalAction;
    modalParams: any;
    gridType: GridConfigurationType;
    saveColumns?: (
        visibleHeaders: string[],
        instanceId: string,
        gridType: string,
    ) => Action;
}

interface HeaderSetting {
    label: string;
    values: string[];
}

const styles = require('./ConfigureColumnsModal.scss');

class ConfigureColumnsModal extends React.Component<Props> {
    handleUpdate = () => {
        const visibleFields: string[] = [];
        this.props.temporaryHeaders.forEach((item: TableHeader) => {
            if (item.active && item.columnName
                && this.props.defaultVidibleHeaders.indexOf(item.columnName) === -1
            ) {
                visibleFields.push(item.columnName);
            }
        });
        if (this.props.saveColumns) {
            const instanceId = this.props.instanceId;
            this.props.saveColumns(visibleFields, instanceId, this.props.gridType);
        }
        this.props.handleColumns(this.props.temporaryHeaders);
        this.props.closeModal();
    }

    handleCloseModal = () => {
        this.props.handleColumns(this.props.headers);
        this.props.setTemporaryHeaders(this.props.headers);
        this.props.closeModal();
    }

    handleColumns = (column: Partial<TableHeader>) => {
        const activeColumns = {};

        const newColumns = this.props.temporaryHeaders.map((item: any) => {
            let answer = item;
            if (item.name === column.name) {
                answer = {
                    ...item,
                    ...column
                };
            }

            if (answer.active) {
                activeColumns[item.name] = true;
            }

            return answer;
        });

        if (column.active) {
            this.props.setTemporaryHeaders(newColumns);
        }

        // Should be at least one checked checkbox
        this.props.headerList.map((headerSetting: HeaderSetting, index: number) => {
            this.props.temporaryHeaders.filter((header: TableHeader) =>
                header.columnName && headerSetting.values.indexOf(header.columnName) !== -1
            ).map((header: TableHeader, idx: number) => {
                if (header.active && header.name !== column.name) {
                    this.props.setTemporaryHeaders(newColumns);
                }
            });
        });
    }

    render () {
        const {isOpen, headerList} = this.props;

        const body = (
            <div className={styles['show-hide-modal-wrapper']}>
                {
                    headerList.map((headerSetting: HeaderSetting, index: number) => {
                        // calculate the height of the section
                        // count of items divide into 2 (number of columns to display list of items) and round up
                        // 25 - is the height of the one item (checkbox with label)
                        const height = Math.ceil(headerSetting.values.length / 2) * 25;
                        return (
                            <div key={index}>
                                <div className={styles['section-title']}>{headerSetting.label}</div>
                                <div className={styles['section-choices']} style={{height: `${height}px`}}>
                                    {
                                        this.props.temporaryHeaders
                                            .filter((header: TableHeader) => {
                                                return header.columnName &&
                                                    headerSetting.values.indexOf(header.columnName) !== -1;
                                            }).map((header: TableHeader, idx: number) => {
                                                const id = `${header.columnName}-${idx}`;

                                                let title = header.title;
                                                if (header.extraTitle ||
                                                    (header.title && typeof header.title === 'string')
                                                ) {
                                                    title = GlobalHelper.replaceElemInString(
                                                        header.extraTitle || header.title as string
                                                    );
                                                }

                                                return (
                                                    <div key={id} className={styles['choice-wrapper']}>
                                                        <Checkbox
                                                            idx={id}
                                                            className="aligned-checkboxes"
                                                            checked={header.active}
                                                            handleChange={() => this.handleColumns({
                                                                active: !header.active,
                                                                name: header.name
                                                            })}
                                                        >
                                                            {title}
                                                        </Checkbox>
                                                    </div>
                                                );
                                        })
                                    }
                                </div>
                            </div>
                        );
                    })
                }
            </div>
        );

        return (
            <Modal
                isOpen={isOpen}
                toggle={this.handleCloseModal}
                title="Show/hide Columns"
                body={body}
                buttons={this.buttons}
            />
        );
    }

    private get buttons() {
        return [
            (
                <ButtonPrimary buttonLarge={true} handleClick={this.handleUpdate}>
                    Update
                </ButtonPrimary>
            ),
            (
                <ButtonDefault buttonLarge={true} inverse={true} handleClick={this.handleCloseModal}>
                    Cancel
                </ButtonDefault>
            )
        ];
    }
}

export default ConfigureColumnsModal;
