import * as React from 'react';

import Modal from 'components/Shared/Modals/Modal';
import TabPanel, { TabItem } from 'components/Shared/Tabs/TabPanel';
import { CloseModalAction } from 'store/Modals/General/actions';

const styles = require('./ModalWithTabs.scss');

interface ProfileStatisticsProps {
    isOpen: boolean;
    closeModal: () => CloseModalAction;
    title: string;
    tabItems: TabItem[];
}

class ModalWithTabs extends React.PureComponent<ProfileStatisticsProps> {

    buttons = [];
    handleClose = () => {
        this.props.closeModal();
    }

    render() {
        const {isOpen, tabItems} = this.props;
        const title = <>{this.props.title}</>;
        const body = (
            <div><TabPanel items={tabItems}/></div>
        );

        return (
            <div className="modal-md">
                <Modal
                    isOpen={isOpen}
                    toggle={this.handleClose}
                    title={title}
                    body={body}
                    size="md"
                    buttons={this.buttons}
                    className={styles.modalContent}
                    modalClassName={styles.modalClass}
                />
            </div>
        );
    }
}

export default ModalWithTabs;