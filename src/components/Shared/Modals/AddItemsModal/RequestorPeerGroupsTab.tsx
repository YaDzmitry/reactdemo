import * as React from 'react';
import { Action } from 'redux';

import GlobalHelper from 'helpers/globalHelper';
import { formatText } from 'helpers/tableHelper';
import { FilterDecoratorChange } from 'decorators/FilterDecorator';

import UiDataTable from 'components/Shared/Ui/Tables/UiDataTable';
import DataTable from 'components/Shared/DataTable/DataTable';
import ButtonPrimary from 'components/Shared/Ui/Buttons/ButtonPrimary';
import ButtonDefault from 'components/Shared/Ui/Buttons/ButtonDefault';
import DynamicFormatCurrency from 'components/DataManager/TableCells/DynamicFormatCurrency';

import { CloseModalAction } from 'store/Modals/General/actions';
import { Chip, FiltersState } from 'store/Filters/types';
import { PeerGroup } from 'store/Fund/types';
import { FilterItem, Id, TableHeader } from 'store/types';
import {
    DEFAULT_VALUE_DASH,
    Alignment, CellTypeChoices, FilterType, FixedElementType, FormatTypeChoices, ScrollableElementType
} from 'store/constants';

const styles = require('components/Shared/Modals/ModalWithTabs.scss');

interface RequestorPeerGroupsTabProps {
    results: PeerGroup[];
    count: number;
    handleChange: FilterDecoratorChange;
    filterPageName: string;
    currentFilter: FiltersState;
    chips: Chip[];
    isLoading: boolean;
    isGridLoading: boolean;
    isAdding: boolean;
    checkedAll: boolean;
    checkedIds: Id[];
    chooseAll: () => void;
    chooseRow: (id: Id) => void;
    checkedCount: number;
    closeModal: () => CloseModalAction;

    addPeerGroups: () => Action;
}

class PeerGroupsTabContent extends React.PureComponent<RequestorPeerGroupsTabProps> {

    handleClose = () => {
        this.props.closeModal();
    }

    handleAddGroups = () => {
        this.props.addPeerGroups();
    }

    render() {
        const {
            chips, handleChange, filterPageName, currentFilter, isLoading, isGridLoading, results, count,
            checkedAll, checkedIds, chooseAll, chooseRow,
        } = this.props;

        const filters: FilterItem[] = this.filters;
        const headers: TableHeader[] = this.headers;
        return (
            <>
                <UiDataTable>
                    <DataTable
                        filters={filters}
                        rows={results}
                        count={count}
                        headers={headers}
                        handleChange={handleChange}
                        hasPagination={true}
                        hasSorting={true}
                        currentFilter={currentFilter}
                        withCheckboxes={true}
                        checkedAll={checkedAll}
                        checkedIds={checkedIds}
                        chooseAll={chooseAll}
                        chooseRow={chooseRow}
                        highlightHeaderOnCheck={false}
                        chips={chips}
                        filterPageName={filterPageName}
                        isLoading={isLoading}
                        isGridLoading={isGridLoading}
                        scrollableElementClassName={`scrollable-table-body`}
                        possibleFixedElementClassName={`fixed-table-header`}
                        fixedElementType={FixedElementType.TableHeader}
                        scrollableElementType={ScrollableElementType.TableBody}
                    />
                </UiDataTable>
                <div className={styles.buttons}>
                    {this.buttons.map((button, idx) => (
                        <div key={idx} className="modal-buttons">
                            {button}
                        </div>
                    ))}
                </div>
            </>
        );
    }

    private get buttons() {
        const {checkedCount} = this.props;

        let buttons = [(
            <ButtonPrimary
                handleClick={this.handleAddGroups}
                className="float-left"
                buttonLarge={true}
                disabled={checkedCount === 0 || this.props.isAdding}
            >
                {this.props.isAdding && 'Adding...'}
                {
                    !this.props.isAdding &&
                    `Add ${checkedCount || ''} Peer Group${checkedCount === 1 ? '' : 's'}`
                }
            </ButtonPrimary>
        )];

        if (!this.props.isAdding) {
            buttons.push((
                <ButtonDefault className="float-left" inverse={true} buttonLarge={true} handleClick={this.handleClose}>
                    Cancel
                </ButtonDefault>
            ));
        }

        return buttons;
    }

    private get filters(): FilterItem[] {
        return [
            {
                name: 'search',
                type: FilterType.Search,
                title: `Search`,
                className: 'col-lg-12',
            },
        ];
    }

    private get headers(): TableHeader[] {
        return [
            {
                name: 'name',
                title: 'Name',
                headerAlignment: Alignment.Left,
                active: true,
            },
            {
                name: 'nav',
                title: 'NAV - EB',
                active: true,
                cellType: CellTypeChoices.Custom,
                excludeSorting: true,
                transformer: row => (
                    <DynamicFormatCurrency
                        row={row}
                        valueField={`ending_balance`}
                        currencyField={`currency`}
                        defaultValue={DEFAULT_VALUE_DASH}
                    />
                )
            },
            {
                name: 'perf',
                title: 'Perf - Monthly',
                active: true,
                cellType: CellTypeChoices.Custom,
                excludeSorting: true,
                transformer: row => (
                    <span>
                    {
                        formatText(
                            GlobalHelper.getValueFromObject(row, 'perf_monthly'),
                            FormatTypeChoices.Percent,
                            DEFAULT_VALUE_DASH,
                        )
                    }
                    </span>
                )
            },
            {
                name: 'fund_count',
                title: '# Funds',
                active: true,
                excludeSorting: true,
                headerAlignment: Alignment.Left,
            },
            {
                name: 'updated_at',
                title: 'Last Updated',
                active: true,
                formatType: FormatTypeChoices.FromNowIfLessThanAMonth,
                excludeSorting: true,
                headerAlignment: Alignment.Left,
            },
        ];
    }
}

export default PeerGroupsTabContent;
