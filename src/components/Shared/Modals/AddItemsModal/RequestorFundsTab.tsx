import * as React from 'react';
import { Action } from 'redux';

import { FilterDecoratorChange } from 'decorators/FilterDecorator';

import UiDataTable from 'components/Shared/Ui/Tables/UiDataTable';
import DataTable from 'components/Shared/DataTable/DataTable';
import AttributesCell from 'components/Fund/TableCells/AttributesCell';
import FirmFundNamesCell from 'components/Shared/DataTable/Cells/FirmFundNames';
import ButtonPrimary from 'components/Shared/Ui/Buttons/ButtonPrimary';
import ButtonDefault from 'components/Shared/Ui/Buttons/ButtonDefault';

import { AsyncFilterName, Chip, FilterInfo, FiltersState } from 'store/Filters/types';
import { FilterItem, Id, TableHeader } from 'store/types';
import {
    DEFAULT_VALUE_DASH,
    Alignment, CellTypeChoices, FilterType, FixedElementType, FormatTypeChoices, ScrollableElementType
} from 'store/constants';
import { FUND_STATUS_CHOICES } from 'store/Fund/constants';
import { GetChoicesAction } from 'store/AsyncFiltersChoices/actions';
import { CloseModalAction } from 'store/Modals/General/actions';
import { FundModel } from 'store/Fund/models/fund';

const styles = require('components/Shared/Modals/ModalWithTabs.scss');

interface RequestorFundsTabProps {
    results: FundModel[];
    count: number;
    handleChange: FilterDecoratorChange;
    filterPageName: string;
    currentFilter: FiltersState;
    chips: Chip[];
    isLoading: boolean;
    isGridLoading: boolean;
    isAdding: boolean;
    checkedAll: boolean;
    checkedIds: Id[];
    chooseAll: () => void;
    chooseRow: (id: Id) => void;
    setAsyncFilters: (filterName: string, filter: FiltersState, pageName: string) => void;
    setFilters: (filterName: string, filter: FiltersState) => void;
    getChoices: (page: string, filterName: string, filter: FiltersState) => GetChoicesAction;
    checkedCount: number;
    addFunds: () => Action;
    closeModal: () => CloseModalAction;
    checkedFundIds: Id[];

    firmFilter: FilterInfo;
    portfolioFilter: FilterInfo;
    creditOfficerFilter: FilterInfo;
    productFilter: FilterInfo;
    strategyFilter: FilterInfo;
    internalIdFilter: FilterInfo;
}

class FundsTabContent extends React.PureComponent<RequestorFundsTabProps> {

    handleClose = () => {
        this.props.closeModal();
    }

    handleAddFunds = () => {
        this.props.addFunds();
    }

    render() {
        const {
            chips, handleChange, filterPageName, currentFilter, isLoading, isGridLoading, results, count,
            checkedAll, checkedIds, chooseAll, chooseRow,
        } = this.props;

        const filters: FilterItem[] = this.filters;
        const headers: TableHeader[] = this.headers;
        return (
            <>
                <UiDataTable>
                    <DataTable
                        filters={filters}
                        rows={results}
                        count={count}
                        headers={headers}
                        handleChange={handleChange}
                        hasPagination={true}
                        hasSorting={true}
                        currentFilter={currentFilter}
                        withCheckboxes={true}
                        checkedAll={checkedAll}
                        checkedIds={checkedIds}
                        chooseAll={chooseAll}
                        chooseRow={chooseRow}
                        highlightHeaderOnCheck={false}
                        chips={chips}
                        filterPageName={filterPageName}
                        isLoading={isLoading}
                        isGridLoading={isGridLoading}
                        scrollableElementClassName={`scrollable-table-body`}
                        possibleFixedElementClassName={`fixed-table-header`}
                        fixedElementType={FixedElementType.TableHeader}
                        scrollableElementType={ScrollableElementType.TableBody}
                    />
                </UiDataTable>
                <div className={styles.buttons}>
                    {this.buttons.map((button, idx) => (
                        <div key={idx} className="modal-buttons">
                            {button}
                        </div>
                    ))}
                </div>
            </>
        );
    }

    private get buttons() {
        const {checkedCount} = this.props;
        let buttons = [(
            <ButtonPrimary
                handleClick={this.handleAddFunds}
                className="float-left"
                buttonLarge={true}
                disabled={checkedCount === 0 || this.props.isAdding}
            >
                {this.props.isAdding && 'Adding...'}
                {
                    !this.props.isAdding &&
                    `Add ${checkedCount || ''} Fund${checkedCount === 1 ? '' : 's'}`
                }
            </ButtonPrimary>
        )];

        if (!this.props.isAdding) {
            buttons.push((
                <ButtonDefault className="float-left" inverse={true} buttonLarge={true} handleClick={this.handleClose}>
                    Cancel
                </ButtonDefault>
            ));
        }

        return buttons;
    }

    private get filters(): FilterItem[] {
        const {
            filterPageName, firmFilter, creditOfficerFilter, productFilter, strategyFilter,
            portfolioFilter, internalIdFilter, getChoices, setAsyncFilters
        } = this.props;

        return [
            {
                name: 'search',
                type: FilterType.Search,
                title: `Search`,
                className: 'col-lg-12',
            },
            {
                name: 'firm_id',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Firms',
                choices: firmFilter.choices,
                choicesCount: firmFilter.count,
                handleRequest: (filter: FiltersState) => {
                    getChoices(filterPageName, AsyncFilterName.firm, filter);
                },
                currentFilter: firmFilter.filter,
                setFilters: setAsyncFilters,
                filterName: AsyncFilterName.firm,
                idPrefix: 'modal',
                isFilterValueObject: true,
            },
            {
                name: 'delivery_status',
                type: FilterType.CreditSelectWithCheckboxesSync,
                title: 'Status',
                choices: FUND_STATUS_CHOICES,
                choicesCount: FUND_STATUS_CHOICES.length,
                currentFilter: this.props.currentFilter,
                idPrefix: 'modal',
            },
            {
                name: 'credit_officer',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Coverage',
                choices: creditOfficerFilter.choices,
                choicesCount: creditOfficerFilter.count,
                handleRequest: (filter: FiltersState) => {
                    getChoices(filterPageName, AsyncFilterName.creditOfficer, filter);
                },
                currentFilter: creditOfficerFilter.filter,
                setFilters: setAsyncFilters,
                filterName: AsyncFilterName.creditOfficer,
                idPrefix: 'modal',
            },
            {
                name: 'product',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Product',
                choices: productFilter.choices,
                choicesCount: productFilter.count,
                handleRequest: (filter: FiltersState) => {
                    getChoices(filterPageName, AsyncFilterName.product, filter);
                },
                currentFilter: productFilter.filter,
                setFilters: setAsyncFilters,
                filterName: AsyncFilterName.product,
                idPrefix: 'modal',
            },
            {
                name: 'strategy',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Strategy',
                choices: strategyFilter.choices,
                choicesCount: strategyFilter.count,
                handleRequest: (filter: FiltersState) => {
                    getChoices(filterPageName, AsyncFilterName.strategy, filter);
                },
                currentFilter: strategyFilter.filter,
                setFilters: setAsyncFilters,
                filterName: AsyncFilterName.strategy,
                idPrefix: 'modal',
            },
            {
                name: 'portfolio_id',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Portfolio',
                choices: portfolioFilter.choices,
                choicesCount: portfolioFilter.count,
                handleRequest: (filter: FiltersState) => {
                    getChoices(filterPageName, AsyncFilterName.portfolio, filter);
                },
                currentFilter: portfolioFilter.filter,
                setFilters: setAsyncFilters,
                filterName: AsyncFilterName.portfolio,
                isFilterValueObject: true,
                idPrefix: 'modal',
            },
            {
                name: 'internal_id',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Internal Id',
                choices: internalIdFilter.choices,
                choicesCount: internalIdFilter.count,
                handleRequest: (filter: FiltersState) => {
                    getChoices(filterPageName, AsyncFilterName.internalId, filter);
                },
                currentFilter: internalIdFilter.filter,
                setFilters: setAsyncFilters,
                filterName: AsyncFilterName.internalId,
                idPrefix: 'modal',
            },
        ];
    }

    private get headers(): TableHeader[] {
        return [
            {
                name: 'firm_fund',
                title: 'Firm\nFund',
                headerAlignment: Alignment.Left,
                alias: 'wide-firm-fund',
                active: true,
                cellType: CellTypeChoices.Custom,
                orderingName: 'firm_name,name',
                transformer: row => (
                    <FirmFundNamesCell
                        firm={row.firm ? row.firm.name : ''}
                        fund={row.name}
                    />
                )
            },
            {
                name: 'fund_attributes.credit_officer',
                title: 'Coverage',
                active: true,
                excludeSorting: true,
                cellType: CellTypeChoices.Custom,
                transformer: row => <AttributesCell fields={row.fund_attributes} attributeType="credit_officer" />
            },
            {
                name: 'fund_attributes.strategy',
                title: 'Strategy',
                active: true,
                excludeSorting: true,
                cellType: CellTypeChoices.Custom,
                transformer: row => <AttributesCell fields={row.fund_attributes} attributeType="strategy" />
            },
            {
                name: 'fund_attributes.product',
                title: 'Product',
                active: true,
                excludeSorting: true,
                cellType: CellTypeChoices.Custom,
                transformer: row => <AttributesCell fields={row.fund_attributes} attributeType="product" />
            },
            {
                name: 'internal_id',
                title: 'Internal ID',
                active: true,
                excludeSorting: true,
                formatType: FormatTypeChoices.String,
                defaultValue: DEFAULT_VALUE_DASH,
            },
        ];
    }
}

export default FundsTabContent;
