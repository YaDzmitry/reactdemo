import * as React from 'react';
import { Action } from 'redux';

import { FilterDecoratorChange } from 'decorators/FilterDecorator';

import UiDataTable from 'components/Shared/Ui/Tables/UiDataTable';
import DataTable from 'components/Shared/DataTable/DataTable';
import ButtonPrimary from 'components/Shared/Ui/Buttons/ButtonPrimary';
import ButtonDefault from 'components/Shared/Ui/Buttons/ButtonDefault';

import { FilterItem, Id, TableHeader } from 'store/types';
import { Alignment, CellTypeChoices, FilterType, FixedElementType, ScrollableElementType } from 'store/constants';
import { CloseModalAction } from 'store/Modals/General/actions';
import { Chip, FiltersState } from 'store/Filters/types';
import { FundModel } from 'store/Fund/models/fund';

const styles = require('components/Shared/Modals/ModalWithTabs.scss');

interface ResponderFundsTabProps {
    results: FundModel[];
    count: number;
    handleChange: FilterDecoratorChange;
    filterPageName: string;
    currentFilter: FiltersState;
    chips: Chip[];
    isLoading: boolean;
    isGridLoading: boolean;
    isAdding: boolean;
    checkedAll: boolean;
    checkedIds: Id[];
    chooseAll: () => void;
    chooseRow: (id: Id) => void;
    checkedCount: number;
    addFunds: () => Action;
    closeModal: () => CloseModalAction;
}

class FundsTabContent extends React.PureComponent<ResponderFundsTabProps> {

    handleClose = () => {
        this.props.closeModal();
    }

    handleAddFunds = () => {
        this.props.addFunds();
    }

    render() {
        const {
            chips, handleChange, filterPageName, currentFilter, isLoading, isGridLoading, results, count,
            checkedAll, checkedIds, chooseAll, chooseRow,
        } = this.props;

        const filters: FilterItem[] = this.filters;
        const headers: TableHeader[] = this.headers;
        return (
            <>
                <UiDataTable>
                    <DataTable
                        filters={filters}
                        rows={results}
                        count={count}
                        headers={headers}
                        handleChange={handleChange}
                        hasPagination={true}
                        hasSorting={true}
                        currentFilter={currentFilter}
                        withCheckboxes={true}
                        checkedAll={checkedAll}
                        checkedIds={checkedIds}
                        chooseAll={chooseAll}
                        chooseRow={chooseRow}
                        highlightHeaderOnCheck={false}
                        chips={chips}
                        filterPageName={filterPageName}
                        isLoading={isLoading}
                        isGridLoading={isGridLoading}
                        scrollableElementClassName={`scrollable-table-body`}
                        possibleFixedElementClassName={`fixed-table-header`}
                        fixedElementType={FixedElementType.TableHeader}
                        scrollableElementType={ScrollableElementType.TableBody}
                    />
                </UiDataTable>
                <div className={styles.buttons}>
                    {this.buttons.map((button, idx) => (
                        <div key={idx} className="modal-buttons">
                            {button}
                        </div>
                    ))}
                </div>
            </>
        );
    }

    private get buttons() {
        const { checkedCount} = this.props;

        let buttons = [(
            <ButtonPrimary
                handleClick={this.handleAddFunds}
                className="float-left"
                buttonLarge={true}
                disabled={checkedCount === 0 || this.props.isAdding}
            >
                {this.props.isAdding && 'Adding...'}
                {
                    !this.props.isAdding &&
                    `Add ${checkedCount || ''} Fund${checkedCount === 1 ? '' : 's'}`
                }
            </ButtonPrimary>
        )];

        if (!this.props.isAdding) {
            buttons.push((
                <ButtonDefault className="float-left" inverse={true} buttonLarge={true} handleClick={this.handleClose}>
                    Cancel
                </ButtonDefault>
            ));
        }

        return buttons;
    }

    private get filters(): FilterItem[] {
        return [
            {
                name: 'search',
                type: FilterType.Search,
                title: `Search`,
                className: 'col-lg-12',
            },
        ];
    }

    private get headers(): TableHeader[] {
        return [
            {
                name: 'firm.name',
                title: 'Firm',
                headerAlignment: Alignment.Left,
                active: true,
                cellType: CellTypeChoices.Text,
            },
            {
                name: 'name',
                title: 'Fund',
                headerAlignment: Alignment.Left,
                active: true,
                cellType: CellTypeChoices.Text,
            },
        ];
    }
}

export default FundsTabContent;