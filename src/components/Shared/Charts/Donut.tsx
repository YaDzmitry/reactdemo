import * as React from 'react';
import 'anychart';

import { DonutPiece } from 'store/Shared/types';
import { StatisticDonutDataType } from 'store/Dashboard/types';
import { ANY_CHART_LICENSE_KEY } from 'store/constants';

const styles = require('./Donut.scss');

interface AnychartEvent {
    pointIndex?: number;
    itemIndex?: number;
}

interface LegendConfig {
    show: boolean;
}

interface Props {
    className?: string;
    pieces: DonutPiece[];
    total: number;
    totalTitle: string;
    totalSubtitle: string;
    centerTotal: number;
    legendConfig?: Partial<LegendConfig>;
    donutType: StatisticDonutDataType;

    onClick?: (piece: DonutPiece) => void;
}

class StatisticDonut extends React.PureComponent<Props> {
    private renderedPieces: DonutPiece[] | null = null;
    private rand = Math.round(Math.random() * 1000000000);
    private renderChartCenterTimeout: number = 0;

    private chart: anychart.charts.Pie;

    componentDidMount() {
        this.initChart();
        this.renderChart(this.props.pieces);
    }

    componentDidUpdate() {
        const {pieces} = this.props;
        if (this.renderedPieces !== pieces) {
            this.renderChart(pieces);
        }
    }

    componentWillUnmount() {
        if (this.chart) {
            this.chart.legend().removeAllListeners();
            this.chart.removeAllListeners();
        }
    }

    render() {
        const {className} = this.props;
        return (
            <div className={className} id={this.containerId} />
        );
    }

    private get containerId() {
        return `anychart-container-${this.props.donutType}-${this.rand}`;
    }

    private get totalPiece(): DonutPiece {
        return {
            name: 'total',
            color: '#fff',
            value: this.props.centerTotal,
            title: this.props.totalTitle,
            subtitle: this.props.totalSubtitle
        };
    }

    private initChart() {
        this.chart = anychart.pie();
        anychart.licenseKey(ANY_CHART_LICENSE_KEY);
        this.chart.credits().enabled(false);
        this.chart
            .title(false)
            .labels(false)
            .container(this.containerId);

        this.chart
            .interactivity()
            .selectionMode('none');

        const legend = this.chart.legend();
        legend
            .position('right')
            .itemsLayout('vertical');

        this.chart.listen('click', (event: AnychartEvent) => {
            this.handleClick(event.pointIndex || event.itemIndex);
        });

        const that = this;
        this.chart.legend().listen('legendItemMouseOver', (event: AnychartEvent) => {
            if (event.itemIndex) {
                const point = that.chart.getPoint(event.itemIndex);
                point.hovered(true);
            }
        });

        this.chart.legend().listen('legendItemMouseOut', (event: AnychartEvent) => {
            if (event.itemIndex) {
                const point = that.chart.getPoint(event.itemIndex);
                point.hovered(false);
            }
        });

        this.chart.listen('mouseOver', (event: AnychartEvent) => {
            const index = event.pointIndex || event.itemIndex;

            if (typeof index !== 'undefined') {
                document.body.style.cursor = 'pointer';
            }

            that.renderChartCenterNeatly(index);
        });

        this.chart.listen('mouseOut', () => {
            document.body.style.cursor = 'auto';
        });
    }

    private renderChart(pieces: DonutPiece[]) {
        this.renderedPieces = pieces;

        if (pieces.length === 0) {
            return;
        }

        const legend = this.chart.legend();
        legend
            .useHtml(true)
            .itemsFormatter(() => {
                return pieces.map(piece => {
                    return {
                        text: this.getLegendText(piece),
                        iconFill: piece.color,
                        iconType: 'circle',
                        iconSize: 11,
                        fontSize: 12,
                        fontFamily: 'Sailec',
                        textIndent: 16
                    };
                });
            });

        this.chart
            .data(pieces.map(piece => ({
                x: piece.title,
                value: piece.value,
                normal: {
                    fill: piece.color
                },
                hovered: {
                    cursor: 'pointer'
                }
            })));

        // set chart title text settings
        this.chart
            // set chart radius
            .radius('80%')
            // create empty area in pie chart
            .innerRadius('88%');

        // set container id for the chart
        this.chart.container(this.containerId);

        // initiate chart drawing
        this.chart.draw();
        this.renderChartCenter();
    }

    private renderChartCenterNeatly = (index?: number) => {
        if (typeof index === 'undefined') {
            if (!this.renderChartCenterTimeout) {
                this.renderChartCenterTimeout = window.setTimeout(
                    () => {
                        this.renderChartCenter();
                        this.renderChartCenterTimeout = 0;
                    },
                    200
                );
            }
        } else {
            if (this.renderChartCenterTimeout) {
                window.clearTimeout(this.renderChartCenterTimeout);
                this.renderChartCenterTimeout = 0;
            }
            this.renderChartCenter(index);
        }
    }

    private renderChartCenter = (index?: number) => {
        const piece = (typeof index !== 'undefined')
            ? this.props.pieces[index]
            : this.totalPiece;

        // create standalone label and set settings
        const label = anychart.standalones.label();
        label.enabled(true)
            .useHtml(true)
            .text(this.getChartCenterText(piece))
            .width('100%')
            .height('100%')
            .adjustFontSize(true, true)
            .minFontSize(10)
            .maxFontSize(25)
            .fontFamily('Sailec')
            .position('center')
            .anchor('center')
            .hAlign('center')
            .vAlign('middle');

        this.chart.center().content(label);
    }

    private getLegendText = (piece: DonutPiece) => {
        const perc = this.props.total > 0
            ? Math.round(piece.value / this.props.total * 100)
            : 0;
        return `<div style="color: #000; font-size: 18px;">${perc}%</div>&nbsp;&nbsp;${piece.title}`;
    }

    private getChartCenterText = (piece: DonutPiece) => {
        let text = `<span style="font-size: 35px; color: ${styles['alizarin-crimson']};">${piece.value}</span><br>` +
            `<span style="font-size: 14px;color: ${styles.shark}">${piece.title}</span>`;

        if (piece.subtitle) {
            text += `<br>` +
                `<span style="font-size: 10px; font-weight: 300; color: ${styles.shark}">` +
                    piece.subtitle +
                `</span>`;
        }

        return text;
    }

    private handleClick(index?: number) {
        if (!this.chart || !index) {
            return;
        }

        if (this.props.onClick) {
            this.props.onClick(this.props.pieces[index]);
        }
    }
}

export default StatisticDonut;
