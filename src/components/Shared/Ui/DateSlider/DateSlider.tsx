import * as React from 'react';
import * as moment from 'moment';
import * as _ from 'lodash';

import SingleDateFilter from 'components/Shared/Filters/SingleDateFilter';
import { FiltersState } from 'store/Filters/types';
import { DEFAULT_DATE_FORMAT } from 'store/constants';
import Slider from 'rc-slider';

const styles = require('./DateSlider.scss');

export enum DateSliderStep {
    Day = 'days',
    Month = 'months',
    Year = 'years'
}

interface Props {
    defaultDate?: string;
    fromDate: string;
    toDate: string;
    step: DateSliderStep;

    changeDate?: (date: string) => void;
}

interface State {
    currentDate: string;
}

interface Marks {
    [key: number]: string;
}

interface MarksInfo {
    fromDate: string;
    toDate: string;
    marks: Marks;
}

const SLIDER_MARK_MONTH_FORMAT = 'MMM YY';
const SLIDER_MARK_YEAR_FORMAT = 'YYYY';

class DateSlider extends React.PureComponent<Props, State> {
    static defaultProps = {
        step: DateSliderStep.Day
    };

    rand: number;
    marksInfo: MarksInfo;

    state = {
        currentDate: this.props.defaultDate || this.props.toDate
    };

    debounceOnChange = _.debounce(
        (date) => this.props.changeDate && this.props.changeDate(date),
        200
    );

    private lastSentDate: string;

    constructor(props: Props) {
        super(props);

        this.rand = Math.floor(Math.random() * 1000000);
    }

    render() {
        return (
            <div className={styles.container}>
                <div className={styles['date-picker']}>
                    <span className={styles['arrow-left']} onClick={this.subtractMonth} />
                    <SingleDateFilter
                        id={this.uniqueIdentify}
                        name={this.uniqueIdentify}
                        customInputIcon={true}
                        inputIconPosition="after"
                        value={this.state.currentDate}
                        handleChange={this.handleChangeDatePicker}
                        showClearDate={false}
                        isOutsideRange={this.isOutsideRange}
                    />
                    <span className={styles['arrow-right']} onClick={this.addMonth}/>
                </div>
                <div className={styles['slider-container']}>
                    <div className={styles['slider-wrapper']}>
                        <Slider
                            min={this.min}
                            max={this.max}
                            defaultValue={this.current}
                            value={this.current}
                            marks={this.marks}
                            onChange={this.handleChangeSlider}
                            onAfterChange={this.handleAfterChangeSlider}
                        />
                        {Object.keys(this.marks).map(key => {
                            let left = +key / (this.max - this.min) * 100;
                            if (left > 99) {
                                return null;
                            }

                            const markStyles = {
                                left: `${left}%`
                            };
                            return (
                                <span key={key} style={markStyles} className={styles['slider-mark']}>
                                    {this.marks[key]}
                                </span>
                            );
                        })}
                    </div>
                </div>
            </div>
        );
    }

    private get current() {
        const from = moment(this.props.fromDate).startOf(this.props.step),
            current = moment(this.state.currentDate);

        return current.diff(from, this.props.step);
    }

    private get min() {
        return 0;
    }

    private get max() {
        const from = moment(this.props.fromDate).startOf(this.props.step),
            to = moment(this.props.toDate).endOf(this.props.step).add(1, 'day');

        return to.diff(from, this.props.step) - 1;
    }

    private get marks(): Marks {
        const {fromDate, toDate, step} = this.props;

        if (!this.marksInfo || this.marksInfo.fromDate !== fromDate || this.marksInfo.toDate !== toDate) {
            const from = moment(this.props.fromDate),
                to = moment(this.props.toDate),
                marks = {},
                period = this.markPeriod,
                current = moment(this.props.fromDate).endOf(period.endOf).add(1, 'days');
            while (current < to) {
                marks[current.diff(from, step)] = (current.format(period.format));
                current.add(period.countSteps, period.step);
            }

            this.marksInfo = {fromDate, toDate, marks};
        }

        return this.marksInfo.marks;
    }

    private get markPeriod() {
        let periods;

        switch (this.props.step) {
            case DateSliderStep.Month:
                periods = [
                    {period: 1, countSteps: 1, step: 'months', format: SLIDER_MARK_MONTH_FORMAT, endOf: 'month'},
                    {period: 3, countSteps: 3, step: 'months', format: SLIDER_MARK_MONTH_FORMAT, endOf: 'month'},
                    {period: 12, countSteps: 1, step: 'years', format: SLIDER_MARK_YEAR_FORMAT, endOf: 'year'},
                    {period: 24, countSteps: 2, step: 'years', format: SLIDER_MARK_YEAR_FORMAT, endOf: 'year'},
                    {period: 48, countSteps: 4, step: 'years', format: SLIDER_MARK_YEAR_FORMAT, endOf: 'year'},
                ];
                break;
            case DateSliderStep.Year:
                periods = [
                    {period: 1, countSteps: 1, step: 'years', format: SLIDER_MARK_YEAR_FORMAT, endOf: 'year'},
                    {period: 2, countSteps: 2, step: 'years', format: SLIDER_MARK_YEAR_FORMAT, endOf: 'year'},
                    {period: 4, countSteps: 4, step: 'years', format: SLIDER_MARK_YEAR_FORMAT, endOf: 'year'},
                ];
                break;
            default:
                periods = [
                    {period: 30, countSteps: 1, step: 'months', format: SLIDER_MARK_MONTH_FORMAT, endOf: 'month'},
                    {period: 90, countSteps: 3, step: 'months', format: SLIDER_MARK_MONTH_FORMAT, endOf: 'month'},
                    {period: 365, countSteps: 1, step: 'years', format: SLIDER_MARK_YEAR_FORMAT, endOf: 'year'},
                    {period: 730, countSteps: 2, step: 'years', format: SLIDER_MARK_YEAR_FORMAT, endOf: 'year'},
                    {period: 1460, countSteps: 4, step: 'years', format: SLIDER_MARK_YEAR_FORMAT, endOf: 'year'},
                ];
                break;
        }
        const countSteps = this.max;

        let bestPeriod;
        for (let i = 0; i < periods.length; i++) {
            bestPeriod = periods[i];

            if (countSteps / periods[i].period <= 10) {
                break;
            }
        }

        return bestPeriod;
    }

    private get uniqueIdentify() {
        return `date-slider-${this.rand}`;
    }

    private handleChangeSlider = (value: number) => {
        const date = moment(this.props.fromDate).add(value, this.props.step).format(DEFAULT_DATE_FORMAT);
        this.changeDate(date, false);
    }

    private handleAfterChangeSlider = (value: number) => {
        const date = moment(this.props.fromDate).add(value, this.props.step).format(DEFAULT_DATE_FORMAT);
        this.changeDate(date);
    }

    private handleChangeDatePicker = (data: FiltersState) => {
        const date = data[this.uniqueIdentify];
        this.changeDate(date);
    }

    private isOutsideRange = (day: moment.Moment): boolean => {
        const from = moment(this.props.fromDate),
            to = moment(this.props.toDate);
        return day < from || day > to;
    }

    private subtractMonth = () => {
        const date = this.modifyDate(-1, 'months');
        this.changeDate(date);
    }

    private addMonth = () => {
        const date = this.modifyDate(1, 'months');
        this.changeDate(date);
    }

    private changeDate(date: string, sendEventUp: boolean = true) {
        date = moment(date).endOf(this.props.step).format(DEFAULT_DATE_FORMAT);

        if (date > this.props.toDate || date < this.props.fromDate) {
            return;
        }

        if (sendEventUp && (this.lastSentDate !== date) && this.props.changeDate) {
            this.debounceOnChange(date);
            this.lastSentDate = date;
        }
        this.setCurrentDate(date);
    }

    private setCurrentDate(date: string) {
        if (date < this.props.fromDate) {
            date = this.props.fromDate;
        }

        if (date > this.props.toDate) {
            date = this.props.toDate;
        }

        this.setState({
            currentDate: date
        });
    }

    private modifyDate(amount: number, unit: moment.DurationInputArg2) {
        const current = moment(this.state.currentDate);

        if (amount > 0) {
            current.add(1, unit);
        } else {
            current.subtract(1, unit);
        }

        return current.format(DEFAULT_DATE_FORMAT);
    }
}

export default DateSlider;
