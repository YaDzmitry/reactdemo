import * as React from 'react';
import { chevronRightIcon, chevronDownIcon, chevronUpIcon, rightArrow } from 'store/constants';
import { NavLink } from 'react-router-dom';

interface Props {
    link: string;
    icon?: string;
    handleClick?: any;
}

const styles = require('./ViewLink.scss');
const ViewLink: React.FC<Props> = ({ children, link, icon, handleClick }) => {
    let iconValue;
    switch (icon) {
        case 'down':
            iconValue = chevronDownIcon;
            break;
        case 'up':
            iconValue = chevronUpIcon;
            break;
        case 'rightArrow':
            iconValue = rightArrow;
            break;
        case 'right':
        default:
            iconValue = chevronRightIcon;
            break;
    }
    return (
        <div
            className={styles.container}
            onClick={handleClick}
        >
            <NavLink className={styles['view-link']} to={link}>
                <span>{children}</span>
                <img src={iconValue} className={styles.image}/>
            </NavLink>
        </div>
    );
};

export default ViewLink;
