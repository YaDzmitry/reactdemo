import * as React from 'react';
import { Async } from 'react-select';
import * as classNames from 'classnames';
import '!style-loader!css-loader!react-select/dist/react-select.css';
import { SEARCH_DEBOUNCE_INTERVAL } from 'store/constants';

interface Props {
    autoload?: boolean;
    placeholder: string;
    value: string;
    loadOptions: any;
    labelKey?: string;
    valueKey?: string;
    cache?: boolean;
    className?: any;
    handleChange: (currentValue: any) => void;
}

const styles = require('./Select.scss');
const debounce = (value: any, func: any) => {
    return new Promise((resolve) => {

        setTimeout(() => {
            resolve(func(value));
        },         SEARCH_DEBOUNCE_INTERVAL);

    });
};

const UiAsyncSelect: React.FC<Props> =
    ({ autoload, value, labelKey, valueKey, placeholder, loadOptions, cache, className, handleChange }) => {
    const ClassNames = classNames({
        'ui-input': true,
        [className]: className,
        [styles.ui_select]: !!styles.ui_select,
    });
    return (
        <Async
            className={ClassNames}
            autoload={autoload}
            placeholder={placeholder}
            valueKey={valueKey}
            labelKey={labelKey}
            loadOptions={(currentValue: any) => debounce(currentValue, loadOptions)}
            value={value}
            cache={cache}
            onChange={handleChange}
        />
    );
};
UiAsyncSelect.defaultProps = {
    autoload: false,
    valueKey: 'id',
    labelKey: 'legal_name',
    className: '',
    cache: false
};
export default UiAsyncSelect;
