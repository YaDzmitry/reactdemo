import * as React from 'react';
import * as classNames from 'classnames';

interface TextareaType {
    className?: any;
    value?: string;
    placeholder?: string;
    type?: string;
    material?: any;
    handleKeyPress?: (event: any) => void;
    handleChange: (value: string) => void;
    label?: string;
    id?: string;
}

const styles = require('./Input.scss');
const Textarea: React.FC<TextareaType> = ({
    className, value, type, placeholder, material, handleChange, handleKeyPress, id, label
}) => {
    const ClassNames = classNames({
        [className]: !!className,
        'ui-input': true,
        [styles.ui_input]: !!styles.ui_input,
        [styles.ui_input_material]: material,
    });

    return (
        <>
            <input
                className={ClassNames}
                type={type}
                value={value}
                placeholder={placeholder}
                onKeyPress={handleKeyPress}
                onChange={(event: any) => handleChange(event.target.value)}
                id={id}
            />
            {label && id && <label htmlFor={id ? id : ''} className={styles.label}>{label}</label>}
        </>
    );
};
Textarea.defaultProps = {
    type: 'text',
    material: false
};

export default Textarea;
