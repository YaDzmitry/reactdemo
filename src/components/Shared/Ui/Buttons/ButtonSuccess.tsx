import * as React from 'react';
import Button from 'reactstrap/lib/Button';
import * as classNames from 'classnames';

import { ButtonTypes } from './ButtonTypes';

const styles = require('./ButtonSuccess.scss');
const ButtonSuccess: React.FC<ButtonTypes> = (
    {children, className, inverse, size, handleClick, buttonLarge}
) => {
    const btnClass = classNames({
        'ui-btn': true,
        'ui-btn-lg': buttonLarge,
        [className]: !!className,
        [styles.button_success]: !!styles.button_success,
        [styles.button_success_inverse]: inverse,
    });
    return (
        <Button
            className={btnClass}
            size={size}
            color="success"
            onClick={() => {
                return handleClick && handleClick();
            }}
        >
            {children}
        </Button>
    );
};

ButtonSuccess.defaultProps = {
    size: 'md',
    buttonLarge: false,
};

export default ButtonSuccess;
