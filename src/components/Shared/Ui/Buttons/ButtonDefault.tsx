import * as React from 'react';
import Button from 'reactstrap/lib/Button';
import * as classNames from 'classnames';

import { ButtonTypes } from './ButtonTypes';

const styles = require('./ButtonDefault.scss');
const ButtonDefault: React.FC<ButtonTypes> = (
    {children, className, inverse, disabled, size, handleClick, buttonLarge}
) => {
    const btnClass = classNames({
        'ui-btn': true,
        'ui-btn-lg': buttonLarge,
        [className]: !!className,
        [styles.button_default]: !!styles.button_default,
        [styles.button_default_inverse]: inverse,
    });

    return (
        <Button
            className={btnClass}
            size={size}
            disabled={disabled}
            onClick={() => {
                return handleClick && handleClick();
            }}
        >
            {children}
        </Button>
    );
};

ButtonDefault.defaultProps = {
    buttonLarge: false,
    size: 'md',
    disabled: false,
};

export default ButtonDefault;
