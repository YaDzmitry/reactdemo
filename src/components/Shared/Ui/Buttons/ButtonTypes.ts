export interface ButtonTypes {
    className?: any;
    inverse?: boolean;
    size?: string;
    disabled?: boolean;
    buttonLarge?: boolean;
    handleClick?: (val?: string) => void;
}

export interface ButtonGroupTypes extends ButtonTypes {
    firstValue: string;
    secondValue: string;
    firstTitle: string;
    secondTitle: string;
    firstInverse?: boolean;
    secondInverse?: boolean;
}

export interface ButtonDropdownItem {
    id: number|string;
    label: string;
}

export interface ButtonDropdownTypes extends ButtonTypes {
    title: string;
    items: ButtonDropdownItem[];
    onItemClick: (item: ButtonDropdownItem) => void;
    right?: boolean;
}