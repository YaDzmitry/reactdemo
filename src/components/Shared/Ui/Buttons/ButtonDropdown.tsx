import * as React from 'react';
import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle } from 'reactstrap';
import * as classNames from 'classnames';

import { ButtonDropdownItem, ButtonDropdownTypes } from './ButtonTypes';

const styles = require('./ButtonDropdown.scss');

interface State {
    dropdownOpen: boolean;
}

export default class ButtonDropdown extends React.PureComponent<ButtonDropdownTypes, State> {
    state = {
        dropdownOpen: false
    };

    toggle = () => {
        this.setState(prevState => ({
            dropdownOpen: !prevState.dropdownOpen
        }));
    }

    render() {
        const { buttonLarge, className, inverse, items, onItemClick, title, right = false } = this.props;

        const btnClass = classNames({
            'ui-btn': true,
            'ui-btn-lg': buttonLarge,
            [styles.button]: !!styles.button,
            [styles.buttonInverse]: inverse,
        });

        return (
            <Dropdown className={className} isOpen={this.state.dropdownOpen} toggle={this.toggle}>
                <DropdownToggle className={btnClass}>
                    {title}
                </DropdownToggle>
                <DropdownMenu
                    right={right}
                    className={styles.menu}
                >
                    {
                        items.map((item: ButtonDropdownItem) =>
                            (
                                <DropdownItem
                                    key={item.id}
                                    onClick={() => onItemClick(item)}
                                    className={styles.listItem}
                                >
                                    {item.label}
                                </DropdownItem>
                            )
                        )
                    }
                </DropdownMenu>
            </Dropdown>
        );
    }
}