import { FilterDecoratorChange } from 'decorators/FilterDecorator';
import { Account } from 'store/Account/models/account';
import { FilterItem } from 'store/types';

export interface HeaderTypes {
    title?: React.ReactNode;
    className?: any;
    inverse?: boolean;
    handleChange?: FilterDecoratorChange;
    currentFilter?: any;
    filters?: FilterItem[];
    account?: Account;
}