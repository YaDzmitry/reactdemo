import * as React from 'react';
import * as classNames from 'classnames';
import { HeaderTypes } from './HeaderTypes';

const styles = require('./SubHeader.scss');
const SubHeader: React.FC<HeaderTypes> = ({children, title, className, inverse}) => {
    const classes = classNames({
        'ui-header': true,
        [className]: !!className,
        [styles.ui_sub_header]: !!styles.ui_sub_header,
        [styles.ui_sub_header_inverse]: inverse,
    });

    let renderContent: any = '';

    if (title) {
        renderContent = (
            <div className={classes}>
                {title}
                <div className={styles.test}>
                    {children}
                </div>
            </div>
        );
    }

    return renderContent;
};

export default SubHeader;
