import * as React from 'react';
import * as classNames from 'classnames';

import { CircleTypes } from './CircleTypes';
import { redNo } from 'store/constants';
import GlobalHelper from 'helpers/globalHelper';

const styles = require('./CircleDanger.scss');
const CircleDanger: React.FC<CircleTypes> = ({children, label, value, className, size, inverse}) => {
    const cssClasses = classNames({
        'ui-circle': true,
        [className]: !!className,
        [styles.circle_danger]: !!styles.circle_danger,
        [size]: !!styles.circle_danger,
        [styles.circle_danger_inverse]: inverse,
    });
    let fontSizeValue;
    if (value) {
        fontSizeValue = GlobalHelper.getFontSize(value.toString());
    }

    return (
        <div className={cssClasses}>
            <div className="circle-item">
                <img className={styles.dangerImage} src={redNo} />
                <div
                    className="circle-item--value"
                    style={{fontSize: fontSizeValue}}
                >
                    {value && value.toLocaleString()}
                </div>
                <div className="circle-item--label">
                    {label}
                </div>
            </div>
            {children}
        </div>
    );
};
CircleDanger.defaultProps = {
  size: 'md',
};

export default CircleDanger;
