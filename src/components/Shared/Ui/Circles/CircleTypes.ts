export interface CircleTypes {
    label: string;
    value: any;
    className?: any;
    inverse?: boolean;
    size?: any;
}