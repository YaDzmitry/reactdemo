import * as React from 'react';
import * as classNames from 'classnames';

import { checkMark } from 'store/constants';
import GlobalHelper from 'helpers/globalHelper';

import { CircleTypes } from './CircleTypes';

const styles = require('./CircleSuccess.scss');
const CircleSuccess: React.FC<CircleTypes> = ({children, label, value, className, size, inverse}) => {
    const cssClasses = classNames({
        'ui-circle': true,
        [className]: !!className,
        [styles.circle_success]: !!styles.circle_success,
        [size]: !!styles.circle_success,
        [styles.circle_success_inverse]: inverse,
    });
    let fontSizeValue;
    if (value) {
        fontSizeValue = GlobalHelper.getFontSize(value.toString());
    }

    return (
        <div className={cssClasses}>
            <div className="circle-item">
                <img className={styles.successImage} src={checkMark} />
                <div
                    className="circle-item--value test"
                    style={{fontSize: fontSizeValue}}
                >
                    {value && value.toLocaleString()}
                </div>
                <div className="circle-item--label">
                    {label}
                </div>
            </div>
            {children}
        </div>
    );
};
CircleSuccess.defaultProps = {
  size: 'md',
};

export default CircleSuccess;
