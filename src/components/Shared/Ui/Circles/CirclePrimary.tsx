import * as React from 'react';
import * as classNames from 'classnames';

import { CircleTypes } from './CircleTypes';
import GlobalHelper from '../../../../helpers/globalHelper';

const styles = require('./CirclePrimary.scss');
const CirclePrimary: React.FC<CircleTypes> = ({children, label, value, className, size, inverse}) => {
    const cssClasses = classNames({
        'ui-circle': true,
        [className]: !!className,
        [styles.circle_primary]: !!styles.circle_primary,
        [size]: !!styles.circle_primary,
        [styles.circle_primary_inverse]: inverse,
    });
    let fontSizeValue;
    if (value) {
        fontSizeValue = GlobalHelper.getFontSize(value.toString());
    }

    return (
        <div className={cssClasses}>
            <div className="circle-item">
                <div
                    className="circle-item--value"
                    style={{fontSize: fontSizeValue}}
                >
                    {value && value.toLocaleString()}
                </div>
                <div className="circle-item--label">
                    {label}
                </div>
            </div>
            {children}
        </div>
    );
};
CirclePrimary.defaultProps = {
  size: 'md',
};

export default CirclePrimary;
