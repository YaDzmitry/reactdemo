import * as React from 'react';
import { gearIcon } from 'store/constants';

const styles = require('./GearButton.scss');

interface Props {
    onClick: () => void;
}

const GearButton: React.FC<Props> = ({onClick}) => (
    <img
        src={gearIcon}
        className={styles.gear}
        onClick={onClick}
        alt="settings"
    />
);

export default GearButton;
