export interface AlertTypes {
    hasCloseIcon: boolean;
    className?: any;
    inverse?: boolean;
    handleClick?: () => void;
}