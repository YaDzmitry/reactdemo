import * as React from 'react';
import { NavigationButton } from 'store/types';
import MainLayout from 'components/Shared/Layouts/MainLayout';

const styles = require('./ProfilePage.scss');

interface Props {
    menuItems: NavigationButton[];
}

const ProfilePage: React.FC<Props> = (props) => (
    <MainLayout menuItems={props.menuItems}>
        <div>
            {props.children}
        </div>
    </MainLayout>
);

export default ProfilePage;
