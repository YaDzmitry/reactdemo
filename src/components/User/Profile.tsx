import * as React from 'react';

import { Form, FormGroup, Label, Col, Input } from 'reactstrap';

import ButtonSuccess from 'components/Shared/Ui/Buttons/ButtonSuccess';

import UpdateProfileRequest from 'store/User/models/updateProfileRequest';
import UpdatePasswordRequest from 'store/User/models/updatePasswordRequest';
import ActiveUser from 'store/Auth/models/activeUser';
import * as userHelper from 'store/User/helpers';

const styles = require('./Profile.scss');

interface Props {
    user: ActiveUser;
    profile: UpdateProfileRequest;
    password: UpdatePasswordRequest;
    changeProfile: (group: string, field: string, value: any) => void;
    updateProfile: () => void;
    updatePassword: () => void;
}

const Profile: React.FC<Props> = ({
    user, profile, password, changeProfile,
    updateProfile, updatePassword
}) => (
    <div className={styles.profile}>
        <Form>
            <FormGroup row={true}>
                <Label for="email" sm={2}>Email</Label>
                <Col sm={10}>
                    <Input
                        type="email"
                        id="email"
                        disabled={true}
                        value={user.email}
                    />
                </Col>
            </FormGroup>

            <FormGroup row={true}>
                <Label for="last-name" sm={2}>Last name</Label>
                <Col sm={10}>
                    <Input
                        type="text"
                        id="last-name"
                        value={profile.last_name}
                        onChange={(event) => changeProfile('profileForm', 'last_name', event.target.value)}
                    />
                </Col>
            </FormGroup>

            <FormGroup row={true}>
                <Label for="first-name" sm={2}>First name</Label>
                <Col sm={10}>
                    <Input
                        type="text"
                        id="first-name"
                        value={profile.first_name}
                        onChange={(event) => changeProfile('profileForm', 'first_name', event.target.value)}
                    />
                </Col>
            </FormGroup>

            <ButtonSuccess buttonLarge={true} handleClick={updateProfile}>Update</ButtonSuccess>
        </Form>

        {user.is_internal && !userHelper.isAdhoc(user) &&
            <Form>
                <FormGroup row={true}>
                    <Label for="old-password" sm={2}>Old password</Label>
                    <Col sm={10}>
                        <Input
                            type="password"
                            id="old-password"
                            value={password.old_password || ''}
                            onChange={(event) => changeProfile('passwordForm', 'old_password', event.target.value)}
                        />
                    </Col>
                </FormGroup>

                <FormGroup row={true}>
                    <Label for="new-password" sm={2}>New password</Label>
                    <Col sm={10}>
                        <Input
                            type="password"
                            id="new-password"
                            value={password.new_password}
                            onChange={(event) => changeProfile('passwordForm', 'new_password', event.target.value)}
                        />
                    </Col>
                </FormGroup>

                <ButtonSuccess buttonLarge={true} handleClick={updatePassword}>Change password</ButtonSuccess>
            </Form>
        }
    </div>
);

export default Profile;
