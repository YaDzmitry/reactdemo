import * as React from 'react';
import memoize from 'memoize-one';

import { Rule, RuleValidation } from 'store/AlertsManager/Rulesets/types';
import {
    Alignment,
    CellTypeChoices,
    FormatTypeChoices,
    TableRowActionIcon,
    TableRowActionType,
    TypesFormat
} from 'store/constants';

import { Id, TableHeader, TableRowAction } from 'store/types';
import {
    AddRuleAction,
    DeleteRuleAction,
    TouchRuleFieldAction,
    UpdateRuleAction
} from 'store/AlertsManager/Rulesets/actions';
import { RuleExclusionOptions, RulePeriodOptions, RuleTypeOptions } from 'store/AlertsManager/Rulesets/constants';

import UiDataTable from 'components/Shared/Ui/Tables/UiDataTable';
import DataTable from 'components/Shared/DataTable/DataTable';
import RulesFooterRow from './RulesFooterRow';
import RuleRow from './RuleRow';

const styles = require('./RulesetEditRules.scss');

interface Props {
    rules: Rule[];
    validations: RuleValidation[];
    disabled?: boolean;
    addRule: () => AddRuleAction;
    deleteRule: (index: number) => DeleteRuleAction;
    updateRule: (index: number, rule: Rule, field: string) => UpdateRuleAction;
    touchRuleField: (rule: Rule, index: number, field: string) => TouchRuleFieldAction;
}

interface State {
    headers: TableHeader[];
}

interface RuleWithIndex extends Rule {
    index: number;
    indexTitle: string;
}

const numberFieldProps = {
    editable: true,
    active: true,
    valueAlignment: Alignment.Right,
    formatType: FormatTypeChoices.NumeralDefault,
    cellType: CellTypeChoices.Numeric,
    params: {
        type: TypesFormat.nav
    },
    hasEditing: () => true
};

const floatFieldProps = {
    ...numberFieldProps,
    formatType: FormatTypeChoices.NumeralDefault,
    params: {
        type: TypesFormat.performance
    }
};

const percentFieldProps = {
    ...numberFieldProps,
    formatType: FormatTypeChoices.NumeralDefault,
    params: {
        type: TypesFormat.performance,
        suffix: '%',
    }
};

const selectFieldProps = {
    cellType: CellTypeChoices.Select,
    editable: true,
    active: true,
    params: {},
    hasEditing: () => true
};

class RulesetEditRules extends React.PureComponent<Props, State> {
    static defaultProps = {
        disabled: false
    };

    static getHeadersFromProps = memoize((disabled: boolean, validations: RuleValidation[]) => [
        {
            name: 'indexTitle',
            title: 'Rules',
            class: `primary w-min ${styles.indexColumn}`,
            active: true,
            disabled: disabled,
            getRowSpan: () => 2,
            valueAlignment: Alignment.Center,
        }, {
            name: 'period',
            title: 'Period*',
            options: RulePeriodOptions,
            class: `${styles.column} ${styles.period}`,
            placeholder: 'period',
            disabled: disabled,
            isCellDisabled: (row: Rule, index: number) => RulesetEditRules.isCellDisabled(validations, index, 'period'),
            isCellInvalid: (row: Rule, index: number) => RulesetEditRules.isCellInvalid(validations, index, 'period'),
            ...selectFieldProps,
            params: {
                optionsFunc: (
                    options: any[],
                    name: string, value: any, index: number
                ) => RulesetEditRules.options(validations, options, index, name)
            },
        }, {
            name: 'threshold',
            title: 'Threshold*',
            class: `${styles.column} ${styles.threshold}`,
            disabled: disabled,
            isCellDisabled: (row: Rule, index: number) =>
                RulesetEditRules.isCellDisabled(validations, index, 'threshold'),
            isCellInvalid: (row: Rule, index: number) =>
                RulesetEditRules.isCellInvalid(validations, index, 'threshold'),
            ...percentFieldProps,
            ...(disabled ? {cellType: CellTypeChoices.Text} : {}),
        }, {
            name: 'type',
            title: 'Type*',
            options: RuleTypeOptions,
            class: `${styles.column} ${styles.type}`,
            disabled: disabled,
            placeholder: 'type',
            isCellDisabled: (row: Rule, index: number) => RulesetEditRules.isCellDisabled(validations, index, 'type'),
            isCellInvalid: (row: Rule, index: number) => RulesetEditRules.isCellInvalid(validations, index, 'type'),
            ...selectFieldProps,
        }, {
            name: 'amount',
            title: 'Amount',
            class: `${styles.column} ${styles.amount}`,
            popover: (
                <>
                <b>Type</b> must be set to <b>Floor</b> to enable the <b>Amount</b> field.
                </>
            ),
            disabled: disabled,
            isCellDisabled: (row: Rule, index: number) => RulesetEditRules.isCellDisabled(validations, index, 'amount'),
            isCellInvalid: (row: Rule, index: number) => RulesetEditRules.isCellInvalid(validations, index, 'amount'),
            isPopoverVisible: (row: Rule, index: number) =>
                RulesetEditRules.isCellDisabled(validations, index, 'amount'),
            ...floatFieldProps,
            ...(disabled ? {cellType: CellTypeChoices.Text} : {}),
        }, {
            name: 'exclusions',
            title: 'Exclusions',
            options: RuleExclusionOptions,
            class: `${styles.column} ${styles.exclusions}`,
            popover: (
                <>
                <b>Type</b> must be set to <b>Peak Balance</b> or <b>Starting Balance</b>
                to enable the <b>Exclusions</b> field.
                </>
            ),
            disabled: disabled,
            placeholder: 'exclusions',
            isCellDisabled: (row: Rule, index: number) =>
                RulesetEditRules.isCellDisabled(validations, index, 'exclusions'),
            isCellInvalid: (row: Rule, index: number) =>
                RulesetEditRules.isCellInvalid(validations, index, 'exclusions'),
            isPopoverVisible: (row: Rule, index: number) =>
                RulesetEditRules.isCellDisabled(validations, index, 'exclusions'),
            ...selectFieldProps,
        }, {
            name: 'perf_share',
            title: 'Perf.Share',
            class: `${styles.column} ${styles.perfShare}`,
            popover: (
                <>
                <b>Type</b> must be set to <b>Peak Balance</b> or <b>Starting Balance</b>
                to enable the <b>Exclusions</b> field.
                </>
            ),
            disabled: disabled,
            isCellDisabled: (row: Rule, index: number) =>
                RulesetEditRules.isCellDisabled(validations, index, 'perf_share'),
            isCellInvalid: (row: Rule, index: number) =>
                RulesetEditRules.isCellInvalid(validations, index, 'perf_share'),
            isPopoverVisible: (row: Rule, index: number) =>
                RulesetEditRules.isCellDisabled(validations, index, 'perf_share'),
            ...percentFieldProps,
            ...(disabled ? {cellType: CellTypeChoices.Text} : {}),
        }]
    );

    state = {
        headers: []
    };

    getRowActions = memoize((disabled, handleDeleteRule): TableRowAction[] => {
        if (disabled) {
            return [{type: TableRowActionType.Link}];
        }

        return [{
            type: TableRowActionType.Image,
            icon: TableRowActionIcon.CrossGray,
            handler: handleDeleteRule
        }];
    });

    static getDerivedStateFromProps(props: Props) {
        return {
            headers: RulesetEditRules.getHeadersFromProps(props.disabled, props.validations)
        };
    }

    private static isCellDisabled = (validations: RuleValidation[], index: number, field: string) => {
        const ruleValidation = validations[index] || {};
        const validation = ruleValidation[field] || {};
        return !!validation.disabled;
    }

    private static isCellInvalid = (validations: RuleValidation[], index: number, field: string) => {
        const ruleValidation = validations[index] || {};
        const validation = ruleValidation[field] || {};
        return !validation.valid && validation.touched && !validation.disabled;
    }

    private static options = (validations: RuleValidation[], allOptions: any[], index: number, field: string) => {
        const ruleValidation = validations[index] || {};
        const validation = ruleValidation[field] || {};

        if (validation.validOptions === null) {
            return allOptions;
        }

        return allOptions.map(option => ({
            ...option,
                disabled: validation.validValues && validation.validValues.indexOf(option.value) < 0,
                isDisabled: option.disabled
            }));
    }

    handleOnBlur = (rule: Rule, rowIdx: number, field: string) => {
        this.props.touchRuleField(rule, rowIdx, field);
    }

    render() {
        const {rules} = this.props;
        const rows = this.props.rules.map(rule => ({
            ...rule,
            component: RuleRow
        }));

        const {headers} = this.state;

        const FooterRow = this.props.disabled ? undefined : this.FooterRow;

        return (
            <div className={styles.rulesList}>
                <UiDataTable className="table-text--center table-text--vertical-middle">
                    <DataTable
                        count={rules.length}
                        headers={headers}
                        rows={rows}
                        rowActions={this.getRowActions(this.props.disabled, this.handleDeleteRule)}
                        visibleHeaders={headers}
                        hasPagination={false}
                        hasSorting={false}
                        withCheckboxes={false}
                        actionsTitle={' '}
                        withoutNoDataFound={true}
                        handleColumnChange={this.handleColumnChange}
                        handleOnBlurInput={this.handleOnBlur}
                        FooterRow={FooterRow}
                    />
                </UiDataTable>
            </div>
        );
    }

    private FooterRow = () => {
        const {headers} = this.state;
        return <RulesFooterRow colSpan={headers.length} addRule={this.props.addRule} />;
    }

    private handleDeleteRule = (id: Id, row: RuleWithIndex) => {
        this.props.deleteRule(row.index);
    }

    private handleColumnChange = (value: string, name: string, index: number) => {
        const {rules, updateRule} = this.props;
        const rule = {...rules[index], [name]: value === undefined ? null : value};
        updateRule(index, rule, name);
    }
}

export default RulesetEditRules;