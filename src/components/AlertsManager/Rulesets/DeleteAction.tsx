import * as React from 'react';
import { PureComponent } from 'react';
import { ButtonTypes } from 'components/Shared/Ui/Buttons/ButtonTypes';
import { Ruleset } from 'store/AlertsManager/Rulesets/types';
import ButtonDanger from 'components/Shared/Ui/Buttons/ButtonDanger';

interface Props {
    ruleset: Ruleset;
    onDelete: () => void;
    deleteButton?: ButtonTypes;
}

const styles = require('./Actions.scss');

export default class DeleteAction extends PureComponent<Props> {
    render() {
        return (
          <>
              <ButtonDanger
                  {...this.props.deleteButton}
                  className={[styles.dangerButton, styles.action].join(' ')}
                  handleClick={this.props.onDelete}
              >
                  Delete Ruleset
              </ButtonDanger>
          </>
        );
    }
}