import * as React from 'react';

import { AsyncFilterName, FilterNames, FiltersState } from 'store/Filters/types';
import { FilterItem, TableHeader } from 'store/types';
import {
    DEFAULT_ASYNC_FILTER, DEFAULT_VALUE_DASH,
    Alignment, CellTypeChoices, FilterType, FixedElementType, FormatTypeChoices, ScrollableElementType
} from 'store/constants';

import Modal from 'components/Shared/Modals/Modal';
import UiDataTable from 'components/Shared/Ui/Tables/UiDataTable';
import DataTable from 'components/Shared/DataTable/DataTable';
import AttributesCell from 'components/Fund/TableCells/AttributesCell';
import { RequestorTargetSectionProps } from 'containers/AlertsManager/Rulesets/RequestorTargetSection';

const styles = require('./TargetFundListModal.scss');

class RequestorTargetFundListModal extends React.PureComponent<RequestorTargetSectionProps> {

    buttons = [];
    headers: TableHeader[] = [
        {
            name: 'firm.name',
            title: 'Firm',
            headerAlignment: Alignment.Left,
            active: true,
            cellType: CellTypeChoices.Text,
        },
        {
            name: 'name',
            title: 'Fund',
            headerAlignment: Alignment.Left,
            active: true,
            cellType: CellTypeChoices.Text,
        },
        {
            name: 'fund_attributes.credit_officer',
                title: 'Coverage',
            active: true,
            cellType: CellTypeChoices.Custom,
            transformer: row => <AttributesCell fields={row.fund_attributes} attributeType="credit_officer" />
        },
        {
            name: 'fund_attributes.strategy',
                title: 'Strategy',
            active: true,
            cellType: CellTypeChoices.Custom,
            transformer: row => <AttributesCell fields={row.fund_attributes} attributeType="strategy" />
        },
        {
            name: 'fund_attributes.product',
                title: 'Product',
            active: true,
            cellType: CellTypeChoices.Custom,
            transformer: row => <AttributesCell fields={row.fund_attributes} attributeType="product" />
        },
        {
            name: 'internal_id',
                title: 'Internal ID',
            active: true,
            formatType: FormatTypeChoices.String,
            defaultValue: DEFAULT_VALUE_DASH,
        },
    ];

    render() {
        const {
            count, results, disabled, chips,
            filterPageName, currentFilter, isLoading, isGridLoading,
            handleChange,
        } = this.props;

        const filters: FilterItem[] = this.filters;
        const body = (
            <UiDataTable>
                <DataTable
                    filters={filters}
                    disabledFilter={disabled}
                    rows={results}
                    count={count}
                    headers={this.headers}
                    handleChange={handleChange}
                    hasPagination={true}
                    hasSorting={false}
                    currentFilter={currentFilter}
                    isLoading={isLoading}
                    chips={chips}
                    filterPageName={filterPageName}
                    isGridLoading={isGridLoading}
                    scrollableElementClassName={`scrollable-table-body`}
                    possibleFixedElementClassName={`fixed-table-header`}
                    fixedElementType={FixedElementType.TableHeader}
                    scrollableElementType={ScrollableElementType.TableBody}
                />
            </UiDataTable>
        );
        return (
            <Modal
                isOpen={this.props.isOpen}
                toggle={this.props.closeModal}
                title={`TARGET:`}
                body={body}
                buttons={this.buttons}
                className={styles.modalContent}
                modalClassName={styles.modalClass}
            />
        );
    }

    private get filters(): FilterItem[] {
        const {disabled} = this.props;

        return [
            {
                name: 'search',
                type: FilterType.Search,
                title: `Search`,
                className: 'col-lg-9 col-md-8',
                disabled,
            },
            {
                name: 'stats',
                type: FilterType.Statistics,
                title: 'Stats', className: 'col-lg-3 col-md-4',
                stats: [
                    {name: 'Firms', value: this.props.firmCount || 0},
                    {name: 'Funds', value: this.props.count || 0},
                ],
                disabled,
            },
            {
                name: 'firm_id',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Firms',
                className: 'col-xl-2 col-lg-3 col-md-4',
                choices: this.props.firmFilter.choices,
                choicesCount: this.props.firmFilter.count,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(FilterNames.rulesetRequestorTargetFundList, AsyncFilterName.firm, filter);
                },
                currentFilter: this.props.firmFilter.filter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.firm,
                defaultFilter: DEFAULT_ASYNC_FILTER,
                idPrefix: 'modal',
                isFilterValueObject: true,
                disabled,
            },
            {
                name: 'credit_officer',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Coverage',
                className: 'col-xl-2 col-lg-3 col-md-4',
                choices: this.props.creditOfficerFilter.choices,
                choicesCount: this.props.creditOfficerFilter.count,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(
                        FilterNames.rulesetRequestorTargetFundList,
                        AsyncFilterName.creditOfficer,
                        filter
                    );
                },
                currentFilter: this.props.creditOfficerFilter.filter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.creditOfficer,
                defaultFilter: DEFAULT_ASYNC_FILTER,
                idPrefix: 'modal',
                disabled,
            },
            {
                name: 'product',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Product',
                className: 'col-xl-2 col-lg-3 col-md-4',
                choices: this.props.productFilter.choices,
                choicesCount: this.props.productFilter.count,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(FilterNames.rulesetRequestorTargetFundList, AsyncFilterName.product, filter);
                },
                currentFilter: this.props.productFilter.filter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.product,
                defaultFilter: DEFAULT_ASYNC_FILTER,
                idPrefix: 'modal',
                disabled,
            },
            {
                name: 'strategy',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Strategy',
                className: 'col-xl-2 col-lg-3 col-md-4',
                choices: this.props.strategyFilter.choices,
                choicesCount: this.props.strategyFilter.count,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(FilterNames.rulesetRequestorTargetFundList, AsyncFilterName.strategy, filter);
                },
                currentFilter: this.props.strategyFilter.filter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.strategy,
                defaultFilter: DEFAULT_ASYNC_FILTER,
                idPrefix: 'modal',
                disabled,
            },
            {
                name: 'portfolio_id',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Portfolio',
                className: 'col-xl-2 col-lg-3 col-md-4',
                choices: this.props.portfolioFilter.choices,
                choicesCount: this.props.portfolioFilter.count,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(
                        FilterNames.rulesetRequestorTargetFundList,
                        AsyncFilterName.portfolio,
                        filter
                    );
                },
                currentFilter: this.props.portfolioFilter.filter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.portfolio,
                defaultFilter: DEFAULT_ASYNC_FILTER,
                idPrefix: 'modal',
                isFilterValueObject: true,
                disabled,
            },
            {
                name: 'internal_id',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Internal Id',
                className: 'col-xl-2 col-lg-3 col-md-4',
                choices: this.props.internalIdFilter.choices,
                choicesCount: this.props.internalIdFilter.count,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(
                        FilterNames.rulesetRequestorTargetFundList,
                        AsyncFilterName.internalId,
                        filter
                    );
                },
                currentFilter: this.props.internalIdFilter.filter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.internalId,
                defaultFilter: DEFAULT_ASYNC_FILTER,
                idPrefix: 'modal',
                disabled,
            },
        ];
    }
}

export default RequestorTargetFundListModal;
