import * as React from 'react';
import * as classNames from 'classnames';

import { DataTableRowProps } from 'components/Shared/DataTable/DataTableRow';
import DataTableRow from 'components/Shared/DataTable/DataTableRow';

const styles = require('./RuleRow.scss');

class RuleRow extends React.PureComponent<DataTableRowProps> {
    render() {
        const {row, className, ...restProps} = this.props;

        const {rowIdx} = this.props;
        row.indexTitle = `#${rowIdx + 1}`;
        row.index = rowIdx;

        const editRowClassName = classNames({
            [className]: true,
            [styles.firstRow]: true
        });

        return (
            <>
                <DataTableRow row={row} className={editRowClassName} {...restProps} />
                <tr className={styles.secondRow}>
                    <td colSpan={this.props.headers.length}>
                        <div className={styles.formulaTitle}>
                            <span className={styles.formula}>Formula:</span>
                            <span className={styles.bracket}>[ </span>
                            (x) / ƒ(y)
                            <span className={styles.bracket}>]</span> - 1 ≤ (z)
                        </div>

                        {!this.isLast &&
                        <div className={styles.or}><span>OR</span></div>
                        }
                    </td>
                </tr>
            </>
        );
    }

    private get isLast() {
        const {rowIdx, rowsCount} = this.props;
        return rowIdx === (rowsCount - 1);
    }
}

export default RuleRow;