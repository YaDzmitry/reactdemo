import * as React from 'react';
import { Popover, PopoverBody } from 'reactstrap';

import { lockedDarkGray } from 'store/constants';
import { Ruleset, RulesetValidation, Version } from 'store/AlertsManager/Rulesets/types';
import ActiveUser from 'store/Auth/models/activeUser';
import Actions from './Actions';
import RulesetStatusCell from './RulesetStatusCell';

const styles = require('./RulesetEdit.scss');

interface ReadonlyTitleProps {
    id: string;
    versionName: string;
}

interface ReadonlyTitleState {
    isPopoverVisible: boolean;
}

class ReadonlyTitle extends React.PureComponent<ReadonlyTitleProps, ReadonlyTitleState> {
    state = {
        isPopoverVisible: false
    };

    handleShowPopover = () => {
        this.setState(prevState => ({isPopoverVisible: true}));
    }

    handleHidePopover = () => {
        this.setState(prevState => ({isPopoverVisible: false}));
    }

    render() {
        const {id, versionName} = this.props;

        return (
            <>
                <img
                    src={lockedDarkGray}
                    id={id}
                    alt="Read only"
                    onMouseOver={this.handleShowPopover}
                    onMouseOut={this.handleHidePopover}
                />
                <Popover placement="top-start" isOpen={this.state.isPopoverVisible} target={id}>
                    <PopoverBody>
                        Previous versions of rulesets are not editable.
                        Please select the latest version to make changes.
                    </PopoverBody>
                </Popover>
                {versionName} (Read only)
            </>
        );
    }
}

interface Props {
    ruleset: Ruleset;
    validation: RulesetValidation;
    isActive: boolean;
    user: ActiveUser;

    selectRulesetVersion: (version: Version) => void;
    publishRuleset: () => void;
    deleteRuleset: () => void;

    currentVersionName: string;
    children: {
        propertiesEdit: React.ReactNode,
        rulesEdit: React.ReactNode,
        targetEdit: React.ReactNode
    };
}

class RulesetEdit extends React.PureComponent<Props> {
    render () {
        const {
            ruleset, validation, isActive, selectRulesetVersion, deleteRuleset, publishRuleset, currentVersionName
        } = this.props;

        const {propertiesEdit, rulesEdit, targetEdit} = this.props.children;

        return (
            <div className={styles.container}>
                <div className={styles.actions}>
                    {isActive &&
                    <div className={styles.status}>
                        Status:
                        <span className={[styles.statusText, styles[ruleset.status + '']].join(' ')}>
                            <RulesetStatusCell
                                value={ruleset.status || ''}
                                changedSinceLastPublish={ruleset.changed_since_last_publish}
                            />
                        </span>
                    </div>
                    }
                    <Actions
                        ruleset={ruleset}
                        selectVersion={selectRulesetVersion}
                        publish={publishRuleset}
                        delete={deleteRuleset}
                        deleteButton={{
                            disabled: !isActive || !ruleset.id
                        }}
                        publishButton={{
                            disabled: !isActive || !ruleset.id || !validation.validOrUntouched
                        }}
                    />
                </div>
                <div className={styles.block}>
                    <div className={styles.blockTitle}>
                        Properties
                        {!isActive && <ReadonlyTitle id="Title1" versionName={currentVersionName}/>}
                    </div>
                    {propertiesEdit}
                </div>
                <div className={styles.block}>
                    <div className={styles.blockTitle}>
                        Rules
                        {!isActive && <ReadonlyTitle id="Title2" versionName={currentVersionName}/>}
                    </div>
                    {rulesEdit}
                </div>
                <div className={styles.block}>
                    <div className={styles.blockTitle}>
                        Target
                        {!isActive && <ReadonlyTitle id="Title3" versionName={currentVersionName}/>}
                    </div>
                    {targetEdit}
                </div>
            </div>
        );
    }
}

export default RulesetEdit;
