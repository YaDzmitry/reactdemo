import * as React from 'react';

import { FilterDecoratorChange } from 'decorators/FilterDecorator';
import DataTable from 'components/Shared/DataTable/DataTable';
import UiDataTable from 'components/Shared/Ui/Tables/UiDataTable';
import Modal from 'components/Shared/Modals/Modal';

import { AsyncFilterName, Chip, FilterInfo, FilterNames, FiltersState } from 'store/Filters/types';
import { FilterItem, Id, TableHeader } from 'store/types';
import {
    DEFAULT_ASYNC_FILTER,
    Alignment, CellTypeChoices, FilterType, FixedElementType, ScrollableElementType
} from 'store/constants';
import { GetTargetFundListAction } from 'store/AlertsManager/Rulesets/actions';
import { CloseModalAction, OpenModalAction } from 'store/Modals/General/actions';
import { ModalParams } from 'store/Modals/General/types';
import { SetFiltersAction } from 'store/Filters/actions';
import { FundModel } from 'store/Fund/models/fund';

export interface Props {
    disabled?: boolean;
    chips: Chip[];
    count: number;
    currentFilter: FiltersState;
    handleChange: FilterDecoratorChange;
    applyFilter: (filter: FiltersState) => GetTargetFundListAction;
    filterPageName: string;
    results: FundModel[];
    firmCount: number;
    fundFilter: FilterInfo;
    getChoices: (page: string, filterName: string, filter: FiltersState) => void;
    setAsyncFilters: (filterName: string, filter: FiltersState, pageName: string) => void;
    openModal: (name: string, params: ModalParams) => OpenModalAction;
    isLoading: boolean;
    isGridLoading: boolean;
    setFilters: (filterName: string, payload: FiltersState) => SetFiltersAction;
    isOpen: boolean;
    closeModal: () => CloseModalAction;
    chooseAll: () => void;
    chooseRow: (id: Id) => void;
}

const styles = require('./TargetFundListModal.scss');

class ResponderTargetFundListModal extends React.PureComponent<Props> {
    static defaultProps = {
        chips: []
    };

    buttons = [];

    headers: TableHeader[] = [
        {
            name: 'firm.name',
            title: 'Firm',
            headerAlignment: Alignment.Left,
            active: true,
            cellType: CellTypeChoices.Text,
        },
        {
            name: 'name',
            title: 'Fund',
            headerAlignment: Alignment.Left,
            active: true,
            cellType: CellTypeChoices.Text,
        },
    ];

    render() {
        const {
            disabled, count, results, currentFilter, filterPageName, chips,
            isLoading, isGridLoading,
            handleChange,
        } = this.props;

        const filters: FilterItem[] = this.filters;

        const body = (
            <UiDataTable>
                <DataTable
                    filters={filters}
                    disabledFilter={disabled}
                    rows={results}
                    count={count}
                    headers={this.headers}
                    handleChange={handleChange}
                    hasPagination={true}
                    hasSorting={false}
                    currentFilter={currentFilter}
                    isLoading={isLoading}
                    chips={chips}
                    filterPageName={filterPageName}
                    isGridLoading={isGridLoading}
                    scrollableElementClassName={`scrollable-table-body`}
                    possibleFixedElementClassName={`fixed-table-header`}
                    fixedElementType={FixedElementType.TableHeader}
                    scrollableElementType={ScrollableElementType.TableBody}
                />
            </UiDataTable>
        );

        return (
            <Modal
                isOpen={this.props.isOpen}
                toggle={this.props.closeModal}
                title={`TARGET:`}
                body={body}
                buttons={this.buttons}
                className={styles.modalContent}
                modalClassName={styles.modalClass}
            />
        );
    }

    private get filters() {
        const {disabled} = this.props;

        return [
            {
                name: 'checked_all',
                type: FilterType.SingleCheckbox,
                className: 'col-lg-3 col-md-4',
                title: 'All Funds at my Firm',
                disabled,
                idPrefix: 'modal',
            },
            {
                name: 'fund_id',
                type: FilterType.CreditSelectWithCheckboxes,
                className: 'col-lg-3 col-md-4',
                title: 'Funds',
                choices: this.props.fundFilter.choices,
                choicesCount: this.props.fundFilter.count,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(FilterNames.rulesetResponderTargetFundList, AsyncFilterName.fund, filter);
                },
                currentFilter: this.props.fundFilter.filter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.fund,
                defaultFilter: DEFAULT_ASYNC_FILTER,
                idPrefix: 'modal',
                isFilterValueObject: true,
                disabled,
            },
            {
                name: 'stats',
                type: FilterType.Statistics,
                title: 'Stats', className: 'col-lg-3 offset-lg-3 col-md-4',
                stats: [
                    {name: 'Firms', value: this.props.count ? 1 : 0},
                    {name: 'Funds', value: this.props.count || 0},
                ],
                disabled,
            },
        ];
    }
}

export default ResponderTargetFundListModal;
