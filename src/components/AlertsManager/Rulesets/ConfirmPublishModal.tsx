import * as React from 'react';
import memoize from 'memoize-one';

import Modal from 'components/Shared/Modals/Modal';
import ButtonDanger from 'components/Shared/Ui/Buttons/ButtonDanger';
import ButtonDefault from 'components/Shared/Ui/Buttons/ButtonDefault';
import AlertDefault from 'components/Shared/Ui/Alerts/AlertDefault';

import { Ruleset } from 'store/AlertsManager/Rulesets/types';

interface Props {
    ruleset: Ruleset;
    isOpen: boolean;
    closeModal: () => void;
    publishRuleset: () => void;
}

class ConfirmPublishModal extends React.PureComponent<Props> {
    getButtons = memoize((handlePublish, handleCancel) => [
        (<ButtonDanger buttonLarge={true} handleClick={handlePublish}>Publish now</ButtonDanger>),
        (<ButtonDefault buttonLarge={true} inverse={true} handleClick={handleCancel}>Cancel</ButtonDefault>)
    ]);

    render() {
        const { isOpen } = this.props;

        const body = (
            <AlertDefault hasCloseIcon={false}>
                The ruleset <b>{this.props.ruleset.name} </b>
                will be published and immediately take effect
            </AlertDefault>
        );
        const title = 'Confirm Publish';

        return (
            <Modal
                className="ui-modal--confirm"
                isOpen={isOpen}
                toggle={this.props.closeModal}
                title={title}
                body={body}
                buttons={this.getButtons(this.props.publishRuleset, this.props.closeModal)}
            />
        );
    }
}

export default ConfirmPublishModal;