import * as React from 'react';
import { PureComponent } from 'react';
import { ButtonDropdownItem, ButtonTypes } from 'components/Shared/Ui/Buttons/ButtonTypes';
import ButtonDropdown from 'components/Shared/Ui/Buttons/ButtonDropdown';
import { Ruleset } from 'store/AlertsManager/Rulesets/types';
import PublishAction from './PublishAction';
import DeleteAction from './DeleteAction';

const styles = require('./Actions.scss');

interface Props {
    ruleset: Ruleset;
    selectVersion: (item: ButtonDropdownItem) => void;
    publish: () => void;
    delete: () => void;
    deleteButton?: ButtonTypes;
    publishButton?: ButtonTypes;
}

export class Actions extends PureComponent<Props> {
    render() {
        const { deleteButton, publishButton, publish, ruleset } = this.props;

        return (
            <div className={styles.container}>
                <DeleteAction
                    ruleset={ruleset}
                    deleteButton={{...deleteButton}}
                    onDelete={this.props.delete}
                />

                {ruleset.versions && ruleset.versions.length > 0 &&
                <ButtonDropdown
                    title="VERSIONS"
                    className={styles.action}
                    items={ruleset.versions || []}
                    right={true}
                    onItemClick={this.handleSelectVersion}
                />}

                <PublishAction
                    ruleset={ruleset}
                    publishButton={{ ...publishButton }}
                    onPublish={publish}
                />
            </div>
        );
    }

    private handleSelectVersion = (version: ButtonDropdownItem) => {
        this.props.selectVersion(version);
    }
}

export default Actions;