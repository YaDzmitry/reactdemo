import * as React from 'react';
import { PureComponent } from 'react';
import ButtonPrimary from 'components/Shared/Ui/Buttons/ButtonPrimary';
import { ButtonTypes } from 'components/Shared/Ui/Buttons/ButtonTypes';
import { Ruleset } from 'store/AlertsManager/Rulesets/types';

interface Props {
    ruleset: Ruleset;
    onPublish: () => void;
    publishButton?: ButtonTypes;
}

const styles = require('./Actions.scss');

export default class PublishAction extends PureComponent<Props> {
    render() {
        return (
          <>
              <ButtonPrimary
                  {...this.props.publishButton}
                  className={styles.action}
                  handleClick={this.props.onPublish}
              >
                  Publish Ruleset
              </ButtonPrimary>
          </>
        );
    }
}