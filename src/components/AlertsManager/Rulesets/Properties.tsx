import * as React from 'react';
import { PureComponent } from 'react';
import * as moment from 'moment';
import * as classnames from 'classnames';

import BasePanel from 'components/Shared/Panels/BasePanel';
import UiSelect from 'components/Shared/Ui/Select';
import UiInput from 'components/Shared/Ui/Input';
import DatePicker from 'components/Shared/Ui/DatePicker';
import { Ruleset } from 'store/AlertsManager/Rulesets/types';
import ActiveUser from 'store/Auth/models/activeUser';
import { isResponder } from 'store/User/helpers';
import { RULESET_TYPE } from 'store/AlertsManager/Rulesets/constants';
import { SelectItem } from 'store/types';

const styles = require('./Properties.scss');

interface Props {
    user: ActiveUser;
    disabled: boolean;
    ruleset: Ruleset;
    changeProperty: (key: string, value: any) => void;
}

export class Properties extends PureComponent<Props> {
    handleFieldChange = (key: string, value) => {
        this.props.changeProperty(key, value);
    }

    handleNameChange = value => this.handleFieldChange('name', value);
    handleTypeChange = (item: SelectItem) => this.handleFieldChange('type', item ? item.value : null);
    handleStartDateChange = value => this.handleFieldChange('eff_start_date', value);
    handleEndDateChange = value => this.handleFieldChange('eff_end_date', value);
    handleCounterpartyChange = value => this.handleFieldChange('counterparty', value);

    render() {
        const {user, disabled} = this.props;

        const itemClassNames = classnames({
            [styles.item]: true,
            [styles.disabled]: disabled
        });

        return (
            <BasePanel>
                <div className={styles.container}>
                    <div className={itemClassNames}>
                        <span className={styles.title}>Ruleset Name</span>
                        <UiInput
                            disabled={disabled}
                            placeholder="Ruleset Name"
                            value={this.name}
                            handleChange={this.handleNameChange}
                            material={true}
                            className={styles.input}
                        />
                    </div>
                    <div className={itemClassNames}>
                        <span className={styles.title}>Type</span>
                        <div className={styles['title-select']}>
                            <UiSelect
                                disabled={disabled}
                                placeholder="Select"
                                value={this.type}
                                options={RULESET_TYPE}
                                handleChange={this.handleTypeChange}
                                clearable={false}
                                classNamePrefix="material-select"
                                customMenuPortalClass={{
                                    fontSize: '13',
                                }}
                            />
                        </div>
                    </div>
                    <div className={itemClassNames}>
                        <span className={styles.title}>Effective Start Date</span>
                        <DatePicker
                            date={this.effectiveStartDate}
                            id="eff_start_date"
                            disabled={disabled}
                            placeholder="-"
                            handleChange={this.handleStartDateChange}
                            material={true}
                            customInputIcon={true}
                            showClearDate={false}
                        />
                    </div>
                    <div className={itemClassNames}>
                        <span className={styles.title}>Effective End Date</span>
                        <DatePicker
                            date={this.effectiveEndDate}
                            id="eff_end_date"
                            disabled={disabled}
                            placeholder="-"
                            handleChange={this.handleEndDateChange}
                            material={true}
                            customInputIcon={true}
                            showClearDate={false}
                        />
                    </div>
                    {isResponder(user) &&
                    <div className={itemClassNames}>
                        <span className={styles.title}>Counterparty</span>
                        <UiInput
                            placeholder="-"
                            disabled={disabled}
                            value={this.counterparty}
                            handleChange={this.handleCounterpartyChange}
                            material={true}
                            className={styles.input}
                        />
                    </div>
                    }
                </div>
            </BasePanel>
        );
    }

    private get name() {
        return this.props.ruleset.name;
    }

    private get type() {
        return this.props.ruleset.type || '';
    }

    private get counterparty() {
        return this.props.ruleset.counterparty;
    }

    private get effectiveStartDate() {
        const {eff_start_date} = this.props.ruleset;

        if (!eff_start_date || eff_start_date === '') {
            return null;
        }

        return moment(eff_start_date);
    }

    private get effectiveEndDate() {
        const {eff_end_date} = this.props.ruleset;

        if (!eff_end_date || eff_end_date === '') {
            return null;
        }

        return moment(eff_end_date);

    }
}

export default Properties;
