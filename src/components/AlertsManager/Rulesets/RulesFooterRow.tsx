import * as React from 'react';
import { AddRuleAction } from 'store/AlertsManager/Rulesets/actions';

import { stateAdd } from 'store/constants';

const styles = require('./RulesFooterRow.scss');

interface Props {
    colSpan: number;
    addRule: () => AddRuleAction;
}

const RulesFooterRow: React.FC<Props> = ({colSpan, addRule}) => (
    <tr>
        <td className={`w-min primary ${styles.leftColumn}`}>
            -
        </td>
        <td colSpan={colSpan}>
            <button className="ui-button--clear" onClick={addRule}>
                <img src={stateAdd} alt=""/>
                <span className="ui-inlineBlock ui-pl-5">Click here to add a rule</span>
            </button>
        </td>
    </tr>
);

export default RulesFooterRow;
