import * as React from 'react';
import { NavLink } from 'react-router-dom';

import { FilterDecoratorChange } from 'decorators/FilterDecorator';

import MainLayout from 'components/Shared/Layouts/MainLayout';
import DataTable from 'components/Shared/DataTable/DataTable';
import UiDataTable from 'components/Shared/Ui/Tables/UiDataTable';
import ButtonPrimary from 'components/Shared/Ui/Buttons/ButtonPrimary';
import EnumValueCell from 'components/Shared/DataTable/Cells/EnumValueCell';

import RulesetStatusCell from './RulesetStatusCell';

import {
    Alignment, CellTypeChoices, FormatTypeChoices, TableBulkActionChoices, TableRowActionIcon, TableRowActionType
} from 'store/constants';
import { FiltersState } from 'store/Filters/types';
import { Ruleset } from 'store/AlertsManager/Rulesets/types';
import { Id, NavigationButton, TableBulkAction, TableHeader, TableRowAction } from 'store/types';
import { RULESET_TYPE } from 'store/AlertsManager/Rulesets/constants';
import { DownloadRulesetListAction } from 'store/AlertsManager/Rulesets/actions';
import BulkUploadModal from 'containers/AlertsManager/Rulesets/BulkUploadModal';

interface Props {
    menuItems:  NavigationButton[];
    results: Ruleset[];
    count: number;
    handleChange: FilterDecoratorChange;
    currentFilter: FiltersState;
    isLoading: boolean;
    isGridLoading: boolean;
    checkedAll: boolean;
    checkedIds: Id[];
    chooseAll: () => void;
    chooseRow: (id: Id) => void;

    createRuleset: () => void;
    download: (checkedIds: Id[], checkedAll: boolean) => DownloadRulesetListAction;
    bulkUpload: () => void;
    isBulkUploadOpen: boolean;
}

const breadCrumbs: JSX.Element = (
    <div className="ap-breadcrumbs">
        <NavLink to="/alerts">Alerts</NavLink>
        <span className="breadcrumb-slash">/</span>
        <span>{`Rulesets`}</span>
    </div>
);

class RulesetListPage extends React.PureComponent<Props> {
    rowActions: TableRowAction[] = [{
        type: TableRowActionType.NavLink,
        icon: TableRowActionIcon.Pencil,
        link: `/rulesets/{id}`,
    }];

    buttons = [(
        <ButtonPrimary className="float-right ml-3" handleClick={() => this.handleCreateRuleset()}>
            NEW RULESET
        </ButtonPrimary>
    ), (
        <ButtonPrimary className="float-right ml-3" inverse={true} handleClick={() => this.handleDownload()}>
            DOWNLOAD
        </ButtonPrimary>
    ), (
        <ButtonPrimary className="float-right ml-3" inverse={true} handleClick={() => this.handleUpload()}>
            BULK UPLOAD
        </ButtonPrimary>
    )];

    bulkActions: TableBulkAction[] = [{
        type: TableBulkActionChoices.DownloadRulesets,
        handler: () => this.props.download(this.props.checkedIds, this.props.checkedAll),
    }];

    headers: TableHeader[] = [
        {
            name: 'name', title: 'Ruleset', active: true, headerAlignment: Alignment.Left
        }, {
            name: 'type',
            title: 'Type',
            active: true,
            headerAlignment: Alignment.Left,
            cellType: CellTypeChoices.Custom,
            transformer: (row: Ruleset) => (
                <EnumValueCell choices={RULESET_TYPE} value={row.type || ''}/>
            ),
            excludeSorting: true,
        }, {
            name: 'eff_start_date',
            title: 'Effective Start Date',
            active: true,
            headerAlignment: Alignment.Left,
            cellType: CellTypeChoices.Text,
            formatType: FormatTypeChoices.DateFormat,
            defaultValue: 'Rolling',
            excludeSorting: true,
        }, {
            name: 'eff_end_date',
            title: 'Effective End Date',
            active: true,
            headerAlignment: Alignment.Left,
            cellType: CellTypeChoices.Text,
            formatType: FormatTypeChoices.DateFormat,
            defaultValue: 'Rolling',
            excludeSorting: true,
        }, {
            name: 'number_of_rules',
            title: 'Number of Rules',
            active: true,
            headerAlignment: Alignment.Left,
            excludeSorting: true,
        }, {
            name: 'status',
            title: 'Status',
            active: true,
            headerAlignment: Alignment.Left,
            cellType: CellTypeChoices.Custom,
            transformer: (row: Ruleset) => (
                <RulesetStatusCell
                    value={row.status || ''}
                    changedSinceLastPublish={row.changed_since_last_publish}
                />
            ),
            excludeSorting: true,
        }];

    render() {
        return (
            <MainLayout
                title={`Rulesets`}
                menuItems={this.props.menuItems}
                breadCrumbs={breadCrumbs}
            >
                <UiDataTable>
                    <DataTable
                        rows={this.props.results}
                        count={this.props.count}
                        currentFilter={this.props.currentFilter}
                        headers={this.headers}
                        buttons={this.buttons}
                        handleChange={this.props.handleChange}
                        withCheckboxes={true}
                        checkedAll={this.props.checkedAll}
                        checkedIds={this.props.checkedIds}
                        chooseAll={this.props.chooseAll}
                        chooseRow={this.props.chooseRow}
                        hasPagination={true}
                        isLoading={this.props.isLoading}
                        isGridLoading={this.props.isGridLoading}
                        hasSorting={true}
                        rowActions={this.rowActions}
                        actionsTitle={' '}
                        bulkActions={this.bulkActions}
                        useColoredScrollBar={false}
                    />
                </UiDataTable>
                {this.props.isBulkUploadOpen && <BulkUploadModal />}
            </MainLayout>
        );
    }

    private handleCreateRuleset = () => {
        this.props.createRuleset();
    }

    private handleDownload = () => {
        this.props.download([], true);
    }

    private handleUpload = () => {
        this.props.bulkUpload();
    }
}

export default RulesetListPage;