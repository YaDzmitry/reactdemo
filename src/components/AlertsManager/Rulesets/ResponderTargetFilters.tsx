import * as React from 'react';

import FilterPanel from 'components/Shared/Filters/FilterPanel';

import { ResponderTargetSectionProps } from 'containers/AlertsManager/Rulesets/ResponderTargetSection';
import { AsyncFilterName, FilterNames, FiltersState } from 'store/Filters/types';
import { FilterItem } from 'store/types';
import { DEFAULT_ASYNC_FILTER, FilterType } from 'store/constants';
import { RESPONDER_TARGET_FUND_LIST_MODAL } from 'store/AlertsManager/Rulesets/constants';

const styles = require('./TargetFilters.scss');

class ResponderTargetFilters extends React.PureComponent<ResponderTargetSectionProps> {
    render() {
        const {disabled} = this.props;
        const filters: FilterItem[] = this.filters;
        return (
            <div className={styles.container}>
                <FilterPanel
                    disabled={disabled}
                    chips={this.props.chips}
                    filters={filters}
                    currentFilter={this.props.currentFilter}
                    handleChange={this.props.handleChange}
                    filterPageName={this.props.filterPageName}
                />
            </div>
        );
    }

    private get filters() {
        const {disabled} = this.props;

        return [
            {
                name: 'checked_all',
                type: FilterType.SingleCheckbox,
                className: 'col-lg-3 col-md-4',
                title: 'All Funds at my Firm',
                disabled,
            },
            {
                name: 'fund_id',
                type: FilterType.CreditSelectWithCheckboxes,
                className: 'col-lg-3 col-md-4',
                title: 'Funds',
                choices: this.props.fundFilter.choices,
                choicesCount: this.props.fundFilter.count,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(FilterNames.rulesetResponderTargetFundList, AsyncFilterName.fund, filter);
                },
                currentFilter: this.props.fundFilter.filter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.fund,
                defaultFilter: DEFAULT_ASYNC_FILTER,
                isFilterValueObject: true,
                disabled,
            },
            {
                name: 'stats',
                type: FilterType.Statistics,
                title: 'Stats', className: 'col-lg-3 offset-lg-3 col-md-4',
                stats: [
                    {name: 'Firms', value: this.props.count ? 1 : 0},
                    {
                        name: 'Funds',
                        value: this.props.count || 0,
                        withViewIcon: true,
                        handleIconClick: this.handleEyeClick,
                    },
                ],
                disabled,
            },
        ];
    }

    private handleEyeClick = () => {
        this.props.openModal(RESPONDER_TARGET_FUND_LIST_MODAL, {});
    }
}

export default ResponderTargetFilters;
