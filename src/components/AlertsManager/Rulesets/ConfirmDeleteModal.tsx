import * as React from 'react';
import memoize from 'memoize-one';

import Modal from 'components/Shared/Modals/Modal';
import ButtonDanger from 'components/Shared/Ui/Buttons/ButtonDanger';
import ButtonDefault from 'components/Shared/Ui/Buttons/ButtonDefault';
import AlertDefault from 'components/Shared/Ui/Alerts/AlertDefault';

import { Ruleset } from 'store/AlertsManager/Rulesets/types';

interface Props {
    ruleset: Ruleset;
    isOpen: boolean;
    closeModal: () => void;
    deleteRuleset: () => void;
}

class ConfirmDeleteModal extends React.PureComponent<Props> {
    getButtons = memoize((handleDelete, handleCancel) => [
        (<ButtonDanger buttonLarge={true} handleClick={handleDelete}>Yes, delete</ButtonDanger>),
        (<ButtonDefault buttonLarge={true} inverse={true} handleClick={handleCancel}>Cancel</ButtonDefault>)
    ]);

    render() {
        const { isOpen } = this.props;

        const body = (
            <AlertDefault hasCloseIcon={false}>
                The ruleset <b>{this.props.ruleset.name} </b>
                will be deleted
            </AlertDefault>
        );
        const title = 'Confirm Ruleset Delete';

        return (
            <Modal
                className="ui-modal--confirm"
                isOpen={isOpen}
                toggle={this.props.closeModal}
                title={title}
                body={body}
                buttons={this.getButtons(this.props.deleteRuleset, this.props.closeModal)}
            />
        );
    }
}

export default ConfirmDeleteModal;