import * as React from 'react';
import * as classNames from 'classnames';

import { RULESET_STATUS } from 'store/AlertsManager/Rulesets/constants';
import { RulesetStatus } from 'store/AlertsManager/Rulesets/types';
import { alertIcon, DEFAULT_VALUE_DASH } from 'store/constants';

interface Props {
    value: string;
    changedSinceLastPublish?: boolean;
    defaultValue?: string;
}

const styles = require('./RulesetStatusCell.scss');

const RulesetStatusCell: React.FC<Props> = ({value, changedSinceLastPublish, defaultValue}) => {
    const foundItem = RULESET_STATUS.find(item => item.value === value);
    const enumValue = foundItem ? foundItem.label : defaultValue;
    let statusClassName = value;
    let icon;

    if (value === RulesetStatus.Active && changedSinceLastPublish) {
        icon = (<img src={alertIcon} alt="warning-status" />);
        statusClassName += '_with_changes';
    } else {
        icon = '';
    }

    const className = classNames({
        [styles['ruleset-status']]: true,
        [styles[statusClassName]]: true,
    });

    return (
        <div>
            <span className={className}>{enumValue} {icon}</span>
        </div>
    );
};
RulesetStatusCell.defaultProps = {
    defaultValue: DEFAULT_VALUE_DASH
};

export default RulesetStatusCell;
