import * as React from 'react';

import FilterPanel from 'components/Shared/Filters/FilterPanel';
import { RequestorTargetSectionProps } from 'containers/AlertsManager/Rulesets/RequestorTargetSection';

import { AsyncFilterName, FilterNames, FiltersState } from 'store/Filters/types';
import { REQUESTOR_TARGET_FUND_LIST_MODAL } from 'store/AlertsManager/Rulesets/constants';
import { FilterItem } from 'store/types';
import { DEFAULT_ASYNC_FILTER, FilterType } from 'store/constants';

const styles = require('./TargetFilters.scss');

class RequestorTargetFilters extends React.PureComponent<RequestorTargetSectionProps> {
    static defaultProps = {
        disabled: false
    };

    render() {
        const {disabled} = this.props;

        const filters: FilterItem[] = this.filters;
        return (
            <div className={styles.container}>
                <FilterPanel
                    disabled={disabled}
                    chips={this.props.chips}
                    filters={filters}
                    currentFilter={this.props.currentFilter}
                    handleChange={this.props.handleChange}
                    filterPageName={this.props.filterPageName}
                />
            </div>
        );
    }

    private get filters(): FilterItem[] {
        const {disabled} = this.props;

        return [
            {
                name: 'search',
                type: FilterType.Search,
                title: `Search`,
                className: 'col-lg-9 col-md-8',
                disabled
            },
            {
                name: 'stats',
                type: FilterType.Statistics,
                title: 'Stats', className: 'col-lg-3 col-md-4',
                stats: [
                    {name: 'Firms', value: this.props.firmCount || 0},
                    {
                        name: 'Funds',
                        value: this.props.count || 0,
                        withViewIcon: true,
                        handleIconClick: this.handleEyeClick,
                    },
                ],
                disabled
            },
            {
                name: 'firm_id',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Firms',
                className: 'col-xl-2 col-lg-3 col-md-4',
                choices: this.props.firmFilter.choices,
                choicesCount: this.props.firmFilter.count,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(FilterNames.rulesetRequestorTargetFundList, AsyncFilterName.firm, filter);
                },
                currentFilter: this.props.firmFilter.filter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.firm,
                defaultFilter: DEFAULT_ASYNC_FILTER,
                isFilterValueObject: true,
                disabled
            },
            {
                name: 'credit_officer',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Coverage',
                className: 'col-xl-2 col-lg-3 col-md-4',
                choices: this.props.creditOfficerFilter.choices,
                choicesCount: this.props.creditOfficerFilter.count,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(
                        FilterNames.rulesetRequestorTargetFundList,
                        AsyncFilterName.creditOfficer,
                        filter
                    );
                },
                currentFilter: this.props.creditOfficerFilter.filter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.creditOfficer,
                defaultFilter: DEFAULT_ASYNC_FILTER,
                disabled
            },
            {
                name: 'product',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Product',
                className: 'col-xl-2 col-lg-3 col-md-4',
                choices: this.props.productFilter.choices,
                choicesCount: this.props.productFilter.count,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(FilterNames.rulesetRequestorTargetFundList, AsyncFilterName.product, filter);
                },
                currentFilter: this.props.productFilter.filter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.product,
                defaultFilter: DEFAULT_ASYNC_FILTER,
                disabled
            },
            {
                name: 'strategy',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Strategy',
                className: 'col-xl-2 col-lg-3 col-md-4',
                choices: this.props.strategyFilter.choices,
                choicesCount: this.props.strategyFilter.count,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(FilterNames.rulesetRequestorTargetFundList, AsyncFilterName.strategy, filter);
                },
                currentFilter: this.props.strategyFilter.filter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.strategy,
                defaultFilter: DEFAULT_ASYNC_FILTER,
                disabled
            },
            {
                name: 'portfolio_id',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Portfolio',
                className: 'col-xl-2 col-lg-3 col-md-4',
                choices: this.props.portfolioFilter.choices,
                choicesCount: this.props.portfolioFilter.count,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(
                        FilterNames.rulesetRequestorTargetFundList,
                        AsyncFilterName.portfolio,
                        filter
                    );
                },
                currentFilter: this.props.portfolioFilter.filter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.portfolio,
                defaultFilter: DEFAULT_ASYNC_FILTER,
                isFilterValueObject: true,
                disabled
            },
            {
                name: 'internal_id',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Internal Id',
                className: 'col-xl-2 col-lg-3 col-md-4',
                choices: this.props.internalIdFilter.choices,
                choicesCount: this.props.internalIdFilter.count,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(
                        FilterNames.rulesetRequestorTargetFundList,
                        AsyncFilterName.internalId,
                        filter
                    );
                },
                currentFilter: this.props.internalIdFilter.filter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.internalId,
                defaultFilter: DEFAULT_ASYNC_FILTER,
                disabled
            },
        ];
    }

    private handleEyeClick = () => {
        this.props.openModal(REQUESTOR_TARGET_FUND_LIST_MODAL, {});
    }
}

export default RequestorTargetFilters;
