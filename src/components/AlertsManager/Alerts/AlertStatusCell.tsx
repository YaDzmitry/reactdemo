import * as React from 'react';
import * as classNames from 'classnames';

import { EvaluationResultStatus } from 'store/AlertsManager/Alerts/types';
import { EvaluationResultStatusTitles } from 'store/AlertsManager/Alerts/constants';
import {
    isAlert, isAwaitingData, isNoAlert, isNoRuleset, isPartialData, isPartialDataAlert
} from 'store/AlertsManager/Alerts/helpers';

interface Props {
    value: EvaluationResultStatus;
}

const styles = require('./AlertStatusCell.scss');

const AlertStatusCell: React.FC<Props> = ({value}) => {
    const statusLabel = EvaluationResultStatusTitles.hasOwnProperty(value) ? EvaluationResultStatusTitles[value] : '';
    const className = classNames({
        [styles['alert-status']]: true,
        [styles.alert]: isAlert(value),
        [styles.partialData]: isPartialData(value),
        [styles.partialDataAlert]: isPartialDataAlert(value),
        [styles.noAlert]: isNoAlert(value),
        [styles.noRuleset]: isNoRuleset(value),
        [styles.awaitingData]: isAwaitingData(value),
    });

    return (
        <div><span className={className}>{statusLabel}</span></div>
    );
};

export default AlertStatusCell;
