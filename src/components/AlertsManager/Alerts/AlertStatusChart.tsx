import * as React from 'react';
import { PureComponent } from 'react';
import * as classNames from 'classnames';

import { EvaluationResultStatusTitles } from 'store/AlertsManager/Alerts/constants';
import { AlertStatusItem, EvaluationResultStatus } from 'store/AlertsManager/Alerts/types';
import { FilterDecoratorChange } from 'decorators/FilterDecorator';
import {
    isAlert, isAwaitingData, isNoAlert, isNoRuleset, isPartialData, isPartialDataAlert
} from 'store/AlertsManager/Alerts/helpers';

const styles = require('./AlertStatusChart.scss');

interface Props {
    data: AlertStatusItem[];
    selectedItem: EvaluationResultStatus;
    handleSelect?: FilterDecoratorChange;
}

const notClickable: EvaluationResultStatus[] = [
    EvaluationResultStatus.NoAlert,
    EvaluationResultStatus.NoRuleset,
    EvaluationResultStatus.AwaitingData,
];

export class AlertStatusChart extends PureComponent<Props> {
    handleClick = (key: EvaluationResultStatus) => {
        if (this.props.handleSelect && notClickable.indexOf(key) === -1) {
            this.props.handleSelect({
                alert_status: key !== this.props.selectedItem ? key : ''
            });
        }
    }

    render() {
        let { data, selectedItem } = this.props;
        data = data.filter(item => !!item.count);

        const totalAlertItemsCount = data.reduce(
            (total: number, item: AlertStatusItem) => total + item.count, 0
        );

        const percents = data.map(
            (item: AlertStatusItem) => (item.count / totalAlertItemsCount * 100).toFixed(0)
        );

        return (
            <div className={styles.container}>
                <div className={styles['text-label']}>Alert Status</div>
                <div className={styles['colors-container']}>
                    {data.map((item: AlertStatusItem, key: number) => {
                        const classes = classNames({
                            [styles.noAlert]: isNoAlert(item.status),
                            [styles.alert]: isAlert(item.status),
                            [styles.awaitingData]: isAwaitingData(item.status),
                            [styles.partialData]: isPartialData(item.status),
                            [styles.partialDataAlert]: isPartialDataAlert(item.status),
                            [styles.noRuleset]: isNoRuleset(item.status),
                            [styles.interactive]: notClickable.indexOf(item.status) === -1,
                            [styles.selected]: item.status === selectedItem
                        });

                        return <div
                            key={item.status}
                            className={classes}
                            onClick={() => this.handleClick(item.status)}
                            style={{width: `${percents[key]}%`}}
                        />;
                    }
                    )}
                </div>
                <div className={styles['labels-container']}>
                    {
                        data.map((item: AlertStatusItem, key: number) => {
                            const value = EvaluationResultStatusTitles[item.status] || item.status;
                            const circleClasses = classNames({
                                [styles.noAlert]: isNoAlert(item.status),
                                [styles.alert]: isAlert(item.status),
                                [styles.awaitingData]: isAwaitingData(item.status),
                                [styles.partialData]: isPartialData(item.status),
                                [styles.partialDataAlert]: isPartialDataAlert(item.status),
                                [styles.noRuleset]: isNoRuleset(item.status),
                                [styles.circle]: true
                            });
                            return (
                                <div className={styles['label-item']} key={item.status}>
                                    <span className={circleClasses}/>
                                    <span>
                                        <span className={styles.percents}>{percents[key]}%</span>
                                        {' '}
                                        <span className={styles.status}>
                                            {value}
                                        </span>
                                    </span>
                                </div>
                            );
                        })
                    }
                </div>
            </div>
        );
    }
}

export default AlertStatusChart;