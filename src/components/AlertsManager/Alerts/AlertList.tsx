import * as React from 'react';

import { FilterDecoratorChange } from 'decorators/FilterDecorator';

import DataTable from 'components/Shared/DataTable/DataTable';
import UiDataTable from 'components/Shared/Ui/Tables/UiDataTable';
import FirmFundNamesCell from 'components/Shared/DataTable/Cells/FirmFundNames';

import { AsyncFilterName, Chip, FiltersState } from 'store/Filters/types';
import {
    DEFAULT_ASYNC_FILTER,
    Alignment, CellTypeChoices, FilterType, TableRowActionIcon, TableRowActionType
} from 'store/constants';
import { FilterItem, SelectItem, TableHeader, TableRowAction } from 'store/types';
import { AlertFund } from 'store/AlertsManager/Alerts/types';
import { GetChoicesAction, SetAsyncFiltersAction } from 'store/AsyncFiltersChoices/actions';
import { AccountType } from 'store/Account/types';

import AlertStatusCell from './AlertStatusCell';
import AlertPeriodCell from './AlertPeriodCell';

interface Props {
    userAccountType: AccountType;
    currentFilter: FiltersState;
    filterPageName: string;
    results: AlertFund[];
    count: number;
    alertCount: number;
    handleChange: FilterDecoratorChange;
    isLoading: boolean;
    isGridLoading: boolean;
    chips: Chip[];
    firmChoices: SelectItem[];
    firmCount: number;
    firmFilter: FiltersState;
    portfolioChoices: SelectItem[];
    portfolioCount: number;
    portfolioFilter: FiltersState;
    creditOfficerChoices: SelectItem[];
    creditOfficerCount: number;
    creditOfficerFilter: FiltersState;
    productChoices: SelectItem[];
    productCount: number;
    productFilter: FiltersState;
    strategyChoices: SelectItem[];
    strategyCount: number;
    strategyFilter: FiltersState;
    internalIdChoices: SelectItem[];
    internalIdCount: number;
    internalIdFilter: FiltersState;
    getChoices: (page: string, filterName: string, filter: FiltersState) => GetChoicesAction;
    setAsyncFilters: (filterName: string, filter: FiltersState, pageName: string) => SetAsyncFiltersAction;
}

class AlertList extends React.PureComponent<Props> {
    rowActions: TableRowAction[] = [{
        type: TableRowActionType.NavLink,
        icon: TableRowActionIcon.View,
        link: `/alerts/{id}`,
    }];

    headers: TableHeader[] = [
        {
            name: 'firm_fund',
            title: 'Firm\nFund Name',
            headerAlignment: Alignment.Left,
            active: true,
            cellType: CellTypeChoices.Custom,
            orderingName: 'firm_name,name',
            alias: 'wide-firm-fund',
            transformer: (row: AlertFund) => {
                return (
                    <FirmFundNamesCell
                        firm={row.firm && row.firm.name || ''}
                        fund={row.name || ''}
                        link={`alerts/${row.id}`}
                    />
                );
            }
        }, {
            name: 'evaluation_result.periods.one_month',
            title: '1 Month',
            active: true,
            cellType: CellTypeChoices.Custom,
            valueAlignment: Alignment.Center,
            transformer: (row: AlertFund) => (
                <AlertPeriodCell period={row.evaluation_result.periods.one_month} />
            ),
        }, {
            name: 'evaluation_result.periods.three_months',
            title: '3 Month',
            active: true,
            cellType: CellTypeChoices.Custom,
            valueAlignment: Alignment.Center,
            transformer: (row: AlertFund) => (
                <AlertPeriodCell period={row.evaluation_result.periods.three_months} />
            ),
        }, {
            name: 'evaluation_result.periods.twelve_months',
            title: '12 Months',
            active: true,
            cellType: CellTypeChoices.Custom,
            valueAlignment: Alignment.Center,
            transformer: (row: AlertFund) => (
                <AlertPeriodCell period={row.evaluation_result.periods.twelve_months} />
            ),
        }, {
            name: 'evaluation_result.periods.other',
            title: 'Other',
            active: true,
            cellType: CellTypeChoices.Custom,
            valueAlignment: Alignment.Center,
            transformer: (row: AlertFund) => (
                <AlertPeriodCell period={row.evaluation_result.periods.other} />
            ),
        }, {
            name: 'evaluation_result.status',
            title: 'Status',
            active: true,
            cellType: CellTypeChoices.Custom,
            headerAlignment: Alignment.Left,
            valueAlignment: Alignment.Left,
            transformer: (row: AlertFund) => (
                <AlertStatusCell value={row.evaluation_result.status} />
            ),
        }];

    render() {
        const filters: FilterItem[] = this.getFilters(this.props.userAccountType);

        return (
            <UiDataTable>
                <DataTable
                    rows={this.props.results}
                    count={this.props.count}
                    currentFilter={this.props.currentFilter}
                    headers={this.headers}
                    filters={filters}
                    handleChange={this.props.handleChange}
                    withCheckboxes={false}
                    hasPagination={true}
                    isLoading={this.props.isLoading}
                    isGridLoading={this.props.isGridLoading}
                    hasSorting={false}
                    rowActions={this.rowActions}
                    actionsTitle={' '}
                    useColoredScrollBar={false}
                    filterPageName={this.props.filterPageName}
                    chips={this.props.chips}
                />
            </UiDataTable>
        );
    }

    private getFilters(userType: AccountType) {
        const responderFilters = [
            { name: 'search', type: FilterType.Search, title: 'Search', className: 'col-lg-9 col-sm-8' },
            {
                name: 'stats',
                type: FilterType.Statistics,
                title: 'Stats', className: 'col-lg-3 col-sm-4',
                stats: [
                    {name: 'Alerts', value: this.props.alertCount || 0},
                    {name: 'Unique Funds', value: this.props.count || 0}
                ],
            },
        ];
        if (userType === AccountType.Responder) {
            return responderFilters;
        } else {
            return [
                ...responderFilters,
                {
                    name: 'firm_id',
                    type: FilterType.CreditSelectWithCheckboxes,
                    title: 'Firms',
                    choices: this.props.firmChoices,
                    choicesCount: this.props.firmCount,
                    handleRequest: (filter: FiltersState) => {
                        this.props.getChoices(this.props.filterPageName, AsyncFilterName.firm, filter);
                    },
                    currentFilter: this.props.firmFilter,
                    setFilters: this.props.setAsyncFilters,
                    filterName: AsyncFilterName.firm,
                    defaultFilter: DEFAULT_ASYNC_FILTER,
                    isFilterValueObject: true,
                },
                {
                    name: 'credit_officer',
                    type: FilterType.CreditSelectWithCheckboxes,
                    title: 'Coverage',
                    choices: this.props.creditOfficerChoices,
                    choicesCount: this.props.creditOfficerCount,
                    handleRequest: (filter: FiltersState) => {
                        this.props.getChoices(
                            this.props.filterPageName,
                            AsyncFilterName.creditOfficer,
                            filter
                        );
                    },
                    currentFilter: this.props.creditOfficerFilter,
                    setFilters: this.props.setAsyncFilters,
                    filterName: AsyncFilterName.creditOfficer,
                    defaultFilter: DEFAULT_ASYNC_FILTER,
                },
                {
                    name: 'product',
                    type: FilterType.CreditSelectWithCheckboxes,
                    title: 'Product',
                    choices: this.props.productChoices,
                    choicesCount: this.props.productCount,
                    handleRequest: (filter: FiltersState) => {
                        this.props.getChoices(this.props.filterPageName, AsyncFilterName.product, filter);
                    },
                    currentFilter: this.props.productFilter,
                    setFilters: this.props.setAsyncFilters,
                    filterName: AsyncFilterName.product,
                    defaultFilter: DEFAULT_ASYNC_FILTER,
                },
                {
                    name: 'strategy',
                    type: FilterType.CreditSelectWithCheckboxes,
                    title: 'Strategy',
                    choices: this.props.strategyChoices,
                    choicesCount: this.props.strategyCount,
                    handleRequest: (filter: FiltersState) => {
                        this.props.getChoices(this.props.filterPageName, AsyncFilterName.strategy, filter);
                    },
                    currentFilter: this.props.strategyFilter,
                    setFilters: this.props.setAsyncFilters,
                    filterName: AsyncFilterName.strategy,
                    defaultFilter: DEFAULT_ASYNC_FILTER,
                },
                {
                    name: 'portfolio_id',
                    type: FilterType.CreditSelectWithCheckboxes,
                    title: 'Portfolio',
                    choices: this.props.portfolioChoices,
                    choicesCount: this.props.portfolioCount,
                    handleRequest: (filter: FiltersState) => {
                        this.props.getChoices(
                            this.props.filterPageName,
                            AsyncFilterName.portfolio,
                            filter
                        );
                    },
                    currentFilter: this.props.portfolioFilter,
                    setFilters: this.props.setAsyncFilters,
                    filterName: AsyncFilterName.portfolio,
                    defaultFilter: DEFAULT_ASYNC_FILTER,
                    isFilterValueObject: true,
                },
                {
                    name: 'internal_id',
                    type: FilterType.CreditSelectWithCheckboxes,
                    title: 'Internal Id',
                    choices: this.props.internalIdChoices,
                    choicesCount: this.props.internalIdCount,
                    handleRequest: (filter: FiltersState) => {
                        this.props.getChoices(
                            this.props.filterPageName,
                            AsyncFilterName.internalId,
                            filter
                        );
                    },
                    currentFilter: this.props.internalIdFilter,
                    setFilters: this.props.setAsyncFilters,
                    filterName: AsyncFilterName.internalId,
                    defaultFilter: DEFAULT_ASYNC_FILTER,
                },
            ];
        }
    }

}

export default AlertList;
