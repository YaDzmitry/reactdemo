import * as React from 'react';
import memoize from 'memoize-one';

import { FilterDecoratorChange } from 'decorators/FilterDecorator';
import { AlertFund, EvaluationResult, FundEvaluationResults } from 'store/AlertsManager/Alerts/types';
import { Id, SelectItem, TableHeader } from 'store/types';
import {
    DEFAULT_ASYNC_COUNTERPARTY_FILTER, DEFAULT_ASYNC_RULESET_NAME_FILTER, DEFAULT_VALUE_DASH,
    Alignment, CellTypeChoices, FilterType, FormatTypeChoices
} from 'store/constants';
import { RuleExclusionsTitles, RuleTypeTitles } from 'store/AlertsManager/Rulesets/constants';
import { AsyncFilterName, Chip, FilterNames, FiltersState } from 'store/Filters/types';

import DataTable from 'components/Shared/DataTable/DataTable';
import UiDataTable from 'components/Shared/Ui/Tables/UiDataTable';
import ButtonPrimary from 'components/Shared/Ui/Buttons/ButtonPrimary';
import DangerCell from 'components/Shared/DataTable/Cells/Danger';
import DynamicFormatCurrency from 'components/DataManager/TableCells/DynamicFormatCurrency';

import StatisticBlock from './StatisticBlock';
import { GetChoicesByFundAction } from 'store/AsyncFiltersChoices/actions';
import { ALERT_STATUS_CHOICES } from 'store/AlertsManager/Alerts/constants';
import {
    getPeriodAlertColumn, isAmountHighlighted, isPerfShareHighlighted, isThresholdHighlighted
} from 'store/AlertsManager/Alerts/helpers';
import { formatText } from 'helpers/tableHelper';
import { isRequestor } from 'store/User/helpers';
import ActiveUser from 'store/Auth/models/activeUser';
import AlertStatusCell from './AlertStatusCell';

interface Props {
    user: ActiveUser;
    statistic: AlertFund;
    evaluationResults: FundEvaluationResults;

    handleChange: FilterDecoratorChange;
    filterPageName: string;
    chips: Chip[];
    currentFilter: FiltersState;

    rulesetNameChoices: SelectItem[];
    rulesetNameCount: number;
    rulesetNameFilter: FiltersState;

    counterpartyChoices: SelectItem[];
    counterpartyCount: number;
    counterpartyFilter: FiltersState;

    isLoading: boolean;
    isGridLoading: boolean;
    setAsyncFilters: (filterName: string, filter: FiltersState, pageName: string) => void;
    getChoicesByFund: (page: string, filterName: string, filter: FiltersState, fundId: Id) => GetChoicesByFundAction;
    createRuleset: () => void;
    fundId: Id;
    period: string;
}

class FundAlertsView extends React.PureComponent<Props> {

    buttons = [(
        <ButtonPrimary className="float-right ml-3" inverse={false} handleClick={this.props.createRuleset} >
            New Ruleset
        </ButtonPrimary>
    )];

    getHeaders = memoize((statistic, isUserRequestor) => {
        let headers: TableHeader[] = [{
            name: 'ruleset_name',
            title: 'Ruleset Name',
            active: true,
            headerAlignment: Alignment.Left,
        }, {
            name: 'alert_status',
            title: 'Alert Status',
            active: true,
            headerAlignment: Alignment.Left,
            cellType: CellTypeChoices.Custom,
            transformer: (row: EvaluationResult) => (
                <AlertStatusCell value={row.alert_status} />
            ),
            excludeSorting: true,
        }, {
            name: 'counterparty',
            title: 'Counterparty',
            active: true,
            headerAlignment: Alignment.Left,
            excludeSorting: true,
        }, {
            name: 'period',
            title: 'Period',
            active: true,
            headerAlignment: Alignment.Left,
            cellType: CellTypeChoices.Custom,
            transformer: (row: EvaluationResult) => {
                return getPeriodAlertColumn(row.period, row.rule.period);
            },
            excludeSorting: true,
        }, {
            name: 'rule.threshold',
            title: 'Threshold',
            active: true,
            headerAlignment: Alignment.Left,
            cellType: CellTypeChoices.Custom,
            transformer: (row: EvaluationResult) => {
                const value = formatText(row.rule.threshold, FormatTypeChoices.NumeralDefault, DEFAULT_VALUE_DASH);
                return isThresholdHighlighted(row.field, row.rule.threshold)
                    ? <DangerCell value={value}/>
                    : <span>{value}</span>;
            },
            excludeSorting: true,
        }, {
            name: 'rule.perf_share',
            title: 'Perf. Share',
            active: true,
            headerAlignment: Alignment.Left,
            cellType: CellTypeChoices.Custom,
            transformer: (row: EvaluationResult) => {
                const value = formatText(row.rule.perf_share, FormatTypeChoices.NumeralDefault, DEFAULT_VALUE_DASH);
                return isPerfShareHighlighted(row.field)
                    ? <DangerCell value={value}/>
                    : <span>{value}</span>;
            },
            excludeSorting: true,
        }, {
            name: 'rule.type',
            title: 'Type',
            active: true,
            headerAlignment: Alignment.Left,
            cellType: CellTypeChoices.Custom,
            transformer: (row: EvaluationResult) => row.rule.type ? RuleTypeTitles[row.rule.type] : DEFAULT_VALUE_DASH,
            excludeSorting: true,
        }, {
            name: 'exclusions',
            title: 'Exclusions',
            active: true,
            headerAlignment: Alignment.Left,
            cellType: CellTypeChoices.Custom,
            transformer: (row: EvaluationResult) => row.rule.exclusions
                ? RuleExclusionsTitles[row.rule.exclusions]
                : DEFAULT_VALUE_DASH,
            excludeSorting: true,
        }, {
            name: 'rule.amount',
            title: 'Amount',
            active: true,
            headerAlignment: Alignment.Left,
            cellType: CellTypeChoices.Custom,
            transformer: (row: EvaluationResult) => {
                const value = (
                    <DynamicFormatCurrency
                        row={row}
                        valueField="rule.amount"
                        currencyValue={statistic ? statistic.currency : ''}
                        defaultValue={DEFAULT_VALUE_DASH}
                    />
                );
                return isAmountHighlighted(row.field) ? <DangerCell value={value}/> : <span>{value}</span>;
            },
            excludeSorting: true,
        }];

        if (isUserRequestor) {
            headers = headers.filter((headerItem: TableHeader) => headerItem.name !== 'counterparty');
            return headers;
        }
        return headers;

    });

    render() {
        const {
            statistic, evaluationResults, chips,
            currentFilter, filterPageName, isLoading, isGridLoading, handleChange, user
        } = this.props;

        return (
            <>
                <StatisticBlock statistic={statistic} user={user}/>
                <UiDataTable>
                    <DataTable
                        rows={evaluationResults && evaluationResults.results || []}
                        count={evaluationResults && evaluationResults.count || 0}
                        currentFilter={currentFilter}
                        headers={this.getHeaders(this.props.statistic, isRequestor(this.props.user))}
                        filters={this.filters}
                        handleChange={handleChange}
                        withCheckboxes={false}
                        hasPagination={true}
                        isLoading={isLoading}
                        isGridLoading={isGridLoading}
                        hasSorting={true}
                        buttons={this.buttons}
                        filterPageName={filterPageName}
                        chips={chips}
                    />
                </UiDataTable>
            </>
        );
    }

    private get filters() {
        const requestorFilters = [
            {
                name: 'alert_status',
                type: FilterType.CreditSelectWithCheckboxesSync,
                title: 'Alert Status',
                choices: ALERT_STATUS_CHOICES,
                choicesCount: ALERT_STATUS_CHOICES.length,
                visible: true,
            },
            {
                name: 'ruleset_name',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Ruleset Name',
                className: 'col-xl-2 col-lg-3 col-md-4',
                choices: this.props.rulesetNameChoices,
                choicesCount: this.props.rulesetNameCount,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoicesByFund(
                        FilterNames.alertView,
                        AsyncFilterName.rulesetName,
                        {...filter, period: this.props.period},
                        this.props.fundId,
                    );
                },
                currentFilter: this.props.rulesetNameFilter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.rulesetName,
                defaultFilter: DEFAULT_ASYNC_RULESET_NAME_FILTER,
                isFilterValueObject: true,
            },
        ];
        if (isRequestor(this.props.user)) {
            return requestorFilters;
        } else {
            return [
                ...requestorFilters,
                {
                    name: 'counterparty',
                    type: FilterType.CreditSelectWithCheckboxes,
                    title: 'Counterparty',
                    className: 'col-xl-2 col-lg-3 col-md-4',
                    choices: this.props.counterpartyChoices,
                    choicesCount: this.props.counterpartyCount,
                    handleRequest: (filter: FiltersState) => {
                        this.props.getChoicesByFund(
                            FilterNames.alertView,
                            AsyncFilterName.counterparty,
                            {...filter, period: this.props.period},
                            this.props.fundId
                        );
                    },
                    currentFilter: this.props.counterpartyFilter,
                    setFilters: this.props.setAsyncFilters,
                    filterName: AsyncFilterName.counterparty,
                    defaultFilter: DEFAULT_ASYNC_COUNTERPARTY_FILTER,
                    isFilterValueObject: true,
                }
            ];
        }
    }
}

export default FundAlertsView;
