import * as React from 'react';

import MainLayout from 'components/Shared/Layouts/MainLayout';

import { AlertPageProps } from 'containers/AlertsManager/Alerts/AlertPage';
import { FilterType } from 'store/constants';
import { FilterItem } from 'store/types';

class AlertPage extends React.PureComponent<AlertPageProps> {

    static headerFilters: FilterItem[] = [
        {
            name: 'period',
            type: FilterType.SingleDate,
            title: 'Start Date',
            material: true,
            customInputIcon: true,
            className: `ui-material`,
            withLabel: false,
        },
    ];

    render() {
        const { handleChange, currentFilter, menuItems } = this.props;
        return (
            <MainLayout
                title={`Alerts`}
                menuItems={menuItems}
                filters={AlertPage.headerFilters}
                handleChange={handleChange}
                currentFilter={currentFilter}
            >
                {this.props.children}
            </MainLayout>
        );
    }

}

export default AlertPage;
