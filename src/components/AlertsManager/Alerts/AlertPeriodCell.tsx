import * as React from 'react';
import * as classNames from 'classnames';

import { EvaluationPeriod, EvaluationResultStatus } from 'store/AlertsManager/Alerts/types';
import {
    isAlert, isAwaitingData, isNoAlert, isNoRuleset, isPartialData, isPartialDataAlert
} from 'store/AlertsManager/Alerts/helpers';

interface Props {
    period?: EvaluationPeriod;
    size?: 'sm' | 'lg';
}

const styles = require('./AlertPeriodCell.scss');

const AlertPeriodCell: React.FC<Props> = ({period, size}) => {
    const periodStatus = period && period.status || EvaluationResultStatus.NoRuleset;
    const className = classNames({
        [styles['alert-period']]: true,
        [styles['small-cell']]: size && size === 'sm',
        [styles.noAlert]: isNoAlert(periodStatus),
        [styles.alert]: isAlert(periodStatus),
        [styles.awaitingData]: isAwaitingData(periodStatus),
        [styles.partialData]: isPartialData(periodStatus),
        [styles.partialDataAlert]: isPartialDataAlert(periodStatus),
        [styles.noRuleset]: isNoRuleset(periodStatus),
    });

    return (
        <div>
            <span className={className}>
                {period && period.evaluation_warnings || 0}/{period && period.count || 0}
            </span>
        </div>
    );
};
AlertPeriodCell.defaultProps = {
    size: 'lg',
};

export default AlertPeriodCell;
