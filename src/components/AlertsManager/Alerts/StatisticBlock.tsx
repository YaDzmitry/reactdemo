import * as React from 'react';
import * as classNames from 'classnames';
import { NavLink } from 'react-router-dom';

import { viewIcon } from 'store/constants';
import { AlertFund, EvaluationPeriod, EvaluationResultStatus } from 'store/AlertsManager/Alerts/types';
import { EvaluationResultStatusTitles } from 'store/AlertsManager/Alerts/constants';
import StatisticColumn from './StatisticColumn';
import {
    isAlert, isAwaitingData, isNoAlert, isNoRuleset, isPartialData, isPartialDataAlert
} from 'store/AlertsManager/Alerts/helpers';
import { RulePeriod } from 'store/AlertsManager/Rulesets/types';
import ActiveUser from 'store/Auth/models/activeUser';
import { isRequestor } from 'store/User/helpers';

const styles = require('./StatisticBlock.scss');

interface Props {
    statistic: AlertFund;
    user: ActiveUser;
}

const StatisticBlock: React.FC<Props> = ({statistic: stat, user}) => {

    const periodTitles = {
        [RulePeriod.OneMonth]: '1 Month',
        [RulePeriod.ThreeMonths]: '3 Months',
        [RulePeriod.TwelveMonths]: '12 Months',
        [RulePeriod.Other]: 'Other',
    };
    if (!stat) {
        return null;
    }

    const status = stat.evaluation_result.status || EvaluationResultStatus.NoAlert;
    const statusClassName = classNames({
        [styles.noAlert]: isNoAlert(status),
        [styles.alert]: isAlert(status),
        [styles.partialData]: isPartialData(status),
        [styles.partialDataAlert]: isPartialDataAlert(status),
        [styles.noRuleset]: isNoRuleset(status),
        [styles.awaitingData]: isAwaitingData(status),
        [styles.status]: true
    });

    const periods = stat.evaluation_result && stat.evaluation_result.periods ? stat.evaluation_result.periods : {};
    const linkToProfile = isRequestor(user) ? `/funds/${stat.id}` : `/funds/${stat.id}/profile`;
    return (
        <>
            <div className={styles.container}>
                <div className={`${styles.firmFund}`}>
                    <div className={styles.fund}>
                        {stat.firm && stat.firm.name || ''}
                    </div>
                    <div className={styles.firm}>
                        <span>{stat.name || ''}</span>
                        <NavLink to={linkToProfile} className={styles['stat-icon']}>
                            <img src={viewIcon} alt={'view'}/>
                        </NavLink>
                    </div>
                </div>
                {
                    Object.keys(periodTitles).map((rulePeriod: RulePeriod, idx: number) => {
                        let period: EvaluationPeriod = {
                            name: rulePeriod,
                            count: 0,
                            evaluation_warnings: 0,
                            status: EvaluationResultStatus.NoRuleset,
                        };
                        if (periods && periods.hasOwnProperty(rulePeriod)) {
                            period = periods[rulePeriod];
                        }
                        return (
                            <div key={idx} className={styles.col}>
                                <StatisticColumn period={period} />
                            </div>
                        );
                    })
                }
                <div className={`${styles.col} ${statusClassName}`}>
                    {EvaluationResultStatusTitles[status]}
                </div>
            </div>
        </>
    );
};

export default StatisticBlock;
