import * as React from 'react';
import * as classNames from 'classnames';

import { EvaluationPeriod } from 'store/AlertsManager/Alerts/types';
import { RulePeriodTitles } from 'store/AlertsManager/Rulesets/constants';
import {
    isAlert, isAwaitingData, isNoAlert, isNoRuleset, isPartialData,
    isPartialDataAlert
} from 'store/AlertsManager/Alerts/helpers';

const styles = require('./StatisticColumn.scss');

interface Props {
    className?: string;
    period: EvaluationPeriod;
}

const StatisticColumn: React.FC<Props> = ({className = '', period}) => {
    const periodStatus = period.status;

    const statusClassName = classNames({
        [styles.noAlert]: isNoAlert(periodStatus),
        [styles.alert]: isAlert(periodStatus),
        [styles.partialData]: isPartialData(periodStatus),
        [styles.awaitingData]: isAwaitingData(periodStatus),
        [styles.partialDataAlert]: isPartialDataAlert(periodStatus),
        [styles.noRuleset]: isNoRuleset(periodStatus),
        [styles.statistic]: true
    });

    return (
        <div className={`${className} ${styles.container}`}>
            <div>{RulePeriodTitles[period.name]}</div>
            <div className={statusClassName}>
                {period.evaluation_warnings}/{period.count}
            </div>
            <div>Alerts</div>
        </div>
    );
};

export default StatisticColumn;
