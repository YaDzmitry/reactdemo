import * as React from 'react';

import { UserAccountRoles, UserAccountStatuses } from 'store/Auth/constants';
import { UserAccount } from 'store/Auth/models/userAccount';
import { messages } from 'store/Account/constants';

import ButtonPrimary from 'components/Shared/Ui/Buttons/ButtonPrimary';
import EditUserForm from 'components/Account/EditUserForm';
import Modal from 'components/Shared/Modals/Modal';

interface Props {
    isOpen: boolean;
    userAccount: UserAccount;

    toggle: () => void;
    save: () => void;
    change: (field: string, value: string) => void;
}

class EditUserModal extends React.PureComponent<Props> {
    render() {
        const {isOpen, userAccount, toggle, change, save} = this.props;
        const body = (
            <EditUserForm
                userAccount={userAccount}
                statuses={UserAccountStatuses}
                roles={UserAccountRoles}
                changeUserAccount={change}
            />
        );
        const buttons = [
            (<ButtonPrimary buttonLarge={true} handleClick={save}>Save</ButtonPrimary>),
            (<ButtonPrimary buttonLarge={true} inverse={true} handleClick={toggle}>Cancel</ButtonPrimary>)
        ];
        return (
            <Modal
                isOpen={isOpen}
                toggle={toggle}
                title={messages.EDIT_USER_TITLE}
                body={body}
                buttons={buttons}
            />
        );
    }
}

export default EditUserModal;