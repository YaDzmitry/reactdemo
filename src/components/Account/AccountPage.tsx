import * as React from 'react';
import { NavigationButton } from '../../store/types';
import MainLayout from '../Shared/Layouts/MainLayout';
import ButtonPrimary from '../Shared/Ui/Buttons/ButtonPrimary';

const styles = require('./AccountPage.scss');

interface Props {
    menuItems: NavigationButton[];
    inviteUser: () => void;
}

const AccountPage: React.FC<Props> = ({menuItems, inviteUser, children}) => (
    <MainLayout menuItems={menuItems}>
        <div className={styles.topPanel}>
            <ButtonPrimary className="float-right ml-3" handleClick={inviteUser} inverse={true}>
                Invite new user...
            </ButtonPrimary>
            <div className="clearfix" />
        </div>
        <div className={styles.content}>
            {children}
        </div>
    </MainLayout>
);

export default AccountPage;
