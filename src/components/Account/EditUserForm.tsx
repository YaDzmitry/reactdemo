import * as React from 'react';
import { Form, FormGroup, Label, Col, Input } from 'reactstrap';

import { UserAccount } from 'store/Auth/models/userAccount';
import { SelectItem } from 'store/types';

interface Props {
    userAccount: UserAccount;
    statuses: SelectItem[];
    roles: SelectItem[];
    changeUserAccount: (field: string, value: string) => void;
}

const EditUserForm: React.FC<Props> = ({userAccount, statuses, roles, changeUserAccount}) => {
    return (
        <Form>
            <FormGroup row={true}>
                <Label for="status" sm={2}>Status</Label>
                <Col sm={10}>
                    <Input
                        type="select"
                        id="status"
                        value={userAccount.status}
                        onChange={event => changeUserAccount('status', event.target.value)}
                    >
                        {statuses.map((status, idx) => (
                            <option key={status.value} value={status.value}>
                                {status.label}
                            </option>
                        ))}
                    </Input>
                </Col>
            </FormGroup>

            <FormGroup row={true}>
                <Label for="role" sm={2}>Role</Label>
                <Col sm={10}>
                    <Input
                        type="select"
                        id="role"
                        value={userAccount.role}
                        onChange={event => changeUserAccount('role', event.target.value)}
                    >
                        {roles.map((role, idx) => (
                            <option key={role.value} value={role.value}>
                                {role.label}
                            </option>
                        ))}
                    </Input>
                </Col>
            </FormGroup>
        </Form>
    );
};

export default EditUserForm;
