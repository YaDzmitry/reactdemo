import MockAdapter from 'axios-mock-adapter';

import mockRequests from './json/requests';

export default (mock: MockAdapter) => {
    mock.onGet(/\/requests\/$/).reply(() => {
        return [200, mockRequests.list];
    });
};
