import { AlertFund, AlertStatusItem, EvaluationResultStatus } from 'store/AlertsManager/Alerts/types';
import { RulePeriod } from 'store/AlertsManager/Rulesets/types';

export const alertStatuses: AlertStatusItem[] = [{
    status: EvaluationResultStatus.NoAlert,
    count: 30,
}, {
    status: EvaluationResultStatus.PartialData,
    count: 25,
}, {
    status: EvaluationResultStatus.PartialDataAlert,
    count: 20,
}, {
    status: EvaluationResultStatus.Alert,
    count: 15,
}, {
    status: EvaluationResultStatus.NoRuleset,
    count: 5,
}, {
    status: EvaluationResultStatus.AwaitingData,
    count: 5,
}];

export const alertList: AlertFund[] = [
    {
        id: 'b112531f-cad0-494a-8a08-cc80c46ff2ba',
        name: 'DIRECT LENDING FUND (CO-INVEST A) LP 1',
        firm: {
            id: '62e930cf-1ca3-4f37-b9ce-911b9feeb4a5',
            name: 'BlueBay Asset Management',
        },
        evaluation_result: {
            status: EvaluationResultStatus.PartialDataAlert,
            periods: {
                one_month: {
                    name: RulePeriod.OneMonth,
                    count: 5,
                    evaluation_warnings: 0,
                    status: EvaluationResultStatus.NoAlert,
                },
                three_months: {
                    name: RulePeriod.ThreeMonths,
                    count: 5,
                    evaluation_warnings: 0,
                    status: EvaluationResultStatus.NoAlert,
                },
                twelve_months: {
                    name: RulePeriod.TwelveMonths,
                    count: 5,
                    evaluation_warnings: 1,
                    status: EvaluationResultStatus.PartialDataAlert,
                },
                other: {
                    name: RulePeriod.ITD,
                    count: 0,
                    evaluation_warnings: 0,
                    status: EvaluationResultStatus.NoAlert,
                },
            }
        }
    },
    {
        id: 'fc1d466b-9114-4371-9038-9ce77976f2',
        name: 'DIRECT LENDING FUND (CO-INVEST A) LP 2',
        firm: {
            id: '625e95c2-22f2-403e-920b-16e2cce5aa2e',
            name: 'BlueBay Asset Management',
        },
        evaluation_result: {
            status: EvaluationResultStatus.Alert,
            periods: {
                one_month: {
                    name: RulePeriod.OneMonth,
                    count: 5,
                    evaluation_warnings: 0,
                    status: EvaluationResultStatus.NoAlert,
                },
                three_months: {
                    name: RulePeriod.ThreeMonths,
                    count: 5,
                    evaluation_warnings: 1,
                    status: EvaluationResultStatus.AwaitingData,
                },
                twelve_months: {
                    name: RulePeriod.TwelveMonths,
                    count: 5,
                    evaluation_warnings: 1,
                    status: EvaluationResultStatus.PartialData,
                },
                other: {
                    name: RulePeriod.ITD,
                    count: 1,
                    evaluation_warnings: 1,
                    status: EvaluationResultStatus.Alert,
                },
            }
        }
    },
    {
        id: '1f310961-c301-4de1-b01b-6464fe2e60ab',
        name: 'DIRECT LENDING FUND (CO-INVEST A) LP 3',
        firm: {
            id: 'd38561f0-5866-4cdd-a037-c6d2cc233124',
            name: 'BlueBay Asset Management',
        },
        evaluation_result: {
            status: EvaluationResultStatus.PartialData,
            periods: {
                one_month: {
                    name: RulePeriod.OneMonth,
                    count: 5,
                    evaluation_warnings: 1,
                    status: EvaluationResultStatus.PartialData,
                },
                three_months: {
                    name: RulePeriod.ThreeMonths,
                    count: 5,
                    evaluation_warnings: 0,
                    status: EvaluationResultStatus.NoAlert,
                },
                twelve_months: {
                    name: RulePeriod.TwelveMonths,
                    count: 5,
                    evaluation_warnings: 0,
                    status: EvaluationResultStatus.NoAlert,
                },
                other: {
                    name: RulePeriod.ITD,
                    count: 2,
                    evaluation_warnings: 0,
                    status: EvaluationResultStatus.NoAlert,
                },
            }
        }
    },
];