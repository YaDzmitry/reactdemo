import { AlertFund, EvaluationResultStatus, FundEvaluationResults, AlertField } from 'store/AlertsManager/Alerts/types';
import { RuleExclusion, RulePeriod, RuleType } from 'store/AlertsManager/Rulesets/types';

export const fundStatistic: AlertFund = {
    id: 'fund-1234567890',
    name: 'Strategic Income Opportunities Fund',
    currency: 'USD',
    firm: {
        id: 'firm-1234567890',
        name: 'Blackrock'
    },
    evaluation_result: {
        status: EvaluationResultStatus.Alert,
        periods: {
            one_month: {
                name: RulePeriod.OneMonth,
                count: 5,
                evaluation_warnings: 2,
                status: EvaluationResultStatus.Alert,
            },
            three_months: {
                name: RulePeriod.ThreeMonths,
                count: 3,
                evaluation_warnings: 0,
                status: EvaluationResultStatus.NoAlert,
            },
            twelve_months: {
                name: RulePeriod.TwelveMonths,
                count: 5,
                evaluation_warnings: 2,
                status: EvaluationResultStatus.PartialDataAlert,
            },
            other: {
                name: RulePeriod.ITD,
                count: 5,
                evaluation_warnings: 2,
                status: EvaluationResultStatus.Alert,
            },
        },
    }
};

export const fundRuleEvaluationResults: FundEvaluationResults = {
    count: 3,
    results: [
        {
            alert_status: EvaluationResultStatus.Alert,
            field: AlertField.PerfShare,
            ruleset_name: 'Ruleset Name 1',
            counterparty: 'counterparty 3',
            period: '2018-08-27',
            rule: {
                id: 'rule_1',
                period: RulePeriod.OneMonth,
                threshold: 4,
                type: RuleType.Perf,
                amount: 235523.32,
                exclusions: RuleExclusion.Redemptions,
                perf_share: 6
            },
        }, {
            alert_status: EvaluationResultStatus.NoAlert,
            field: AlertField.ThresholdAndPerfShare,
            ruleset_name: 'Ruleset Name 2',
            counterparty: 'counterparty 1',
            period: '2018-08-27',
            rule: {
                id: 'rule_2',
                period: RulePeriod.TwelveMonths,
                threshold: 7,
                type: RuleType.PeakBalance,
                amount: -1234241.12,
                exclusions: RuleExclusion.NetFlow,
                perf_share: 3.45
            },
        }, {
            alert_status: EvaluationResultStatus.Alert,
            field: AlertField.Amount,
            ruleset_name: 'Ruleset Name 2',
            counterparty: 'counterparty 2',
            period: '2018-08-27',
            rule: {
                id: 'rule_3',
                period: RulePeriod.ITD,
                threshold: 44,
                type: RuleType.Floor,
                amount: 23.45,
                exclusions: RuleExclusion.None,
                perf_share: 87.34
            },
        }
    ]
};