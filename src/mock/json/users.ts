import { AccountType } from 'store/Account/types';
import { UserGlobalRole } from 'store/Auth/types';

export default {
    info: {
        id: '1',
        email: 'test_mock@mail.ru',
        first_name: 'First',
        last_name: 'Last',
        type: 'standard',
        status: 'active',
        last_login: '2018-03-01T10:29:42.350383Z',
        created_at: '2018-02-26T08:08:11.846049Z',
        account_relations: [
            {
                id: '1',
                account: {
                    id: '1',
                    name: 'Account #1',
                    status: 'test',
                    type: AccountType.Requestor,
                    account_settings: {
                        display_fund_data_source: false
                    },
                    diligence_id: '',
                },
                status: 'active',
                role: 'account_admin',
                created_at: '2018-02-28T10:43:31.778000Z',
                user_account_settings: {
                    display_fund_data_source: false
                }
            }, {
                id: '2',
                account: {
                    id: '2',
                    name: 'Account #2',
                    status: 'test',
                    type: AccountType.Requestor,
                    account_settings: {
                        display_fund_data_source: false
                    },
                    diligence_id: '',
                },
                status: 'active',
                role: 'user',
                created_at: '2018-02-28T10:43:31.778000Z',
                user_account_settings: {
                    display_fund_data_source: true
                }
            }, {
                id: '3',
                account: {
                    id: '3',
                    name: 'Account #3',
                    status: 'test',
                    type: AccountType.Requestor,
                    account_settings: {
                        display_fund_data_source: false
                    },
                    diligence_id: '',
                },
                status: 'active',
                role: 'account_admin',
                created_at: '2018-02-28T10:43:31.778000Z',
                user_account_settings: {
                    display_fund_data_source: false
                }
            }
        ],
        active_account: {
            id: '1',
            account: {
                id: '1',
                name: 'Account #1',
                status: 'test',
                type: AccountType.Requestor,
                account_settings: {
                    display_fund_data_source: false
                },
                diligence_id: '',
            },
            status: 'active',
            role: 'account_admin',
            created_at: '2018-02-27T22:12:17.971000Z',
            user_account_settings: {
                display_fund_data_source: false
            }
        },
        is_internal: true,
        global_role: UserGlobalRole.None,
        has_diligence_account: false,
    }
};