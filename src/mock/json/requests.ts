export default {
    list: {
        'count': 6,
        'next': null,
        'previous': null,
        'results': [
            {
                'id': '1',
                'name': 'Direct Lending Fund (Co-InvestA) LP',
                'firm': {
                    'id': '10',
                    'name': 'BlueBay Asset Management'
                },
                'request': {
                    'requesting_account': {
                        'id': '1',
                        'status': 'client',
                        'type': 'requestor',
                        'name': 'Account #1'
                    },
                    'responding_account': {
                        'id': '2',
                        'status': 'client',
                        'type': 'responder',
                        'name': 'Account #2'
                    },
                    'due_date_unit': 5,
                    'due_date_unit_type': 'calendar',
                    'frequency': 'monthly',
                    'data_start_date': '',
                    'send_date': '',
                    'status': 'pending',
                    'type': 'recurring',
                    'acceptance_type': 'accepted',
                    'rejection_message': 'Rejection message',

                    'requested_fields': [
                        {'id': '1', 'name': 'beginning_balance'},
                        {'id': '2', 'name': 'ending_balance'},
                        {'id': '3', 'name': 'nav_per_share'}
                    ],
                    'fund_contacts': [
                        {
                            'id': '1',
                            'first_name': 'Petr',
                            'last_name': 'Petrov',
                            'email': 'petr.petrov@gmail.com'
                        }, {
                            'id': '2',
                            'first_name': 'Sidor',
                            'last_name': 'Sidorov',
                            'email': 'sidor.sidorov@gmail.com'
                        }, {
                            'id': '3',
                            'first_name': 'Oleg',
                            'last_name': 'Olegov',
                            'email': 'oleg.olegov@gmail.com'
                        }
                    ]
                }
            },
            {
                'id': '2',
                'name': 'Direct Lending Fund I, LP',
                'firm': {
                    'id': '20',
                    'name': 'BlueBay Asset Management'
                },
                'request': {
                    'requesting_account': {
                        'id': '3',
                        'status': 'test',
                        'type': 'requestor',
                        'name': 'Account Requestor'
                    },
                    'responding_account': {
                        'id': '33',
                        'status': 'test',
                        'type': 'responder',
                        'name': 'Account Responder'
                    },
                    'due_date_unit': 10,
                    'due_date_unit_type': 'business',
                    'frequency': 'monthly',
                    'data_start_date': '',
                    'send_date': '',
                    'status': 'active',
                    'type': 'recurring',
                    'acceptance_type': 'accepted',
                    'rejection_message': 'Rejection message',
                    'requested_fields': [
                        {'id': '3', 'name': 'perf_month'},
                        {'id': '4', 'name': 'perf_mtd'},
                        {'id': '5', 'name': 'perf_ytd'},
                        {'id': '6', 'name': 'redemptions'}
                    ],
                    'fund_contacts': [
                    ]
                }
            },
            {
                'id': '3',
                'name': 'Direct Lending Fund II SP',
                'firm': {
                    'id': '30',
                    'name': 'BlueBay Asset Management'
                },
                'request': {
                    'requesting_account': {
                        'id': '3',
                        'status': 'test',
                        'type': 'requestor',
                        'name': 'Account Requestor'
                    },
                    'responding_account': {
                        'id': '40',
                        'status': 'test',
                        'type': 'responder',
                        'name': 'Account #10'
                    },
                    'due_date_unit': 21,
                    'due_date_unit_type': 'business',
                    'frequency': 'monthly',
                    'data_start_date': '',
                    'send_date': '',
                    'status': 'inactive',
                    'type': 'recurring',
                    'acceptance_type': 'accepted',
                    'rejection_message': 'Rejection message',
                    'requested_fields': [
                        {'id': '3', 'name': 'perf_month'}
                    ],
                    'fund_contacts': [
                        {
                            'id': '1',
                            'first_name': 'Petr',
                            'last_name': 'Petrov',
                            'email': 'petr.petrov@gmail.com'
                        }, {
                            'id': '2',
                            'first_name': 'Sidor',
                            'last_name': 'Sidorov',
                            'email': 'sidor.sidorov@gmail.com'
                        }, {
                            'id': '3',
                            'first_name': 'Oleg',
                            'last_name': 'Olegov',
                            'email': 'oleg.olegov@gmail.com'
                        }
                    ]
                }
            },
            {
                'id': '4',
                'name': 'Global Fund SPC - Total Return Credit SP',
                'firm': {
                    'id': '40',
                    'name': 'BlueBay Asset Management'
                },
                'request': {
                    'requesting_account': {
                        'id': '3',
                        'status': 'test',
                        'type': 'requestor',
                        'name': 'Account Requestor'
                    },
                    'responding_account': {
                        'id': '51',
                        'status': 'test',
                        'type': 'responder',
                        'name': 'Account #51'
                    },
                    'due_date_unit': 24,
                    'due_date_unit_type': 'calendar',
                    'frequency': 'monthly',
                    'data_start_date': '',
                    'send_date': '',
                    'status': 'sending',
                    'type': 'recurring',
                    'acceptance_type': 'accepted',
                    'rejection_message': 'Rejection message',
                    'requested_fields': [
                        {'id': '1', 'name': 'beginning_balance'},
                        {'id': '2', 'name': 'subscriptions'},
                        {'id': '3', 'name': 'income'},
                        {'id': '4', 'name': 'perf_weekly'},
                        {'id': '5', 'name': 'perf_month'},
                        {'id': '6', 'name': 'perf_ytd'},
                    ],
                    'fund_contacts': [
                        {
                            'id': '1',
                            'first_name': 'Petr',
                            'last_name': 'Petrov',
                            'email': 'petr.petrov@gmail.com'
                        }, {
                            'id': '2',
                            'first_name': 'Sidor',
                            'last_name': 'Sidorov',
                            'email': 'sidor.sidorov@gmail.com'
                        }
                    ]
                }
            },
            {
                'id': '5',
                'name': 'Direct Lending Fund (Co-InvestA) LP',
                'firm': {
                    'id': '40',
                    'name': 'BlueBay Asset Management'
                },
                'request': {
                    'requesting_account': {
                        'id': '3',
                        'status': 'test',
                        'type': 'requestor',
                        'name': 'Account Requestor'
                    },
                    'responding_account': {
                        'id': '51',
                        'status': 'test',
                        'type': 'responder',
                        'name': 'Account #51'
                    },
                    'due_date_unit': 24,
                    'due_date_unit_type': 'calendar',
                    'frequency': 'monthly',
                    'data_start_date': '',
                    'send_date': '',
                    'status': 'archived',
                    'type': 'recurring',
                    'acceptance_type': 'accepted',
                    'rejection_message': 'Rejection message',
                    'requested_fields': [
                        {'id': '1', 'name': 'beginning_balance'},
                        {'id': '2', 'name': 'ending_balance'},
                        {'id': '3', 'name': 'income'},
                        {'id': '4', 'name': 'perf_daily'},
                        {'id': '5', 'name': 'perf_qtd'},
                        {'id': '6', 'name': 'perf_ytd'},
                    ],
                    'fund_contacts': [
                        {
                            'id': '1',
                            'first_name': 'Petr',
                            'last_name': 'Petrov',
                            'email': 'petr.petrov@gmail.com'
                        }
                    ]
                }
            },
            {
                'id': '6',
                'name': 'Direct Lending Fund (Co-InvestA) LP',
                'firm': {
                    'id': '40',
                    'name': 'BlueBay Asset Management'
                },
                'request': null
            }
        ]
    }
};