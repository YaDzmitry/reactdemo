import { FundStatistics } from 'store/Fund/types';
import { BenchmarkType } from 'store/Profiles/Groups/types';

export const initialFundStatistics: FundStatistics[] = [
    {
        id: 'b112531f-cad0-494a-8a08-cc80c46ff2ba',
        name: 'DIRECT LENDING FUND (CO-INVEST A) LP 1',
        currency: 'USD',
        entity_type: BenchmarkType.Fund,
        volatility_ann: '12.54',
        return_ann: '10.23',
        drawdown_ann: '5.84',
        datasets: {
            history_datasets: [],
            latest_dataset: null,
        }
    },
];

export const fundStatistics: FundStatistics[] = [
    {
        id: 'b112531f-cad0-494a-8a08-cc80c46ff2bb',
        name: 'BBG Barc U.S. Universal Index 1',
        entity_type: BenchmarkType.Fund,
        currency: 'USD',
        volatility_ann: '15.77',
        return_ann: '15.32',
        drawdown_ann: '7.86',
        datasets: {
            history_datasets: [],
            latest_dataset: null,
        }
    },
    {
        id: 'b112531f-cad0-494a-8a08-cc80c46ff2bc',
        name: 'BBG Barc U.S. Universal Index 2',
        entity_type: BenchmarkType.Fund,
        currency: 'USD',
        volatility_ann: 0.09999,
        return_ann: null,
        drawdown_ann: null,
        datasets: {
            history_datasets: [],
            latest_dataset: null,
        }
    },
    {
        id: 'b112531f-cad0-494a-8a08-cc80c46ff2bd',
        name: 'BBG Barc U.S. Universal Index 3',
        entity_type: BenchmarkType.Fund,
        currency: 'USD',
        volatility_ann: null,
        return_ann: 8.45,
        drawdown_ann: -8.5555,
        datasets: {
            history_datasets: [],
            latest_dataset: null,
        }
    },
];