export default [
    { header: 'LEI:', value: '12345678901234567890' },
    { header: 'SEC:', value: '123-456789' },
    { header: 'Internal ID:', value: 'ABC123' },
    { header: 'Fund Type:', value: 'Hedge Fund' },
    { header: 'Domicile Country:', value: 'United States' },
    { header: 'Domicile State:', value: 'New York' },
    { header: 'Number of Owners:', value: '10' },
    { header: 'Form D File Number:', value: '021-28940' },
    { header: 'Min. Investment:', value: '$1,000,000' },
];