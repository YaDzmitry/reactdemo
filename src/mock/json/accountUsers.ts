import { AccountType } from 'store/Account/types';

export default {
    list: {
        'count': 30,
        'next': null,
        'previous': null,
        'results': [
            {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T21:59:32.546000Z',
                'role': 'user',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:54:16.581056Z',
                    'email': 'test1@mail.ru',
                    'first_name': 'Petr',
                    'id': 1,
                    'inviter': null,
                    'last_login': null,
                    'last_name': 'Petrov',
                    'status': 'Active',
                    'type': 'standard'
                }
            },
            {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test2@mail.ru',
                    'first_name': 'Sidor',
                    'id': 2,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Sidorov',
                    'status': 'Active',
                    'type': 'standard'
                }
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test3@mail.ru',
                    'first_name': 'Oleg',
                    'id': 3,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Olegov',
                    'status': 'Active',
                    'type': 'standard'
                },
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test4@mail.ru',
                    'first_name': 'Alexandr',
                    'id': 4,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Alexandrov',
                    'status': 'Active',
                    'type': 'standard'
                }
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test5@mail.ru',
                    'first_name': 'Pavel',
                    'id': 5,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Pavlov',
                    'status': 'Active',
                    'type': 'standard'
                }
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test6@mail.ru',
                    'first_name': 'Anton',
                    'id': 6,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Antonov',
                    'status': 'Active',
                    'type': 'standard'
                }
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test7@mail.ru',
                    'first_name': 'Ilia',
                    'id': 7,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Petrov',
                    'status': 'Active',
                    'type': 'standard'
                }
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T21:59:32.546000Z',
                'role': 'user',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:54:16.581056Z',
                    'email': 'test1@mail.ru',
                    'first_name': 'Petr',
                    'id': 1,
                    'inviter': null,
                    'last_login': null,
                    'last_name': 'Petrov',
                    'status': 'Active',
                    'type': 'standard'
                }
            },
            {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test2@mail.ru',
                    'first_name': 'Sidor',
                    'id': 2,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Sidorov',
                    'status': 'Active',
                    'type': 'standard'
                }
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test3@mail.ru',
                    'first_name': 'Oleg',
                    'id': 3,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Olegov',
                    'status': 'Active',
                    'type': 'standard'
                },
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test4@mail.ru',
                    'first_name': 'Alexandr',
                    'id': 4,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Alexandrov',
                    'status': 'Active',
                    'type': 'standard'
                }
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test5@mail.ru',
                    'first_name': 'Pavel',
                    'id': 5,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Pavlov',
                    'status': 'Active',
                    'type': 'standard'
                }
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test6@mail.ru',
                    'first_name': 'Anton',
                    'id': 6,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Antonov',
                    'status': 'Active',
                    'type': 'standard'
                }
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test7@mail.ru',
                    'first_name': 'Ilia',
                    'id': 7,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Petrov',
                    'status': 'Active',
                    'type': 'standard'
                }
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T21:59:32.546000Z',
                'role': 'user',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:54:16.581056Z',
                    'email': 'test1@mail.ru',
                    'first_name': 'Petr',
                    'id': 1,
                    'inviter': null,
                    'last_login': null,
                    'last_name': 'Petrov',
                    'status': 'Active',
                    'type': 'standard'
                }
            },
            {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test2@mail.ru',
                    'first_name': 'Sidor',
                    'id': 2,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Sidorov',
                    'status': 'Active',
                    'type': 'standard'
                }
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test3@mail.ru',
                    'first_name': 'Oleg',
                    'id': 3,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Olegov',
                    'status': 'Active',
                    'type': 'standard'
                },
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test4@mail.ru',
                    'first_name': 'Alexandr',
                    'id': 4,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Alexandrov',
                    'status': 'Active',
                    'type': 'standard'
                }
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test5@mail.ru',
                    'first_name': 'Pavel',
                    'id': 5,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Pavlov',
                    'status': 'Active',
                    'type': 'standard'
                }
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test6@mail.ru',
                    'first_name': 'Anton',
                    'id': 6,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Antonov',
                    'status': 'Active',
                    'type': 'standard'
                }
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test7@mail.ru',
                    'first_name': 'Ilia',
                    'id': 7,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Petrov',
                    'status': 'Active',
                    'type': 'standard'
                }
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T21:59:32.546000Z',
                'role': 'user',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:54:16.581056Z',
                    'email': 'test1@mail.ru',
                    'first_name': 'Petr',
                    'id': 1,
                    'inviter': null,
                    'last_login': null,
                    'last_name': 'Petrov',
                    'status': 'Active',
                    'type': 'standard'
                }
            },
            {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test2@mail.ru',
                    'first_name': 'Sidor',
                    'id': 2,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Sidorov',
                    'status': 'Active',
                    'type': 'standard'
                }
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test3@mail.ru',
                    'first_name': 'Oleg',
                    'id': 3,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Olegov',
                    'status': 'Active',
                    'type': 'standard'
                },
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test4@mail.ru',
                    'first_name': 'Alexandr',
                    'id': 4,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Alexandrov',
                    'status': 'Active',
                    'type': 'standard'
                }
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test5@mail.ru',
                    'first_name': 'Pavel',
                    'id': 5,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Pavlov',
                    'status': 'Active',
                    'type': 'standard'
                }
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test6@mail.ru',
                    'first_name': 'Anton',
                    'id': 6,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Antonov',
                    'status': 'Active',
                    'type': 'standard'
                }
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test7@mail.ru',
                    'first_name': 'Ilia',
                    'id': 7,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Petrov',
                    'status': 'Active',
                    'type': 'standard'
                }
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test6@mail.ru',
                    'first_name': 'Anton',
                    'id': 6,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Antonov',
                    'status': 'Active',
                    'type': 'standard'
                }
            }, {
                'account': {
                    'id': 1,
                    'name': 'Account #1',
                    'status': 'test',
                    'type': AccountType.Requestor,
                },
                'created_at': '2018-02-27T22:12:17.971000Z',
                'role': 'account_admin',
                'status': 'active',
                'user': {
                    'created_at': '2018-02-27T18:53:07.565512Z',
                    'email': 'test7@mail.ru',
                    'first_name': 'Ilia',
                    'id': 7,
                    'inviter': null,
                    'last_login': '2018-02-27T18:57:54.495857Z',
                    'last_name': 'Petrov',
                    'status': 'Active',
                    'type': 'standard'
                }
            }
        ]
    }
};