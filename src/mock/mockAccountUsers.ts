import MockAdapter from 'axios-mock-adapter';

import mockData from './json/accountUsers';

export default (mock: MockAdapter) => {
    mock.onGet(/\/accounts\/users\//).reply(config => {
        const {limit, offset} = config.params;
        return [200, {
            ...mockData.list,
            results: mockData.list.results.filter((row, idx) => {
                return idx >= offset && idx < (limit + offset);
            })
        }];

        // return [403, {
        //     detail: 'You do not have permission to perform this action.'
        // }];
    });

    mock.onDelete(/\/accounts\/users\/\d+\//).reply(config => {
        return [
            200,
            1
        ];
        // return [
        //     404,
        //     {detail: 'Not found.'}
        // ];
    });

    mock.onPatch(/\/accounts\/users\/\d+\//).reply(config => {
        return [
            200,
            {}
        ];
        // return [
        //     404,
        //     {detail: 'Not found.'}
        // ];
    });

    mock.onPost(/\/accounts\/users\//).reply(config => {
        return [
            200,
            {}
        ];
        // return [
        //     400,
        //     {
        //         email: ['Enter a valid email address.'],
        //         role: ['"test" is not a valid choice.']
        //     }
        // ];
    });
};