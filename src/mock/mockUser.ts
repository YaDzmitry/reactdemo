import MockAdapter from 'axios-mock-adapter';
import { AxiosRequestConfig } from 'axios';

import mockUsers from './json/users';

import { UserAccount } from 'store/Auth/models/userAccount';
import ActiveUser from 'store/Auth/models/activeUser';

export default (mock: MockAdapter) => {
    mock.onGet(/\/users\/info\//).reply(() => {
        return [200, mockUsers.info];
    });
    // mock.onGet(/\/users\/info/).reply(500, {
    //     errors: ['error1', 'error2']
    // });
    // mock.onGet(/\/users\/info/).reply(500, 'String error response');

    mock.onPatch(/\/switch-account\//).reply((config: AxiosRequestConfig) => {
        let userInfo: ActiveUser = mockUsers.info;
        const data = JSON.parse(config.data);
        const relation: UserAccount | null = userInfo.account_relations
            .find(rel => {
                return rel.account.id === data.account_id;
            }) || null;

        if (relation) {
            userInfo.active_account = <UserAccount> relation;
            return [200, userInfo];
        }

        return [400, {
            account_id: 'The user is not associated with the account'
        }];
    });

    mock.onPost(/\/reset-password\//).reply((config: AxiosRequestConfig) => {
        return [204, {}];

        // return [404, {
        //     detail: 'Not found.'
        // }];
    });

    mock.onPatch(/\/reset-password\/.+\/.+\//).reply((config: AxiosRequestConfig) => {
        return [204, {}];

        // return [404, {
        //     detail: 'Not found.'
        // }];
    });
};