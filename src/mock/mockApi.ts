import MockAdapter from 'axios-mock-adapter';
import { AxiosInstance } from 'axios';

export default (axios: AxiosInstance) => {
    let mock = new MockAdapter(axios);
    mock.onAny().passThrough();
};
