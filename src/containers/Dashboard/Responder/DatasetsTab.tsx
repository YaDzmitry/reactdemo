import * as React from 'react';
import { connect } from 'react-redux';
import { cloneDeep } from 'lodash';

import { CustomTableRow, NavigationButton } from 'store/types';
import { AppState, Id } from 'store/types';
import { acceptRequest, denyRequest, getRequestors } from 'store/DataManager/actionCreators';
import { getDatasetListCount, getRequestors as getRequestorsSelector } from 'store/DataManager/selectors';
import { getRequests } from 'store/Diligence/DiligenceRequest/actionCreators';
import { openModal } from 'store/Modals/General/actionCreators';

import { Requestors } from 'containers/DataManager/Requestors';
import DatasetList from 'containers/DataManager/DatasetList';

import ViewLink from 'components/Shared/Ui/ViewLink';
import DashboardPage from 'components/Dashboard/DashboardPage';

import { getDiligenceRequestState } from 'store/Diligence/DiligenceRequest/selectors';

import { getUser } from 'store/Auth/selectors';
import ActiveUser from 'store/Auth/models/activeUser';
import { ModalParams } from 'store/Modals/General/types';
import { OpenModalAction } from 'store/Modals/General/actions';
import { GetRequestsAction } from 'store/Diligence/DiligenceRequest/actions';
import { GetRequestorsToDataManager } from 'store/DataManager/actions';
import { DatasetFund } from 'store/DataManager/models/dataset';

interface OuterProps {
    menuItems: NavigationButton[];
}

interface Props {
    openModal: (name: string, params: ModalParams) => OpenModalAction;
    getRequestors: (fundId?: Id) => GetRequestorsToDataManager;
    getRequests: () => GetRequestsAction;
    acceptRequest: (requestId: Id) => void;
    denyRequest: (requestId: Id, message: string | null) => void;
    data: DatasetFund[];
    countFunds: number;
    diligenceRequestCount: number;

    user: ActiveUser;
}

type FinalProps = OuterProps & Props;

class DatasetsTab extends React.PureComponent<FinalProps> {
    render() {
        let { data } = this.props;
        const pendingRequestsCount = data.length;
        let dataToShow: (DatasetFund | {rowType: string, colspan: number })[] = cloneDeep(data);
        if (dataToShow.length < 5) {
            let counter = 0;
            while (dataToShow.length < 5) {
                let rowType = CustomTableRow.WhiteRow;
                if (counter === 0) {
                    rowType = CustomTableRow.WhiteRowWithBorder;
                }
                dataToShow.push({
                    rowType,
                    colspan: 7,
                });
                counter++;
            }
        }

        const {menuItems} = this.props;

        return (
            <DashboardPage menuItems={menuItems}>
                <div className="home-pending-requests">
                    <div>Pending Requests ({pendingRequestsCount})</div>
                    <div>
                        <ViewLink
                            link="/requests/pending-requests"
                            icon="rightArrow"
                        >
                            <span>Go to pending requests</span>
                        </ViewLink>
                    </div>
                </div>
                <Requestors
                    openModal={this.props.openModal}
                    getRequestors={this.props.getRequestors}
                    acceptRequest={this.props.acceptRequest}
                    denyRequest={this.props.denyRequest}
                    data={dataToShow}
                    useColoredScrollBar={true}
                />
                <div className="home-my-funds">
                    <div>My Funds ({this.props.countFunds})</div>
                    <div>
                        <ViewLink
                            link="/funds"
                            icon="rightArrow"
                        >
                            <span>Go to Funds</span>
                        </ViewLink>
                    </div>
                </div>
                <DatasetList />
            </DashboardPage>
        );
    }
}

const mapStateToProps = (state: AppState) => ({
    data: getRequestorsSelector(state),
    countFunds: getDatasetListCount(state),
    diligenceRequestCount: getDiligenceRequestState(state).allRequests.length,

    user: getUser(state)
});

const mapDispatchToProps = {
    openModal,
    getRequestors,
    acceptRequest,
    denyRequest,
    getRequests,
};

export default connect(mapStateToProps, mapDispatchToProps)(DatasetsTab);
