import * as React from 'react';
import { connect } from 'react-redux';
import { Route, Switch, withRouter } from 'react-router';
import { compose } from 'recompose';

import { AppState, NavigationButton } from 'store/types';

import { hasDiligenceAccount } from 'store/User/helpers';
import { getUser } from 'store/Auth/selectors';
import ActiveUser from 'store/Auth/models/activeUser';

import DatasetsTab from './DatasetsTab';
import QuestionnairesTab from './QuestionnairesTab';

const menuItemsWithDiligenceRequests: NavigationButton[] = [
    {path: '/dashboard', title: 'Datasets'},
    {path: '/dashboard/questionnaires', title: 'Questionnaires'},
    {path: '/dashboard/documents', title: 'Documents', disabled: true}
];

const menuItems: NavigationButton[] = [
    {path: '/dashboard', title: 'Datasets'},
    {path: '/dashboard/documents', title: 'Documents', disabled: true}
];

interface Props {
    user: ActiveUser;
}

class DashboardPageContainer extends React.PureComponent<Props> {
    render() {
        const tabs = hasDiligenceAccount(this.props.user)
            ? menuItemsWithDiligenceRequests
            : menuItems;

        return (
            <Switch>
                <Route
                    path="/dashboard"
                    exact={true}
                    render={() => (
                        <DatasetsTab menuItems={tabs} />
                    )}
                />
                <Route
                    path="/dashboard/questionnaires"
                    render={() => (
                        <QuestionnairesTab menuItems={tabs} />
                    )}
                />
            </Switch>
        );
    }
}

const mapStateToProps = (state: AppState) => ({
    user: getUser(state)
});

export default compose(
    withRouter,
    connect(mapStateToProps, null)
)(DashboardPageContainer);
