import * as React from 'react';

import DashboardPage from 'components/Dashboard/DashboardPage';
import { AppState, NavigationButton } from 'store/types';
import RequestList from 'containers/Diligence/DiligenceRequest/RequestList';
import { getDiligenceRequestState } from 'store/Diligence/DiligenceRequest/selectors';
import { connect } from 'react-redux';

interface OuterProps {
    menuItems: NavigationButton[];
}

interface Props {
    count: number;
}

type FinalProps = OuterProps & Props;

const QuestionnairesTab: React.FC<FinalProps> = ({count, menuItems}) => (
    <DashboardPage title={`Diligence Requests (${count})`} menuItems={menuItems}>
        <RequestList />
    </DashboardPage>
);

const mapStateToProps = (state: AppState) => ({
    count: getDiligenceRequestState(state).count
});

export default connect(mapStateToProps)(QuestionnairesTab);
