import * as React from 'react';
import { connect } from 'react-redux';

import { AppState } from 'store/types';

import Chart from './Chart';
import { makeGetChartCount } from 'store/Dashboard/selectors';
import { DashboardChartType } from 'store/Dashboard/types';
import AumOrPerfChartBlock from 'components/Dashboard/AumOrPerfChartBlock';
import { DashboardChartTypeTitles } from 'store/Dashboard/constants';

interface Props {
    chartType: DashboardChartType;
    chartPeriod: string;
    fundsCount: number;
}

const mapStateToProps = (state: AppState, ownProps: Props) => {
    const getChartCount = makeGetChartCount();

    return {
        title: DashboardChartTypeTitles[ownProps.chartType],
        chartFundsCount: getChartCount(state, {
            chartType: ownProps.chartType,
            chartPeriod: ownProps.chartPeriod
        }),
        Chart: <Chart chartType={ownProps.chartType} chartPeriod={ownProps.chartPeriod} />,
    };
};

export default connect(mapStateToProps)(AumOrPerfChartBlock);
