import { connect } from 'react-redux';

import BoxChart from 'components/Dashboard/BoxChart';
import { AppState } from 'store/types';
import { getBoxChartData } from 'store/Dashboard/selectors';
import { BoxChartPeriodType } from 'store/Dashboard/types';

interface Props {
    periodType: BoxChartPeriodType;
}

const mapStateToProps = (state: AppState, ownProps: Props) => ({
    items: getBoxChartData(state, {chartPeriod: ownProps.periodType})
});

export default connect(mapStateToProps)(BoxChart);
