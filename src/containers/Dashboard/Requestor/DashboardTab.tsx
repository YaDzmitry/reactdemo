import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import memoize from 'memoize-one';

import { FilterDecoratorChange } from 'decorators/FilterDecorator';
import Filter from 'decorators/FilterExtDecorator';
import AdvancedFilter from 'decorators/AdvancedFilterDecorator';

import { AppState, NavigationButton, TableRowAction } from 'store/types';
import { DefaultStateDashboardAttributeList, gearIcon, TableRowActionIcon, TableRowActionType } from 'store/constants';
import { AsyncFilterName, Chip, FilterInfo, FilterNames, FiltersState } from 'store/Filters/types';
import { getChips, getFilters, getIsLoading } from 'store/Filters/selectors';
import { FundAttributes } from 'store/Fund/types';
import { getFilterInfo, getSearchResults } from 'store/AsyncFiltersChoices/selectors';
import {
    clearAsyncFilters,
    getChoices,
    searchFilterChoices,
    setAsyncFilters,
    setGlobalChoices
} from 'store/AsyncFiltersChoices/actionCreators';
import {
    DatePeriod,
    StatisticDonutDataType,
    DashboardGridItem,
    DashboardChartGroup, DashboardChartType,
} from 'store/Dashboard/types';
import { DonutPiece } from 'store/Shared/types';
import DashboardTab from 'components/Dashboard/DashboardTab';
import StatisticDonutContainer from './StatisticDonut';

import {
    getDashboardRows,
    getDashboardRowsCount,
    getDashboardFundsCount,
    getChartDatePeriod, getDashboardFirmsCount,
} from 'store/Dashboard/selectors';
import {
    AdvancedFilterDecoratorSearch,
    AdvancedFilterDecoratorToggle,
    AdvancedFilterDecoratorUpdate
} from 'decorators/AdvancedFilterDecorator';
import { FilterEntity } from 'store/Diligence/DiligenceRequest/types';
import { isModalOpen as isAdvancedFilterModalOpen } from 'store/Modals/General/selectors';
import { ADVANCED_FILTERS_MODAL_NAME } from 'store/AdvancedFilters/constants';
import { getModalFilters, getModalFiltersName } from 'store/AdvancedFilters/selectors';
import { updateDashboardSettings, applyFilter as applyDashboardFilter } from 'store/Dashboard/actionCreators';
import { setFilters } from 'store/Filters/actionCreators';
import { closeModal, openModal } from 'store/Modals/General/actionCreators';
import { updateFiltersModal } from 'store/AdvancedFilters/actionCreators';
import { searchRequestAttribute } from 'store/Diligence/DiligenceRequest/actionCreators';
import DashboardPage from 'components/Dashboard/DashboardPage';
import DropDownWithCheckboxes from 'components/Shared/DropDownArea/DropDownWithCheckboxes';
import {
    dashboardChartDonutMapping,
    dashboardChartGroups, dashboardChartGroupsMapping, DashboardChartGroupsTitles
} from 'store/Dashboard/constants';
import { CheckboxOption } from 'store/DataManager/types';
import { UserAccountSettings } from 'store/Auth/models/userAccount';
import {
    getSelectedDashboardChartsTypes,
    getSelectedDashboardChartsGroups
} from 'store/Auth/selectors';
import ChartBlock from './ChartBlock';

const filterName = FilterNames.requestorDashboard;

interface OuterProps {
    menuItems: NavigationButton[];
}

interface Props {
    rows: DashboardGridItem[];
    rowsCount: number;
    fundsCount: number;
    firmsCount: number;

    datePeriod: DatePeriod;
    chartGroups: DashboardChartGroup[];
    chartTypes: DashboardChartType[];
    attrType: FundAttributes;
    filterName: string;

    firmFilter: FilterInfo;
    portfolioFilter: FilterInfo;
    creditOfficerFilter: FilterInfo;
    productFilter: FilterInfo;
    strategyFilter: FilterInfo;

    setAsyncFilters: (filterName: string, filter: FiltersState, pageName: string) => void;
    getGridConfiguration: () => void;
    setFilters: (filterName: string, filter: FiltersState) => void;
    getChoices: (page: string, filterName: string, filter: FiltersState) => void;
    currentFilter: FiltersState;
    handleChange: FilterDecoratorChange;
    chips: Chip[];
    isLoading: boolean;
    applyFilter: (filter: FiltersState) => void;
    updateDashboardSettings: (filterName: string, settings: UserAccountSettings, waitResponse: boolean) => void;
    /* ADVANCED FILTERS */
    handleToggleModal?: AdvancedFilterDecoratorToggle;
    handleUpdateModal?: AdvancedFilterDecoratorUpdate;
    handleAttributeSearch?: AdvancedFilterDecoratorSearch;
    isModalOpen: boolean;
    advancedFilter?: FiltersState;
    searchFilterEntities: FilterEntity[];

    // Only for clear global choices list
    setGlobalChoices: (results: Object[], count: number) => void;
}

class DashboardTabContainer extends React.PureComponent<Props & OuterProps & RouteComponentProps<{}>> {
    rowActions: TableRowAction[] = [{
        type: TableRowActionType.NavLink,
        icon: TableRowActionIcon.View,
        link: '#',
    }];

    getDashboardCharts = memoize((chartTypes, fundsCount, currentFilter) => {
        const charts = {};
        chartTypes.forEach(chartType => {
            charts[chartType] = dashboardChartDonutMapping.hasOwnProperty(chartType)
                ? (
                    <StatisticDonutContainer
                        donutType={dashboardChartDonutMapping[chartType]}
                        onClick={this.handleDonutClick}
                    />
                )
                : (
                <ChartBlock
                    key={chartType}
                    chartType={chartType}
                    fundsCount={fundsCount}
                    chartPeriod={currentFilter.period}
                />);
        });
        return charts;
    });

    getSettingsButtons = memoize(chartGroups => {
        return [(
            <DropDownWithCheckboxes
                key={'settings-gear-button'}
                icon={gearIcon}
                items={dashboardChartGroups.map(group => ({
                    label: DashboardChartGroupsTitles[group],
                    value: group,
                    checked: chartGroups.indexOf(group) >= 0
                }))}
                change={this.handleSettingsChange}
            />
        )];
    });

    componentDidMount() {
        this.props.setGlobalChoices([], 0);
    }

    render() {
        const {menuItems, chartTypes, chartGroups, fundsCount,
            currentFilter, applyFilter, isModalOpen, searchFilterEntities, advancedFilter,
            handleToggleModal, handleUpdateModal, handleAttributeSearch
        } = this.props;

        return (
            <DashboardPage menuItems={menuItems} menuSubItems={this.getSettingsButtons(chartGroups)}>
                <DashboardTab
                    {...this.props}
                    filterPageName={filterName}
                    onEyeClick={this.handleEyeClick}
                    applyFilter={applyFilter}

                    handleToggleModal={handleToggleModal}
                    handleUpdateModal={handleUpdateModal}
                    handleAttributeSearch={handleAttributeSearch}
                    isFilterModalOpen={isModalOpen}
                    searchFilterEntities={searchFilterEntities}
                    currentModalFilter={advancedFilter}
                >
                    {{
                        DashboardCharts: this.getDashboardCharts(chartTypes, fundsCount, currentFilter),
                    }}
                </DashboardTab>
            </DashboardPage>
        );
    }

    private handleDonutClick = (dataType: StatisticDonutDataType, piece: DonutPiece) => {
        if (dataType === StatisticDonutDataType.Alerts) {
            return;
        }

        this.redirectToFund(null, null, piece.name);
    }

    private handleEyeClick = (attrType: FundAttributes, row: DashboardGridItem) => {
        this.redirectToFund(attrType, row.value);
    }

    private redirectToFund(attrType: FundAttributes | null = null, attrValue: string | null = null,
                           status: string | null = null
    ) {
        const currentFilter = {
            ...this.props.currentFilter,
            delivery_status: [] as string[]
        };

        if (status) {
            currentFilter.delivery_status = [status];
        }

        if (attrType && attrValue) {
            currentFilter[attrType] = [{
                label: attrValue,
                value: attrValue
            }];
        }

        this.props.setFilters(FilterNames.fundList, currentFilter);
        this.props.history.push(`/funds`);
    }

    private handleSettingsChange = (item: CheckboxOption, checked: boolean, items: CheckboxOption[]) => {
        let chartTypes: string[] = [];
        items
            .filter(oneItem => oneItem.checked)
            .forEach(checkedItem => {
                chartTypes = chartTypes.concat(dashboardChartGroupsMapping[checkedItem.value]);
            });

        this.props.updateDashboardSettings(
            filterName,
            { dashboard_charts: chartTypes as DashboardChartType[] },
            false
        );
    }
}

const mapStateToProps = (state: AppState) => {
    const currentPageName = FilterNames.requestorDashboard,
        currentFilter = getFilters(state, filterName);

    return {
        currentFilter,
        isModalOpen: isAdvancedFilterModalOpen(state, {name: ADVANCED_FILTERS_MODAL_NAME}),
        advancedFilter: getModalFilters(state),
        advancedFilterName: getModalFiltersName(state),
        searchFilterEntities: getSearchResults(state),

        chartGroups: getSelectedDashboardChartsGroups(state),
        chartTypes: getSelectedDashboardChartsTypes(state),
        datePeriod: getChartDatePeriod(state),
        rows: getDashboardRows(state),
        rowsCount: getDashboardRowsCount(state),
        fundsCount: getDashboardFundsCount(state),
        firmsCount: getDashboardFirmsCount(state),
        chips: getChips(state, filterName),
        isLoading: getIsLoading(state),
        portfolioFilter: getFilterInfo(state, {
            page: currentPageName,
            filterName: AsyncFilterName.portfolio
        }),
        creditOfficerFilter:  getFilterInfo(state, {
            page: currentPageName,
            filterName: AsyncFilterName.creditOfficer,
            labelField: 'value',
            valueField: 'value',
        }),
        productFilter: getFilterInfo(state, {
            page: currentPageName,
            filterName: AsyncFilterName.product,
            labelField: 'value',
            valueField: 'value',
        }),
        strategyFilter: getFilterInfo(state, {
            page: currentPageName,
            filterName: AsyncFilterName.strategy,
            labelField: 'value',
            valueField: 'value',
        }),
        firmFilter: getFilterInfo(state, {
            page: currentPageName,
            filterName: AsyncFilterName.firm
        }),
    };
};

const mapDispatchToProps = {
    applyFilter: applyDashboardFilter,
    setFilters,
    updateDashboardSettings,

    /* ADVANCED FILTER */
    openFiltersModal: openModal,
    closeFiltersModal: closeModal,

    updateFiltersModal,
    searchRequestAttribute,
    searchFilterChoices,
    setGlobalChoices,
    clearAsyncFilters,

    getChoices,
    setAsyncFilters,
};

export default  compose <{}, OuterProps>(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps),
    component => Filter(
        component, {...DefaultStateDashboardAttributeList },
        filterName, true, 0
    ),
    component => AdvancedFilter(component, filterName)
)(DashboardTabContainer);
