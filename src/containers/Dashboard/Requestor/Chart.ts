import { connect } from 'react-redux';

import Chart from 'components/Dashboard/Chart';
import { AppState } from 'store/types';
import { makeGetChartData } from 'store/Dashboard/selectors';
import { DashboardChartType } from 'store/Dashboard/types';
import { DEFAULT_CURRENCY_VALUE } from 'store/constants';

interface Props {
    chartPeriod: string;
    chartType: DashboardChartType;
}

const makeMapStateToProps = () => {
    const getChartData = makeGetChartData();
    const mapStateToProps = (state: AppState, ownProps: Props) => ({
        items: getChartData(state, {chartType: ownProps.chartType, chartPeriod: ownProps.chartPeriod}),
        chartType: ownProps.chartType,
        currencyName: DEFAULT_CURRENCY_VALUE
    });
    return mapStateToProps;
};

export default connect(makeMapStateToProps)(Chart);
