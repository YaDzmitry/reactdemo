import * as React from 'react';
import { connect } from 'react-redux';

import { AppState } from 'store/types';

import BoxChartBlock from 'components/Dashboard/BoxChartBlock';

import BoxChart from './BoxChart';
import { getBoxChartCount } from 'store/Dashboard/selectors';
import { BoxChartPeriodType } from 'store/Dashboard/types';

interface Props {
    periodType: BoxChartPeriodType;
}

const mapStateToProps = (state: AppState, ownProps: Props) => ({
    boxChartCount: getBoxChartCount(state, {chartPeriod: ownProps.periodType}),
    BoxChart: <BoxChart periodType={ownProps.periodType} />
});

export default connect(mapStateToProps)(BoxChartBlock);
