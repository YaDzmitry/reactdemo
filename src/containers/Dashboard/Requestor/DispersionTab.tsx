import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { compose } from 'recompose';
import { connect } from 'react-redux';
import memoize from 'memoize-one';

import { FilterDecoratorChange } from 'decorators/FilterDecorator';
import Filter from 'decorators/FilterExtDecorator';
import AdvancedFilter, {
    AdvancedFilterDecoratorSearch,
    AdvancedFilterDecoratorToggle,
    AdvancedFilterDecoratorUpdate
} from 'decorators/AdvancedFilterDecorator';

import { AppState, NavigationButton, TableRowAction } from 'store/types';
import { getSelectedDispersionChartsTypes } from 'store/Auth/selectors';
import { DefaultStateDashboardAttributeList, TableRowActionIcon, TableRowActionType } from 'store/constants';
import { AsyncFilterName, Chip, FilterInfo, FilterNames, FiltersState } from 'store/Filters/types';
import { getChips, getFilters, getIsLoading } from 'store/Filters/selectors';
import { FundAttributes } from 'store/Fund/types';
import { getFilterInfo, getSearchResults } from 'store/AsyncFiltersChoices/selectors';
import {
    clearAsyncFilters,
    getChoices,
    searchFilterChoices,
    setAsyncFilters,
    setGlobalChoices
} from 'store/AsyncFiltersChoices/actionCreators';
import { DashboardGridItem, DatePeriod, DispersionChartType } from 'store/Dashboard/types';
import DashboardTab from 'components/Dashboard/DashboardTab';
import DashboardPage from 'components/Dashboard/DashboardPage';

import {
    getBoxChartDatePeriod,
    getDashboardFirmsCount,
    getDashboardFundsCount,
    getDashboardRows,
    getDashboardRowsCount,
} from 'store/Dashboard/selectors';
import { FilterEntity } from 'store/Diligence/DiligenceRequest/types';
import { isModalOpen as isAdvancedFilterModalOpen } from 'store/Modals/General/selectors';
import { ADVANCED_FILTERS_MODAL_NAME } from 'store/AdvancedFilters/constants';
import { getModalFilters, getModalFiltersName } from 'store/AdvancedFilters/selectors';
import { updateDispersionSettings, applyDispersionFilter } from 'store/Dashboard/actionCreators';
import { setFilters } from 'store/Filters/actionCreators';
import { closeModal, openModal } from 'store/Modals/General/actionCreators';
import { updateFiltersModal } from 'store/AdvancedFilters/actionCreators';
import { searchRequestAttribute } from 'store/Diligence/DiligenceRequest/actionCreators';
import BoxChartBlock from './BoxChartBlock';
import DropDownWithCheckboxes from 'components/Shared/DropDownArea/DropDownWithCheckboxes';
import { CheckboxOption } from 'store/DataManager/types';
import { gearIcon } from 'store/constants';
import {
    dispersionChartsTypes,
    BoxChartDropdownPeriodTitles
} from 'store/Dashboard/constants';
import { UserAccountSettings } from 'store/Auth/models/userAccount';

const filterName = FilterNames.requestorDashboardDispersion;

interface OuterProps {
    menuItems: NavigationButton[];
}

interface Props {
    rows: DashboardGridItem[];
    rowsCount: number;
    fundsCount: number;
    firmsCount: number;
    aumFundsCount: number;
    perfFundsCount: number;

    datePeriod: DatePeriod;
    chartTypes: DispersionChartType[];
    attrType: FundAttributes;
    filterName: string;

    firmFilter: FilterInfo;
    portfolioFilter: FilterInfo;
    creditOfficerFilter: FilterInfo;
    productFilter: FilterInfo;
    strategyFilter: FilterInfo;

    setAsyncFilters: (filterName: string, filter: FiltersState, pageName: string) => void;
    getGridConfiguration: () => void;
    setFilters: (filterName: string, filter: FiltersState) => void;
    getChoices: (page: string, filterName: string, filter: FiltersState) => void;
    currentFilter: FiltersState;
    handleChange: FilterDecoratorChange;
    chips: Chip[];
    isLoading: boolean;
    applyFilter: (filter: FiltersState) => void;
    updateDispersionSettings: (filterName: string, settings: UserAccountSettings, waitResponse: boolean) => void;
    /* ADVANCED FILTERS */
    handleToggleModal?: AdvancedFilterDecoratorToggle;
    handleUpdateModal?: AdvancedFilterDecoratorUpdate;
    handleAttributeSearch?: AdvancedFilterDecoratorSearch;
    isModalOpen: boolean;
    advancedFilter?: FiltersState;
    searchFilterEntities: FilterEntity[];

    // Only for clear global choices list
    setGlobalChoices: (results: Object[], count: number) => void;
}

type FinalProps = Props & OuterProps & RouteComponentProps<{}>;

class DispersionTabContainer extends React.PureComponent<FinalProps> {
    rowActions: TableRowAction[] = [{
        type: TableRowActionType.NavLink,
        icon: TableRowActionIcon.View,
        link: '#',
    }];

    getBoxCharts = memoize((chartTypes, fundsCount, currentFilter) => {
        const charts = {};
        chartTypes.forEach(chartType => {
            charts[chartType] = (
                <BoxChartBlock
                    key={chartType}
                    periodType={chartType}
                    fundsCount={fundsCount}
                    currentFilter={currentFilter}
                />
            );
        });
        return charts;
    });

    getSettingsButtons = memoize(chartTypes => {
        return [(
            <DropDownWithCheckboxes
                key={'settings-gear-button'}
                icon={gearIcon}
                items={dispersionChartsTypes.map(period => ({
                    label: BoxChartDropdownPeriodTitles[period],
                    value: period,
                    checked: chartTypes.indexOf(period) >= 0
                }))}
                change={this.handleSettingsChange}
            />
        )];
    });

    componentDidMount() {
        this.props.setGlobalChoices([], 0);
    }

    render() {
        const {
            menuItems, chartTypes, fundsCount,
            currentFilter, applyFilter, isModalOpen, searchFilterEntities, advancedFilter,
            handleToggleModal, handleUpdateModal, handleAttributeSearch
        } = this.props;

        return (
            <DashboardPage menuItems={menuItems} menuSubItems={this.getSettingsButtons(chartTypes)}>
                <DashboardTab
                    {...this.props}
                    filterPageName={filterName}
                    onEyeClick={this.handleEyeClick}
                    applyFilter={applyFilter}

                    handleToggleModal={handleToggleModal}
                    handleUpdateModal={handleUpdateModal}
                    handleAttributeSearch={handleAttributeSearch}
                    isFilterModalOpen={isModalOpen}
                    searchFilterEntities={searchFilterEntities}
                    currentModalFilter={advancedFilter}
                >
                    {{
                        DispersionCharts: this.getBoxCharts(chartTypes, fundsCount, currentFilter)
                    }}
                </DashboardTab>
            </DashboardPage>
        );
    }

    private handleEyeClick = (attrType: FundAttributes, row: DashboardGridItem) => {
        this.redirectToFund(attrType, row.value);
    }

    private redirectToFund(attrType: FundAttributes | null = null, attrValue: string | null = null,
                           status: string | null = null
    ) {
        const currentFilter = {
            ...this.props.currentFilter,
            delivery_status: [] as string[]
        };

        if (status) {
            currentFilter.delivery_status = [status];
        }

        if (attrType && attrValue) {
            currentFilter[attrType] = [{
                label: attrValue,
                value: attrValue
            }];
        }

        this.props.setFilters(FilterNames.fundList, currentFilter);
        this.props.history.push(`/funds`);
    }

    private handleSettingsChange = (item: CheckboxOption, checked: boolean, items: CheckboxOption[]) => {
        const chartTypes = items
            .filter(oneItem => oneItem.checked)
            .map(oneItem => oneItem.value as string);

        this.props.updateDispersionSettings(
            filterName,
            { dispersion_charts: chartTypes as DispersionChartType[] },
            false
        );
    }
}

const mapStateToProps = (state: AppState) => {
    const currentPageName = FilterNames.requestorDashboardDispersion,
        currentFilter = getFilters(state, filterName);

    return {
        currentFilter,
        isModalOpen: isAdvancedFilterModalOpen(state, {name: ADVANCED_FILTERS_MODAL_NAME}),
        advancedFilter: getModalFilters(state),
        advancedFilterName: getModalFiltersName(state),
        searchFilterEntities: getSearchResults(state),

        datePeriod: getBoxChartDatePeriod(state),
        chartTypes: getSelectedDispersionChartsTypes(state),
        rows: getDashboardRows(state),
        rowsCount: getDashboardRowsCount(state),
        fundsCount: getDashboardFundsCount(state),
        firmsCount: getDashboardFirmsCount(state),
        chips: getChips(state, filterName),
        isLoading: getIsLoading(state),
        portfolioFilter: getFilterInfo(state, {
            page: currentPageName,
            filterName: AsyncFilterName.portfolio
        }),
        creditOfficerFilter:  getFilterInfo(state, {
            page: currentPageName,
            filterName: AsyncFilterName.creditOfficer,
            labelField: 'value',
            valueField: 'value',
        }),
        productFilter: getFilterInfo(state, {
            page: currentPageName,
            filterName: AsyncFilterName.product,
            labelField: 'value',
            valueField: 'value',
        }),
        strategyFilter: getFilterInfo(state, {
            page: currentPageName,
            filterName: AsyncFilterName.strategy,
            labelField: 'value',
            valueField: 'value',
        }),
        firmFilter: getFilterInfo(state, {
            page: currentPageName,
            filterName: AsyncFilterName.firm
        }),
    };
};

const mapDispatchToProps = {
    applyFilter: applyDispersionFilter,
    setFilters,
    updateDispersionSettings,

    /* ADVANCED FILTER */
    openFiltersModal: openModal,
    closeFiltersModal: closeModal,

    updateFiltersModal,
    searchRequestAttribute,
    searchFilterChoices,
    setGlobalChoices,
    clearAsyncFilters,

    getChoices,
    setAsyncFilters,
};

export default  compose<{}, OuterProps>(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps),
    component => Filter(
        component, {...DefaultStateDashboardAttributeList },
        filterName, true, 0
    ),
    component => AdvancedFilter(component, filterName)
)(DispersionTabContainer);
