import * as React from 'react';
import { connect } from 'react-redux';
import { Route, Switch, withRouter } from 'react-router';
import { compose } from 'recompose';

import { AppState, NavigationButton } from 'store/types';

import DashboardTab from './DashboardTab';
import DispersionTab from './DispersionTab';

const menuItems: NavigationButton[] = [
    {path: '/dashboard', title: 'Dashboard'},
    {path: '/dashboard/dispersion', title: 'Dispersion'},
];

interface Props {
}

class DashboardPageContainer extends React.PureComponent<Props> {
    render() {
        return (
            <Switch>
                <Route
                    path="/dashboard"
                    exact={true}
                    render={() => (
                        <DashboardTab menuItems={menuItems} />
                    )}
                />
                <Route
                    path="/dashboard/dispersion"
                    render={() => (
                        <DispersionTab menuItems={menuItems} />
                    )}
                />
            </Switch>
        );
    }
}

const mapStateToProps = (state: AppState) => ({
});

export default compose(
    withRouter,
    connect(mapStateToProps, null)
)(DashboardPageContainer);
