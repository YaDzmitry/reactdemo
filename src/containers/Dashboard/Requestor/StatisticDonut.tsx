import * as React from 'react';
import { connect } from 'react-redux';

import { AppState } from 'store/types';

import { StatisticDonutContainerProps, StatisticDonutDataType } from 'store/Dashboard/types';
import { DonutPiece } from 'store/Shared/types';
import { getDonutPieces, getDonutTotal } from 'store/Dashboard/selectors';
import { getDonutName } from 'store/Dashboard/helpers';
import StatisticDonut, { Props as StatisticDonutProps } from 'components/Dashboard/StatisticDonut';
import { StatisticDonutDataTypeTitles } from 'store/Dashboard/constants';

interface Props {
    onClick?: (
        dataType: StatisticDonutDataType, piece: DonutPiece, attrValue?: string
    ) => void;
}

class StatisticDonutContainer extends React.PureComponent<Props & StatisticDonutProps> {
    render() {
        return (
            <StatisticDonut {...this.props} onClick={this.handleClick} />
        );
    }

    private handleClick = (piece: DonutPiece) => {
        if (this.props.onClick) {
            const {donutType} = this.props;
            this.props.onClick(donutType, piece);
        }
    }
}

const mapStateToProps = (state: AppState, ownProps: StatisticDonutContainerProps) => ({
    title: StatisticDonutDataTypeTitles[ownProps.donutType],
    pieces: getDonutPieces(state, {
        donutName: getDonutName(ownProps.donutType)
    }),
    total: getDonutTotal(state, {
        donutName: getDonutName(ownProps.donutType)
    })
});

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(StatisticDonutContainer);
