import { connect } from 'react-redux';

import DatasetsColumnsModal from 'components/Fund/FundDatasetsColumnsModal';

import { AppState } from 'store/types';
import { closeModal } from 'store/Modals/General/actionCreators';
import { getModalParams, isModalOpen } from 'store/Modals/General/selectors';
import { PUBLISHED_DATASETS_COLUMNS_MODAL } from 'store/DataManager/types';
import { getHeaders } from 'store/VisibleHeaders/selectors';
import { headerNames } from 'store/VisibleHeaders/types';
import { getDatasetDetailTemporaryHeaders } from 'store/DataManager/selectors';
import { saveSettingsDatasetFund, setDatasetDetailTemporaryHeaders } from 'store/DataManager/actionCreators';

const mapStateToProps = (state: AppState) => {
    const modalParams = getModalParams(state) || {};
    return {
        temporaryHeaders: getDatasetDetailTemporaryHeaders(state),
        headers: getHeaders(state, {name: headerNames.fundDatasets}),
        isOpen: isModalOpen(state, {name: PUBLISHED_DATASETS_COLUMNS_MODAL}),
        modalParams,
    };
};

const mapDispatchToProps = {
    closeModal,
    setDatasetDetailTemporaryHeaders,
    saveSettingsDatasetFund,
};

export default connect(mapStateToProps, mapDispatchToProps)(DatasetsColumnsModal);