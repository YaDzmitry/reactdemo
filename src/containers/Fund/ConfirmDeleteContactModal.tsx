import * as React from 'react';
import { connect } from 'react-redux';

import { getModalParams, isModalOpen } from 'store/Modals/General/selectors';
import { closeModal } from 'store/Modals/General/actionCreators';
import { CONFIRM_DELETE_CONTACT_MODAL, CONFIRM_DELETE_CONTACT_MODAL_TITLE } from 'store/Fund/constants';
import Modal from 'components/Shared/Modals/Modal';
import { deleteContacts } from 'store/Fund/actionCreators';
import { AppState, Id } from 'store/types';
import ButtonDanger from '../../components/Shared/Ui/Buttons/ButtonDanger';
import { DeleteContactsModel } from '../../store/Fund/models/contact';
import ButtonDefault from '../../components/Shared/Ui/Buttons/ButtonDefault';

interface Props {
    isOpen: boolean;
    closeModal: () => any;
    deleteContacts: (fundId: Id, ids: Id[]) => any;
    modalParams: DeleteContactsModel;
}

class ConfirmDeleteModal extends React.PureComponent<Props> {
    constructor(props: Props) {
        super(props);

        this.handleToggle = this.handleToggle.bind(this);
    }

    handleToggle() {
        this.props.closeModal();
    }

    render() {
        const {isOpen, modalParams} = this.props;
        let fundId = '', ids: Id[] = [];
        if (modalParams) {
            fundId = modalParams.fundId;
            ids = modalParams.contactIds;
        }

        const body = (
            <p>Are you sure that you're going to delete {ids.length} contact{ids.length === 1 ? '' : 's'}?</p>
        );
        const title = <>{CONFIRM_DELETE_CONTACT_MODAL_TITLE}</>;
        const buttons = [
            (
                <ButtonDanger
                    buttonLarge={true}

                    handleClick={() => this.props.deleteContacts(fundId, ids)}
                >
                    Yes, delete
                </ButtonDanger>
            ),
            (
                <ButtonDefault
                    buttonLarge={true}
                    inverse={true}
                    handleClick={this.handleToggle}
                >
                    cancel
                </ButtonDefault>
            )
        ];

        return (
            <Modal
                className="ui-modal--confirm"
                isOpen={isOpen}
                toggle={this.handleToggle}
                title={title}
                body={body}
                buttons={buttons}
            />
        );
    }
}

const mapStateToProps = (state: AppState) => ({
    isOpen: isModalOpen(state, {name: CONFIRM_DELETE_CONTACT_MODAL}),
    modalParams: getModalParams(state),
});

const mapDispatchToProps = {
    closeModal,
    deleteContacts,
};

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmDeleteModal);