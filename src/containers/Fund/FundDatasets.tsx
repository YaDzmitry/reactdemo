import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { RouteComponentProps, withRouter } from 'react-router';
import { NavLink } from 'react-router-dom';
import { History, Location } from 'history';

import ActionDecorator from 'decorators/ActionDecorator';
import FilterDecorator, { FilterDecoratorChange } from 'decorators/FilterDecorator';
import { setFilters } from 'store/Filters/actionCreators';
import { datasetDownload } from 'store/DataManager/actionCreators';
import {
    applyDatasetsFilter, clearFundDatasetHistory, exportFundDatasets,
    getFundDatasetHistory, saveRequestorDataset, clearSelectedPeriod, setDatasetListIsUpdated, getFund,
    makeDatasetTheLatest
} from 'store/Fund/actionCreators';
import { FilterNames, FiltersState } from 'store/Filters/types';
import { getFilters, getIsLoading } from 'store/Filters/selectors';
import { DefaultStateDatasetsTab } from 'store/constants';
import { openModal } from 'store/Modals/General/actionCreators';
import { getHeaders, getHeadersVisibleCount } from 'store/VisibleHeaders/selectors';
import { headerNames } from 'store/VisibleHeaders/types';
import { setHeaders } from 'store/VisibleHeaders/actionCreators';
import { getActionIsInProgress } from 'store/Actions/selectors';
import MainLayout from 'components/Shared/Layouts/MainLayout';
import { Id, NavigationButton, TableHeader } from 'store/types';
import { FundModel } from 'store/Fund/models/fund';
import {
    getDatasetListIsUpdated,
    getDatasetsCountState, getDatasetsState, getFundProfileSelectedPeriod, getSingleFundState
} from 'store/Fund/selectors';
import DatasetsPage from 'components/Fund/DatasetsPage';
import { Action, bindActionCreators, Dispatch } from 'redux';
import {
    APPLY_DATASETS_FILTER, ClearSelectedPeriodAction, SaveRequestorDatasetAction,
    ClearFundDatasetHistoryAction, GetFundDatasetHistoryAction, SetDatasetListIsUpdatedAction, GetFundAction,
    MakeDatasetTheLatestAction
} from 'store/Fund/actions';
import { Dataset, DatasetWithRowType } from 'store/DataManager/models/dataset';
import { SetHeadersAction } from 'store/VisibleHeaders/actions';
import { getLatestUploadStatus } from 'store/BulkUpload/selectors';
import { UploadStatus, UploadType } from 'store/BulkUpload/types';

const filterName = FilterNames.datasetsTab;

interface Props {
    menuItems: NavigationButton[];
    currentFilter: FiltersState;
    currentFund: FundModel;
    headers: TableHeader[];
    results: Dataset[];
    count: number;
    getFundDatasetHistory: (id: Id, dataset: Dataset) => GetFundDatasetHistoryAction;
    clearFundDatasetHistory: (id: Id, period?: string) => ClearFundDatasetHistoryAction;
    exportFundDatasets: (id: Id) => void;
    openModal: (name: string, params: any) => any;
    handleColumns: (columns: any) => void;
    handleChange: FilterDecoratorChange;
    isGridLoading: boolean;
    setHeaders: (block: string, headers: TableHeader[]) => SetHeadersAction;
    saveRequestorDataset: (dataset: Dataset) => SaveRequestorDatasetAction;
    makeDatasetTheLatest: (dataset: DatasetWithRowType) => MakeDatasetTheLatestAction;
    history: History;
    location: Location;
    selectedPeriod: string | null;
    clearSelectedPeriod: () => ClearSelectedPeriodAction;
    uploadStatus: UploadStatus;
    applyFilter: FilterDecoratorChange;
    setDatasetListIsUpdated: (flag: boolean) => SetDatasetListIsUpdatedAction;
    getFund: (fundId: Id, withRequest?: boolean) => GetFundAction;
    datasetListIsUpdated: boolean;
}

interface RouteParams {
    id: Id;
}

class Datasets extends React.PureComponent<Props> {
    render() {
        const {
            count,
            currentFund,
            currentFilter,
            isGridLoading,
            handleChange,
            headers,
            menuItems,
            results,
            history,
            location,
            selectedPeriod,
        } = this.props;

        const breadCrumbs = (
            <div className="ap-breadcrumbs">
                <NavLink to="/firms">Firms</NavLink>
                <span className="breadcrumb-slash">/</span>
                <NavLink to={`/firms/${currentFund.firm.id}`}>{currentFund.firm.name}</NavLink>
                <span className="breadcrumb-slash">/</span>
                <NavLink to="/funds">Funds</NavLink>
                <span className="breadcrumb-slash">/</span>
                <span>{currentFund.name}</span>
            </div>
        );
        return (
            <MainLayout
                title={currentFund.name}
                menuItems={menuItems}
                breadCrumbs={breadCrumbs}
            >
                <DatasetsPage
                    count={count}
                    headers={headers}
                    results={results}
                    currentFund={currentFund}
                    getFundDatasetHistory={this.props.getFundDatasetHistory}
                    clearFundDatasetHistory={this.props.clearFundDatasetHistory}
                    exportFundDatasets={this.props.exportFundDatasets}
                    openModal={this.props.openModal}
                    handleColumns={this.props.handleColumns}
                    handleChange={handleChange}
                    currentFilter={currentFilter}
                    isGridLoading={isGridLoading}
                    selectedPeriod={selectedPeriod}
                    setHeaders={this.props.setHeaders}
                    saveRequestorDataset={this.props.saveRequestorDataset}
                    makeDatasetTheLatest={this.props.makeDatasetTheLatest}
                    history={history}
                    location={location}
                    clearSelectedPeriod={this.props.clearSelectedPeriod}
                    uploadStatus={this.props.uploadStatus}
                    applyFilter={this.props.applyFilter}
                    setDatasetListIsUpdated={this.props.setDatasetListIsUpdated}
                    getFund={this.props.getFund}
                    datasetListIsUpdated={this.props.datasetListIsUpdated}
                />
            </MainLayout>
        );
    }
}

const mapStateToProps = (state: any, ownProps: any) => ({
    currentFund: getSingleFundState(state),
    currentFilter: getFilters(state, filterName),
    count: getDatasetsCountState(state),
    results: getDatasetsState(state),
    isLoading: getIsLoading(state),
    isGridLoading: getActionIsInProgress(state, {name: APPLY_DATASETS_FILTER}),
    headers: getHeaders(state, {name: headerNames.fundDatasets}),
    visibleHeadersCount: getHeadersVisibleCount(state, {name: headerNames.fundDatasets}),
    selectedPeriod: getFundProfileSelectedPeriod(state),
    uploadStatus: getLatestUploadStatus(state, {type: UploadType.DataManagerRequestor}),
    datasetListIsUpdated: getDatasetListIsUpdated(state),
});

const mapDispatchToProps = (dispatch: Dispatch<Action>, ownProps: Props & RouteComponentProps<RouteParams>) => ({
    ...bindActionCreators(
        {
            applyFilter: ((filter: any) => applyDatasetsFilter({...filter, id: ownProps.match.params.id})),
            setHeaders,
            openModal,
            setFilters,
            download: datasetDownload,
            getFundDatasetHistory,
            clearFundDatasetHistory,
            exportFundDatasets,
            saveRequestorDataset,
            makeDatasetTheLatest,
            clearSelectedPeriod,
            setDatasetListIsUpdated,
            getFund,
        },
        dispatch
    ),
});

export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps),
    ActionDecorator,
    (component: any) => FilterDecorator(component, { ...DefaultStateDatasetsTab }, filterName )
)(Datasets);
