import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { connect } from 'react-redux';
import { compose } from 'recompose';

import DataReportingPage from 'components/Fund/DataReportingPage';
import RequestEditor from 'containers/Shared/RequestEditor';

import { getFund, getFundReportingHistory } from 'store/Fund/actionCreators';
import {
    saveEditedRequest, sendRequestFromDataReporting, setEditedRequest, toggleEditMode
} from 'store/Requests/actionCreators';
import {
    getSingleFundState, getDataReportingState as getDataReportingStateSelector
} from 'store/Fund/selectors';
import { AppState, Id, NavigationButton } from 'store/types';
import { FundModel } from 'store/Fund/models/fund';
import { getEditedRequest, isEditMode as isEditModeSelector } from 'store/Requests/selectors';
import { Request } from 'store/Requests/models/request';
import { FundRequestRelation } from 'store/Requests/types';
import { FundReportingHistory } from 'store/Fund/types';
import { getActionIsInProgress } from 'store/Actions/selectors';
import { GET_FUND } from 'store/Fund/actions';
import { openModal } from 'store/Modals/General/actionCreators';
import { SEND_BULK_REQUESTS_MODAL } from 'store/Requests/constants';

interface Props {
    menuItems: NavigationButton[];
    fund: FundModel;
    history: FundReportingHistory[];
    editedRequest: Request | null;
    isEditMode: boolean;
    setEditedRequest: (request: Request | null) => any;
    saveEditedRequest: (relaions: FundRequestRelation[], request: Request) => any;
    getFund: (id: Id, withRequest: boolean) => any;
    getFundReportingHistory: (fundId: Id) => any;
    toggleEditMode: (mode?: boolean) => any;
    fundIsLoading: boolean;
    openModal: (modalName: string, params: any) => any;
    sendRequest: (fundId: Id) => any;
}

interface RouteProps {
    id: Id;
}

type FinalProps = Props & RouteComponentProps<RouteProps>;

class DataReporting extends React.PureComponent<FinalProps> {
    componentDidMount() {
        const { id } = this.props.match.params;
        this.props.setEditedRequest(null);
        this.props.toggleEditMode(false);
        this.props.getFund(id, true);
        this.props.getFundReportingHistory(id);
    }

    handleToggleMode = () => {
        this.props.toggleEditMode();
    }

    handleSaveEditedRequest = () => {
        const {fund, editedRequest} = this.props;
        if (editedRequest) {
            this.props.saveEditedRequest(
                [{fundId: fund.id, requestId: editedRequest.id || null}],
                editedRequest
            );
        }
    }

    openConfirmSendModal = () => {
        this.props.openModal(SEND_BULK_REQUESTS_MODAL, {fundId: this.props.fund.id, singleRowAction: true});
    }

    render() {
        const {menuItems, fund, history, editedRequest, isEditMode, fundIsLoading, sendRequest} = this.props;
        const { id: fundId } = this.props.match.params;

        return (fund && fund.id === fundId)
            ? (
                <DataReportingPage
                    menuItems={menuItems}
                    fund={fund}
                    history={history}
                    editedRequest={editedRequest}
                    saveEditedRequest={this.handleSaveEditedRequest}
                    isEditMode={isEditMode}
                    fundIsLoading={fundIsLoading}
                    toggleMode={this.handleToggleMode}
                    openConfirmSendModal={this.openConfirmSendModal}
                    sendRequest={sendRequest}
                >
                {{
                    RequestEditor:
                        <RequestEditor
                            fundId={fundId}
                            isEditMode={isEditMode}
                            createRequest={this.handleToggleMode}
                        />
                }}
                </DataReportingPage>
            )
            : null;
    }
}

const mapStateToProps = (state: AppState) => ({
    fund: getSingleFundState(state),
    history: getDataReportingStateSelector(state),
    editedRequest: getEditedRequest(state),
    isEditMode: isEditModeSelector(state),
    fundIsLoading: getActionIsInProgress(state, {name: GET_FUND})
});

const mapDispatchToProps = {
    getFund,
    setEditedRequest,
    saveEditedRequest,
    getFundReportingHistory,
    toggleEditMode,
    openModal,
    sendRequest: sendRequestFromDataReporting
};

export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps)
)(DataReporting);