import * as React from 'react';
import { PureComponent } from 'react';
import { connect } from 'react-redux';
import DataEntry from 'components/Shared/DataEntry/DataEntry';
import { DataEntryPart } from 'store/Fund/types';
import { AppState } from 'store/types';
import { getFundsDataEntry } from 'store/Fund/selectors';
import { getFundsDataEntries } from 'store/Fund/actionCreators';
import { getDataEntry, getPeriod } from 'store/Filters/selectors';
import { GetFundsDataEntriesAction } from 'store/Fund/actions';
import { getUser } from 'store/Auth/selectors';
import ActiveUser from 'store/Auth/models/activeUser';

interface Props {
    dataEntries: DataEntryPart[];
    period: string;
    selectedDataEntry: string;
    getDataEntries: (period: string) => GetFundsDataEntriesAction;
    handleSelect?: ((data: any) => void) | undefined;
    user: ActiveUser;
}

class DataEntryContainer extends PureComponent<Props> {
    componentDidMount() {
        this.props.getDataEntries(this.props.period);
    }

    componentDidUpdate(prevProps: Props) {
        if (prevProps.period !== this.props.period) {
            this.props.getDataEntries(this.props.period);
        }
    }

    render() {
        const { dataEntries, selectedDataEntry, handleSelect, user } = this.props;

        return (
            <DataEntry
                data={dataEntries}
                selectedItem={selectedDataEntry}
                handleSelect={handleSelect}
                user={user}
            />
        );
    }
}

const mapStateToProps = (state: AppState) => ({
    dataEntries: getFundsDataEntry(state),
    period: getPeriod(state),
    selectedDataEntry: getDataEntry(state),
    user: getUser(state),
});

const mapDispatchToProps = {
    getDataEntries: getFundsDataEntries
};

export default connect(mapStateToProps, mapDispatchToProps)(DataEntryContainer);