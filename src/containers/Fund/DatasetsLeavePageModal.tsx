import * as React from 'react';
import { connect } from 'react-redux';
import { Location } from 'history';
import { compose } from 'recompose';

import { AppState, ButtonType } from 'store/types';
import { closeModal as closeModalCreator } from 'store/Modals/General/actionCreators';
import { DATASETS_LEAVE_PAGE_MODAL_TITLE } from 'store/Fund/constants';
import ConfirmModal from 'containers/Shared/ConfirmModal';
import { getModalParams } from 'store/Modals/General/selectors';
import { withRouter } from 'react-router';

interface ModalParams {
    location: Location;
    handleYesClick: any;
}

interface Props {
    closeModal: () => any;
    modalParams: ModalParams;
}

class DatasetsLeavePageModal extends React.PureComponent<Props> {
    handleYesClick = () => {
        const {modalParams, closeModal} = this.props;
        if (modalParams.location && modalParams.handleYesClick) {
            // Navigate to the previous blocked location
            if (modalParams.location && modalParams.location.hasOwnProperty('pathname')) {
                modalParams.handleYesClick((modalParams.location as Location).pathname);
                closeModal();
            }
        }
    }

    render() {
        const {closeModal} = this.props;
        return (
            <ConfirmModal
                title={DATASETS_LEAVE_PAGE_MODAL_TITLE}
                body={'Data will be discarded if not saved. Are you sure you want to go to another page?'}
                chooseNo={closeModal}
                chooseYes={this.handleYesClick}
                yesType={ButtonType.Danger}
            />
        );
    }
}

const mapStateToProps = (state: AppState) => ({
    modalParams: getModalParams(state),
});

const mapDispatchToProps = {
    closeModal: closeModalCreator,
};

export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps),
)(DatasetsLeavePageModal);