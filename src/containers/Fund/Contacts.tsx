import * as React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { match } from 'react-router';
import { cloneDeep } from 'lodash';
import { Action } from 'redux';
import memoize from 'memoize-one';

import ActionDecorator from 'decorators/ActionDecorator';
import { BASE_DILIGENCE_URL } from 'helpers/diligenceApiHelper';

import MainLayout from 'components/Shared/Layouts/MainLayout';
import DataTable from 'components/Shared/DataTable/DataTable';
import UiDataTable from 'components/Shared/Ui/Tables/UiDataTable';
import ButtonPrimary from 'components/Shared/Ui/Buttons/ButtonPrimary';
import ContactsPage from 'components/Fund/ContactsPage';

import { AppState, NavigationButton, TableHeader, TableRowAction, TableBulkAction, Id } from 'store/types';
import { ContactModel, DeleteContactsModel, NewContactModel, UpdateContactModel } from 'store/Fund/models/contact';
import { FundModel } from 'store/Fund/models/fund';
import { CONFIRM_DELETE_CONTACT_MODAL, ID_FOR_NEW_ROW } from 'store/Fund/constants';
import {
    DefaultStateContactList,
    TableBulkActionChoices, TableRowActionType, TableRowActionIcon, Alignment
} from 'store/constants';
import { openModal } from 'store/Modals/General/actionCreators';
import { getContacts, editContact, addContact, unsetAddingContactResult } from 'store/Fund/actionCreators';
import { getFundState, getSingleFundState } from 'store/Fund/selectors';
import { getFilters } from 'store/Filters/selectors';
import { setCheckedIds, setFilters } from 'store/Filters/actionCreators';
import { FilterNames } from 'store/Filters/types';
import { AddContactAction, EditContactAction, GetContactsAction } from 'store/Fund/actions';
import { OpenModalAction } from 'store/Modals/General/actions';
import { getUser } from 'store/Auth/selectors';
import ActiveUser from 'store/Auth/models/activeUser';
import { hasDiligenceId } from 'store/Account/helpers';

import ConfirmDeleteModal from './ConfirmDeleteContactModal';

interface Props {
    menuItems: NavigationButton[];
    checkedAll: boolean;
    checkedIds: Id[];
    chooseAll: () => void;
    chooseRow: (id: Id) => void;
    results: ContactModel[];
    count: number;
    currentFund: FundModel;
    getContacts: (fundId: Id) => GetContactsAction;
    editContact: (fundId: Id, contactId: Id, contact: UpdateContactModel) => EditContactAction;
    addContact: (contact: NewContactModel) => AddContactAction;
    openConfirmDeleteModal: (name: string, params: DeleteContactsModel) => OpenModalAction;
    match: match<{id: Id}>;
    baseUrl?: string;
    addingContactResult?: boolean;
    unsetAddingContactResult: () => Action;
    user: ActiveUser;
}

interface State {
    editingRow?: ContactModel;
    isEditing: boolean;
    editingRowNumber: number;
}

const filterName = FilterNames.contactList;

class Contacts extends React.PureComponent<Props, State> {
    defaultState = {
        editingRow: { id: ID_FOR_NEW_ROW, first_name: '', last_name: '', email: ''},
        isEditing: false,
        editingRowNumber: -1,
    };
    state = this.defaultState;

    bulkActions: TableBulkAction[] = [
        { type: TableBulkActionChoices.Delete, handler: () => this.toggleConfirmDelete() },
    ];

    getButtons = memoize((isEditing, handleAddAction) => [(
        <ButtonPrimary
            className="float-right ml-3"
            handleClick={handleAddAction}
            disabled={isEditing}
        >
            Add New Contact
        </ButtonPrimary>
    )]);

    getHeaders = memoize((isEditableRowNew, isCurrentRowEditing): TableHeader[] => [
        {
            name: 'first_name',
            title: 'First Name',
            active: true,
            headerAlignment: Alignment.Left,
            valueAlignment: Alignment.Left,
        },
        {
            name: 'last_name',
            title: 'Last Name',
            active: true,
            headerAlignment: Alignment.Left,
            valueAlignment: Alignment.Left,
        },
        {
            name: 'email',
            title: 'Email',
            active: true,
            headerAlignment: Alignment.Left,
            valueAlignment: Alignment.Left,
            excludeEditField: (row: ContactModel) => !isEditableRowNew(),
            popover: (
                <>
                    The email address of contact cannot be edited. Instead, delete this contact and create new one.
                </>
            ),
            isPopoverVisible: (row: ContactModel, idx: number) => {
                return isCurrentRowEditing(row, idx) && !isEditableRowNew();
            },
        }]
    );

    getRowActions = memoize((
        isEditing,
        isCurrentRowEditing,
        handleEditAction,
        handleDeleteAction,
        handleCancelAction,
        handleSaveAction,
    ): TableRowAction[] => [
        {
            type: TableRowActionType.Image,
            icon: TableRowActionIcon.Pencil,
            handler: (id: Id) => handleEditAction(id),
            disabled: (row: ContactModel, tableRowId: number) => {
                return isEditing && !isCurrentRowEditing(row, tableRowId);
            },
            isVisible: (row: ContactModel, tableRowId: number) => {
                return !isCurrentRowEditing(row, tableRowId);
            },
        },
        {
            type: TableRowActionType.Image,
            icon: TableRowActionIcon.Trash,
            handler: (id: Id) => handleDeleteAction(id),
            disabled: (row: ContactModel, tableRowId: number) => {
                return isEditing && !isCurrentRowEditing(row, tableRowId);
            },
            isVisible: (row: ContactModel, tableRowId: number) => {
                return !isCurrentRowEditing(row, tableRowId);
            },
        },
        {
            type: TableRowActionType.Image,
            icon: TableRowActionIcon.Close,
            handler: (id: Id) => handleCancelAction(id),
            isVisible: (row: ContactModel, tableRowId: number) => {
                return isCurrentRowEditing(row, tableRowId);
            },
        },
        {
            type: TableRowActionType.Image,
            icon: TableRowActionIcon.Refresh,
            handler: (id: Id) => handleSaveAction(id),
            isVisible: (row: ContactModel, tableRowId: number) => {
                return isCurrentRowEditing(row, tableRowId);
            },
        }]
    );

    componentDidMount() {
        const fundId = this.getFundId();
        this.props.getContacts(fundId);
        setFilters(FilterNames.contactList, DefaultStateContactList.filter);
    }

    componentDidUpdate(prevProps: Props) {
        if (this.props.addingContactResult !== prevProps.addingContactResult && this.props.addingContactResult) {
            this.setState(this.defaultState);
            this.props.unsetAddingContactResult();
        }
        if (this.props.count < prevProps.count && this.state.isEditing) {
            this.props.results[this.props.count] = this.state.editingRow;
            this.setState({editingRowNumber: this.props.count});
        }
    }

    isEditableRowNew = (): boolean => {
        return this.state.isEditing && this.state.editingRow && this.state.editingRow.id === ID_FOR_NEW_ROW || false;
    }

    isCurrentRowEditing = (row: ContactModel, tableRowId: number): boolean => {
        return this.state.isEditing && tableRowId === this.state.editingRowNumber;
    }

    getFundId = () => {
        return this.props.currentFund.id || this.props.match.params.id || '';
    }

    renderBreadcrumbs = () => {
        if (this.props.baseUrl === '/funds') {
            return (
                <div className="ap-breadcrumbs">
                    <span className="breadcrumb-slash">/</span>
                    <NavLink to="/funds">Funds</NavLink>
                    <span className="breadcrumb-slash">/</span>
                    <span>{this.props.currentFund.name}</span>
                </div>
            );
        }

        return (
            <div className="ap-breadcrumbs">
                <NavLink to="/firms">Firms</NavLink>
                <span className="breadcrumb-slash">/</span>
                <NavLink to={`/firms/${this.props.currentFund.firm.id}`}>{this.props.currentFund.firm.name}</NavLink>
                <span className="breadcrumb-slash">/</span>
                <NavLink to="/funds">Funds</NavLink>
                <span className="breadcrumb-slash">/</span>
                <span>{this.props.currentFund.name}</span>
            </div>
        );
    }

    toggleConfirmDelete = () => {
        const fundId = this.getFundId();
        let contactIds: Id[] = this.props.checkedIds;
        if (this.props.checkedAll && this.props.results) {
            contactIds = this.props.results.map(
                (contact: ContactModel) => {
                    return contact.id || ID_FOR_NEW_ROW;
                }
            ).filter((id: Id) => {
                return id !== ID_FOR_NEW_ROW
                    && (this.props.checkedIds.length > 0 && this.props.checkedIds.indexOf(id) === -1
                        || this.props.checkedIds.length === 0);
            });
        }
        this.props.openConfirmDeleteModal(CONFIRM_DELETE_CONTACT_MODAL, {fundId, contactIds} );
    }

    openInDiligence = () => {
        const {id, firm} = this.props.currentFund;
        const url = `${BASE_DILIGENCE_URL}/portfolios/master/advisers/${firm.id}?section=vehicles&vehicleId=${id}`;
        window.open(url, '_blank');
    }

    handleAddAction = () => {
        if (hasDiligenceId(this.props.user.active_account)) {
            this.openInDiligence();
        } else {
            const countRows = this.props.count;
            const newRow: ContactModel = { id: ID_FOR_NEW_ROW, first_name: '', last_name: '', email: '' };
            this.props.results[countRows] = newRow;
            this.setState({ editingRow: newRow, isEditing: true, editingRowNumber: countRows });
        }
    }

    handleEditAction = (id: Id) => {
        if (hasDiligenceId(this.props.user.active_account)) {
            this.openInDiligence();
        } else {
            const chosenId = this.props.results.findIndex((resultRow) => {
                return resultRow.id === id;
            });
            const chosenRow = this.props.results.find((resultRow) => {
                return resultRow.id === id;
            });
            this.setState({ editingRow: chosenRow, isEditing: true, editingRowNumber: chosenId });
        }
    }

    handleCancelAction = (id: Id) => {
        const editableRow = this.props.results.find((resultRow) => {
            return resultRow.id === id;
        });
        const chosenId = this.props.results.findIndex((resultRow) => {
            return resultRow.id === id;
        });
        if (editableRow && editableRow.id === ID_FOR_NEW_ROW) {
            this.props.results.splice(chosenId, 1);
        }
        this.setState(this.defaultState);
    }

    handleSaveAction = (id: Id) => {
        const fundId = this.getFundId();
        if (id === ID_FOR_NEW_ROW) {
            const newContact: NewContactModel = {
                fund: fundId,
                first_name: this.state.editingRow.first_name,
                last_name: this.state.editingRow.last_name,
                email: this.state.editingRow.email,
            };
            this.props.addContact(newContact);
        } else {
            const updatedContact: UpdateContactModel = {
                first_name: this.state.editingRow.first_name,
                last_name: this.state.editingRow.last_name,
                email: this.state.editingRow.email,
            };
            this.props.editContact(fundId, id, updatedContact);
            this.setState(this.defaultState);
        }
    }

    handleColumnChange = (value: any, name: string) => {
        const editingRow = cloneDeep(this.state.editingRow);
        editingRow[name] = value;
        this.setState({editingRow});
    }

    handleDeleteAction = (id: Id) => {
        if (hasDiligenceId(this.props.user.active_account)) {
            this.openInDiligence();
        } else {
            const fundId = this.getFundId();
            this.props.openConfirmDeleteModal(CONFIRM_DELETE_CONTACT_MODAL, {fundId, contactIds: [id]});
        }
    }

    render() {
        const breadCrumbs = this.renderBreadcrumbs();
        return (
            <MainLayout
                title={this.props.currentFund.name}
                menuItems={this.props.menuItems}
                breadCrumbs={breadCrumbs}
            >
                <ContactsPage>
                    <UiDataTable>
                        <DataTable
                            buttons={this.getButtons(this.state.isEditing, this.handleAddAction)}
                            rows={this.props.results}
                            count={this.props.results.length}
                            headers={this.getHeaders(this.isEditableRowNew, this.isCurrentRowEditing)}
                            hasSorting={false}
                            hasPagination={false}
                            withCheckboxes={!hasDiligenceId(this.props.user.active_account)}
                            bulkActions={this.bulkActions}
                            checkedAll={this.props.checkedAll}
                            checkedIds={this.props.checkedIds}
                            chooseAll={this.props.chooseAll}
                            chooseRow={this.props.chooseRow}
                            rowActions={this.getRowActions(
                                this.state.isEditing,
                                this.isCurrentRowEditing,
                                this.handleEditAction,
                                this.handleDeleteAction,
                                this.handleCancelAction,
                                this.handleSaveAction,
                            )}
                            editingRowNumber={this.state.isEditing ? this.state.editingRowNumber : undefined}
                            editingRow={this.state.isEditing && this.state.editingRow || undefined}
                            handleColumnChange={this.handleColumnChange}
                        />
                    </UiDataTable>
                    <ConfirmDeleteModal />
                </ContactsPage>
            </MainLayout>
        );
    }
}

const mapStateToProps = (state: AppState) => ({
    results: getFundState(state).contacts,
    count: getFundState(state).contacts.length,
    currentFund: getSingleFundState(state),
    currentFilter: getFilters(state, filterName),
    checkedAll: getFilters(state, filterName).checkedAll,
    checkedIds: getFilters(state, filterName).checkedIds,
    addingContactResult: getFundState(state).addingContactResult,
    user: getUser(state),
});

const mapDispatchToProps = {
    editContact,
    addContact,
    getContacts,
    openConfirmDeleteModal: openModal,
    setCheckedIds,
    unsetAddingContactResult,
};

export default connect(mapStateToProps, mapDispatchToProps)(ActionDecorator(Contacts, filterName));
