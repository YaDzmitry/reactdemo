import * as React from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router';

import { isRequestor, isResponder } from 'store/User/helpers';
import { getUser } from 'store/Auth/selectors';
import { AppState, NavigationButton } from 'store/types';

import FundPage from 'components/Fund/FundPage';
import MainLayout from 'components/Shared/Layouts/MainLayout';

import Profile from './Profile/Profile';
import Contacts from './Contacts';
import FundList from './FundList';
import DatasetList from '../DataManager/DatasetList';
import DataReporting from './DataReporting';
import Datasets from './FundDatasets';
import DataManagerFund from '../DataManager/Fund';

const Fund: React.SFC<any> = (props) => {
    const url = props.match.url;
    let menuItems: NavigationButton[] = [{path: url, title: ''}];
    let switchRouter: any = '';
    if (isRequestor(props.user)) {
        switchRouter = (
            <Switch>
                <Route
                    path="/funds/:id"
                    exact={true}
                    render={(properties: any) => <Profile menuItems={menuItems} {...properties}  />}
                />
                <Route
                    path="/funds"
                    exact={true}
                    component={FundList}
                />
                <Route
                    path="/funds/:id/datasets"
                    exact={true}
                    render={(properties: any) => <Datasets menuItems={menuItems} {...properties}  />}
                />
                <Route
                    path="/funds/:id/contacts"
                    exact={true}
                    render={(properties: any) => <Contacts menuItems={menuItems} {...properties}  />}
                />
                <Route
                    path="/funds/:id/data-reporting"
                    exact={true}
                    render={(properties: any) => <DataReporting menuItems={menuItems} {...properties} />}
                />
            </Switch>
        );
        menuItems = [
            {path: url, title: 'Profile'},
            {path: `${url}/datasets`, title: 'Datasets'},
            {path: `${url}/contacts`, title: 'Contacts'},
            {path: `${url}/data-reporting`, title: 'Data Reporting'},
        ];
    } else if (isResponder(props.user)) {
        menuItems = [
            {path: `${url}/data`, title: 'Data'},
            {path: `${url}/requestors`, title: 'Requestors'},
            {path: `${url}/acl-users`, title: 'Users'},
            {path: `${url}/profile`, title: 'Profile'},
        ];
        switchRouter = (
            <Switch>
                <Route
                    path="/funds"
                    exact={true}
                    render={(properties: any) => (
                        <MainLayout title="Funds" menuItems={[]}>
                            <DatasetList {...properties} />
                        </MainLayout>
                    )}
                />
                <Route
                    path="/funds/:id/profile"
                    render={(properties: any) => <Profile baseUrl={'/funds'} menuItems={menuItems} {...properties}  />}
                />
                <Route
                    path="/funds/:fundId"
                    render={(data: any) => <DataManagerFund baseUrl={'/funds'} {...data}/>}
                />
            </Switch>
        );
    }

    return (
        <FundPage menuItems={menuItems}>
            {switchRouter}
        </FundPage>
    );
};

const mapStateToProps = (state: AppState) => ({
    user: getUser(state)
});

export default connect(mapStateToProps, null)(Fund);
