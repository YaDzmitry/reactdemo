import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router-dom';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';

import RequestorPeerGroupsTab from 'containers/Shared/Modals/AddItemsModal/RequestorPeerGroupsTabContainer';

import { AppState } from 'store/types';
import { FilterNames } from 'store/Filters/types';
import { addPeerGroupsToProfileStatistics, getPeerGroupListForAddItemsModal } from 'store/Fund/actionCreators';
import { getSingleFundId } from 'store/Fund/selectors';
import { ADD_PEER_GROUPS_TO_PROFILE_STATISTICS, GET_PEER_GROUP_LIST_FOR_ADD_ITEMS_MODAL } from 'store/Fund/actions';
import { getActionIsInProgress } from 'store/Actions/selectors';
import { getRememberSelectedFund } from 'store/Auth/selectors';

const filterName = FilterNames.requestorAddItemsModalPeerGroupsList;

const mapStateToProps = (state: AppState) => ({
    fundId: getSingleFundId(state),
    rememberSelected: getRememberSelectedFund(state),
    isGridLoading: getActionIsInProgress(state, {name: GET_PEER_GROUP_LIST_FOR_ADD_ITEMS_MODAL}),
    isAdding: getActionIsInProgress(state, {name: ADD_PEER_GROUPS_TO_PROFILE_STATISTICS}),
});

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        addPeerGroups: () => {
            dispatch(addPeerGroupsToProfileStatistics(ownProps.fundId, filterName, ownProps.rememberSelected));
        },
        ...bindActionCreators(
            {
                applyFilter: getPeerGroupListForAddItemsModal,
            },
            dispatch
        )
    };
};

export default compose(
    connect(mapStateToProps),
    connect(null, mapDispatchToProps),
)(RequestorPeerGroupsTab);