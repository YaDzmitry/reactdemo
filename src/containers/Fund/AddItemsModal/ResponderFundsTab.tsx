import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { bindActionCreators } from 'redux';

import ResponderFundsTab from 'containers/Shared/Modals/AddItemsModal/ResponderFundsTabContainer';

import { AppState } from 'store/types';
import { FilterNames } from 'store/Filters/types';
import { addFundsToProfileStatistics, getFundListForAddItemsModal } from 'store/Fund/actionCreators';
import { getSingleFundId } from 'store/Fund/selectors';
import { getRememberSelectedFund } from 'store/Auth/selectors';
import { getActionIsInProgress } from 'store/Actions/selectors';
import { ADD_FUNDS_TO_PROFILE_STATISTICS, GET_FUND_LIST_FOR_ADD_ITEMS_MODAL } from 'store/Fund/actions';

const filterName = FilterNames.responderAddItemsModalFundList;

const mapStateToProps = (state: AppState) => ({
    fundId: getSingleFundId(state),
    rememberSelected: getRememberSelectedFund(state),
    isGridLoading: getActionIsInProgress(state, {name: GET_FUND_LIST_FOR_ADD_ITEMS_MODAL}),
    isAdding: getActionIsInProgress(state, {name: ADD_FUNDS_TO_PROFILE_STATISTICS}),
});

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        addFunds: () => {
            dispatch(addFundsToProfileStatistics(ownProps.fundId, filterName, ownProps.rememberSelected));
        },
        ...bindActionCreators(
            {
                applyFilter: getFundListForAddItemsModal,
            },
            dispatch
        )
    };
};

export default compose(
    connect(mapStateToProps),
    connect(null, mapDispatchToProps),
)(ResponderFundsTab);