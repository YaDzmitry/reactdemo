import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';

import ModalWithTabs from 'components/Shared/Modals/ModalWithTabs';
import { TabItem } from 'components/Shared/Tabs/TabPanel';
import BenchmarksTab from 'components/Shared/Modals/AddItemsModal/BenchmarksTab';
import RequestorFundsTab from 'containers/Fund/AddItemsModal/RequestorFundsTab';
import ResponderFundsTab from 'containers/Fund/AddItemsModal/ResponderFundsTab';
import RequestorPeerGroupsTab from 'containers/Fund/AddItemsModal/RequestorPeerGroupsTab';

import { AppState } from 'store/types';
import { isModalOpen } from 'store/Modals/General/selectors';
import { closeModal } from 'store/Modals/General/actionCreators';
import { ADD_ITEMS_TO_STATISTICS_MODAL, ADD_ITEMS_TO_STATISTICS_MODAL_TITLE } from 'store/Fund/constants';
import { getActiveAccount } from 'store/Auth/selectors';
import { CloseModalAction } from 'store/Modals/General/actions';
import { isRequestorUserAccount, isResponderUserAccount } from 'store/User/helpers';
import { UserAccount } from 'store/Auth/models/userAccount';

interface Props {
    title: string;
    isOpen: boolean;
    closeModal: () => CloseModalAction;
    userAccount: UserAccount;
}

class AddItemsModal extends React.PureComponent<Props> {
    tabItems: TabItem[] = [
        {title: 'Funds', content: (
            <>
            {isRequestorUserAccount(this.props.userAccount) && <RequestorFundsTab />}
            {isResponderUserAccount(this.props.userAccount) && <ResponderFundsTab />}
            </>
        )},
        {title: 'Benchmarks', content: (<BenchmarksTab />), disabled: true},
        {
            title: 'Peer Groups',
            content: (<RequestorPeerGroupsTab />),
            disabled: isResponderUserAccount(this.props.userAccount),
        },
    ];

    render() {

        return (
            <ModalWithTabs {...this.props} tabItems={this.tabItems}/>
        );
    }

}

const mapStateToProps = (state: AppState) => ({
    userAccount: getActiveAccount(state),
    isOpen: isModalOpen(state, {name: ADD_ITEMS_TO_STATISTICS_MODAL}),
    title: ADD_ITEMS_TO_STATISTICS_MODAL_TITLE,
});

const mapDispatchToProps = {
    closeModal
};

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
)(AddItemsModal);
