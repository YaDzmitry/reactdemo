import * as React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { RouteComponentProps, withRouter } from 'react-router';
import { compose } from 'recompose';

import FundProfilePage from 'components/Fund/Profile/FundProfilePage';
import MainLayout from 'components/Shared/Layouts/MainLayout';
import { InfoTableData } from 'components/Shared/DataTable/InfoTable';
import GearButton from 'components/Shared/Ui/IconButtons/GearButton';
import FundProfileSettingsModal from 'containers/Fund/Profile/FundProfileSettingsModal';
import AddItemsToProfileStatisticsModal from 'containers/Fund/AddItemsModal/AddItemsModal';

import {
    getFund, removeEntityFromProfileStatistics, setFundProfileHistoryPeriod,
    changeChartInterval as changeChartIntervalCreator, selectPeriod
} from 'store/Fund/actionCreators';
import { AppState, Id, NavigationButton, FilterItem } from 'store/types';
import {
    getFundDetailsData, getFundProfile, getFundStatistics, getMainFundDatasets, getServiceProvidersData,
    getSingleFundState, getYearMonthData, getAlertsOverviewState, getChartConfig, getFundStatisticsData,
} from 'store/Fund/selectors';
import { FundModel, EntityDatasets } from 'store/Fund/models/fund';
import { HistoryPeriodChoices } from 'store/constants';
import {
    FundStatistics,
    FundProfileFilterState,
    ChartConfig,
    FundProfileTableData
} from 'store/Fund/types';
import {
    ChangeChartIntervalAction,
    RemoveEntityFromProfileStatisticsAction,
    SelectPeriodAction,
} from 'store/Fund/actions';
import { openModal } from 'store/Modals/General/actionCreators';
import { OpenModalAction } from 'store/Modals/General/actions';
import { ModalParams } from 'store/Modals/General/types';
import { ADD_ITEMS_TO_STATISTICS_MODAL, FUND_PROFILE_SETTINGS_MODAL } from 'store/Fund/constants';
import { AlertsOverview } from 'store/AlertsManager/Alerts/types';
import { setFilters } from 'store/Filters/actionCreators';
import { FilterNames, FiltersState } from 'store/Filters/types';
import { FilterType } from 'store/constants';
import { getFilters, getFiltersOrUserSettings } from 'store/Filters/selectors';
import Filter, { FilterDecoratorChange } from 'decorators/FilterDecorator';
import { applyFilterFundProfile } from 'store/Fund/actionCreators';
import { DefaultFundProfileState } from 'store/Fund/constants';
import { getUserAccount, getUserAccountSettings } from 'store/Auth/selectors';
import { Account } from 'store/Account/models/account';
import { UserAccountSettings } from 'store/Auth/models/userAccount';

export interface RouteProps {
    id: Id;
}

const filterName = FilterNames.fundProfile;

interface Props {
    match: any;
    menuItems: NavigationButton[];
    currentFund: FundModel;
    fundDetails: InfoTableData[];
    serviceProviders: InfoTableData[];
    mainFundDatasets: EntityDatasets;
    navTableData: FundProfileTableData;
    performanceTableData: FundProfileTableData;
    currentHistoryPeriod: HistoryPeriodChoices;
    getFund: (fundId: Id, withRequest?: boolean) => any;
    setHistoryPeriod: (historyPeriod: HistoryPeriodChoices) => any;
    baseUrl?: string;
    statistics: FundStatistics[];
    chartStatistics: FundStatistics[];
    chartConfig: ChartConfig;
    removeEntityStatistics: (fundId: Id, removedEntityId: Id, rememberSelected: boolean | undefined)
        => RemoveEntityFromProfileStatisticsAction;
    openModal: (name: string, params: ModalParams) => OpenModalAction;
    alertsOverview: AlertsOverview[];
    setFilters: (filterName: string, filter: FiltersState) => void;
    showDataEntry: boolean;
    currentFilter: FundProfileFilterState;
    handleChange: FilterDecoratorChange;
    account: Account;
    userAccountSettings: UserAccountSettings;
    changeChartInterval: (start: number, end: number) => ChangeChartIntervalAction;
    selectPeriod: (period: string | null) => SelectPeriodAction;
    datasetsFilter: FiltersState;
}

type FinalProps = Props & RouteComponentProps<RouteProps>;

class Profile extends React.PureComponent<FinalProps> {
    settingButtons: React.ReactNode[];

    constructor(props: FinalProps) {
        super(props);

        this.settingButtons = [(
            <GearButton key={'settings-gear-button'} onClick={this.handleGearClick} />
        )];
    }

    componentDidMount() {
        if (this.routeFundId && this.routeFundId !== this.fundId) {
            this.initFund(this.routeFundId);
        }
    }

    componentDidUpdate() {
        if (this.routeFundId && this.fundId && this.routeFundId !== this.fundId) {
            this.initFund(this.routeFundId);
        }
    }

    initFund(fundId: string) {
        this.props.getFund(fundId);
    }

    renderBreadCrumps = () => {
        if (this.props.baseUrl === '/funds') {
            return (
                <div className="ap-breadcrumbs">
                    <NavLink to="/funds">Funds</NavLink>
                    <span className="breadcrumb-slash">/</span>
                    <span>{this.props.currentFund.name}</span>
                </div>
            );
        }

        return (
            <div className="ap-breadcrumbs">
                <NavLink to="/firms">Firms</NavLink>
                <span className="breadcrumb-slash">/</span>
                <NavLink to={`/firms/${this.props.currentFund.firm.id}`}>{this.props.currentFund.firm.name}</NavLink>
                <span className="breadcrumb-slash">/</span>
                <NavLink to="/funds">Funds</NavLink>
                <span className="breadcrumb-slash">/</span>
                <span>{this.props.currentFund.name}</span>
            </div>
        );
    }

    openAddFundToStatsModal = () => {
        this.props.openModal(ADD_ITEMS_TO_STATISTICS_MODAL, {});
    }

    handleGearClick = () => {
        this.props.openModal(FUND_PROFILE_SETTINGS_MODAL, {});
    }

    viewFundAlerts = (period: string) => {
        const currentFilter: FiltersState = {
            period: period,
        };

        this.props.setFilters(FilterNames.alertList, currentFilter);
        this.props.history.push(`/alerts/${this.fundId}`);
    }

    removeFund = (removedEntityId: Id) => {
        /**
         * Remove Entity from statistic/profile chart
         * and save user fund profile settings with left entities if rememberSelected is True
         */
        const {userAccountSettings} = this.props;

        this.props.removeEntityStatistics(this.fundId, removedEntityId, userAccountSettings.remember_selected_funds);
    }

    render() {
        const breadCrumbs = this.renderBreadCrumps();
        const {
            menuItems, currentFund, mainFundDatasets, setHistoryPeriod, currentHistoryPeriod, fundDetails,
            serviceProviders, navTableData, performanceTableData, statistics, chartStatistics,
            alertsOverview, showDataEntry, currentFilter, handleChange, account, changeChartInterval, chartConfig,
            datasetsFilter, setFilters: setFiltersCreator,
        } = this.props;
        const filters: FilterItem[] = [
            {
                name: 'display_fund_data_source',
                type: FilterType.DottedToggle,
                title: 'Show Data Source',
                withLabel: true,
                visible: true,
                className: 'zero-height-filter'
            }
        ];

        return (
            <MainLayout
                title={currentFund.name}
                menuItems={menuItems}
                menuSubItems={this.settingButtons}
                breadCrumbs={breadCrumbs}
                filters={filters}
                currentFilter={currentFilter}
                handleChange={handleChange}
                account={account}
            >
                {this.routeFundId === this.fundId && <FundProfilePage
                    fund={currentFund}
                    mainFundDatasets={mainFundDatasets}
                    setHistoryPeriod={setHistoryPeriod}
                    currentHistoryPeriod={currentHistoryPeriod}
                    fundDetails={fundDetails}
                    serviceProviders={serviceProviders}
                    navTableData={navTableData}
                    performanceTableData={performanceTableData}
                    statistics={statistics}
                    chartStatistics={chartStatistics}
                    removeFund={this.removeFund}
                    openModal={this.openAddFundToStatsModal}
                    alertsOverview={alertsOverview}
                    viewFundAlerts={this.viewFundAlerts}
                    showDataEntry={showDataEntry}
                    account={account}
                    changeChartInterval={changeChartInterval}
                    chartConfig={chartConfig}
                    selectPeriod={this.props.selectPeriod}
                    datasetsFilter={datasetsFilter}
                    setFilters={setFiltersCreator}
                />}
                <AddItemsToProfileStatisticsModal />
                <FundProfileSettingsModal />
            </MainLayout>
        );
    }

    private get fundId() {
        return this.props.currentFund.id;
    }

    private get routeFundId() {
        return (this.props.match && this.props.match.params && this.props.match.params.id) || null;
    }
}

const mapStateToProps = (state: AppState) => ({
    currentFund: getSingleFundState(state),
    fundDetails: getFundDetailsData(state),
    serviceProviders: getServiceProvidersData(state),
    currentHistoryPeriod: getFundProfile(state).historyPeriod,
    mainFundDatasets: getMainFundDatasets(state),
    navTableData: getYearMonthData(state, {field: 'ending_balance'}),
    performanceTableData: getYearMonthData(state, {field: 'perf_month'}),
    statistics: getFundStatistics(state),
    chartStatistics: getFundStatisticsData(state),
    alertsOverview: getAlertsOverviewState(state),
    currentFilter: getFiltersOrUserSettings(state, filterName),
    showDataEntry: getFiltersOrUserSettings(state, filterName).display_fund_data_source,
    account: getUserAccount(state),
    chartConfig: getChartConfig(state),
    userAccountSettings: getUserAccountSettings(state),
    datasetsFilter: getFilters(state, FilterNames.datasetsTab),
});

const mapDispatchToProps = {
    openModal,
    getFund,
    setHistoryPeriod: setFundProfileHistoryPeriod,
    removeEntityStatistics: removeEntityFromProfileStatistics,
    setFilters,
    applyFilter: applyFilterFundProfile,
    changeChartInterval: changeChartIntervalCreator,
    selectPeriod,
};

export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps),
    (component: React.Component) => Filter(component, {...DefaultFundProfileState}, filterName, true)
)(Profile);
