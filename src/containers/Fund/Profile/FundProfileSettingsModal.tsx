import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';

import FundProfileSettings from 'components/Fund/Profile/FundProfileSettingsModel/FundProfileSettings';

import { AppState } from 'store/types';
import { isModalOpen } from 'store/Modals/General/selectors';
import { closeModal } from 'store/Modals/General/actionCreators';
import { FUND_PROFILE_SETTINGS_MODAL } from 'store/Fund/constants';
import { getEnableShowDataSource, getRememberSelectedFund, getUser } from 'store/Auth/selectors';
import { getSingleFundId } from 'store/Fund/selectors';
import { updateAccountSettings, updateUserAccountSettings } from 'store/Auth/actionCreators';

const mapStateToProps = (state: AppState) => ({
    activeUser: getUser(state),
    isOpen: isModalOpen(state, {name: FUND_PROFILE_SETTINGS_MODAL}),
    rememberSelectedFunds: getRememberSelectedFund(state),
    enableShowDataSource: getEnableShowDataSource(state),
    fundId: getSingleFundId(state),
});

const mapDispatchToProps = {
    closeModal,
    updateUserAccountSettings,
    updateAccountSettings,
};

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
)(FundProfileSettings);
