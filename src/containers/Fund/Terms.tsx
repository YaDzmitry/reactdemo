import * as React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';

import MainLayout from 'components/Shared/Layouts/MainLayout';
import { getFund } from '../../store/Fund/actionCreators';
import { AppState, Id, NavigationButton } from '../../store/types';
import { getSingleFundState } from '../../store/Fund/selectors';
import FundTermsPage from '../../components/Fund/Terms';
import { InfoTableData } from '../../components/Shared/DataTable/InfoTable';
import { FundModel } from '../../store/Fund/models/fund';

interface Props {
    match: any;
    menuItems: NavigationButton[];
    currentFund: FundModel;
    fundDetails: InfoTableData[];
    getFund: (fundId: Id, withRequest?: boolean) => any;
}

class Terms extends React.PureComponent<Props> {

    componentDidMount() {
        const fundId = this.props.match && this.props.match.params && this.props.match.params.id
            ? this.props.match.params.id
            : 0;
        if (fundId) {
            this.props.getFund(fundId);
        }
    }

    render() {
        const breadCrumbs = (
            <div className="ap-breadcrumbs">
                <NavLink to="/funds">Funds</NavLink>
                <span className="breadcrumb-slash">/</span>
                <span>{this.props.currentFund.name}</span>
            </div>
        );
        return (
            <MainLayout
                title={this.props.currentFund.name}
                menuItems={this.props.menuItems}
                breadCrumbs={breadCrumbs}
            >
                <FundTermsPage/>
            </MainLayout>
        );
    }
}

const mapStateToProps = (state: AppState) => ({
    currentFund: getSingleFundState(state),
});

const mapDispatchToProps = { getFund };

export default connect(mapStateToProps, mapDispatchToProps)(Terms);
