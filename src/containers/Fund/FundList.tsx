import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';

import Action from 'decorators/ActionDecorator';
import Filter, { FilterDecoratorChange } from 'decorators/FilterExtDecorator';

import { applyFilter, downloadFunds, getGridConfiguration, searchFilterChoices } from 'store/Fund/actionCreators';
import { ADD_FUND_MODAL, FUND_LIST_COLUMNS_MODAL, FUND_STATUS_CHOICES } from 'store/Fund/constants';
import { FundModel } from 'store/Fund/models/fund';
import { getFundState } from 'store/Fund/selectors';
import { closeModal, openModal } from 'store/Modals/General/actionCreators';

import { isModalOpen } from 'store/Modals/General/selectors';
import { getChips, getFilters, getIsLoading } from 'store/Filters/selectors';
import { AsyncFilterName, Chip, FilterNames, FiltersState } from 'store/Filters/types';
import { getUser } from 'store/Auth/selectors';
import { setCheckedIds, setFilters } from 'store/Filters/actionCreators';
import ActiveUser from 'store/Auth/models/activeUser';
import { AccountType } from 'store/Account/types';
import { hasDiligenceId } from 'store/Account/helpers';
import { BULK_UPLOAD_MODAL } from 'store/Requests/constants';
import { setHeaders } from 'store/VisibleHeaders/actionCreators';
import { headerNames } from 'store/VisibleHeaders/types';
import { getHeaders } from 'store/VisibleHeaders/selectors';
import {
    getChoices, setAsyncFilters, setGlobalChoices, clearAsyncFilters
} from 'store/AsyncFiltersChoices/actionCreators';
import {
    getFiltersChoices,
    getFiltersChoicesCount,
    getFiltersValue,
    getSearchResults
} from 'store/AsyncFiltersChoices/selectors';
import {
    AppState, Id, NavigationButton, TableRowAction, SelectItem, TableHeader
} from 'store/types';
import {
    DefaultStateFundList, DEFAULT_ASYNC_FILTER,
    TableBulkActionChoices, TableRowActionType, TableRowActionIcon, FilterType
} from 'store/constants';
import { GridConfigurationType } from 'store/Fund/types';

import ConfirmDeleteFundModal from './ConfirmDeleteFundModal';
import BulkUploadModal from './BulkUploadModal';
import FundListColumnsModal from './FundListColumns';
import AddFundModal from './AddFundModal';

import DataTable from 'components/Shared/DataTable/DataTable';
import FundListPage from 'components/Fund/FundListPage';
import UiDataTable from 'components/Shared/Ui/Tables/UiDataTable';
import ButtonPrimary from 'components/Shared/Ui/Buttons/ButtonPrimary';

import { BASE_DILIGENCE_URL } from 'helpers/diligenceApiHelper';

import { fundListHeaders } from './constants';
import DataEntryContainer from './DataEntry';
import { GetFundListColumnsAction } from 'store/Fund/actions';
import { SetHeadersAction } from 'store/VisibleHeaders/actions';
import { FilterEntity } from 'store/Diligence/DiligenceRequest/types';

/* ADVANCED FILTERS */
import AdvancedFilter, {
    AdvancedFilterDecoratorSearch,
    AdvancedFilterDecoratorToggle,
    AdvancedFilterDecoratorUpdate
} from 'decorators/AdvancedFilterDecorator';
import { updateFiltersModal } from 'store/AdvancedFilters/actionCreators';
import { searchRequestAttribute } from 'store/Diligence/DiligenceRequest/actionCreators';
import { getModalFilters, getModalFiltersName } from 'store/AdvancedFilters/selectors';
import { FirmModel } from 'store/Firm/models/firm';
import { PortfolioModel } from 'store/Portfolio/models/portfolio';
import { ADVANCED_FILTERS_MODAL_NAME } from 'store/AdvancedFilters/constants';

interface Props {
    checkedAll: boolean;
    checkedIds: Id[];
    chooseAll: () => void;
    chooseRow: (id: Id) => void;
    downloadFunds: (checkedIds: Id[], checkedAll: boolean) => void;
    results: FundModel[];
    count: number;
    firmCount: number;
    applyFilter: FilterDecoratorChange;
    openModal: (name: string, params: any) => void;
    handleChange: FilterDecoratorChange;
    menuItems:  NavigationButton[];
    currentFilter: FiltersState;
    isLoading: boolean;
    isBulkUploadOpen: boolean;
    user: ActiveUser;
    chips: Chip[];
    handleColumns: (columns: any) => void;
    firmChoices: SelectItem[];
    firmsCount: number;
    firmFilter: FiltersState;
    portfolioChoices: SelectItem[];
    portfolioCount: number;
    portfolioFilter: FiltersState;
    setAsyncFilters: (filterName: string, filter: FiltersState, pageName: string) => void;
    getGridConfiguration: (gridType: string, instanceId: Id) => GetFundListColumnsAction;
    setFilters: (filterName: string, filter: FiltersState) => void;
    creditOfficerChoices: SelectItem[];
    creditOfficerCount: number;
    creditOfficerFilter: FiltersState;
    productChoices: SelectItem[];
    productCount: number;
    productFilter: FiltersState;
    strategyChoices: SelectItem[];
    strategyCount: number;
    strategyFilter: FiltersState;
    internalIdChoices: { internal_id: string }[];
    internalIdCount: number;
    internalIdFilter: FiltersState;
    getChoices: (page: string, filterName: string, filter: FiltersState) => void;
    searchFilterChoices: (filterName: string, query: string, filter: FiltersState) => void;
    visibleHeaders: TableHeader[];
    setHeaders: (block: string, headers: TableHeader[]) => SetHeadersAction;

    searchFilterEntities: FilterEntity[];
    setGlobalChoices: (results: FirmModel[] | FundModel[] | PortfolioModel[], count: number) => void;
    /* ADVANCED FILTERS */
    handleToggleModal?: AdvancedFilterDecoratorToggle;
    handleUpdateModal?: AdvancedFilterDecoratorUpdate;
    handleAttributeSearch?: AdvancedFilterDecoratorSearch;
    isModalOpen: boolean;
    advancedFilter?: FiltersState;
}

const filterName = FilterNames.fundList;

class FundList extends React.PureComponent<Props> {
    rowActions: TableRowAction[] = [{
        type: TableRowActionType.NavLink,
        icon: TableRowActionIcon.View,
        link: '/funds/{id}',
    }];

    bulkActions = [
        {
            type: TableBulkActionChoices.ExportFunds,
            handler: () => this.props.downloadFunds(
                this.props.checkedIds,
                this.props.checkedAll,
            ),
        },
    ];

    componentDidMount() {
        this.props.setHeaders(headerNames.fundList, fundListHeaders);
        this.props.getGridConfiguration(GridConfigurationType.FundList, this.props.user.active_account.id);
        this.props.setGlobalChoices([], 0);
    }

    toggleColumnsSettings = () => {
        this.props.openModal(FUND_LIST_COLUMNS_MODAL, {});
    }

    toggleAddNewFund = () => {
        if (hasDiligenceId(this.props.user.active_account)) {
            window.open(BASE_DILIGENCE_URL + '/portfolios/master/vehicles?addNewFund', '_blank');
        } else {
            this.props.openModal(ADD_FUND_MODAL, {});
        }
    }

    toggleBulkUpload = () => {
        if (hasDiligenceId(this.props.user.active_account)) {
            window.open(BASE_DILIGENCE_URL + '/portfolios/master/edit?section=import', '_blank');
        } else {
            this.props.openModal(BULK_UPLOAD_MODAL, {});
        }
    }

    handleDownload = () => {
        this.props.downloadFunds([], true);
    }

    render() {
        const buttons = this.props.user.active_account.account.type === AccountType.Responder ? [] : [
            (
                <ButtonPrimary className="float-right ml-3" handleClick={this.toggleColumnsSettings} inverse={true}>
                    Columns
                </ButtonPrimary>
            ),
            (
                <ButtonPrimary className="float-right ml-3" handleClick={this.toggleAddNewFund}>
                    Add New Fund
                </ButtonPrimary>
            ),
            (
                <ButtonPrimary className="float-right ml-3" handleClick={this.toggleBulkUpload} inverse={true}>
                    Bulk Upload
                </ButtonPrimary>
            ),
            (
                <ButtonPrimary className="float-right ml-3" handleClick={this.handleDownload} inverse={true}>
                    Download
                </ButtonPrimary>
            )
        ];

        const filters: any = this.getFilters();
        const advancedFilters = this.getAdvancedFilters();
        return (
            <FundListPage
                count={this.props.count}
                applyFilter={this.props.handleChange}
                currentFilter={this.props.currentFilter}
            >
                <DataEntryContainer handleSelect={this.props.handleChange} />
                <UiDataTable>
                    <DataTable
                        buttons={buttons}
                        filters={filters}
                        rows={this.props.results}
                        count={this.props.count}
                        headers={fundListHeaders}
                        visibleHeaders={this.props.visibleHeaders}
                        handleChange={this.props.handleChange}
                        hasPagination={true}
                        hasSorting={true}
                        currentFilter={this.props.currentFilter}
                        withCheckboxes={true}
                        checkedAll={this.props.checkedAll}
                        checkedIds={this.props.checkedIds}
                        chooseAll={this.props.chooseAll}
                        chooseRow={this.props.chooseRow}
                        rowActions={this.rowActions}
                        bulkActions={this.bulkActions}
                        isLoading={this.props.isLoading}
                        chips={this.props.chips}
                        filterPageName={filterName}
                        scrollableHeader={true}

                        handleToggleModal={this.props.handleToggleModal}
                        handleUpdateModal={this.props.handleUpdateModal}
                        handleAttributeSearch={this.props.handleAttributeSearch}
                        isFilterModalOpen={this.props.isModalOpen}
                        advancedFilters={advancedFilters}
                        searchFilterEntities={this.props.searchFilterEntities}
                        currentModalFilter={this.props.advancedFilter}
                    />
                </UiDataTable>
                <AddFundModal/>
                <ConfirmDeleteFundModal/>
                <FundListColumnsModal
                    handleColumns={this.props.handleColumns}
                />
                {this.props.isBulkUploadOpen && <BulkUploadModal />}
            </FundListPage>
        );
    }

    private getAdvancedFilters() {
        return [
            {
                name: 'firm_id',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Firms',
                choices: this.props.firmChoices,
                choicesCount: this.props.firmsCount,
                visible: false,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(FilterNames.fundList, AsyncFilterName.firm, filter);
                },
                currentFilter: this.props.firmFilter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.firm,
                defaultFilter: DEFAULT_ASYNC_FILTER,
                isFilterValueObject: true,
            },
            {
                name: 'delivery_status',
                type: FilterType.CreditSelectWithCheckboxesSync,
                title: 'Status',
                choices: FUND_STATUS_CHOICES,
                choicesCount: FUND_STATUS_CHOICES.length,
                visible: false,
                currentFilter: this.props.currentFilter,
            },
            {
                name: 'credit_officer',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Coverage',
                choices: this.props.creditOfficerChoices,
                choicesCount: this.props.creditOfficerCount,
                visible: false,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(
                        FilterNames.fundList,
                        AsyncFilterName.creditOfficer,
                        filter
                    );
                },
                currentFilter: this.props.creditOfficerFilter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.creditOfficer,
                defaultFilter: DEFAULT_ASYNC_FILTER,
            },
            {
                name: 'product',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Product',
                choices: this.props.productChoices,
                choicesCount: this.props.productCount,
                visible: false,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(FilterNames.fundList, AsyncFilterName.product, filter);
                },
                currentFilter: this.props.productFilter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.product,
                defaultFilter: DEFAULT_ASYNC_FILTER,
            },
            {
                name: 'strategy',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Strategy',
                choices: this.props.strategyChoices,
                choicesCount: this.props.strategyCount,
                visible: false,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(FilterNames.fundList, AsyncFilterName.strategy, filter);
                },
                currentFilter: this.props.strategyFilter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.strategy,
                defaultFilter: DEFAULT_ASYNC_FILTER,
            },
            {
                name: 'portfolio_id',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Portfolio',
                choices: this.props.portfolioChoices,
                choicesCount: this.props.portfolioCount,
                visible: false,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(
                        FilterNames.fundList,
                        AsyncFilterName.portfolio,
                        filter
                    );
                },
                currentFilter: this.props.portfolioFilter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.portfolio,
                defaultFilter: DEFAULT_ASYNC_FILTER,
                isFilterValueObject: true,
            },
            {
                name: 'internal_id',
                type: FilterType.CreditSelectWithCheckboxes,
                title: 'Internal Id',
                choices: this.props.internalIdChoices,
                choicesCount: this.props.internalIdCount,
                visible: false,
                handleRequest: (filter: FiltersState) => {
                    this.props.getChoices(
                        FilterNames.fundList,
                        AsyncFilterName.internalId,
                        filter
                    );
                },
                currentFilter: this.props.internalIdFilter,
                setFilters: this.props.setAsyncFilters,
                filterName: AsyncFilterName.internalId,
                defaultFilter: DEFAULT_ASYNC_FILTER,
            },
        ];
    }

    private getFilters() {
        return [
            {
                name: 'search',
                type: FilterType.Search,
                title: 'Search',
                className: 'col-lg-9 col-sm-8',
                pageName: filterName
            },
            {
                name: 'stats',
                type: FilterType.Statistics,
                title: 'Stats', className: 'col-lg-3 col-sm-4',
                stats: [
                    {name: 'Firms', value: this.props.firmCount || 0},
                    {name: 'Funds', value: this.props.count || 0}
                ]
            }
        ];
    }
}

const mapStateToProps = (state: AppState) => ({
    results: getFundState(state).list,
    count: getFundState(state).count,
    firmCount: getFundState(state).firmCount,
    currentFilter: getFilters(state, filterName),
    checkedAll: getFilters(state, filterName).checkedAll,
    checkedIds: getFilters(state, filterName).checkedIds,
    isLoading: getIsLoading(state),
    isBulkUploadOpen: isModalOpen(state, {name: BULK_UPLOAD_MODAL}),
    user: getUser(state),
    chips: getChips(state, filterName),
    portfolioChoices: getFiltersChoices(
        state,
        {page: FilterNames.fundList, filterName: AsyncFilterName.portfolio}
    ),
    portfolioCount: getFiltersChoicesCount(
        state,
        {page: FilterNames.fundList, filterName: AsyncFilterName.portfolio}
    ),
    portfolioFilter: getFiltersValue(
        state,
        {page: FilterNames.fundList, filterName: AsyncFilterName.portfolio}
    ),
    creditOfficerChoices: getFiltersChoices(
        state,
        {
            page: FilterNames.fundList,
            filterName: AsyncFilterName.creditOfficer,
            labelField: 'value',
            valueField: 'value'
        }
    ),
    creditOfficerCount: getFiltersChoicesCount(
        state,
        {page: FilterNames.fundList, filterName: AsyncFilterName.creditOfficer}
    ),
    creditOfficerFilter: getFiltersValue(
        state,
        {page: FilterNames.fundList, filterName: AsyncFilterName.creditOfficer}
    ),
    productChoices: getFiltersChoices(
        state,
        {page: FilterNames.fundList, filterName: AsyncFilterName.product, labelField: 'value', valueField: 'value'}
    ),
    productCount: getFiltersChoicesCount(
        state,
        {page: FilterNames.fundList, filterName: AsyncFilterName.product}
    ),
    productFilter: getFiltersValue(
        state,
        {page: FilterNames.fundList, filterName: AsyncFilterName.product}
    ),
    strategyChoices: getFiltersChoices(
        state,
        {page: FilterNames.fundList, filterName: AsyncFilterName.strategy, labelField: 'value', valueField: 'value'}
    ),
    strategyCount: getFiltersChoicesCount(
        state,
        {page: FilterNames.fundList, filterName: AsyncFilterName.strategy}
    ),
    strategyFilter: getFiltersValue(
        state,
        {page: FilterNames.fundList, filterName: AsyncFilterName.strategy}
    ),
    firmChoices: getFiltersChoices(
        state,
        {page: FilterNames.fundList, filterName: AsyncFilterName.firm}
    ),
    firmsCount: getFiltersChoicesCount(
        state,
        {page: FilterNames.fundList, filterName: AsyncFilterName.firm}
    ),
    firmFilter: getFiltersValue(
        state,
        {page: FilterNames.fundList, filterName: AsyncFilterName.firm}
    ),
    internalIdChoices: getFiltersChoices(
        state,
        {
            page: FilterNames.fundList,
            filterName: AsyncFilterName.internalId,
            labelField: 'internal_id',
            valueField: 'internal_id'
        }
    ),
    internalIdCount: getFiltersChoicesCount(
        state,
        {page: FilterNames.fundList, filterName: AsyncFilterName.internalId}
    ),
    internalIdFilter: getFiltersValue(
        state,
        {page: FilterNames.fundList, filterName: AsyncFilterName.internalId}
    ),
    visibleHeaders: getHeaders(state, {name: headerNames.fundList}),
    /* ADVANCED FILTER */
    // isModalOpen: isAdvancedFilterModalOpen(state, {name: ADVANCED_FILTERS_MODAL_NAME, filterName: filterName}),
    isModalOpen: isModalOpen(state, {name: ADVANCED_FILTERS_MODAL_NAME}),
    advancedFilter: getModalFilters(state),
    advancedFilterName: getModalFiltersName(state),
    searchFilterEntities: getSearchResults(state)
});

const mapDispatchToProps = {
    applyFilter,
    getChoices,
    getGridConfiguration,
    downloadFunds,
    setFilters,
    setCheckedIds,
    openModal,
    setHeaders,
    setAsyncFilters,
    /* ADVANCED FILTER */
    openFiltersModal: openModal,
    closeFiltersModal: closeModal,
    updateFiltersModal,
    searchRequestAttribute,
    searchFilterChoices,
    setGlobalChoices,
    clearAsyncFilters,
};

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    (component) => Action(component, filterName),
    (component) => Filter(component, {...DefaultStateFundList}, filterName, true),
    (component) => AdvancedFilter(component, filterName)
)(FundList);
