import * as React from 'react';
import { connect } from 'react-redux';

import { getModalParams, isModalOpen } from 'store/Modals/General/selectors';
import { closeModal } from 'store/Modals/General/actionCreators';
import { CONFIRM_DELETE_FUND_MODAL, CONFIRM_DELETE_FUND_MODAL_TITLE } from 'store/Fund/constants';
import Modal from 'components/Shared/Modals/Modal';
import { deleteFunds } from 'store/Fund/actionCreators';

import { AppState, Id } from 'store/types';
import ButtonDanger from '../../components/Shared/Ui/Buttons/ButtonDanger';
import { DeleteFundsModel } from '../../store/Fund/models/fund';
import ButtonDefault from '../../components/Shared/Ui/Buttons/ButtonDefault';

interface Props {
    isOpen: boolean;
    closeModal: () => any;
    deleteFunds: (ids: Id[], portfolioId: Id, checkedAll: boolean) => any;
    modalParams: DeleteFundsModel;
}

class ConfirmDeleteFundModal extends React.PureComponent<Props> {

    render() {
        return this.props.isOpen && this.renderBody();
    }

    handleToggle = () => {
        this.props.closeModal();
    }

    renderBody() {
        const { isOpen, modalParams } = this.props;
        let checkedIds: Id[] = [];
        let portfolioId: Id = '';
        let checkedAll: boolean = false;
        let count: number = 0;

        if (modalParams) {
            checkedIds = modalParams.ids;
            portfolioId = modalParams.portfolioId;
            checkedAll = modalParams.checkedAll;
            count = modalParams.count;
        }

        let numberSelected: number = 0;
        if (checkedIds && count) {
            if (checkedAll) {
                if (checkedIds.length > 0) {
                    numberSelected = count - checkedIds.length;
                } else {
                    numberSelected = count;
                }
            } else {
                if (checkedIds.length > 0) {
                    numberSelected = checkedIds.length;
                }
            }
        }

        const body = (
            <p>Are you sure that you're going to delete {numberSelected} fund{numberSelected > 1 ? 's' : ''}?</p>
        );
        const title = <>{CONFIRM_DELETE_FUND_MODAL_TITLE}</>;
        const buttons = [
            (
                <ButtonDanger
                    size="lg"
                    handleClick={() => this.props.deleteFunds(checkedIds, portfolioId, checkedAll)}
                >
                    Yes, delete
                </ButtonDanger>
            ),
            (<ButtonDefault  inverse={true} handleClick={this.handleToggle}>Cancel</ButtonDefault>)
        ];

        return (
            <Modal
                className="ui-modal--confirm"
                isOpen={isOpen}
                toggle={this.handleToggle}
                title={title}
                body={body}
                buttons={buttons}
            />
        );
    }
}

const mapStateToProps = (state: AppState) => ({
    isOpen: isModalOpen(state, {name: CONFIRM_DELETE_FUND_MODAL}),
    modalParams: getModalParams(state),
});

const mapDispatchToProps = {
    closeModal,
    deleteFunds,
};

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmDeleteFundModal);