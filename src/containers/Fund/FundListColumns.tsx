import { connect } from 'react-redux';

import { AppState } from 'store/types';
import { closeModal } from 'store/Modals/General/actionCreators';
import { getModalParams, isModalOpen } from 'store/Modals/General/selectors';
import { getHeaders } from 'store/VisibleHeaders/selectors';
import { headerNames } from 'store/VisibleHeaders/types';
import FundListColumnsModal from 'components/Fund/FundListColumnsModal';
import { FUND_LIST_COLUMNS_MODAL } from 'store/Fund/constants';
import { getFundListTemporaryHeaders } from 'store/Fund/selectors';
import { saveFundListColumns, setFundListTemporaryHeaders } from 'store/Fund/actionCreators';
import { getActiveAccountId } from 'store/Auth/selectors';

const mapStateToProps = (state: AppState) => {
    const modalParams = getModalParams(state) || {};
    return {
        accountId: getActiveAccountId(state),
        temporaryHeaders: getFundListTemporaryHeaders(state),
        headers: getHeaders(state, {name: headerNames.fundList}),
        isOpen: isModalOpen(state, {name: FUND_LIST_COLUMNS_MODAL}),
        modalParams,
    };
};

const mapDispatchToProps = {
    closeModal,
    setFundListTemporaryHeaders,
    saveFundListColumns,
};

export default connect(mapStateToProps, mapDispatchToProps)(FundListColumnsModal);