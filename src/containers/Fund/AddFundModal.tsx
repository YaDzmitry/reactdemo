import * as React from 'react';
import { connect } from 'react-redux';
import { debounce } from 'lodash';

import ButtonHelper from 'helpers/buttonHelper';
import FundHelper from 'helpers/fundHelper';
import NotificationHelper from 'helpers/notificationHelper';
import ButtonDefault from 'components/Shared/Ui/Buttons/ButtonDefault';

import { isModalOpen } from 'store/Modals/General/selectors';
import { closeModal } from 'store/Modals/General/actionCreators';
import { ADD_FUND_MODAL, ADD_FUND_MODAL_TITLE } from 'store/Fund/constants';
import Modal from 'components/Shared/Modals/Modal';
import { SEARCH_DEBOUNCE_INTERVAL } from 'store/constants';
import { AppState } from 'store/types';
import AddNewFundForm from 'components/Fund/AddNewFundForm';
import { NewFundModel } from 'store/Portfolio/models/fund';
import { changeNewFund, clearFirmFromErm, createFund, getFirmFromErm } from 'store/Fund/actionCreators';
import ButtonPrimary from 'components/Shared/Ui/Buttons/ButtonPrimary';
import { getFundState } from 'store/Fund/selectors';
import { getActionIsInProgress } from 'store/Actions/selectors';
import { GET_FIRM_FROM_ERM } from 'store/Fund/actions';

interface Props {
    getFirmFromErm: any;
    clearFirmFromErm: any;
    funds: any[];
    isOpen: boolean;
    isGridLoading: boolean;
    closeModal: () => any;
    portfolioId?: any;
    changeNewFund: (field: string, value: string) => any;
    createFund: (newFund: NewFundModel) => any;
    newFund: NewFundModel;
}

class AddFundModal extends React.PureComponent<Props> {

    getDataAfterDebounce = debounce(
        (filter: any) => {
            if (filter.search.length > 0) {
                this.props.getFirmFromErm(filter);
            }
        },
        SEARCH_DEBOUNCE_INTERVAL
    );

    handleToggle = () => {
        this.props.closeModal();
    }

    handleChange = (field: string, value: string) => {
        this.props.changeNewFund(field, value);
    }

    handleSave = () => {
        const fund: any = this.props.newFund;

        const firm: any = FundHelper.getOwnerFirm(fund.name);

        if (firm.hasOwnProperty('error')) {
            NotificationHelper.error( {message: firm.error, time: 10000});
        } else {
            let newFund = {
                    portfolio_id: this.props.portfolioId,
                    ap_id: fund.name.id,
                    name: fund.name.legal_name,
                    alias: '',
                    firm: FundHelper.getOwnerFirm(fund.name)
                }
            ;
            this.props.createFund(newFund);
        }

    }

    getFirmFromErm = (filter: any) => {
        this.getDataAfterDebounce(filter);
    }

    render() {
        const {isOpen, newFund} = this.props;
        const body = (
            <AddNewFundForm
                funds={this.props.funds}
                getFirmFromErm={this.getFirmFromErm}
                isGridLoading={this.props.isGridLoading}
                changeNewFund={this.handleChange}
                clearFirmFromErm={this.props.clearFirmFromErm}
                newFund={newFund}
            />
        );

        const title = <>{ADD_FUND_MODAL_TITLE}</>;
        const buttons = [
            (
            <ButtonPrimary
                disabled={ButtonHelper.isDisabled(this.props.newFund.name)}
                handleClick={this.handleSave}
                className="float-left"
                buttonLarge={true}
            >
                Add Fund
            </ButtonPrimary>),
            (
            <ButtonDefault
                className="float-left"
                inverse={true}
                buttonLarge={true}
                handleClick={this.props.closeModal}
            >
                Cancel
            </ButtonDefault>
            )

        ];

        return (
            <div className="modal-md">
                <Modal
                    className="ui-modal button--left"
                    isOpen={isOpen}
                    toggle={this.handleToggle}
                    title={title}
                    body={body}
                    size="lg"
                    buttons={buttons}
                />
            </div>
        );
    }
}

const mapStateToProps = (state: AppState) => ({
    isOpen: isModalOpen(state, {name: ADD_FUND_MODAL}),
    newFund: getFundState(state).newFund,
    isGridLoading: getActionIsInProgress(state, {name: GET_FIRM_FROM_ERM}),
    funds: getFundState(state).potentialFunds
});

const mapDispatchToProps = {
    closeModal,
    changeNewFund,
    createFund,
    getFirmFromErm,
    clearFirmFromErm
};

export default connect(mapStateToProps, mapDispatchToProps)(AddFundModal);
