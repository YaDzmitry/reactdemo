import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';

import Filter, { FilterDecoratorChange } from 'decorators/FilterDecorator';
import AlertPage from 'components/AlertsManager/Alerts/AlertPage';
import AlertStatusChart from './AlertStatusChart';
import AlertList from './AlertList';

import { DefaultStateAlertList } from 'store/constants';
import { AppState, NavigationButton } from 'store/types';
import { FilterNames, FiltersState } from 'store/Filters/types';
import { getAlertList } from 'store/AlertsManager/Alerts/actionCreators';
import { getFilters } from 'store/Filters/selectors';
import { setFilters } from 'store/Filters/actionCreators';

export const AlertListFilterName = FilterNames.alertList;

export interface AlertPageProps {
    handleChange: FilterDecoratorChange;
    currentFilter: FiltersState;
    menuItems:  NavigationButton[];
}

class AlertPageContainer extends React.PureComponent<AlertPageProps> {
    render() {
        const {handleChange, currentFilter} = this.props;
        return (
            <AlertPage {...this.props}>
                <AlertStatusChart handleSelect={handleChange} />
                <AlertList currentFilter={currentFilter} handleChange={handleChange}/>
            </AlertPage>
        );
    }
}

const mapStateToProps = (state: AppState) => ({
    currentFilter: getFilters(state, AlertListFilterName),
});

const mapDispatchToProps = {
    applyFilter: getAlertList,
    setFilters,
};

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    (component: React.Component) => Filter(component, DefaultStateAlertList, AlertListFilterName, true)
)(AlertPageContainer);
