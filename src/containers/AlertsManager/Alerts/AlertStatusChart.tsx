import * as React from 'react';
import { connect } from 'react-redux';

import { AppState } from 'store/types';
import AlertStatusChart from 'components/AlertsManager/Alerts/AlertStatusChart';
import { AlertStatusItem, EvaluationResultStatus } from 'store/AlertsManager/Alerts/types';
import { getAlertStatusItems } from 'store/AlertsManager/Alerts/actionCreators';
import { GetAlertStatusItemsAction } from 'store/AlertsManager/Alerts/actions';
import { getAlertStatus, getPeriod, selectAlertStatusItems } from 'store/AlertsManager/Alerts/selectors';
import { FilterDecoratorChange } from 'decorators/FilterDecorator';

interface Props {
    period: string;
    alertStatusItems: AlertStatusItem[];
    selectedItem: EvaluationResultStatus;
    getAlertStatusItems: (period: string) => GetAlertStatusItemsAction;
    handleSelect?: FilterDecoratorChange;
}

class AlertStatusChartContainer extends React.PureComponent<Props> {
    componentDidMount() {
        this.props.getAlertStatusItems(this.props.period);
    }

    componentDidUpdate(prevProps: Props) {
        if (prevProps.period !== this.props.period) {
            this.props.getAlertStatusItems(this.props.period);
        }
    }

    render() {
        const { alertStatusItems, selectedItem, handleSelect } = this.props;

        return (
            <AlertStatusChart
                data={alertStatusItems}
                selectedItem={selectedItem}
                handleSelect={handleSelect}
            />
        );
    }
}

const mapStateToProps = (state: AppState) => ({
    alertStatusItems: selectAlertStatusItems(state),
    period: getPeriod(state),
    selectedItem: getAlertStatus(state),
});

const mapDispatchToProps = {
    getAlertStatusItems
};

export default connect(mapStateToProps, mapDispatchToProps)(AlertStatusChartContainer);