import * as React from 'react';
import { connect } from 'react-redux';

import AlertList from 'components/AlertsManager/Alerts/AlertList';

import { AppState } from 'store/types';
import { getChips, getIsLoading } from 'store/Filters/selectors';
import { AsyncFilterName } from 'store/Filters/types';
import { getAlertCount, getCount, getList } from 'store/AlertsManager/Alerts/selectors';
import { getActionIsInProgress } from 'store/Actions/selectors';
import { GET_ALERT_LIST } from 'store/AlertsManager/Alerts/actions';
import {
    getFiltersChoices, getFiltersChoicesCount, getFiltersValue
} from 'store/AsyncFiltersChoices/selectors';
import { getChoices, setAsyncFilters } from 'store/AsyncFiltersChoices/actionCreators';
import { getUserAccountType } from 'store/Auth/selectors';
import { AlertListFilterName as filterName } from './AlertPage';

const mapStateToProps = (state: AppState) => ({
    userAccountType: getUserAccountType(state),
    results: getList(state),
    count: getCount(state),
    alertCount: getAlertCount(state),
    filterPageName: filterName,
    isLoading: getIsLoading(state),
    isGridLoading: getActionIsInProgress(state, {name: GET_ALERT_LIST}),
    chips: getChips(state, filterName),
    portfolioChoices: getFiltersChoices(state, {page: filterName, filterName: AsyncFilterName.portfolio}),
    portfolioCount: getFiltersChoicesCount(state, {page: filterName, filterName: AsyncFilterName.portfolio}),
    portfolioFilter: getFiltersValue(state, {page: filterName, filterName: AsyncFilterName.portfolio}),
    creditOfficerChoices: getFiltersChoices(
        state,
        {
            page: filterName,
            filterName: AsyncFilterName.creditOfficer,
            labelField: 'value',
            valueField: 'value'
        }
    ),
    creditOfficerCount: getFiltersChoicesCount(state, {page: filterName, filterName: AsyncFilterName.creditOfficer}),
    creditOfficerFilter: getFiltersValue(state, {page: filterName, filterName: AsyncFilterName.creditOfficer}),
    productChoices: getFiltersChoices(
        state,
        {page: filterName, filterName: AsyncFilterName.product, labelField: 'value', valueField: 'value'}
    ),
    productCount: getFiltersChoicesCount(state, {page: filterName, filterName: AsyncFilterName.product}),
    productFilter: getFiltersValue(state, {page: filterName, filterName: AsyncFilterName.product}),
    strategyChoices: getFiltersChoices(
        state,
        {page: filterName, filterName: AsyncFilterName.strategy, labelField: 'value', valueField: 'value'}
    ),
    strategyCount: getFiltersChoicesCount(state, {page: filterName, filterName: AsyncFilterName.strategy}),
    strategyFilter: getFiltersValue(state, {page: filterName, filterName: AsyncFilterName.strategy}),
    firmChoices: getFiltersChoices(state, {page: filterName, filterName: AsyncFilterName.firm}),
    firmCount: getFiltersChoicesCount(state, {page: filterName, filterName: AsyncFilterName.firm}),
    firmFilter: getFiltersValue(state, {page: filterName, filterName: AsyncFilterName.firm}),
    internalIdChoices: getFiltersChoices(
        state,
        {
            page: filterName,
            filterName: AsyncFilterName.internalId,
            labelField: 'internal_id',
            valueField: 'internal_id'
        }
    ),
    internalIdCount: getFiltersChoicesCount(state, {page: filterName, filterName: AsyncFilterName.internalId}),
    internalIdFilter: getFiltersValue(state, {page: filterName, filterName: AsyncFilterName.internalId}),
});

const mapDispatchToProps = {
    getChoices,
    setAsyncFilters,
};

export default connect(mapStateToProps, mapDispatchToProps)(AlertList);