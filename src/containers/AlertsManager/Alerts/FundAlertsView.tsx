import * as React from 'react';
import { connect } from 'react-redux';
import { NavLink, RouteComponentProps, withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { bindActionCreators, Dispatch } from 'redux';
import { cloneDeep } from 'lodash';

import { AppState, Id, NavigationButton, SelectItem } from 'store/types';

import {
    DEFAULT_ASYNC_COUNTERPARTY_FILTER, DEFAULT_ASYNC_RULESET_NAME_FILTER, DefaultStateFundAlertsView
} from 'store/constants';
import { default as Filter, FilterDecoratorChange } from 'decorators/FilterDecorator';

import { getFundEvaluationResults, getFundStatistic } from 'store/AlertsManager/Alerts/actionCreators';
import { AlertsAction, GET_FUND_EVALUATION_RESULTS, GetFundStatisticAction } from 'store/AlertsManager/Alerts/actions';
import { getViewedFundEvaluationResults, getViewedFundStatistic } from 'store/AlertsManager/Alerts/selectors';
import { AlertFund, FundEvaluationResults } from 'store/AlertsManager/Alerts/types';
import { getFilterInfo } from 'store/AsyncFiltersChoices/selectors';
import { AsyncFilterName, Chip, FilterNames, FiltersState } from 'store/Filters/types';
import { setFilters } from 'store/Filters/actionCreators';
import { getChoicesByFund, setAsyncFilters } from 'store/AsyncFiltersChoices/actionCreators';
import { getChips, getFilters, getIsLoading } from 'store/Filters/selectors';
import { getActionIsInProgress } from 'store/Actions/selectors';
import { GetChoicesByFundAction } from 'store/AsyncFiltersChoices/actions';

import FundAlertsView from 'components/AlertsManager/Alerts/FundAlertsView';
import MainLayout from 'components/Shared/Layouts/MainLayout';
import AlertPage from 'components/AlertsManager/Alerts/AlertPage';
import { getUser } from 'store/Auth/selectors';
import { isResponder } from 'store/User/helpers';
import ActiveUser from 'store/Auth/models/activeUser';
import { FiltersAction } from 'store/Filters/actions';

const filterName = FilterNames.alertView;

interface RouteProps {
    id: Id;
}

interface Props {
    menuItems: NavigationButton[];
    user: ActiveUser;
    statistic: AlertFund;
    evaluationResults: FundEvaluationResults;

    handleChange: FilterDecoratorChange;
    filterPageName: string;
    currentFilter: FiltersState;
    alertListFilter: FiltersState;
    chips: Chip[];
    getChoicesByFund: (page: string, filterName: string, filter: FiltersState, fundId: Id) => GetChoicesByFundAction;
    isLoading: boolean;
    isGridLoading: boolean;

    rulesetNameChoices: SelectItem[];
    rulesetNameCount: number;
    rulesetNameFilter: FiltersState;

    counterpartyChoices: SelectItem[];
    counterpartyCount: number;
    counterpartyFilter: FiltersState;

    setAsyncFilters: (filterName: string, filter: FiltersState, pageName: string) => void;
    applyPeriodFilter: FilterDecoratorChange;
    getFundStatistic: (id: Id, period: string) => GetFundStatisticAction;
}

class FundAlertsViewContainer extends React.PureComponent<Props & RouteComponentProps<RouteProps>> {
    componentDidMount() {
        this.props.getFundStatistic(this.routeId, this.props.alertListFilter.period);
    }

    render() {
        const {
            statistic, evaluationResults,
            chips, user,
            handleChange,
            filterPageName, currentFilter, isLoading, isGridLoading,
            rulesetNameChoices, rulesetNameCount, rulesetNameFilter,
            counterpartyChoices, counterpartyCount, counterpartyFilter,
        } = this.props;

        return (
            <MainLayout
                title={`Alerts`}
                menuItems={this.props.menuItems}
                breadCrumbs={this.getBreadCrumbs()}
                filters={AlertPage.headerFilters}
                handleChange={this.props.applyPeriodFilter}
                currentFilter={this.props.alertListFilter}
            >
                <FundAlertsView
                    user={user}
                    statistic={statistic}
                    evaluationResults={evaluationResults}

                    chips={chips}
                    handleChange={handleChange}
                    filterPageName={filterPageName}
                    currentFilter={currentFilter}

                    rulesetNameChoices={rulesetNameChoices}
                    rulesetNameCount={rulesetNameCount}
                    rulesetNameFilter={rulesetNameFilter}

                    counterpartyChoices={counterpartyChoices}
                    counterpartyCount={counterpartyCount}
                    counterpartyFilter={counterpartyFilter}

                    isLoading={isLoading}
                    isGridLoading={isGridLoading}
                    setAsyncFilters={this.props.setAsyncFilters}
                    getChoicesByFund={this.props.getChoicesByFund}
                    createRuleset={this.handleCreateRuleset}
                    fundId={this.routeId}
                    period={this.props.alertListFilter.period}
                />
            </MainLayout>
        );
    }

    get routeId() {
        return this.props.match.params.id;
    }

    private handleCreateRuleset = () => {
        this.props.history.push('/rulesets/create');
    }

    private getBreadCrumbs = () => (
        <div className="ap-breadcrumbs">
            <NavLink to="/alerts">Alerts</NavLink>
            <span className="breadcrumb-slash">/</span>
            <span>{this.props.statistic && this.props.statistic.name || 'Fund'}</span>
        </div>
    )
}

const mapStateToProps = (state: AppState) => {
    const rulesetNameFilter = getFilterInfo(state, {
        page: FilterNames.alertView,
        filterName: AsyncFilterName.rulesetName,
        labelField: 'name',
        valueField: 'name',
    });

    const counterpartyFilter = getFilterInfo(state, {
        page: FilterNames.alertView,
        filterName: AsyncFilterName.counterparty,
        labelField: 'counterparty',
        valueField: 'counterparty',
    });

    return {
        user: getUser(state),
        evaluationResults: getViewedFundEvaluationResults(state),
        statistic: getViewedFundStatistic(state),

        chips: getChips(state, filterName),
        filterPageName: filterName,
        currentFilter: getFilters(state, filterName),
        alertListFilter: getFilters(state, FilterNames.alertList),

        rulesetNameChoices: rulesetNameFilter.choices,
        rulesetNameCount: rulesetNameFilter.count,
        rulesetNameFilter: rulesetNameFilter.filter,

        counterpartyChoices: counterpartyFilter.choices,
        counterpartyCount: counterpartyFilter.count,
        counterpartyFilter: counterpartyFilter.filter,

        isLoading: getIsLoading(state),
        isGridLoading: getActionIsInProgress(state, {name: GET_FUND_EVALUATION_RESULTS}),
    };
};

const mapDispatchToProps = (dispatch: Dispatch<AlertsAction | FiltersAction>,
                            ownProps: Props & RouteComponentProps<RouteProps>) => {
    const fundId = ownProps.match.params.id;
    return {
        applyFilter: (filter: FiltersState) => {
            dispatch(getFundEvaluationResults(fundId, {...filter, period: ownProps.alertListFilter.period || ''}));
        },
        applyPeriodFilter: (periodFilter: FiltersState) => {
            const newFilter = {
                ...ownProps.currentFilter,
                ...periodFilter,
            };
            dispatch(getFundEvaluationResults(fundId, newFilter));
            dispatch(getFundStatistic(fundId, periodFilter.period));
            dispatch(setFilters(FilterNames.alertList, {...ownProps.alertListFilter, ...periodFilter}));
            dispatch(setFilters(
                FilterNames.alertView,
                {...cloneDeep(DefaultStateFundAlertsView.filter), ruleset_status: ownProps.currentFilter.ruleset_status}
            ));
            dispatch(getChoicesByFund(
                FilterNames.alertView,
                AsyncFilterName.rulesetName,
                {...periodFilter, ...DEFAULT_ASYNC_RULESET_NAME_FILTER},
                fundId
            ));
            if (isResponder(ownProps.user)) {
                dispatch(getChoicesByFund(
                    FilterNames.alertView,
                    AsyncFilterName.counterparty,
                    {...periodFilter, ...DEFAULT_ASYNC_COUNTERPARTY_FILTER},
                    fundId
                ));
            }
        },
        ...bindActionCreators(
            {
                setFilters,
                setAsyncFilters,
                getChoicesByFund,
                getFundStatistic,
            },
            dispatch
        )
    };
};

export default compose(
    withRouter,
    connect(mapStateToProps),
    connect(null, mapDispatchToProps),
    (component: React.Component) => Filter(component, DefaultStateFundAlertsView, filterName)
)(FundAlertsViewContainer);
