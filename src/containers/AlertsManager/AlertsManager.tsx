import * as React from 'react';
import { Route, Switch } from 'react-router';

import { NavigationButton } from 'store/types';
import AlertPage from './Alerts/AlertPage';
import RulesetList from './Rulesets/RulesetList';
import RulesetEdit from './Rulesets/RulesetEdit';
import AlertFundView from './Alerts/FundAlertsView';

const AlertsManager: React.SFC<any> = () => {
    const menuItems: NavigationButton[] = [
        {
            path: '/alerts', title: 'Alerts', exact: false
        },
        {
            path: '/rulesets', title: 'Rulesets', exact: false,
            children: [
                {path: '/ruleset-versions', title: 'Rulesets'}
            ]
        },
    ];

    return (
        <Switch>
            <Route
                path="/alerts"
                exact={true}
                render={(properties: any) => <AlertPage menuItems={menuItems} {...properties}  />}
            />
            <Route
                path="/alerts/:id"
                exact={true}
                render={(properties: any) => <AlertFundView menuItems={menuItems} {...properties}  />}
            />
            <Route
                path="/rulesets"
                exact={true}
                render={(properties: any) => <RulesetList menuItems={menuItems} {...properties}  />}
            />
            <Route
                path="/rulesets/:id"
                exact={true}
                render={(properties: any) => <RulesetEdit menuItems={menuItems} {...properties}  />}
            />
            <Route
                path="/ruleset-versions/:id"
                exact={true}
                render={(properties: any) => <RulesetEdit menuItems={menuItems} {...properties}  />}
            />
        </Switch>
    );
};

export default AlertsManager;
