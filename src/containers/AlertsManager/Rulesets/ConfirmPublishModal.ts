import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { RouteComponentProps, withRouter } from 'react-router';

import { AppState } from 'store/types';
import { isModalOpen } from 'store/Modals/General/selectors';
import { closeModal } from 'store/Modals/General/actionCreators';
import { CONFIRM_PUBLISH_RULESET_MODAL } from 'store/AlertsManager/Rulesets/constants';
import { publishRuleset } from 'store/AlertsManager/Rulesets/actionCreators';
import { getEdited } from 'store/AlertsManager/Rulesets/selectors';
import { Ruleset } from 'store/AlertsManager/Rulesets/types';
import { RulesetsAction } from 'store/AlertsManager/Rulesets/actions';

import ConfirmPublishModal from 'components/AlertsManager/Rulesets/ConfirmPublishModal';

interface Props {
    ruleset: Ruleset;
}

const mapStateToProps = (state: AppState) => ({
    isOpen: isModalOpen(state, {name: CONFIRM_PUBLISH_RULESET_MODAL}),
    ruleset: getEdited(state),
});

const mapDispatchToProps = (dispatch: Dispatch<RulesetsAction>, ownProps: Props & RouteComponentProps<{}>) => ({
    publishRuleset: () => {
        dispatch(publishRuleset(ownProps.ruleset));
    },
    ...bindActionCreators(
        {
            closeModal,
        },
        dispatch
    )
});

export default compose(
    withRouter,
    connect(mapStateToProps),
    connect(null, mapDispatchToProps)
)(ConfirmPublishModal);