import * as React from 'react';
import { RouteComponentProps, withRouter } from 'react-router';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { compose } from 'recompose';

import { AppState, Id, NavigationButton } from 'store/types';

import MainLayout from 'components/Shared/Layouts/MainLayout';

import {
    deleteRuleset as deleteRulesetCreator, publishRuleset as publishRulesetCreator,
    updateRulesetProperty as updateRulesetPropertyCreator,
    addRule as addRuleCreator, deleteRule as deleteRuleCreator, updateRule as updateRuleCreator,
    initRulesetCreation,
    initRulesetEditing,
    touchRuleField as touchRuleFieldCreator
} from 'store/AlertsManager/Rulesets/actionCreators';

import {
    DeleteRulesetAction,
    AddRuleAction,
    DeleteRuleAction,
    InitRulesetCreationAction,
    InitRulesetEditingAction,
    UpdateRuleAction,
    UpdateRulesetPropertyAction,
    PublishRulesetAction, TouchRuleFieldAction
} from 'store/AlertsManager/Rulesets/actions';
import {
    getCurrentVersionName,
    getEdited,
    getEditedValidation,
    isEditedActive
} from 'store/AlertsManager/Rulesets/selectors';
import { getUser } from 'store/Auth/selectors';
import { Rule, Ruleset, RulesetPropertyType, RulesetValidation, Version } from 'store/AlertsManager/Rulesets/types';
import ActiveUser from 'store/Auth/models/activeUser';
import { isRequestor } from 'store/User/helpers';
import { OpenModalAction } from 'store/Modals/General/actions';
import { openModal } from 'store/Modals/General/actionCreators';
import {
    CONFIRM_DELETE_RULESET_MODAL,
    CONFIRM_PUBLISH_RULESET_MODAL
} from 'store/AlertsManager/Rulesets/constants';
import { CREATE_NEW_ENTITY_ID } from 'store/constants';

import RulesetEdit from 'components/AlertsManager/Rulesets/RulesetEdit';
import RulesetEditRules from 'components/AlertsManager/Rulesets/RulesetEditRules';
import Properties from 'components/AlertsManager/Rulesets/Properties';
import RequestorTargetSection from 'containers/AlertsManager/Rulesets/RequestorTargetSection';
import ResponderTargetSection from 'containers/AlertsManager/Rulesets/ResponderTargetSection';
import ConfirmDeleteModal from './ConfirmDeleteModal';

import ConfirmPublishModal from './ConfirmPublishModal';

interface Props {
    menuItems: NavigationButton[];
    ruleset: Ruleset;
    validation: RulesetValidation;
    isActive: boolean;

    publishRuleset: (ruleset: Ruleset) => PublishRulesetAction;
    deleteRuleset: (ruleset: Ruleset) => DeleteRulesetAction;

    updateRulesetProperty: (name: string, value: RulesetPropertyType) => UpdateRulesetPropertyAction;

    addRule: () => AddRuleAction;
    deleteRule: (index: number) => DeleteRuleAction;
    updateRule: (index: number, rule: Rule) => UpdateRuleAction;
    touchRuleField: (rule: Rule, index: number, field: string) => TouchRuleFieldAction;
    user: ActiveUser;

    initRulesetCreation: () => InitRulesetCreationAction;
    initRulesetEditing: (id: Id, active: boolean) => InitRulesetEditingAction;

    openModal: (name: string) => OpenModalAction;
    currentVersionName: string;
}

interface RouteParams {
    id: Id;
}

const breadCrumbs = (
    <div className="ap-breadcrumbs">
        <NavLink to="/alerts">Alerts</NavLink>
        <span className="breadcrumb-slash">/</span>
        <NavLink to="/rulesets">Rulesets</NavLink>
        <span className="breadcrumb-slash">/</span>
        <span>{`Ruleset Properties`}</span>
    </div>
);

class RulesetEditContainer extends React.PureComponent<Props & RouteComponentProps<RouteParams>> {
    private currentId: Id | null = null;

    componentDidMount() {
        if (!this.isCreate) {
            this.initEditing();
        } else {
            this.props.initRulesetCreation();
        }
    }

    componentDidUpdate() {
        // Sets Id of created ruleset to url in browser
        if (this.isCreate && this.props.ruleset.id) {
            this.currentId = this.props.ruleset.id;
            this.props.history.replace(`/rulesets/${this.props.ruleset.id}`);
        }

        if (!this.isCreate && this.currentId !== this.routeId) {
            this.initEditing();
        }
    }

    initEditing() {
        this.currentId = this.routeId;
        this.props.initRulesetEditing(this.currentId, this.isCurrent);
    }

    render() {
        const {
            ruleset, validation, isActive, user,
            addRule, deleteRule, updateRule, touchRuleField
        } = this.props;

        const disabled = !isActive;

        return (
            <MainLayout
                title={`Rulesets`}
                menuItems={this.props.menuItems}
                breadCrumbs={breadCrumbs}
            >
                <RulesetEdit
                    ruleset={ruleset}
                    validation={validation}
                    isActive={isActive}
                    user={user}
                    selectRulesetVersion={this.handleSelectRulesetVersion}
                    publishRuleset={this.handlePublishRuleset}
                    deleteRuleset={this.handleDeleteRuleset}
                    currentVersionName={this.props.currentVersionName}
                >{{
                    propertiesEdit: <Properties
                        user={user}
                        disabled={disabled}
                        ruleset={ruleset}
                        changeProperty={this.handlePropertyChange}
                    />,
                    rulesEdit: <RulesetEditRules
                        rules={ruleset.rules || []}
                        validations={validation.rules || []}
                        disabled={disabled}
                        addRule={addRule}
                        deleteRule={deleteRule}
                        updateRule={updateRule}
                        touchRuleField={touchRuleField}
                    />,
                    targetEdit: isRequestor(user)
                        ? (
                            <RequestorTargetSection
                                entityType={ruleset.entity_type}
                                entityId={ruleset.id}
                                disabled={disabled}
                            />
                        )
                        : <ResponderTargetSection
                            entityType={ruleset.entity_type}
                            entityId={ruleset.id}
                            disabled={disabled}
                        />
                }}
                </RulesetEdit>
                <ConfirmDeleteModal />
                <ConfirmPublishModal />
            </MainLayout>
        );
    }

    get routeId() {
        return this.props.match.params.id;
    }

    get isCreate() {
        return this.routeId === CREATE_NEW_ENTITY_ID;
    }

    get isCurrent() {
        return this.props.location.pathname.search(/\/rulesets\/.*$/) !== -1;
    }

    private handlePropertyChange = (name: string, value: RulesetPropertyType) => {
        const {updateRulesetProperty} = this.props;
        updateRulesetProperty(name, value);
    }

    private handlePublishRuleset = () => {
        this.props.openModal(CONFIRM_PUBLISH_RULESET_MODAL);
    }

    private handleDeleteRuleset = () => {
        this.props.openModal(CONFIRM_DELETE_RULESET_MODAL);
    }

    private handleSelectRulesetVersion = (version: Version) => {
        if (version.current) {
            this.props.history.push(`/rulesets/${version.id}`);
        } else {
            this.props.history.push(`/ruleset-versions/${version.id}`);
        }
    }
}

const mapStateToProps = (state: AppState) => ({
    ruleset: getEdited(state),
    validation: getEditedValidation(state),
    isActive: isEditedActive(state),
    user: getUser(state),
    currentVersionName: getCurrentVersionName(state),
});

const mapDispatchToProps = {
    openModal,
    initRulesetCreation,
    initRulesetEditing,

    deleteRuleset: deleteRulesetCreator,
    publishRuleset: publishRulesetCreator,

    updateRulesetProperty: updateRulesetPropertyCreator,
    addRule: addRuleCreator,
    deleteRule: deleteRuleCreator,
    updateRule: updateRuleCreator,
    touchRuleField: touchRuleFieldCreator
};

export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps)
)(RulesetEditContainer);
