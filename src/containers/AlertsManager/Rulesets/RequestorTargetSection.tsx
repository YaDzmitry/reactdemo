import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { Action, bindActionCreators, Dispatch } from 'redux';

import { default as Filter, FilterDecoratorChange } from 'decorators/FilterDecorator';
import ActionDecorator from 'decorators/ActionDecorator';

import RequestorTargetFilters from 'components/AlertsManager/Rulesets/RequestorTargetFilters';
import RequestorTargetFundListModal from 'components/AlertsManager/Rulesets/RequestorTargetFundListModal';

import { DefaultStateRulesetRequestorTarget } from 'store/constants';
import { AppState, Id, SelectItem } from 'store/types';
import { getTargetFundList, updateRulesetTarget } from 'store/AlertsManager/Rulesets/actionCreators';
import { AsyncFilterName, Chip, FilterInfo, FilterNames, FiltersState } from 'store/Filters/types';
import { getTargetFundListObject } from 'store/AlertsManager/Rulesets/selectors';
import { getChips, getFilters, getIsLoading } from 'store/Filters/selectors';
import { getFilterInfo } from 'store/AsyncFiltersChoices/selectors';
import { getChoices, setAsyncFilters } from 'store/AsyncFiltersChoices/actionCreators';
import { setCheckedIds, setFilters } from 'store/Filters/actionCreators';
import { closeModal, openModal } from 'store/Modals/General/actionCreators';
import { REQUESTOR_TARGET_FUND_LIST_MODAL } from 'store/AlertsManager/Rulesets/constants';
import {
    GET_TARGET_FUND_LIST,
    GetTargetFundListAction,
    UpdateRulesetTargetAction
} from 'store/AlertsManager/Rulesets/actions';
import { getActionIsInProgress } from 'store/Actions/selectors';
import { isModalOpen } from 'store/Modals/General/selectors';
import { SetFiltersAction } from 'store/Filters/actions';
import { CloseModalAction, OpenModalAction } from 'store/Modals/General/actions';
import { ModalParams } from 'store/Modals/General/types';
import { TargetFund } from 'store/Fund/types';
import { RulesetEntityType } from 'store/AlertsManager/Rulesets/types';
import { isScrollerChange } from 'store/Filters/helpers';

const filterName = FilterNames.rulesetRequestorTargetFundList;

export interface OutterProps {
    entityType?: RulesetEntityType;
    entityId?: Id;
    disabled?: boolean;
}

export interface RequestorTargetFiltersProps {
    chips: Chip[];
    currentFilter: FiltersState;
    handleChange: FilterDecoratorChange;
    applyFilter: (filter: FiltersState) => GetTargetFundListAction;
    filterPageName: string;
    count: number;
    firmCount: number;

    firmFilter: FilterInfo;
    portfolioFilter: FilterInfo;
    creditOfficerFilter: FilterInfo;
    productFilter: FilterInfo;
    strategyFilter: FilterInfo;
    internalIdFilter: FilterInfo;

    getChoices: (page: string, filterName: string, filter: FiltersState) => void;
    setAsyncFilters: (filterName: string, filter: FiltersState, pageName: string) => void;
    openModal: (name: string, params: ModalParams) => OpenModalAction;
    updateRulesetTarget: () => UpdateRulesetTargetAction;
}

interface RequestorTargetFiltersModalProps {
    results: TargetFund[];
    isOpen: boolean;
    closeModal: () => CloseModalAction;
    isLoading: boolean;
    isGridLoading: boolean;
    setFilters: (filterName: string, payload: FiltersState) => SetFiltersAction;
    chooseAll: () => void;
    chooseRow: (id: Id) => void;
}

export type RequestorTargetSectionProps = OutterProps & RequestorTargetFiltersProps & RequestorTargetFiltersModalProps;

class RequestorTargetSection extends React.PureComponent<RequestorTargetSectionProps> {

    render() {
        return (
            <>
                <RequestorTargetFilters {...this.props} handleChange={this.handleChange} />
                <RequestorTargetFundListModal
                    {...this.props}
                    handleChange={this.handleChange}
                    chooseRow={this.chooseRow}
                    chooseAll={this.chooseAll}
                />
            </>
        );
    }

    private handleChange = (data: FiltersState) => {
        this.props.handleChange(data);
        if (!isScrollerChange(data)) {
            this.props.updateRulesetTarget();
        }
    }

    private chooseRow = (id: Id) => {
    }

    private chooseAll = () => {
    }
}

const mapStateToProps = (state: AppState) => ({
    filterPageName: filterName,
    results: getTargetFundListObject(state).list,
    count: getTargetFundListObject(state).count,
    firmCount: getTargetFundListObject(state).firmCount,
    currentFilter: getFilters(state, filterName),
    chips: getChips(state, filterName),

    portfolioFilter: getFilterInfo(state, {
        page: FilterNames.rulesetRequestorTargetFundList,
        filterName: AsyncFilterName.portfolio
    }),
    creditOfficerFilter:  getFilterInfo(state, {
        page: FilterNames.rulesetRequestorTargetFundList,
        filterName: AsyncFilterName.creditOfficer,
        labelField: 'value',
        valueField: 'value',
    }),
    productFilter: getFilterInfo(state, {
        page: FilterNames.rulesetRequestorTargetFundList,
        filterName: AsyncFilterName.product,
        labelField: 'value',
        valueField: 'value',
    }),
    strategyFilter: getFilterInfo(state, {
        page: FilterNames.rulesetRequestorTargetFundList,
        filterName: AsyncFilterName.strategy,
        labelField: 'value',
        valueField: 'value',
    }),
    firmFilter: getFilterInfo(state, {
        page: FilterNames.rulesetRequestorTargetFundList,
        filterName: AsyncFilterName.firm,
    }),
    internalIdFilter: getFilterInfo(state, {
        page: FilterNames.rulesetRequestorTargetFundList,
        filterName: AsyncFilterName.internalId,
        labelField: 'internal_id',
        valueField: 'internal_id',
    }),

    isOpen: isModalOpen(state, {name: REQUESTOR_TARGET_FUND_LIST_MODAL}),
    isLoading: getIsLoading(state),
    isGridLoading: getActionIsInProgress(state, {name: GET_TARGET_FUND_LIST}),
});

const mapDispatchToProps = (dispatch: Dispatch<Action>, ownProps: OutterProps) => ({
    ...bindActionCreators(
        {
            applyFilter: (filter: FiltersState) => {
                return getTargetFundList(filter, ownProps.entityType, ownProps.entityId);
            },
            setFilters,
            setCheckedIds,
            getChoices,
            setAsyncFilters,
            openModal,
            closeModal,
            updateRulesetTarget,
        },
        dispatch
    )
});

export default compose<RequestorTargetSectionProps, OutterProps>(
    connect(mapStateToProps, mapDispatchToProps),
    (component: React.Component) => ActionDecorator(component, filterName),
    (component: React.Component) => Filter(component, DefaultStateRulesetRequestorTarget, filterName, true)
)(RequestorTargetSection);
