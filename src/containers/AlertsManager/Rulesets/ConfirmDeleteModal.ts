import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { compose } from 'recompose';

import { AppState } from 'store/types';
import { isModalOpen } from 'store/Modals/General/selectors';
import { closeModal } from 'store/Modals/General/actionCreators';
import { CONFIRM_DELETE_RULESET_MODAL } from 'store/AlertsManager/Rulesets/constants';
import { deleteRuleset } from 'store/AlertsManager/Rulesets/actionCreators';
import { getEdited } from 'store/AlertsManager/Rulesets/selectors';
import { Ruleset } from 'store/AlertsManager/Rulesets/types';

import ConfirmDeleteModal from 'components/AlertsManager/Rulesets/ConfirmDeleteModal';
import { RulesetsAction } from '../../../store/AlertsManager/Rulesets/actions';

interface Props {
    ruleset: Ruleset;
}

const mapStateToProps = (state: AppState) => ({
    isOpen: isModalOpen(state, {name: CONFIRM_DELETE_RULESET_MODAL}),
    ruleset: getEdited(state),
});

const mapDispatchToProps = (dispatch: Dispatch<RulesetsAction>, ownProps: Props & RouteComponentProps<{}>) => ({
    deleteRuleset: () => {
        dispatch(deleteRuleset(ownProps.ruleset, ownProps.history));
    },
    ...bindActionCreators(
        {
            closeModal,
        },
        dispatch
    )
});

export default compose(
    withRouter,
    connect(mapStateToProps),
    connect(null, mapDispatchToProps)
)(ConfirmDeleteModal);