import * as React from 'react';
import { RouterProps, withRouter } from 'react-router';
import { connect } from 'react-redux';
import { compose } from 'recompose';

import Filter, { FilterDecoratorChange } from 'decorators/FilterDecorator';
import Action from 'decorators/ActionDecorator';

import RulesetListPage from 'components/AlertsManager/Rulesets/RulesetListPage';

import { DefaultStateRulesetList } from 'store/constants';
import { AppState, Id, NavigationButton } from 'store/types';
import { getFilters, getIsLoading } from 'store/Filters/selectors';
import { FilterNames, FiltersState } from 'store/Filters/types';
import { setCheckedIds, setFilters } from 'store/Filters/actionCreators';
import { getCount, getList } from 'store/AlertsManager/Rulesets/selectors';
import { downloadRulesetList, getRulesetList, initRulesetCreation } from 'store/AlertsManager/Rulesets/actionCreators';
import { getActionIsInProgress } from 'store/Actions/selectors';
import { Ruleset } from 'store/AlertsManager/Rulesets/types';
import {
    DownloadRulesetListAction, GET_RULESET_LIST, InitRulesetCreationAction
} from 'store/AlertsManager/Rulesets/actions';
import { openModal } from 'store/Modals/General/actionCreators';
import { OpenModalAction } from 'store/Modals/General/actions';
import { BULK_UPLOAD_MODAL } from 'store/AlertsManager/Rulesets/constants';
import { isModalOpen } from 'store/Modals/General/selectors';

interface Props {
    menuItems: NavigationButton[];
    results: Ruleset[];
    count: number;
    handleChange: FilterDecoratorChange;
    currentFilter: FiltersState;
    isLoading: boolean;
    isGridLoading: boolean;
    checkedAll: boolean;
    checkedIds: Id[];
    chooseAll: () => void;
    openModal: (name: string, params: any) => OpenModalAction;
    chooseRow: (id: Id) => void;
    initRulesetCreation: () => InitRulesetCreationAction;
    downloadRulesetList: (checkedIds: Id[], checkedAll: boolean) => DownloadRulesetListAction;
    isBulkUploadOpen: boolean;
}

const filterName = FilterNames.rulesetList;

class RulesetList extends React.PureComponent<Props & RouterProps> {
    handleBulkUpload = () => {
        this.props.openModal(BULK_UPLOAD_MODAL, {});
    }

    render() {
        return (
            <>
                <RulesetListPage
                    {...this.props}
                    createRuleset={this.handleCreateRuleset}
                    download={this.props.downloadRulesetList}
                    bulkUpload={this.handleBulkUpload}
                    isBulkUploadOpen={this.props.isBulkUploadOpen}
                />
            </>
        );
    }

    private handleCreateRuleset = () => {
        this.props.initRulesetCreation();
        this.props.history.push('/rulesets/create');
    }
}

const mapStateToProps = (state: AppState) => ({
    results: getList(state),
    count: getCount(state),
    currentFilter: getFilters(state, filterName),
    isLoading: getIsLoading(state),
    isGridLoading: getActionIsInProgress(state, {name: GET_RULESET_LIST}),
    checkedAll: getFilters(state, filterName).checkedAll,
    checkedIds: getFilters(state, filterName).checkedIds,
    isBulkUploadOpen: isModalOpen(state, {name: BULK_UPLOAD_MODAL}),
});

const mapDispatchToProps = {
    applyFilter: getRulesetList,
    setFilters,
    openModal,
    setCheckedIds,
    initRulesetCreation,
    downloadRulesetList,
};

export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps),
    (component: React.Component) => Action(component, filterName),
    (component: React.Component) => Filter(component, { ...DefaultStateRulesetList }, filterName)
)(RulesetList);
