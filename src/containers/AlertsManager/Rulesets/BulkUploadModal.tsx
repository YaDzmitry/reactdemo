import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps, withRouter } from 'react-router';
import { compose } from 'recompose';

import { API_BULK_IMPORT_URL } from 'helpers/creditApiHelper';
import { BulkUploadModal } from 'components/Shared/BulkUpload/BulkUploadModal';

import { isModalOpen } from 'store/Modals/General/selectors';
import { closeModal } from 'store/Modals/General/actionCreators';
import { bulkUpload } from 'store/AlertsManager/Rulesets/actionCreators';
import { getUploadHistory } from 'store/BulkUpload/actionCreator';
import { setCancelImportExport } from 'store/BulkUpload/actionCreator';
import { AppState, Id, TableRowAction } from 'store/types';
import { getBulkUploadHistory, getBulkUploadProgress } from 'store/BulkUpload/selectors';
import { BulkUploadProgress, UploadType } from 'store/BulkUpload/types';
import { FormatTypeChoices, TableRowActionIcon, TableRowActionType } from 'store/constants';
import { BULK_UPLOAD_MODAL } from 'store/AlertsManager/Rulesets/constants';
import { getUser } from 'store/Auth/selectors';
import ActiveUser from 'store/Auth/models/activeUser';
import { BULK_ITEM_NAME, BULK_ITEM_NAME_PLURAL } from 'store/BulkUpload/constants';
import { GetUploadHistoryAction, SetCancelImportExportAction } from 'store/BulkUpload/actions';
import { CloseModalAction } from 'store/Modals/General/actions';

interface Props {
    isOpen: boolean;
    setCancelImportExport: (fileId: Id) => SetCancelImportExportAction;
    closeModal: () => CloseModalAction;
    uploadHistory: BulkUploadProgress[];
    getUploadHistory: (uploadType: UploadType) => GetUploadHistoryAction;
    importProgress: BulkUploadProgress[];
    bulkUpload: (file: FormData) => void;
    user: ActiveUser;
}

interface RouteProps {
}

class BulkUploadModalContainer extends React.PureComponent<Props & RouteComponentProps<RouteProps>> {
    rowActions: TableRowAction[] = [
        {
            type: TableRowActionType.Link,
            icon: TableRowActionIcon.BulkUpload,
            link: `${API_BULK_IMPORT_URL}/files/{id}/`,
        },
        {
            type: TableRowActionType.Link,
            icon: TableRowActionIcon.Download,
            link: `${API_BULK_IMPORT_URL}/files/{id}/error/`,
            isVisible: (row: any) => {
                return !!row.related_file;
            },
        },
    ];

    componentDidMount() {
        this.props.getUploadHistory(UploadType.Ruleset);
    }

    handleCancelUpload = (lastFileId: Id) => {
        this.props.setCancelImportExport(lastFileId);
    }

    handleEditResult = (lastFileId: Id) => {
        this.props.history.push(`/rulesets`);
    }

    handleUpload = (files: File[]) => {
        const formData = new FormData();
        formData.append('import_file', files[0]);
        formData.append('type', String(UploadType.Ruleset));
        this.props.bulkUpload(formData);
    }

    render() {
        const {isOpen} = this.props;
        const successMessage = (
            <>
                <div>Success! Upload Complete</div>
                <div>You can now view and edit your recently uploaded rulesets or upload new rulesets</div>
            </>
        );

        const processedWithErrorsMessage = (
            <>
                <div>Completed - contains errors! Upload Complete</div>
                <div>You can now view and edit your recently uploaded rulesets or upload new rulesets below</div>
            </>
        );

        return (
            <BulkUploadModal
                isOpen={isOpen}
                size={'lg'}
                rowActions={this.rowActions}
                closeModal={this.props.closeModal}
                uploadHistory={this.props.uploadHistory}
                progress={this.props.importProgress}
                upload={this.handleUpload}
                cancelUpload={this.handleCancelUpload}
                editResult={this.handleEditResult}
                user={this.props.user}
                entityNameSingle={BULK_ITEM_NAME[UploadType.Ruleset]}
                entityNamePlural={BULK_ITEM_NAME_PLURAL[UploadType.Ruleset]}
                entityNameFormatType={FormatTypeChoices.CountRulesets}
            >
                {{successMessage, processedWithErrorsMessage}}
            </BulkUploadModal>
        );
    }
}

const mapStateToProps = (state: AppState) => ({
    isOpen: isModalOpen(state, {name: BULK_UPLOAD_MODAL}),
    importProgress: getBulkUploadProgress(state, {type: UploadType.Ruleset}),
    uploadHistory: getBulkUploadHistory(state, {type: UploadType.Ruleset}),
    user: getUser(state),
});

const mapDispatchToProps = {
    setCancelImportExport,
    closeModal,
    bulkUpload,
    getUploadHistory,
};

export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps)
)(BulkUploadModalContainer);