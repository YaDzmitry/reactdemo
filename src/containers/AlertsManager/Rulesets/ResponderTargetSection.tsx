import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { Action, bindActionCreators, Dispatch } from 'redux';

import { default as Filter, FilterDecoratorChange } from 'decorators/FilterDecorator';
import ActionDecorator from 'decorators/ActionDecorator';

import ResponderTargetFilters from 'components/AlertsManager/Rulesets/ResponderTargetFilters';
import ResponderTargetFundListModal from 'components/AlertsManager/Rulesets/ResponderTargetFundListModal';

import { DefaultStateRulesetResponderTarget } from 'store/constants';
import { AppState, Id, SelectItem } from 'store/types';
import { getTargetFundList, updateRulesetTarget } from 'store/AlertsManager/Rulesets/actionCreators';
import { AsyncFilterName, Chip, FilterInfo, FilterNames, FiltersState } from 'store/Filters/types';
import { getTargetFundListObject } from 'store/AlertsManager/Rulesets/selectors';
import { getFilters, getIsLoading } from 'store/Filters/selectors';
import { getFilterInfo } from 'store/AsyncFiltersChoices/selectors';
import { getChoices, setAsyncFilters } from 'store/AsyncFiltersChoices/actionCreators';
import { setCheckedIds, setFilters } from 'store/Filters/actionCreators';
import { closeModal, openModal } from 'store/Modals/General/actionCreators';
import { RESPONDER_TARGET_FUND_LIST_MODAL } from 'store/AlertsManager/Rulesets/constants';
import { isModalOpen } from 'store/Modals/General/selectors';
import { getActionIsInProgress } from 'store/Actions/selectors';
import {
    GET_TARGET_FUND_LIST,
    GetTargetFundListAction,
    UpdateRulesetTargetAction
} from 'store/AlertsManager/Rulesets/actions';
import { CloseModalAction, OpenModalAction } from 'store/Modals/General/actions';
import { ModalParams } from 'store/Modals/General/types';
import { SetCheckedIdsAction, SetFiltersAction } from 'store/Filters/actions';
import { FundModel } from 'store/Fund/models/fund';
import { RulesetEntityType } from 'store/AlertsManager/Rulesets/types';
import { isScrollerChange } from 'store/Filters/helpers';

const filterName = FilterNames.rulesetResponderTargetFundList;

export interface OutterProps {
    entityType?: RulesetEntityType;
    entityId?: Id;
    disabled?: boolean;
}

export interface ResponderTargetFiltersProps {
    chips: Chip[];
    count: number;
    currentFilter: FiltersState;
    handleChange: FilterDecoratorChange;
    applyFilter: (filter: FiltersState) => GetTargetFundListAction;
    filterPageName: string;
    firmCount: number;
    fundFilter: FilterInfo;
    getChoices: (page: string, filterName: string, filter: FiltersState) => void;
    setAsyncFilters: (filterName: string, filter: FiltersState, pageName: string) => void;
    openModal: (name: string, params: ModalParams) => OpenModalAction;
    updateRulesetTarget: () => UpdateRulesetTargetAction;
}

interface ResponderTargetFiltersModalProps {
    results: FundModel[];
    isOpen: boolean;
    closeModal: () => CloseModalAction;
    isLoading: boolean;
    isGridLoading: boolean;
    setFilters: (filterName: string, payload: FiltersState) => SetFiltersAction;
    setCheckedIds: (filterName: string, payload: FiltersState) => SetCheckedIdsAction;
    chooseAll: () => void;
    chooseRow: (id: Id) => void;
}

export type ResponderTargetSectionProps = OutterProps & ResponderTargetFiltersProps & ResponderTargetFiltersModalProps;

class ResponderTargetSection extends React.PureComponent<ResponderTargetSectionProps> {

    render() {
        return (
            <>
                <ResponderTargetFilters
                    {...this.props}
                    handleChange={this.handleChange}
                />

                <ResponderTargetFundListModal
                    {...this.props}
                    handleChange={this.handleChange}
                    chooseRow={this.chooseRow}
                    chooseAll={this.chooseAll}
                />
            </>
        );
    }

    private handleChange = (data: FiltersState) => {
        this.props.handleChange(data);
        if (!isScrollerChange(data)) {
            this.props.updateRulesetTarget();
        }
    }

    private chooseRow = (id: Id) => {
    }

    private chooseAll = () => {
    }
}

const mapStateToProps = (state: AppState) => ({
    filterPageName: filterName,
    results: getTargetFundListObject(state).list,
    count: getTargetFundListObject(state).count,
    firmCount: getTargetFundListObject(state).firmCount,
    currentFilter: getFilters(state, filterName),

    fundFilter: getFilterInfo(state, {
        page: FilterNames.rulesetResponderTargetFundList,
        filterName: AsyncFilterName.fund,
    }),

    isOpen: isModalOpen(state, {name: RESPONDER_TARGET_FUND_LIST_MODAL}),
    isLoading: getIsLoading(state),
    isGridLoading: getActionIsInProgress(state, {name: GET_TARGET_FUND_LIST}),
});

const mapDispatchToProps = (dispatch: Dispatch<Action>, ownProps: OutterProps & ResponderTargetFiltersProps) => ({
    ...bindActionCreators(
        {
            applyFilter: (filter: FiltersState) => {
                return getTargetFundList(filter, ownProps.entityType, ownProps.entityId);
            },
            setFilters: (pageFilterName: string, payload: FiltersState) => {
                if (payload.checked_all && ownProps.currentFilter.fund_id &&
                    ownProps.currentFilter.fund_id.length > 0
                ) {
                    payload.fund_id = [];
                }
                if (payload.fund_id && payload.fund_id.length > 0 && ownProps.currentFilter.checked_all) {
                    payload.checked_all = false;
                }
                return setFilters(pageFilterName, payload);
            },
            setCheckedIds,
            getChoices,
            setAsyncFilters,
            openModal,
            closeModal,
            updateRulesetTarget
        },
        dispatch
    )
});

export default compose<ResponderTargetSectionProps, OutterProps>(
    connect(mapStateToProps, null),
    connect(null, mapDispatchToProps),
    (component: React.Component) => ActionDecorator(component, filterName),
    (component: React.Component) => Filter(component, DefaultStateRulesetResponderTarget, filterName, true)
)(ResponderTargetSection);
