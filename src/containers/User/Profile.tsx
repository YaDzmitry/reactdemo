import * as React from 'react';
import { NavigationButton } from '../../store/types';
import { Route, RouteComponentProps } from 'react-router';

import ProfilePage from '../../components/User/ProfilePage';

import { connect } from 'react-redux';
import Common from './Common';

const menuItems: NavigationButton[] = [
    {path: '/profile', title: 'Profile'}
];

interface Props {
}

class ProfileContainer extends React.PureComponent<Props & RouteComponentProps<{}>> {
    render() {
        const {match} = this.props;

        return (
            <ProfilePage menuItems={menuItems}>
                <Route path={match.url} exact={true} component={Common} />
            </ProfilePage>
        );
    }
}

export default connect()(ProfileContainer);