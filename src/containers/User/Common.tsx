import * as React from 'react';
import { connect } from 'react-redux';

import Profile from '../../components/User/Profile';
import { AppState } from '../../store/types';
import ActiveUser from '../../store/Auth/models/activeUser';
import { getUser } from '../../store/Auth/selectors';
import { updateProfile, updatePassword } from '../../store/User/actionCreators';
import * as userHelper from '../../store/User/helpers';
import UpdateProfileRequest from '../../store/User/models/updateProfileRequest';
import UpdatePasswordRequest from '../../store/User/models/updatePasswordRequest';

interface Props {
    user: ActiveUser;
    updateProfile: (request: UpdateProfileRequest) => any;
    updatePassword: (request: UpdatePasswordRequest) => any;
}

interface State {
    profileForm: UpdateProfileRequest;
    passwordForm: UpdatePasswordRequest;
}

class Common extends React.PureComponent<Props, State> {
    componentWillMount() {
        const { user } = this.props;
        this.setState({
            profileForm: userHelper.getProfileForm(user),
            passwordForm: userHelper.getPasswordForm()
        });
    }

    handleChangeProfile = (group: keyof State, field: string, value: any) => {
        this.setState(prevState => {
            return {
                [group]: {
                    ...prevState[group],
                    [field]: value
                }
            } as Pick<State, keyof State>;

        });
    }

    handleUpdateProfile = () => {
        this.props.updateProfile(this.state.profileForm);
    }

    handleUpdatePassword = () => {
        this.props.updatePassword(this.state.passwordForm);
        this.setState({
            passwordForm: userHelper.getPasswordForm()
        });
    }

    render () {
        const { profileForm, passwordForm } = this.state;
        const { user } = this.props;
        return (
            <Profile
                user={user}
                profile={profileForm}
                password={passwordForm}
                changeProfile={this.handleChangeProfile}
                updateProfile={this.handleUpdateProfile}
                updatePassword={this.handleUpdatePassword}
            />
        );
    }
}

const mapStateToProps = (state: AppState) => ({
    user: getUser(state)
});

const mapDispatchToProps = { updateProfile, updatePassword };

export default connect(mapStateToProps, mapDispatchToProps)(Common);