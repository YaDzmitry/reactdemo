import * as React from 'react';
import { connect } from 'react-redux';

import ButtonDanger from 'components/Shared/Ui/Buttons/ButtonDanger';
import ButtonDefault from 'components/Shared/Ui/Buttons/ButtonDefault';
import Modal from 'components/Shared/Modals/Modal';

import { getModalParams, isModalOpen } from 'store/Modals/General/selectors';
import { closeModal } from 'store/Modals/General/actionCreators';
import { deletePortfolios } from 'store/Portfolio/actionCreators';
import { getPortfolioState } from 'store/Portfolio/selectors';
import { PortfolioModel } from 'store/Portfolio/models/portfolio';
import { AppState, Id } from 'store/types';
import { CONFIRM_DELETE_USERS_MODAL } from 'store/Account/constants';
import { deleteUsers } from 'store/Account/actionCreators';
import { UserAccount } from 'store/Auth/models/userAccount';

interface Props {
    isOpen: boolean;
    portfoliosCollection: PortfolioModel[];
    checkedIds: Id[];
    checkedAll: boolean;
    count: number;
    closeModal: () => any;
    deletePortfolios: any;
    deleteUsers: (ids: Id[], checkedAll: boolean) => any;
    params: any;
    rows: UserAccount[];
}

class ConfirmDeleteModal extends React.PureComponent<Props> {
    constructor(props: Props) {
        super(props);

        this.handleToggle = this.handleToggle.bind(this);
    }

    handleToggle() {
        this.props.closeModal();
    }

    render() {
        const { isOpen, checkedAll, count } = this.props;
        let checkedIds;
        checkedIds = this.props.checkedIds.map((item: Id) => {
            let newId = item;
            this.props.rows.forEach((row: UserAccount) => {
               if (row.id === item && row.user) {
                   newId = row.user.id;
               }
               return false;
            });
            return newId;
        });
        if (this.props.params && this.props.params.id) {
            checkedIds.push(this.props.params.id);
        }
        let user;
        let numberSelected: number = 0;
        if (checkedIds && count) {
            if (checkedAll) {
                if (checkedIds.length > 0) {
                    numberSelected = count - checkedIds.length;
                } else {
                    numberSelected = count;
                }
            } else {
                if (checkedIds.length > 0) {
                    numberSelected = checkedIds.length;
                }
            }
        }

        if (numberSelected === 1) {
            user =  'Are you sure you want to delete this user';
        } else {
            user = `Are you sure that you're going to delete ${numberSelected} users`;
        }
        const body = (
            <p>{user}?</p>
        );
        const title = <>Confirm Users Delete</>;
        const buttons = [
            (
                <ButtonDanger
                    buttonLarge={true}
                    handleClick={() => this.props.deleteUsers(checkedIds, checkedAll)}
                >Yes, delete
                </ButtonDanger>),
            (<ButtonDefault buttonLarge={true} inverse={true} handleClick={this.handleToggle}>cancel</ButtonDefault>)
        ];

        return (
            <Modal
                className="ui-modal--confirm"
                isOpen={isOpen}
                toggle={this.handleToggle}
                title={title}
                body={body}
                buttons={buttons}
            />
        );
    }
}

const mapStateToProps = (state: AppState) => ({
    isOpen: isModalOpen(state, {name: CONFIRM_DELETE_USERS_MODAL}),
    params: getModalParams(state),
    portfoliosCollection: getPortfolioState(state).list
});

const mapDispatchToProps = {
    closeModal,
    deletePortfolios,
    deleteUsers,
};

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmDeleteModal);