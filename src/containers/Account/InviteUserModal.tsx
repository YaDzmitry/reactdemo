import * as React from 'react';
import { connect } from 'react-redux';

import InviteUserModal from 'components/Shared/Modals/InviteUserModal';

import { AppState } from 'store/types';
import { isModalOpen } from 'store/Modals/General/selectors';
import { closeModal } from 'store/Modals/General/actionCreators';
import { inviteUser, changeInvitingUser } from 'store/Account/actionCreators';
import { getAccountUsersFilter, getInvitedUser } from 'store/Account/selectors';
import { messages } from 'store/Account/constants';
import { InvitedUser } from 'store/Shared/models/invitedUser';
import AccountUsersFilter from 'store/Account/models/filter';
import { UserAccountRoles } from 'store/Auth/constants';

interface Props {
    isOpen: boolean;
    invitedUser: InvitedUser;
    currentFilter: AccountUsersFilter;
    closeModal: () => any;
    changeInvitingUser: (invitedUser: InvitedUser) => any;
    inviteUser: (invitedUser: InvitedUser, filter: AccountUsersFilter) => any;
}

export const name = 'INVITE_USER_MODAL';

class InviteUserModalContainer extends React.PureComponent<Props> {
    handleChange = (field: string, value: string) => {
        this.props.changeInvitingUser({
            ...this.props.invitedUser,
            [field]: value
        });
    }

    handleSave = () => {
        const {invitedUser, currentFilter} = this.props;
        this.props.inviteUser(invitedUser, currentFilter);
    }

    render() {
        const {isOpen, closeModal: handleCloseModal, invitedUser} = this.props;

        return (
            <InviteUserModal
                isOpen={isOpen}
                title={messages.INVITE_USER_TITLE}
                toggle={handleCloseModal}
                invitedUser={invitedUser}
                change={this.handleChange}
                save={this.handleSave}
                roles={UserAccountRoles}
            />
        );
    }
}

const mapStateToProps = (state: AppState) => ({
    isOpen: isModalOpen(state, {name}),
    invitedUser: getInvitedUser(state),
    currentFilter: getAccountUsersFilter(state),
});

const mapDispatchToProps = { closeModal, changeInvitingUser, inviteUser };

export default connect(mapStateToProps, mapDispatchToProps)(InviteUserModalContainer);