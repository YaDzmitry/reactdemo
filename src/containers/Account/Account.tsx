import * as React from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';

import Action from 'decorators/ActionDecorator';
import Filter, { FilterDecoratorChange } from 'decorators/FilterDecorator';

import AccountPage from 'components/Account/AccountPage';
import DataTable from 'components/Shared/DataTable/DataTable';
import UiDataTable from 'components/Shared/Ui/Tables/UiDataTable';
import SelectFilter from 'components/Shared/Filters/SelectFilter';
import ConfirmDeleteModal from './ConfirmDeleteModal';
import ConfirmModal from '../Shared/ConfirmModal';
import EditUserModal from './EditUserModal';
import InviteUserModal, { name as inviteModalName } from './InviteUserModal';

import { AppState, Id, NavigationButton, TableHeader, TableRowAction } from 'store/types';
import {
    DefaultUsersList,
    CellTypeChoices, FormatTypeChoices, TableBulkActionChoices, TableRowActionIcon, TableRowActionType,
} from 'store/constants';
import { openModal, closeModal } from 'store/Modals/General/actionCreators';
import { getModalParams } from 'store/Modals/General/selectors';
import { getActiveAccount } from 'store/Auth/selectors';
import { getAccountUsers, getAccountUsersCount } from 'store/Account/selectors';
import { UserAccount } from 'store/Auth/models/userAccount';
import {
    applyFilter, deleteUser, startEditingUser, startInvitingUser, updateUserRole
} from 'store/Account/actionCreators';
import AccountUsersFilter from 'store/Account/models/filter';
import { CONFIRM_DELETE_USERS_MODAL, messages } from 'store/Account/constants';
import { UserAccountRoles } from 'store/Auth/constants';
import { FilterNames } from 'store/Filters/types';
import { getFilters, getIsLoading } from 'store/Filters/selectors';
import { APPLY_FILTER } from 'store/Account/actions';
import { getActionIsInProgress } from 'store/Actions/selectors';
import { setCheckedIds, setFilters } from 'store/Filters/actionCreators';

const menuItems: NavigationButton[] = [
    {
        path: '/account',
        title: 'Users',
    }
];

interface Props {
    checkedAll: boolean;
    checkedIds: Id[];
    handleChange: FilterDecoratorChange;
    clearChecked: () => void;
    chooseAll: () => void;
    chooseRow: (id: Id) => void;
    account: UserAccount;
    users: UserAccount[];
    count: number;
    currentFilter: AccountUsersFilter;
    modalParams: any;
    applyFilter: (filter: AccountUsersFilter) => any;
    deleteUser: (id: Id, filter: AccountUsersFilter) => any;
    startEditingUser: (id: Id) => any;
    startInvitingUser: () => any;
    openModal: (name: string, params: any) => any;
    closeModal: () => any;
    updateUserRole: (id: Id, role: string) => any;
}

const filterName = FilterNames.usersList;

class Account extends React.PureComponent<Props> {
    headers: TableHeader[] = [
        {
            name: 'name',
            title: 'Name',
            active: true,
            cellType: CellTypeChoices.Custom,
            transformer: row => (<span>{row.user.first_name} {row.user.last_name}</span>)},
        { name: 'user.email', title: 'Email', active: true, },
        { name: 'user.status', title: 'Status', active: true, formatType: FormatTypeChoices.ContainsUnderscore, },
        {
            name: 'role',
            title: 'Role',
            active: true,
            cellType: CellTypeChoices.Custom,
            transformer: row => (
                <div className="eighties">
                    <SelectFilter
                        name={'role'}
                        placeholder={'Role'}
                        value={row.role}
                        clearable={false}
                        options={UserAccountRoles}
                        handleChange={(e) => this.props.updateUserRole(row.user.id, e.role)}
                    />
                </div>
            ),
        },
    ];

    rowActions: TableRowAction[] = [
        {
            type: TableRowActionType.Image,
            icon: TableRowActionIcon.Trash,
            handler: (id: Id, row: any) => this.props.openModal(CONFIRM_DELETE_USERS_MODAL, {id: row.user.id}),
        },
    ];

    bulkActions = [
        { type: TableBulkActionChoices.Delete, handler: () => this.props.openModal(CONFIRM_DELETE_USERS_MODAL, {})}
    ];

    constructor(props: Props) {
        super(props);

        this.handleDeleteAction = this.handleDeleteAction.bind(this);
        this.handleInviteAction = this.handleInviteAction.bind(this);
    }

    componentWillReceiveProps(nextProps: Props) {
        if (nextProps.account.account.id !== this.props.account.account.id) {
            this.applyEmptyFilter();
        }
    }

    handleDeleteAction(id: Id) {
        this.props.openModal(CONFIRM_DELETE_USERS_MODAL, {id});
    }

    handleInviteAction() {
        this.props.startInvitingUser();
        this.props.openModal(inviteModalName, {});
    }

    handleDeleteYes = () => {
        const { currentFilter, modalParams } = this.props;
        this.props.deleteUser(modalParams.id, currentFilter);
    }

    handleDeleteNo = () => {
        this.props.closeModal();
    }

    render() {
        const { users } = this.props;
        const confirmBody = <>{messages.CONFIRM_DELETE_USER_QUESTION}</>;
        const confirmTitle = <>{messages.CONFIRM_DELETE_USER_TITLE}</>;
        return (
            <>
            <AccountPage menuItems={menuItems} inviteUser={this.handleInviteAction}>
                <UiDataTable>
                    <DataTable
                        rows={users}
                        rowActions={this.rowActions}
                        bulkActions={this.bulkActions}
                        headers={this.headers}
                        hasPagination={false}
                        hasSorting={false}
                        withCheckboxes={true}
                        actionsTitle={' '}
                        checkedAll={this.props.checkedAll}
                        checkedIds={this.props.checkedIds}
                        handleChange={this.props.handleChange}
                        chooseAll={this.props.chooseAll}
                        chooseRow={this.props.chooseRow}
                        count={this.props.count}
                    />
                </UiDataTable>
            </AccountPage>
            <ConfirmModal
                title={confirmTitle}
                body={confirmBody}
                chooseYes={this.handleDeleteYes}
                chooseNo={this.handleDeleteNo}
            />
            <ConfirmDeleteModal
                checkedIds={this.props.checkedIds}
                checkedAll={this.props.checkedAll}
                count={this.props.count}
                rows={users}
            />
            <EditUserModal />
            <InviteUserModal />
            </>
        );
    }

    private applyEmptyFilter() {
        this.props.applyFilter({
            page: 0
        });
    }
}

const mapStateToProps = (state: AppState) => ({
    account: getActiveAccount(state),
    users: getAccountUsers(state),
    count: getAccountUsersCount(state),
    currentFilter: getFilters(state, filterName),
    modalParams: getModalParams(state),
    checkedAll: getFilters(state, filterName).checkedAll,
    checkedIds: getFilters(state, filterName).checkedIds,
    isGridLoading: getActionIsInProgress(state, {name: APPLY_FILTER}),
    isLoading: getIsLoading(state),
});

const mapDispatchToProps = {
    closeModal,
    applyFilter,
    deleteUser,
    startEditingUser,
    startInvitingUser,
    updateUserRole,
    openModal,
    setCheckedIds,
    setFilters,
};

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    (component: React.Component) => Action(component, filterName),
    (component: React.Component) => Filter(component, {...DefaultUsersList}, filterName),
)(Account);
