import * as React from 'react';
import { connect } from 'react-redux';

import EditUserModal from 'components/Account/EditUserModal';

import { AppState } from 'store/types';
import { isModalOpen } from 'store/Modals/General/selectors';
import { closeModal } from 'store/Modals/General/actionCreators';
import { changeEditingUser, updateUser } from 'store/Account/actionCreators';
import { getAccountUsersFilter, getEditedUser } from 'store/Account/selectors';
import { UserAccount } from 'store/Auth/models/userAccount';
import AccountUsersFilter from 'store/Account/models/filter';

interface Props {
    isOpen: boolean;
    userAccount: UserAccount;
    currentFilter: AccountUsersFilter;
    closeModal: () => any;
    changeEditingUser: (userAccount: UserAccount) => any;
    updateUser: (userAccount: UserAccount, filter: AccountUsersFilter) => any;
}

export const name = 'EDIT_USER_MODAL';

class EditUserModalContainer extends React.PureComponent<Props> {
    handleToggle = () => {
        this.props.closeModal();
    }

    handleChange = (field: string, value: string) => {
        this.props.changeEditingUser({
            ...this.props.userAccount,
            [field]: value
        });
    }

    handleSave = () => {
        const {userAccount, currentFilter} = this.props;
        this.props.updateUser(userAccount, currentFilter);
    }

    render() {
        const {isOpen, userAccount, closeModal: handleCloseModal} = this.props;
        return (
            <EditUserModal
                isOpen={isOpen}
                userAccount={userAccount}
                toggle={handleCloseModal}
                save={this.handleSave}
                change={this.handleChange}
            />
        );
    }
}

const mapStateToProps = (state: AppState) => ({
    isOpen: isModalOpen(state, {name}),
    userAccount: getEditedUser(state),
    currentFilter: getAccountUsersFilter(state),
});

const mapDispatchToProps = { closeModal, changeEditingUser, updateUser };

export default connect(mapStateToProps, mapDispatchToProps)(EditUserModalContainer);