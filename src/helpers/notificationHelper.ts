import { toast } from 'react-toastify';

const AUTO_CLOSE = 5000;

class NotificationHelper {
    static success (message?: string) {
        const notification = message || 'Success!';
        toast.success(notification, {
            position: toast.POSITION.TOP_RIGHT,
            hideProgressBar: true,
        });
    }

    static error (data: any) {
        const notification = data.message || 'Error';
        const autoClose = data.time || AUTO_CLOSE;
        toast.error(notification, {
            position: toast.POSITION.TOP_RIGHT,
            hideProgressBar: true,
            autoClose: autoClose
        });
    }
}

export default NotificationHelper;