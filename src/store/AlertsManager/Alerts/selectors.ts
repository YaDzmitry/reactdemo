import { createSelector } from 'reselect';

import { getAlertsManagerState } from '../selectors';

import { AlertFund, AlertsState, ViewedFund } from './types';
import { initialState } from './constants';
import { AlertsManagerState } from '../types';

import { AppState } from 'store/types';
import { getFilters } from 'store/Filters/selectors';
import { FilterNames } from 'store/Filters/types';

export const getAlertsState = createSelector(
    getAlertsManagerState,
    (manager: AlertsManagerState): AlertsState => manager.alerts || initialState
);

export const getViewedFund = createSelector(
    getAlertsState,
    (state: AlertsState) => state.viewedFund
);

export const getViewedFundStatistic = createSelector(
    getViewedFund,
    (fund: ViewedFund): AlertFund | null => fund ? fund.statistic : null
);

export const getViewedFundEvaluationResults = createSelector(
    getViewedFund,
    (fund: ViewedFund) => fund ? fund.evaluation_results : {count: 0, results: []}
);
export const getList = createSelector(
    getAlertsState,
    (state: AlertsState) => {
        return state.list;
    }
);

export const getCount = createSelector(
    getAlertsState,
    (state: AlertsState) => state.count
);

export const getAlertCount = createSelector(
    getAlertsState,
    (state: AlertsState) => state.alertCount
);

export const selectAlertStatusItems = createSelector(
    getAlertsState,
    (state: AlertsState) => state.alertStatusItems
);

export const getPeriod = (state: AppState) => {
    const filters = getFilters(state, FilterNames.alertList);

    return filters.period;
};

export const getAlertStatus = (state: AppState) => {
    const filters = getFilters(state, FilterNames.alertList);

    return filters.alert_status;
};