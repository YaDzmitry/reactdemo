import * as moment from 'moment';

import { EvaluationResultStatus, AlertField } from './types';
import { RulePeriod } from 'store/AlertsManager/Rulesets/types';
import { MonthsCountByPeriod } from 'store/AlertsManager/Rulesets/constants';
import { formatText } from 'helpers/tableHelper';
import { FULL_MONTH_AND_YEAR, FormatTypeChoices } from 'store/constants';

export function isAlert(status: EvaluationResultStatus) {
    return status === EvaluationResultStatus.Alert;
}

export function isNoAlert(status: EvaluationResultStatus) {
    return status === EvaluationResultStatus.NoAlert;
}

export function isPartialData(status: EvaluationResultStatus) {
    return status === EvaluationResultStatus.PartialData;
}

export function isPartialDataAlert(status: EvaluationResultStatus) {
    return status === EvaluationResultStatus.PartialDataAlert;
}

export function isNoRuleset(status: EvaluationResultStatus) {
    return status === EvaluationResultStatus.NoRuleset;
}

export function isAwaitingData(status: EvaluationResultStatus) {
    return status === EvaluationResultStatus.AwaitingData;
}

export function isThresholdHighlighted(field: AlertField, value?: number): boolean {
    // Do not highlight the Threshold if it set to 0 #c5664
    if (!field || !value) {
        return false;
    }

    return field === AlertField.Threshold || field === AlertField.ThresholdAndPerfShare;
}

export function isPerfShareHighlighted(field: AlertField): boolean {
    if (!field) {
        return false;
    }
    return field === AlertField.ThresholdAndPerfShare || field === AlertField.PerfShare;
}

export function isAmountHighlighted(field: AlertField): boolean {
    if (!field) {
        return false;
    }
    return field === AlertField.Amount;
}

export function getPeriodAlertColumn(period: string, periodType?: RulePeriod): string {
    let periodString: string = formatText(period, FormatTypeChoices.FullMonthAndYear);
    let firstPeriod: string = '';
    switch (periodType) {
        case RulePeriod.ITD:
            break;
        case RulePeriod.OneMonth:
        case RulePeriod.ThreeMonths:
        case RulePeriod.SixMonths:
        case RulePeriod.TwelveMonths:
            const monthsCount = MonthsCountByPeriod[periodType];
            firstPeriod = moment(period).subtract(monthsCount, 'months').format(FULL_MONTH_AND_YEAR);
            periodString = `${firstPeriod} - ${periodString}`;
            break;
        default:
    }

    return periodString;
}
