import { combineReducers } from 'redux';

import { alertsReducer } from './Alerts/reducer';
import { rulesetsReducer } from './Rulesets/reducer';

export const alertsManagerReducer = combineReducers({
    alerts: alertsReducer,
    rulesets: rulesetsReducer
});