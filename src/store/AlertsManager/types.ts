import { AlertsState } from './Alerts/types';
import { RulesetsState } from './Rulesets/types';

export interface AlertsManagerState {
    alerts: AlertsState;
    rulesets: RulesetsState;
}