import { AppState } from 'store/types';

import { initialState } from './Alerts/constants';
import { AlertsManagerState } from './types';

export const getAlertsManagerState = (state: AppState) => state.alertsManager || initialState;
