import { Action } from 'redux';

export const DO_APP_HEART_BEAT = 'store/heartBeat';

export interface DoAppHeartBeatAction extends Action {}

export const doAppHeartBeat = () => ({type: DO_APP_HEART_BEAT});