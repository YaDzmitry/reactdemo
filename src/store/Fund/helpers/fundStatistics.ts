import { FundStatistics } from 'store/Fund/types';
import { BenchmarkType } from 'store/Profiles/Groups/types';

export function isPeerGroup(item: FundStatistics): boolean {
    return item.entity_type === BenchmarkType.FundGroup;
}