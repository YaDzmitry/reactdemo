import { ContactModel } from '../models/contact';

export function createEmptyContact(): ContactModel {
    return {
        email: '',
        first_name: '',
        last_name: ''
    };
}

export function getContactFio(contact: ContactModel) {
    return [contact.first_name || '', contact.last_name || ''].join(' ').trim();
}

export function getContactDescription(contact: ContactModel) {
    const fio = getContactFio(contact);

    return fio ? `${fio} <${contact.email}>` : contact.email;
}