import { DataEntry } from 'store/Fund/types';

export function isRequestor(dataEntry?: DataEntry | null) {
    return dataEntry && dataEntry === DataEntry.Requestor;
}

export function isPipe(dataEntry?: DataEntry | null) {
    return dataEntry && dataEntry === DataEntry.Pipe;
}

export function isFundManager(dataEntry?: DataEntry | null) {
    return dataEntry && dataEntry === DataEntry.FundManager;
}
