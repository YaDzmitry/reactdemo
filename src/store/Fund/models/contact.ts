import { Id } from '../../types';

export interface ContactModel {
    id?: Id;
    first_name: string;
    last_name: string;
    email: string;
}

export interface NewContactModel {
    fund: Id;
    first_name: string;
    last_name: string;
    email: string;
}

export interface UpdateContactModel {
    first_name?: string;
    last_name?: string;
    email?: string;
}

export interface DeleteContactsModel {
    fundId: Id;
    contactIds: Id[];
}
