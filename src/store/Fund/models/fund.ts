import { Request } from 'store/Requests/models/request';
import { Acl } from 'store/DataManager/models/acl';
import { AclUser } from 'store/DataManager/models/aclUser';
import { Id } from 'store/types';
import { AlertsOverview } from 'store/AlertsManager/Alerts/types';
import { FundStatisticsDataset } from 'store/Fund/types';

export interface FundModel {
    id: Id;
    ap_id: string;
    name: string;
    alias: string;
    fund_count?: number;
    internal_id: string;
    fund_details: FundDetails;
    service_providers: ServiceProviders;
    request?: Request | null;
    acl?: Acl | null;
    acl_user?: AclUser;
    has_datasets?: any;
    currency?: string;
    firm?: any;
    fund_attributes: FundAttributeModel[];
    alerts?: AlertsOverview[];
    fund_profile_user_settings?: FundProfileUserSettingsModel;
}

export interface ErmFundRelationshipModel {
    id: number;
    relationship_type: string;
    data_source: string;
    start: FundModel;
    end: FundModel;
    created_at: string;
    updated_at?: string;
    is_valid: boolean;
    valid_from: string | null;
    valid_to: string | null;
}

export interface EntityDatasets {
    history_datasets: FundStatisticsDataset[];
    latest_dataset: FundStatisticsDataset | null;
}

export interface FundDetails {
    lei: string[];
    sec: string[];
    cik: string[];
    ticker: string[];
    class_id: string[];
    series_id: string[];
    fund_type: string | null; // string/constant or null
    domicile_state: string | null;
    domicile_country: string | null;
    number_of_owners: string | null;
    form_file_number: string[] | null;
    minimum_investment: string | null;
}

export interface ServiceProviders {
    administrator: string[];
    prime_broker: string[];
    custodian: string[];
    auditor: string[];
    marketers: string[];
}

export interface NewFundModel {
    ap_id: any;
    name: string;
    alias: string;
}

export interface DeleteFundsModel {
    ids: Id[];
    checkedAll: boolean;
    portfolioId: Id;
    count: number;
}

export interface FundAttributeModel {
    value: string;
    attribute_type: string;
}

export interface AttributeModel {
    countAttributes: number;
    attributes: FundAttributeModel[];
}

export interface FundProfileUserSettingsModel {
    /**
     * Interface for Fund Profile settings.
     * Backend should return all defined setting keys with default values.
     * However partial update is possible, so keep not required keys here.
     */
    remembered_selected_funds?: Id[];
    remembered_selected_groups?: Id[];
}
