import { Id } from 'store/types';
import { AccountType } from '../types';

export interface AccountSettings {
    display_fund_data_source?: boolean;
}

export interface Account {
    id: Id;
    name: string;
    status: string;
    type: AccountType;
    account_settings: AccountSettings;
    diligence_id: string;
}