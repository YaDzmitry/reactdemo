import { UserAccount } from 'store/Auth/models/userAccount';

export function hasDiligenceId(activeAccount: UserAccount) {
    return !!activeAccount.account.diligence_id;
}
