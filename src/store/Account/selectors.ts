import { createSelector } from 'reselect';

import { AppState } from '../types';
import { AccountState } from './types';
import { initialState } from './constants';

export const getAccountState = (state: AppState): AccountState => state.account || initialState;

export const getAccountUsers = createSelector(
    getAccountState,
    (state: AccountState) => state.users
);

export const getAccountUsersCount = createSelector(
    getAccountState,
    (state: AccountState) => state.usersCount
);

export const getAccountUsersFilter = createSelector(
    getAccountState,
    (state: AccountState) => state.filter
);

export const getEditedUser = createSelector(
    getAccountState,
    (state: AccountState) => state.editedUser
);

export const getInvitedUser = createSelector(
    getAccountState,
    (state: AccountState) => state.invitedUser
);