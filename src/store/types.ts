import * as React from 'react';
import { CSSProperties } from 'react';
import * as moment from 'moment';
import { ValueType } from 'react-select/lib/types';

import { AuthState } from './Auth/types';
import { ActionsState } from './Actions/types';
import { AccountState } from './Account/types';
import {
    Alignment,
    CellTypeChoices,
    FilterType,
    FormatTypeChoices,
    TableBulkActionChoices,
    TableRowActionIcon,
    TableRowActionType,
} from './constants';
import { ModalsState } from './Modals/types';
import { PortfolioState } from './Portfolio/types';
import { FundState } from './Fund/types';
import { FiltersState } from './Filters/types';
import { FirmState } from './Firm/types';
import { UserState } from './Diligence/User/types';
import { RequestsState } from './Requests/types';
import { DataManagerState } from './DataManager/types';
import { BulkUploadState } from './BulkUpload/types';
import { HeadersState } from './VisibleHeaders/types';
import { RedirectState } from './Redirect/types';
import { AsyncFiltersChoicesState } from './AsyncFiltersChoices/types';
import { DashboardState } from './Dashboard/types';
import { AlertsManagerState } from './AlertsManager/types';
import { ProfilesState } from './Profiles/types';
import { DiligenceDataManagerState } from './Diligence/DataManager/types';
import { DiligenceRequestState } from './Diligence/DiligenceRequest/types';
import { DiligenceAuthState } from './Diligence/Auth/types';
import { DiligenceFilterDropdownState } from './Diligence/FilterDropdown/types';
import { AdvancedFiltersModalState } from './AdvancedFilters/types';

export interface AppState {
    actions?: ActionsState;
    asyncFiltersChoices?: AsyncFiltersChoicesState;
    modals?: ModalsState;
    auth?: AuthState;
    account?: AccountState;
    portfolios?: PortfolioState;
    funds?: FundState;
    firms?: FirmState;
    filters?: FiltersState;
    user?: UserState;
    requests?: RequestsState;
    dataManager?: DataManagerState;
    redirect?: RedirectState;
    bulkUpload?: BulkUploadState;
    headers?: HeadersState;
    dashboard?: DashboardState;
    alertsManager?: AlertsManagerState;
    profiles?: ProfilesState;
    diligenceAuth?: DiligenceAuthState;
    diligenceDataManager?: DiligenceDataManagerState;
    diligenceRequests?: DiligenceRequestState;
    diligenceFilters?: FiltersState;
    diligenceFilterDropdown?: DiligenceFilterDropdownState;
    advancedFiltersModal?: AdvancedFiltersModalState;
}
