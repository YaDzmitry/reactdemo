import ActiveUser from './models/activeUser';
import { UserAccountRole } from './types';

export const isAdmin = (user: ActiveUser) => {
    return user.active_account.role === UserAccountRole.Admin;
};

export const isAuthenticated = (user?: ActiveUser) => {
    return !!user && user.hasOwnProperty('email') && user.active_account &&
        (!user.hasOwnProperty('is_authenticated') || user.is_authenticated);
};