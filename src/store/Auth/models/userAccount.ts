import { Id } from 'store/types';
import { Account } from 'store/Account/models/account';
import { DashboardChartType, DispersionChartType } from 'store/Dashboard/types';
import User from './user';

export interface UserAccountSettings {
    display_fund_data_source?: boolean;
    remember_selected_funds?: boolean;
    dispersion_charts?: DispersionChartType[];
    dashboard_charts?: DashboardChartType[];
}

export interface UserAccount {
    id: Id;
    status: string;
    role: string;
    created_at: string;
    account: Account;
    user?: User;
    user_account_settings: UserAccountSettings;
}
