import { UserGlobalRole, UserStatus, UserType } from '../types';
import { Id } from '../../types';

interface User {
    id: Id;
    inviter: number | null;
    type: UserType;
    status: UserStatus;
    global_role: UserGlobalRole;
    last_login: string | null;
    created_at: string | null;
    is_locked: boolean;
    locked_at: string | null;
    email: string;
    first_name: string;
    last_name: string;
    uuid: string;
}

export default User;