import { UserAccount } from 'store/Auth/models/userAccount';
import { Id } from 'store/types';
import { UserGlobalRole } from 'store/Auth/types';

interface ActiveUser {
    id: Id;
    email: string;
    first_name: string;
    last_name: string;
    type: string;
    status: string;
    last_login: string;
    created_at: string;
    account_relations: UserAccount[];
    active_account: UserAccount;
    is_authenticated?: boolean;
    is_anonymous?: boolean;
    is_internal: boolean;
    test?: boolean;
    global_role: UserGlobalRole;
    has_diligence_account: boolean;
    accounts?: any[];
}

export default ActiveUser;