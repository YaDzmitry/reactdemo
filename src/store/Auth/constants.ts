import { UserAccountRole, UserAccountStatus } from './types';

export const SWITCH_ACCOUNT_SUCCESS_MESSAGE = 'Account was switched successfully';
export const SWITCH_ACCOUNT_FAIL_MESSAGE = 'Account switching failed';

export const INITIALIZE_RESET_PASSWORD_SUCCESS_MESSAGE = 'The password reset process is initialized. ' +
    'Please check your email.';
export const INITIALIZE_RESET_PASSWORD_FAIL_MESSAGE = 'The password reset process failed.';

export const RESET_PASSWORD_SUCCESS_MESSAGE = 'The password reset task is queued. Please check your email.';
export const RESET_PASSWORD_FAIL_MESSAGE = 'The password reset process failed.';

export const initialState = {
    isInitialized: false
};

export const UserAccountStatuses = [
    {label: 'Pending', value: UserAccountStatus.Pending},
    {label: 'Active', value: UserAccountStatus.Active},
    {label: 'Suspended', value: UserAccountStatus.Suspended},
];

export const UserAccountRoles = [
    {label: 'User', value: UserAccountRole.User},
    {label: 'Admin', value: UserAccountRole.Admin},
];

export const ADHOC_NAME = 'adhoc';