export interface ActionsState {
    [key: string]: ActionInfo | boolean;
}

export interface ActionInfo {
    name: string;
    status: ActionStatus;
    errors: string[];
    message?: string;
}

export enum ActionStatus {
    InProgress = 'in-progress',
    Success = 'success',
    Fail = 'fail'
}