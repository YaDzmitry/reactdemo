interface UpdatePasswordRequest {
    old_password?: string | null;
    new_password?: string;
}

export default UpdatePasswordRequest;