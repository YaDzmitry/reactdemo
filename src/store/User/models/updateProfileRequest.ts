interface UpdateProfileRequest {
    last_name?: string;
    first_name?: string;
}

export default UpdateProfileRequest;